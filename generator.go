// Copyright 2023 The libpgmp-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"

	"modernc.org/cc/v4"
	util "modernc.org/ccgo/v3/lib"
	ccgo "modernc.org/ccgo/v4/lib"
)

const (
	archivePath = "gmp-6.3.0.tar.xz"
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
	j      = strconv.Itoa(runtime.GOMAXPROCS(-1))
	sed    = "sed"
)

func fail(rc int, msg string, args ...any) {
	fmt.Fprintf(os.Stderr, "\nFAIL\n%s\n", debug.Stack())
	fmt.Fprintln(os.Stderr, strings.TrimSpace(fmt.Sprintf(msg, args...)))
	os.Exit(rc)
}

func main() {
	if ccgo.IsExecEnv() {
		if err := ccgo.NewTask(goos, goarch, os.Args, os.Stdout, os.Stderr, nil).Main(); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		return
	}

	f, err := os.Open(archivePath)
	if err != nil {
		fail(1, "cannot open tar file: %v\n", err)
	}

	f.Close()

	_, extractedArchivePath := filepath.Split(archivePath)
	extractedArchivePath = extractedArchivePath[:len(extractedArchivePath)-len(".tar.gz")]
	tempDir := os.Getenv("GO_GENERATE_DIR")
	dev := os.Getenv("GO_GENERATE_DEV") != ""
	switch {
	case tempDir != "":
		util.MustShell(true, "sh", "-c", fmt.Sprintf("rm -rf %s", filepath.Join(tempDir, extractedArchivePath)))
	default:
		var err error
		if tempDir, err = os.MkdirTemp("", "libgmp-generate"); err != nil {
			fail(1, "creating temp dir: %v\n", err)
		}

		defer func() {
			switch os.Getenv("GO_GENERATE_KEEP") {
			case "":
				os.RemoveAll(tempDir)
			default:
				fmt.Printf("%s: temporary directory kept\n", tempDir)
			}
		}()
	}
	libRoot := filepath.Join(tempDir, extractedArchivePath)
	makeRoot := libRoot
	fmt.Fprintf(os.Stderr, "archivePath %s\n", archivePath)
	fmt.Fprintf(os.Stderr, "extractedArchivePath %s\n", extractedArchivePath)
	fmt.Fprintf(os.Stderr, "tempDir %s\n", tempDir)
	fmt.Fprintf(os.Stderr, "libRoot %s\n", libRoot)
	fmt.Fprintf(os.Stderr, "makeRoot %s\n", makeRoot)

	os.MkdirAll(filepath.Join("internal", "libtests"), 0770)
	tests := filepath.Join("internal", goos, goarch)
	os.MkdirAll(tests, 0770)
	if tests, err = filepath.Abs(tests); err != nil {
		fail(1, "%s\n", err)
	}

	os.RemoveAll(filepath.Join(tests, "tests"))
	os.RemoveAll(filepath.Join("include", goos, goarch))
	util.MustShell(true, "tar", "xf", archivePath, "-C", tempDir)
	util.MustShell(true, "sh", "-c", fmt.Sprintf("cp %s* .", filepath.Join(makeRoot, "COPYING")))
	result := filepath.Join("libgmp.go")
	util.MustInDir(true, makeRoot, func() (err error) {
		cflags := []string{
			"-DNDEBUG",
		}
		if s := cc.LongDouble64Flag(goos, goarch); s != "" {
			cflags = append(cflags, s)
		}
		util.MustShell(true, "sh", "-c", "go mod init example.com/libgmp; go get modernc.org/libc@latest")
		if dev {
			util.MustShell(true, "sh", "-c", "go work init ; go work use . $GOPATH/src/modernc.org/libc")
		}
		util.MustShell(true, "sh", "-c", fmt.Sprintf("CFLAGS='%s' ./configure "+
			"--disable-assembly "+
			"--disable-assert "+
			"--disable-cxx "+
			"--disable-shared "+
			"--enable-alloca "+
			"",
			strings.Join(cflags, " ")),
		)
		util.MustShell(true, sed, "-i", `s/#define HAVE_OBSTACK_VPRINTF.*//`, "config.h")
		args := []string{os.Args[0]}
		if dev {
			args = append(
				args,
				"-absolute-paths",
				"-keep-object-files",
				"-positions",
			)
		}
		args = append(args,
			"--prefix-enumerator=_",
			"--prefix-external=x_",
			"--prefix-field=F",
			"--prefix-macro=m_",
			"--prefix-static-internal=_",
			"--prefix-static-none=_",
			"--prefix-tagged-enum=_",
			"--prefix-tagged-struct=T",
			"--prefix-tagged-union=T",
			"--prefix-typename=T",
			"--prefix-undefined=_",
			"-D__attribute_pure__=",
			"-extended-errors",
			"-ignore-unsupported-alignment",
		)
		if err := ccgo.NewTask(goos, goarch, append(args, "-exec", "make", "-j", j), os.Stdout, os.Stderr, nil).Exec(); err != nil {
			fail(1, "%s\n", err)
		}

		if err := ccgo.NewTask(goos, goarch, append(args, "-exec", "make", "-j", j, "check"), os.Stdout, os.Stderr, nil).Exec(); err != nil {
			fail(1, "%s\n", err)
		}

		filepath.Walk("tests", func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				fail(1, "%s\n", err)
			}

			if info.IsDir() || !strings.HasSuffix(path, ".o.go") {
				return nil
			}

			dir, nm := filepath.Split(path)
			dir = filepath.Join(tests, dir)
			if err := os.MkdirAll(dir, 0770); err != nil {
				fail(1, "%s\n", err)
			}

			util.MustCopyFile(false, filepath.Join(dir, nm), path, nil)
			return nil
		})

		if err := ccgo.NewTask(goos, goarch, append(args,
			"-o", result,
			"--package-name", "libgmp",
			filepath.Join(".libs", "libgmp.a"),
		), os.Stdout, os.Stderr, nil).Main(); err != nil {
			fail(1, "%s\n", err)
		}

		util.MustShell(true, sed, "-i", `s/\<T__\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/t__\1/g`, result)
		util.MustShell(true, sed, "-i", `s/\<x___\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/X__\1/g`, result)
		return nil
	})

	util.MustCopyFile(true, filepath.Join("include", goos, goarch, "gmp.h"), filepath.Join(libRoot, "gmp.h"), nil)
	fn := fmt.Sprintf("ccgo_%s_%s.go", goos, goarch)
	util.MustCopyFile(true, fn, filepath.Join(makeRoot, result), nil)
	fn = fmt.Sprintf("ccgo_%s_%s.ago", goos, goarch)
	util.MustCopyFile(true, filepath.Join("internal", "libtests", fn), filepath.Join(makeRoot, "tests", ".libs", "libtests.ago"), nil)
	util.MustShell(true, "gofmt", "-l", "-s", "-w", ".")
	util.Shell("git", "status")
}
