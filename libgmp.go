// Copyright 2023 The libgmp-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libgmp is a ccgo/v4 version of libgmp.a (https://gmplib.org)
package libgmp // import "modernc.org/libgmp"
