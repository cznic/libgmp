// Copyright 2023 The libgmp-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libgmp // import "modernc.org/libgmp"

import (
	"context"
	"flag"
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
	"sync"
	"testing"
	"time"

	_ "modernc.org/ccgo/v3/lib" // generator.go
	"modernc.org/ccgo/v4/lib"
)

var (
	oXTags     = flag.String("xtags", "", "passed as -tags to go build of tests") //TODO
	goos       = runtime.GOOS
	goarch     = runtime.GOARCH
	gomaxprocs = runtime.GOMAXPROCS(-1)
	oRe        = flag.String("re", "", "")

	re *regexp.Regexp
)

func TestMain(m *testing.M) {
	flag.Parse()
	if s := *oRe; s != "" {
		re = regexp.MustCompile(s)
	}
	rc := m.Run()
	os.Exit(rc)
}

type test struct {
	buildok   int
	compileok int
	err       error
	fail      int
	out       []byte
	pass      int
	path      string
	testN     int
}

var skips = []string{
	// Won't fix
	"mpn/t-get_d.o.go", // FAIL internal/linux/amd64/tests/mpn/t-get_d.o.go:848:5: undefined: "setjmp"

	// Exit status 77 == skip
	"mpn/t-addaddmul.o.go",

	//TODO
	"misc/t-printf.o.go", // undefined: obstack_init
}

func Test(t *testing.T) {
	tempDir := t.TempDir()
	libtests := filepath.Join("internal", "libtests", fmt.Sprintf("ccgo_%s_%s.ago", goos, goarch))
	var files, skip, compileok, buildok, fail, pass int
	limiter := make(chan struct{}, gomaxprocs)
	var wg sync.WaitGroup
	var tasks []*test
	filepath.Walk(filepath.Join("internal", goos, goarch, "tests"), func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			t.Fatal(err)
		}

		if info.IsDir() || !strings.HasSuffix(path, ".o.go") {
			return nil
		}

		if re != nil && !re.MatchString(path) {
			return nil
		}

		files++
		slash := filepath.ToSlash(path)
		for _, v := range skips {
			if strings.HasSuffix(slash, v) {
				skip++
				t.Logf("%s: SKIP (list)", path)
				return nil
			}
		}

		b, err := os.ReadFile(path)
		if err != nil {
			t.Fatal(err)
		}

		if !strings.Contains(string(b), "\nfunc ppmain() {") {
			skip++
			t.Logf("%s: SKIP (no main)", path)
			return nil
		}

		wg.Add(1)
		task := &test{path: path, testN: files}
		tasks = append(tasks, task)
		return nil
	})

	for i, task := range tasks {
		limiter <- struct{}{}
		t.Log(i, task.path)

		go func(task *test) {

			defer func() {
				<-limiter
				wg.Done()
			}()

			goFile := filepath.Join(tempDir, fmt.Sprintf("test%v.go", task.testN))
			bin := filepath.Join(tempDir, fmt.Sprintf("test%v", task.testN))
			if goos == "windows" {
				bin += ".exe"
			}

			if task.err = ccgo.NewTask(
				goos, goarch,
				[]string{
					"",
					"-no-main-minimize",
					"--prefix-enumerator=_",
					"--prefix-external=x_",
					"--prefix-field=F",
					"--prefix-macro=m_",
					"--prefix-static-internal=_",
					"--prefix-static-none=_",
					"--prefix-tagged-enum=_",
					"--prefix-tagged-struct=T",
					"--prefix-tagged-union=T",
					"--prefix-typename=T",
					"--prefix-undefined=_",
					"-extended-errors",
					"-positions",
					"-absolute-paths",
					"-ignore-unsupported-alignment",
					"-o", goFile,
					task.path,
					libtests,
					"-lgmp",
				},
				os.Stdout, os.Stderr,
				nil,
			).Main(); task.err != nil {
				return
			}

			task.compileok++
			if task.out, task.err = exec.Command("go", "build", "-o", bin, goFile).CombinedOutput(); task.err != nil {
				return
			}

			task.buildok++
			if task.out, task.err = run(bin, tempDir); task.err != nil {
				task.fail++
			} else {
				task.pass++
			}
		}(task)
	}

	wg.Wait()
	for _, v := range tasks {
		compileok += v.compileok
		buildok += v.buildok
		fail += v.fail
		pass += v.pass
		if v.err != nil || v.fail != 0 {
			t.Errorf("%s: %s FAIL %v", v.path, v.out, v.err)
		}
	}
	t.Logf("files=%v skip=%v compileok=%v buildok=%v fail=%v pass=%v", files, skip, compileok, buildok, fail, pass)
	// all_test.go:168: files=183 skip=10 compileok=173 buildok=173 fail=0 pass=173
}

func run(bin, inDir string) (out []byte, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Minute)

	defer cancel()

	cmd := exec.CommandContext(ctx, bin)
	cmd.Dir = inDir
	cmd.WaitDelay = 20 * time.Second
	return cmd.CombinedOutput()
}
