// Code generated for linux/386 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -ignore-unsupported-alignment -DHAVE_CONFIG_H -I. -I../.. -I../.. -I../../tests -DNDEBUG -mlong-double-64 -c -o t-printf.o.go t-printf.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvASSERT_FILE = "__FILE__"
const mvASSERT_LINE = "__LINE__"
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBINV_NEWTON_THRESHOLD = 300
const mvBMOD_1_TO_MOD_1_THRESHOLD = 10
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCHECK_VFPRINTF_FILENAME = "t-printf.tmp"
const mvCOLL_WEIGHTS_MAX = 2
const mvDC_BDIV_Q_THRESHOLD = 180
const mvDC_DIVAPPR_Q_THRESHOLD = 200
const mvDELAYTIMER_MAX = 0x7fffffff
const mvDIVEXACT_1_THRESHOLD = 0
const mvDIVEXACT_BY3_METHOD = 0
const mvDIVEXACT_JEB_THRESHOLD = 25
const mvDOPRNT_CONV_FIXED = 1
const mvDOPRNT_CONV_GENERAL = 3
const mvDOPRNT_CONV_SCIENTIFIC = 2
const mvDOPRNT_JUSTIFY_INTERNAL = 3
const mvDOPRNT_JUSTIFY_LEFT = 1
const mvDOPRNT_JUSTIFY_NONE = 0
const mvDOPRNT_JUSTIFY_RIGHT = 2
const mvDOPRNT_SHOWBASE_NO = 2
const mvDOPRNT_SHOWBASE_NONZERO = 3
const mvDOPRNT_SHOWBASE_YES = 1
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFAC_DSC_THRESHOLD = 400
const mvFAC_ODD_THRESHOLD = 35
const mvFFT_FIRST_K = 4
const mvFIB_TABLE_LIMIT = 47
const mvFIB_TABLE_LUCNUM_LIMIT = 46
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFOPEN_MAX = 1000
const mvF_LOCK = 1
const mvF_OK = 0
const mvF_TEST = 3
const mvF_TLOCK = 2
const mvF_ULOCK = 0
const mvGCDEXT_DC_THRESHOLD = 600
const mvGCD_DC_THRESHOLD = 1000
const mvGET_STR_DC_THRESHOLD = 18
const mvGET_STR_PRECOMPUTE_THRESHOLD = 35
const mvGMP_LIMB_BITS = 32
const mvGMP_LIMB_BYTES = "SIZEOF_MP_LIMB_T"
const mvGMP_MPARAM_H_SUGGEST = "./mpn/generic/gmp-mparam.h"
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_ALARM = 1
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_CONST = 1
const mvHAVE_ATTRIBUTE_MALLOC = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_ATTRIBUTE_NORETURN = 1
const mvHAVE_CLOCK = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_CONFIG_H = 1
const mvHAVE_DECL_FGETC = 1
const mvHAVE_DECL_FSCANF = 1
const mvHAVE_DECL_OPTARG = 1
const mvHAVE_DECL_SYS_ERRLIST = 0
const mvHAVE_DECL_SYS_NERR = 0
const mvHAVE_DECL_UNGETC = 1
const mvHAVE_DECL_VFPRINTF = 1
const mvHAVE_DLFCN_H = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FCNTL_H = 1
const mvHAVE_FLOAT_H = 1
const mvHAVE_GETPAGESIZE = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_HIDDEN_ALIAS = 1
const mvHAVE_HOST_CPU_FAMILY_x86_64 = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTPTR_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LANGINFO_H = 1
const mvHAVE_LIMB_LITTLE_ENDIAN = 1
const mvHAVE_LOCALECONV = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_DOUBLE = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_MEMORY_H = 1
const mvHAVE_MEMSET = 1
const mvHAVE_MMAP = 1
const mvHAVE_MPROTECT = 1
const mvHAVE_NL_LANGINFO = 1
const mvHAVE_NL_TYPES_H = 1
const mvHAVE_POPEN = 1
const mvHAVE_PTRDIFF_T = 1
const mvHAVE_QUAD_T = 1
const mvHAVE_RAISE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGALTSTACK = 1
const mvHAVE_SIGSTACK = 1
const mvHAVE_SPEED_CYCLECOUNTER = 2
const mvHAVE_STACK_T = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDLIB_H = 1
const mvHAVE_STRCHR = 1
const mvHAVE_STRERROR = 1
const mvHAVE_STRINGS_H = 1
const mvHAVE_STRING_H = 1
const mvHAVE_STRNLEN = 1
const mvHAVE_STRTOL = 1
const mvHAVE_STRTOUL = 1
const mvHAVE_SYSCONF = 1
const mvHAVE_SYS_MMAN_H = 1
const mvHAVE_SYS_PARAM_H = 1
const mvHAVE_SYS_RESOURCE_H = 1
const mvHAVE_SYS_STAT_H = 1
const mvHAVE_SYS_SYSINFO_H = 1
const mvHAVE_SYS_TIMES_H = 1
const mvHAVE_SYS_TIME_H = 1
const mvHAVE_SYS_TYPES_H = 1
const mvHAVE_TIMES = 1
const mvHAVE_UINT_LEAST32_T = 1
const mvHAVE_UNISTD_H = 1
const mvHAVE_VSNPRINTF = 1
const mvHGCD_APPR_THRESHOLD = 400
const mvHGCD_REDUCE_THRESHOLD = 1000
const mvHGCD_THRESHOLD = 400
const mvHOST_NAME_MAX = 255
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT32_MAX"
const mvINTPTR_MIN = "INT32_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_HIGHBIT = "INT_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvINV_APPR_THRESHOLD = "INV_NEWTON_THRESHOLD"
const mvINV_NEWTON_THRESHOLD = 200
const mvIOV_MAX = 1024
const mvLIMBS_PER_ULONG = 1
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_HIGHBIT = "LONG_MIN"
const mvLONG_MAX = "__LONG_MAX"
const mvLSYM_PREFIX = ".L"
const mvLT_OBJDIR = ".libs/"
const mvL_INCR = 1
const mvL_SET = 0
const mvL_XTND = 2
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMATRIX22_STRASSEN_THRESHOLD = 30
const mvMAX_OUTPUT = 1024
const mvMB_LEN_MAX = 4
const mvMOD_BKNP1_ONLY3 = 0
const mvMPN_FFT_TABLE_SIZE = 16
const mvMPN_TOOM22_MUL_MINSIZE = 6
const mvMPN_TOOM2_SQR_MINSIZE = 4
const mvMPN_TOOM32_MUL_MINSIZE = 10
const mvMPN_TOOM33_MUL_MINSIZE = 17
const mvMPN_TOOM3_SQR_MINSIZE = 17
const mvMPN_TOOM42_MULMID_MINSIZE = 4
const mvMPN_TOOM42_MUL_MINSIZE = 10
const mvMPN_TOOM43_MUL_MINSIZE = 25
const mvMPN_TOOM44_MUL_MINSIZE = 30
const mvMPN_TOOM4_SQR_MINSIZE = 30
const mvMPN_TOOM53_MUL_MINSIZE = 17
const mvMPN_TOOM54_MUL_MINSIZE = 31
const mvMPN_TOOM63_MUL_MINSIZE = 49
const mvMPN_TOOM6H_MUL_MINSIZE = 46
const mvMPN_TOOM6_SQR_MINSIZE = 46
const mvMPN_TOOM8H_MUL_MINSIZE = 86
const mvMPN_TOOM8_SQR_MINSIZE = 86
const mvMP_BASES_BIG_BASE_CTZ_10 = 9
const mvMP_BASES_CHARS_PER_LIMB_10 = 9
const mvMP_BASES_NORMALIZATION_STEPS_10 = 2
const mvMP_EXP_T_MAX = "MP_SIZE_T_MAX"
const mvMP_EXP_T_MIN = "MP_SIZE_T_MIN"
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMULLO_BASECASE_THRESHOLD = 0
const mvMULLO_BASECASE_THRESHOLD_LIMIT = "MULLO_BASECASE_THRESHOLD"
const mvMULMID_TOOM42_THRESHOLD = "MUL_TOOM22_THRESHOLD"
const mvMULMOD_BNM1_THRESHOLD = 16
const mvMUL_TOOM22_THRESHOLD = 30
const mvMUL_TOOM22_THRESHOLD_LIMIT = "MUL_TOOM22_THRESHOLD"
const mvMUL_TOOM32_TO_TOOM43_THRESHOLD = 100
const mvMUL_TOOM32_TO_TOOM53_THRESHOLD = 110
const mvMUL_TOOM33_THRESHOLD = 100
const mvMUL_TOOM33_THRESHOLD_LIMIT = "MUL_TOOM33_THRESHOLD"
const mvMUL_TOOM42_TO_TOOM53_THRESHOLD = 100
const mvMUL_TOOM42_TO_TOOM63_THRESHOLD = 110
const mvMUL_TOOM43_TO_TOOM54_THRESHOLD = 150
const mvMUL_TOOM44_THRESHOLD = 300
const mvMUL_TOOM6H_THRESHOLD = 350
const mvMUL_TOOM8H_THRESHOLD = 450
const mvMUPI_DIV_QR_THRESHOLD = 200
const mvMU_BDIV_QR_THRESHOLD = 2000
const mvMU_BDIV_Q_THRESHOLD = 2000
const mvMU_DIVAPPR_Q_THRESHOLD = 2000
const mvMU_DIV_QR_THRESHOLD = 2000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvODD_CENTRAL_BINOMIAL_OFFSET = 8
const mvODD_CENTRAL_BINOMIAL_TABLE_LIMIT = 18
const mvODD_DOUBLEFACTORIAL_TABLE_LIMIT = 19
const mvODD_FACTORIAL_EXTTABLE_LIMIT = 34
const mvODD_FACTORIAL_TABLE_LIMIT = 16
const mvPACKAGE = "gmp"
const mvPACKAGE_BUGREPORT = "gmp-bugs@gmplib.org (see https://gmplib.org/manual/Reporting-Bugs.html)"
const mvPACKAGE_NAME = "GNU MP"
const mvPACKAGE_STRING = "GNU MP 6.3.0"
const mvPACKAGE_TARNAME = "gmp"
const mvPACKAGE_URL = "http://www.gnu.org/software/gmp/"
const mvPACKAGE_VERSION = "6.3.0"
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPOSIX_CLOSE_RESTART = 0
const mvPP = 0xC0CFD797
const mvPP_FIRST_OMITTED = 31
const mvPP_INVERTED = 0x53E5645C
const mvPREINV_MOD_1_TO_MOD_1_THRESHOLD = 10
const mvPRIMESIEVE_HIGHEST_PRIME = 5351
const mvPRIMESIEVE_NUMBEROF_TABLE = 56
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT32_MAX"
const mvPTRDIFF_MIN = "INT32_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvREDC_1_TO_REDC_N_THRESHOLD = 100
const mvRETSIGTYPE = "void"
const mvRE_DUP_MAX = 255
const mvR_OK = 4
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEEK_DATA = 3
const mvSEEK_HOLE = 4
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSET_STR_DC_THRESHOLD = 750
const mvSET_STR_PRECOMPUTE_THRESHOLD = 2000
const mvSHRT_HIGHBIT = "SHRT_MIN"
const mvSHRT_MAX = 0x7fff
const mvSIEVESIZE = 512
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZEOF_MP_LIMB_T = 4
const mvSIZEOF_UNSIGNED = 4
const mvSIZEOF_UNSIGNED_LONG = 4
const mvSIZEOF_UNSIGNED_SHORT = 2
const mvSIZEOF_VOID_P = 4
const mvSIZE_MAX = "UINT32_MAX"
const mvSQRLO_BASECASE_THRESHOLD = 0
const mvSQRLO_BASECASE_THRESHOLD_LIMIT = "SQRLO_BASECASE_THRESHOLD"
const mvSQRLO_DC_THRESHOLD = "MULLO_DC_THRESHOLD"
const mvSQRLO_DC_THRESHOLD_LIMIT = "SQRLO_DC_THRESHOLD"
const mvSQRLO_SQR_THRESHOLD = "MULLO_MUL_N_THRESHOLD"
const mvSQRMOD_BNM1_THRESHOLD = 16
const mvSQR_BASECASE_THRESHOLD = 0
const mvSQR_TOOM2_THRESHOLD = 50
const mvSQR_TOOM3_THRESHOLD = 120
const mvSQR_TOOM3_THRESHOLD_LIMIT = "SQR_TOOM3_THRESHOLD"
const mvSQR_TOOM4_THRESHOLD = 400
const mvSQR_TOOM6_THRESHOLD = "MUL_TOOM6H_THRESHOLD"
const mvSQR_TOOM8_THRESHOLD = "MUL_TOOM8H_THRESHOLD"
const mvSSIZE_MAX = "LONG_MAX"
const mvSTDC_HEADERS = 1
const mvSTDERR_FILENO = 2
const mvSTDIN_FILENO = 0
const mvSTDOUT_FILENO = 1
const mvSYMLOOP_MAX = 40
const mvTABLE_LIMIT_2N_MINUS_POPC_2N = 49
const mvTARGET_REGISTER_STARVED = 0
const mvTIME_WITH_SYS_TIME = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTUNE_SQR_TOOM2_MAX = "SQR_TOOM2_MAX_GENERIC"
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT32_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSE_LEADING_REGPARM = 0
const mvUSE_PREINV_DIVREM_1 = 1
const mvUSHRT_MAX = 65535
const mvVERSION = "6.3.0"
const mvWANT_FFT = 1
const mvWANT_TMP_ALLOCA = 1
const mvWANT_TMP_DEBUG = 0
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_OK = 2
const mvW_TYPE_SIZE = "GMP_LIMB_BITS"
const mvX_OK = 1
const mv_CS_GNU_LIBC_VERSION = 2
const mv_CS_GNU_LIBPTHREAD_VERSION = 3
const mv_CS_PATH = 0
const mv_CS_POSIX_V5_WIDTH_RESTRICTED_ENVS = 4
const mv_CS_POSIX_V6_ILP32_OFF32_CFLAGS = 1116
const mv_CS_POSIX_V6_ILP32_OFF32_LDFLAGS = 1117
const mv_CS_POSIX_V6_ILP32_OFF32_LIBS = 1118
const mv_CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = 1119
const mv_CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = 1120
const mv_CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = 1121
const mv_CS_POSIX_V6_ILP32_OFFBIG_LIBS = 1122
const mv_CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = 1123
const mv_CS_POSIX_V6_LP64_OFF64_CFLAGS = 1124
const mv_CS_POSIX_V6_LP64_OFF64_LDFLAGS = 1125
const mv_CS_POSIX_V6_LP64_OFF64_LIBS = 1126
const mv_CS_POSIX_V6_LP64_OFF64_LINTFLAGS = 1127
const mv_CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = 1128
const mv_CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = 1129
const mv_CS_POSIX_V6_LPBIG_OFFBIG_LIBS = 1130
const mv_CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = 1131
const mv_CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = 1
const mv_CS_POSIX_V7_ILP32_OFF32_CFLAGS = 1132
const mv_CS_POSIX_V7_ILP32_OFF32_LDFLAGS = 1133
const mv_CS_POSIX_V7_ILP32_OFF32_LIBS = 1134
const mv_CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = 1135
const mv_CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = 1136
const mv_CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = 1137
const mv_CS_POSIX_V7_ILP32_OFFBIG_LIBS = 1138
const mv_CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = 1139
const mv_CS_POSIX_V7_LP64_OFF64_CFLAGS = 1140
const mv_CS_POSIX_V7_LP64_OFF64_LDFLAGS = 1141
const mv_CS_POSIX_V7_LP64_OFF64_LIBS = 1142
const mv_CS_POSIX_V7_LP64_OFF64_LINTFLAGS = 1143
const mv_CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = 1144
const mv_CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = 1145
const mv_CS_POSIX_V7_LPBIG_OFFBIG_LIBS = 1146
const mv_CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = 1147
const mv_CS_POSIX_V7_THREADS_CFLAGS = 1150
const mv_CS_POSIX_V7_THREADS_LDFLAGS = 1151
const mv_CS_POSIX_V7_WIDTH_RESTRICTED_ENVS = 5
const mv_CS_V6_ENV = 1148
const mv_CS_V7_ENV = 1149
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_H_HAVE_VA_LIST = 1
const mv_GMP_IEEE_FLOATS = 1
const mv_GNU_SOURCE = 1
const mv_ILP32 = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_PC_2_SYMLINKS = 20
const mv_PC_ALLOC_SIZE_MIN = 18
const mv_PC_ASYNC_IO = 10
const mv_PC_CHOWN_RESTRICTED = 6
const mv_PC_FILESIZEBITS = 13
const mv_PC_LINK_MAX = 0
const mv_PC_MAX_CANON = 1
const mv_PC_MAX_INPUT = 2
const mv_PC_NAME_MAX = 3
const mv_PC_NO_TRUNC = 7
const mv_PC_PATH_MAX = 4
const mv_PC_PIPE_BUF = 5
const mv_PC_PRIO_IO = 11
const mv_PC_REC_INCR_XFER_SIZE = 14
const mv_PC_REC_MAX_XFER_SIZE = 15
const mv_PC_REC_MIN_XFER_SIZE = 16
const mv_PC_REC_XFER_ALIGN = 17
const mv_PC_SOCK_MAXBUF = 12
const mv_PC_SYMLINK_MAX = 19
const mv_PC_SYNC_IO = 9
const mv_PC_VDISABLE = 8
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_C_BIND = "_POSIX_VERSION"
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX2_VERSION = "_POSIX_VERSION"
const mv_POSIX_ADVISORY_INFO = "_POSIX_VERSION"
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_ASYNCHRONOUS_IO = "_POSIX_VERSION"
const mv_POSIX_BARRIERS = "_POSIX_VERSION"
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CHOWN_RESTRICTED = 1
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_CLOCK_SELECTION = "_POSIX_VERSION"
const mv_POSIX_CPUTIME = "_POSIX_VERSION"
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_FSYNC = "_POSIX_VERSION"
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_IPV6 = "_POSIX_VERSION"
const mv_POSIX_JOB_CONTROL = 1
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAPPED_FILES = "_POSIX_VERSION"
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MEMLOCK = "_POSIX_VERSION"
const mv_POSIX_MEMLOCK_RANGE = "_POSIX_VERSION"
const mv_POSIX_MEMORY_PROTECTION = "_POSIX_VERSION"
const mv_POSIX_MESSAGE_PASSING = "_POSIX_VERSION"
const mv_POSIX_MONOTONIC_CLOCK = "_POSIX_VERSION"
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_NO_TRUNC = 1
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RAW_SOCKETS = "_POSIX_VERSION"
const mv_POSIX_READER_WRITER_LOCKS = "_POSIX_VERSION"
const mv_POSIX_REALTIME_SIGNALS = "_POSIX_VERSION"
const mv_POSIX_REGEXP = 1
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SAVED_IDS = 1
const mv_POSIX_SEMAPHORES = "_POSIX_VERSION"
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SHARED_MEMORY_OBJECTS = "_POSIX_VERSION"
const mv_POSIX_SHELL = 1
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SPAWN = "_POSIX_VERSION"
const mv_POSIX_SPIN_LOCKS = "_POSIX_VERSION"
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREADS = "_POSIX_VERSION"
const mv_POSIX_THREAD_ATTR_STACKADDR = "_POSIX_VERSION"
const mv_POSIX_THREAD_ATTR_STACKSIZE = "_POSIX_VERSION"
const mv_POSIX_THREAD_CPUTIME = "_POSIX_VERSION"
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_PRIORITY_SCHEDULING = "_POSIX_VERSION"
const mv_POSIX_THREAD_PROCESS_SHARED = "_POSIX_VERSION"
const mv_POSIX_THREAD_SAFE_FUNCTIONS = "_POSIX_VERSION"
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMEOUTS = "_POSIX_VERSION"
const mv_POSIX_TIMERS = "_POSIX_VERSION"
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_POSIX_V6_ILP32_OFFBIG = 1
const mv_POSIX_V7_ILP32_OFFBIG = 1
const mv_POSIX_VDISABLE = 0
const mv_POSIX_VERSION = 200809
const mv_REDIR_TIME64 = 1
const mv_SC_2_CHAR_TERM = 95
const mv_SC_2_C_BIND = 47
const mv_SC_2_C_DEV = 48
const mv_SC_2_FORT_DEV = 49
const mv_SC_2_FORT_RUN = 50
const mv_SC_2_LOCALEDEF = 52
const mv_SC_2_PBS = 168
const mv_SC_2_PBS_ACCOUNTING = 169
const mv_SC_2_PBS_CHECKPOINT = 175
const mv_SC_2_PBS_LOCATE = 170
const mv_SC_2_PBS_MESSAGE = 171
const mv_SC_2_PBS_TRACK = 172
const mv_SC_2_SW_DEV = 51
const mv_SC_2_UPE = 97
const mv_SC_2_VERSION = 46
const mv_SC_ADVISORY_INFO = 132
const mv_SC_AIO_LISTIO_MAX = 23
const mv_SC_AIO_MAX = 24
const mv_SC_AIO_PRIO_DELTA_MAX = 25
const mv_SC_ARG_MAX = 0
const mv_SC_ASYNCHRONOUS_IO = 12
const mv_SC_ATEXIT_MAX = 87
const mv_SC_AVPHYS_PAGES = 86
const mv_SC_BARRIERS = 133
const mv_SC_BC_BASE_MAX = 36
const mv_SC_BC_DIM_MAX = 37
const mv_SC_BC_SCALE_MAX = 38
const mv_SC_BC_STRING_MAX = 39
const mv_SC_CHILD_MAX = 1
const mv_SC_CLK_TCK = 2
const mv_SC_CLOCK_SELECTION = 137
const mv_SC_COLL_WEIGHTS_MAX = 40
const mv_SC_CPUTIME = 138
const mv_SC_DELAYTIMER_MAX = 26
const mv_SC_EXPR_NEST_MAX = 42
const mv_SC_FSYNC = 15
const mv_SC_GETGR_R_SIZE_MAX = 69
const mv_SC_GETPW_R_SIZE_MAX = 70
const mv_SC_HOST_NAME_MAX = 180
const mv_SC_IOV_MAX = 60
const mv_SC_IPV6 = 235
const mv_SC_JOB_CONTROL = 7
const mv_SC_LINE_MAX = 43
const mv_SC_LOGIN_NAME_MAX = 71
const mv_SC_MAPPED_FILES = 16
const mv_SC_MEMLOCK = 17
const mv_SC_MEMLOCK_RANGE = 18
const mv_SC_MEMORY_PROTECTION = 19
const mv_SC_MESSAGE_PASSING = 20
const mv_SC_MINSIGSTKSZ = 249
const mv_SC_MONOTONIC_CLOCK = 149
const mv_SC_MQ_OPEN_MAX = 27
const mv_SC_MQ_PRIO_MAX = 28
const mv_SC_NGROUPS_MAX = 3
const mv_SC_NPROCESSORS_CONF = 83
const mv_SC_NPROCESSORS_ONLN = 84
const mv_SC_NZERO = 109
const mv_SC_OPEN_MAX = 4
const mv_SC_PAGESIZE = 30
const mv_SC_PAGE_SIZE = 30
const mv_SC_PASS_MAX = 88
const mv_SC_PHYS_PAGES = 85
const mv_SC_PRIORITIZED_IO = 13
const mv_SC_PRIORITY_SCHEDULING = 10
const mv_SC_RAW_SOCKETS = 236
const mv_SC_READER_WRITER_LOCKS = 153
const mv_SC_REALTIME_SIGNALS = 9
const mv_SC_REGEXP = 155
const mv_SC_RE_DUP_MAX = 44
const mv_SC_RTSIG_MAX = 31
const mv_SC_SAVED_IDS = 8
const mv_SC_SEMAPHORES = 21
const mv_SC_SEM_NSEMS_MAX = 32
const mv_SC_SEM_VALUE_MAX = 33
const mv_SC_SHARED_MEMORY_OBJECTS = 22
const mv_SC_SHELL = 157
const mv_SC_SIGQUEUE_MAX = 34
const mv_SC_SIGSTKSZ = 250
const mv_SC_SPAWN = 159
const mv_SC_SPIN_LOCKS = 154
const mv_SC_SPORADIC_SERVER = 160
const mv_SC_SS_REPL_MAX = 241
const mv_SC_STREAMS = 174
const mv_SC_STREAM_MAX = 5
const mv_SC_SYMLOOP_MAX = 173
const mv_SC_SYNCHRONIZED_IO = 14
const mv_SC_THREADS = 67
const mv_SC_THREAD_ATTR_STACKADDR = 77
const mv_SC_THREAD_ATTR_STACKSIZE = 78
const mv_SC_THREAD_CPUTIME = 139
const mv_SC_THREAD_DESTRUCTOR_ITERATIONS = 73
const mv_SC_THREAD_KEYS_MAX = 74
const mv_SC_THREAD_PRIORITY_SCHEDULING = 79
const mv_SC_THREAD_PRIO_INHERIT = 80
const mv_SC_THREAD_PRIO_PROTECT = 81
const mv_SC_THREAD_PROCESS_SHARED = 82
const mv_SC_THREAD_ROBUST_PRIO_INHERIT = 247
const mv_SC_THREAD_ROBUST_PRIO_PROTECT = 248
const mv_SC_THREAD_SAFE_FUNCTIONS = 68
const mv_SC_THREAD_SPORADIC_SERVER = 161
const mv_SC_THREAD_STACK_MIN = 75
const mv_SC_THREAD_THREADS_MAX = 76
const mv_SC_TIMEOUTS = 164
const mv_SC_TIMERS = 11
const mv_SC_TIMER_MAX = 35
const mv_SC_TRACE = 181
const mv_SC_TRACE_EVENT_FILTER = 182
const mv_SC_TRACE_EVENT_NAME_MAX = 242
const mv_SC_TRACE_INHERIT = 183
const mv_SC_TRACE_LOG = 184
const mv_SC_TRACE_NAME_MAX = 243
const mv_SC_TRACE_SYS_MAX = 244
const mv_SC_TRACE_USER_EVENT_MAX = 245
const mv_SC_TTY_NAME_MAX = 72
const mv_SC_TYPED_MEMORY_OBJECTS = 165
const mv_SC_TZNAME_MAX = 6
const mv_SC_UIO_MAXIOV = 60
const mv_SC_V6_ILP32_OFF32 = 176
const mv_SC_V6_ILP32_OFFBIG = 177
const mv_SC_V6_LP64_OFF64 = 178
const mv_SC_V6_LPBIG_OFFBIG = 179
const mv_SC_V7_ILP32_OFF32 = 237
const mv_SC_V7_ILP32_OFFBIG = 238
const mv_SC_V7_LP64_OFF64 = 239
const mv_SC_V7_LPBIG_OFFBIG = 240
const mv_SC_VERSION = 29
const mv_SC_XBS5_ILP32_OFF32 = 125
const mv_SC_XBS5_ILP32_OFFBIG = 126
const mv_SC_XBS5_LP64_OFF64 = 127
const mv_SC_XBS5_LPBIG_OFFBIG = 128
const mv_SC_XOPEN_CRYPT = 92
const mv_SC_XOPEN_ENH_I18N = 93
const mv_SC_XOPEN_LEGACY = 129
const mv_SC_XOPEN_REALTIME = 130
const mv_SC_XOPEN_REALTIME_THREADS = 131
const mv_SC_XOPEN_SHM = 94
const mv_SC_XOPEN_STREAMS = 246
const mv_SC_XOPEN_UNIX = 91
const mv_SC_XOPEN_VERSION = 89
const mv_SC_XOPEN_XCU_VERSION = 90
const mv_SC_XOPEN_XPG2 = 98
const mv_SC_XOPEN_XPG3 = 99
const mv_SC_XOPEN_XPG4 = 100
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_ENH_I18N = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv_XOPEN_UNIX = 1
const mv_XOPEN_VERSION = 700
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 2
const mv__FLT_EVAL_METHOD__ = 2
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG -mlong-double-64"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__ILP32__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LAHF_SAHF__ = 1
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 2147483647
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "ll"
const mv__PRIPTR = ""
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 12
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__code_model_32__ = 1
const mv__gmp_doprnt_mpf = "__gmp_doprnt_mpf2"
const mv__gnu_linux__ = 1
const mv__i386 = 1
const mv__i386__ = 1
const mv__i686 = 1
const mv__i686__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pentiumpro = 1
const mv__pentiumpro__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_newalloc = "_mpz_realloc"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvbinvert_limb_table = "__gmp_binvert_limb_table"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_init_primesieve = "__gmp_init_primesieve"
const mvgmp_nextprime = "__gmp_nextprime"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_primesieve = "__gmp_primesieve"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvi386 = 1
const mvjacobi_table = "__gmp_jacobi_table"
const mvlinux = 1
const mvmodlimb_invert = "binvert_limb"
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui (x[0], 3L) == 0"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpn_fft_mul = "mpn_nussbaumer_mul"
const mvmpn_get_d = "__gmpn_get_d"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_gcd = "__gmpz_divexact_gcd"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_inp_str_nowhite = "__gmpz_inp_str_nowhite"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucas_mod = "__gmpz_lucas_mod"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_n_pow_ui = "__gmpz_n_pow_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_oddfac_1 = "__gmpz_oddfac_1"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_prodlimbs = "__gmpz_prodlimbs"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_stronglucas = "__gmpz_stronglucas"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvrestrict = "__restrict"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint32

type tnva_list = ppuintptr

type tnwchar_t = ppint32

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnsize_t = ppuint32

type tnptrdiff_t = ppint32

type tnssize_t = ppint32

type tnoff_t = ppint64

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlocale_t = ppuintptr

type tnuintptr_t = ppuint32

type tnintptr_t = ppint32

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tnpid_t = ppint32

type tnuid_t = ppuint32

type tngid_t = ppuint32

type tnuseconds_t = ppuint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

type tngmp_uint_least32_t = ppuint32

type tngmp_intptr_t = ppint32

type tngmp_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tngmp_pi2_t = struct {
	fdinv21 tnmp_limb_t
	fdinv32 tnmp_limb_t
	fdinv53 tnmp_limb_t
}

type tutmp_align_t = struct {
	fdd           [0]ppfloat64
	fdp           [0]ppuintptr
	fdl           tnmp_limb_t
	fd__ccgo_pad3 [4]byte
}

type tstmp_reentrant_t = struct {
	fdnext ppuintptr
	fdsize tnsize_t
}

type tngmp_randfnptr_t = struct {
	fdrandseed_fn  ppuintptr
	fdrandget_fn   ppuintptr
	fdrandclear_fn ppuintptr
	fdrandiset_fn  ppuintptr
}

type tetoom6_flags = ppint32

const ectoom6_all_pos = 0
const ectoom6_vm1_neg = 1
const ectoom6_vm2_neg = 2

type tetoom7_flags = ppint32

const ectoom7_w1_neg = 1
const ectoom7_w3_neg = 2

type tngmp_primesieve_t = struct {
	fdd       ppuint32
	fds0      ppuint32
	fdsqrt_s0 ppuint32
	fds       [513]ppuint8
}

type tsfft_table_nk = struct {
	fd__ccgo0 uint32
}

type tsbases = struct {
	fdchars_per_limb    ppint32
	fdlogb2             tnmp_limb_t
	fdlog2b             tnmp_limb_t
	fdbig_base          tnmp_limb_t
	fdbig_base_inverted tnmp_limb_t
}

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tuieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type tnmp_double_limb_t = struct {
	fdd0 tnmp_limb_t
	fdd1 tnmp_limb_t
}

type tshgcd_matrix1 = struct {
	fdu [2][2]tnmp_limb_t
}

type tshgcd_matrix = struct {
	fdalloc tnmp_size_t
	fdn     tnmp_size_t
	fdp     [2][2]tnmp_ptr
}

type tsgcdext_ctx = struct {
	fdgp    tnmp_ptr
	fdgn    tnmp_size_t
	fdup    tnmp_ptr
	fdusize ppuintptr
	fdun    tnmp_size_t
	fdu0    tnmp_ptr
	fdu1    tnmp_ptr
	fdtp    tnmp_ptr
}

type tspowers = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tnpowers_t = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tsdoprnt_params_t = struct {
	fdbase         ppint32
	fdconv         ppint32
	fdexpfmt       ppuintptr
	fdexptimes4    ppint32
	fdfill         ppint8
	fdjustify      ppint32
	fdprec         ppint32
	fdshowbase     ppint32
	fdshowpoint    ppint32
	fdshowtrailing ppint32
	fdsign         ppint8
	fdwidth        ppint32
}

type tndoprnt_format_t = ppuintptr

type tndoprnt_memory_t = ppuintptr

type tndoprnt_reps_t = ppuintptr

type tndoprnt_final_t = ppuintptr

type tsdoprnt_funs_t = struct {
	fdformat tndoprnt_format_t
	fdmemory tndoprnt_memory_t
	fdreps   tndoprnt_reps_t
	fdfinal  tndoprnt_final_t
}

type tsgmp_asprintf_t = struct {
	fdresult ppuintptr
	fdbuf    ppuintptr
	fdsize   tnsize_t
	fdalloc  tnsize_t
}

type tsgmp_snprintf_t = struct {
	fdbuf  ppuintptr
	fdsize tnsize_t
}

type tngmp_doscan_scan_t = ppuintptr

type tngmp_doscan_step_t = ppuintptr

type tngmp_doscan_get_t = ppuintptr

type tngmp_doscan_unget_t = ppuintptr

type tsgmp_doscan_funs_t = struct {
	fdscan  tngmp_doscan_scan_t
	fdstep  tngmp_doscan_step_t
	fdget   tngmp_doscan_get_t
	fdunget tngmp_doscan_unget_t
}

type tn__jmp_buf = [6]ppuint32

type tnjmp_buf = [1]ts__jmp_buf_tag

type ts__jmp_buf_tag = struct {
	fd__jb tn__jmp_buf
	fd__fl ppuint32
	fd__ss [32]ppuint32
}

type tnsigjmp_buf = [1]ts__jmp_buf_tag

/* Establish ostringstream and istringstream.  Do this here so as to hide
   the conditionals, rather than putting stuff in each test program.

   Oldish versions of g++, like 2.95.2, don't have <sstream>, only
   <strstream>.  Fake up ostringstream and istringstream classes, but not a
   full implementation, just enough for our purposes.  */

var Xoption_check_printf ppint32

var Xcheck_vfprintf_fp ppuintptr

/* From any of the tests run here. */

func Xcheck_plain(cgtls *iqlibc.ppTLS, aawant ppuintptr, aafmt_orig ppuintptr, cgva ppuintptr) {
	cgbp := cgtls.ppAlloc(1040)
	defer cgtls.ppFree(1040)

	var aaap tnva_list
	var aafmt, aap, aaq, ccv2, ccv3 ppuintptr
	var aafmtsize tnsize_t
	var aagot_len, aawant_len ppint32
	var pp_ /* got at bp+0 */ [1024]ppint8
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaap, aafmt, aafmtsize, aagot_len, aap, aaq, aawant_len, ccv2, ccv3
	aaap = cgva

	if !(Xoption_check_printf != 0) {
		return
	}

	aafmtsize = Xstrlen(cgtls, aafmt_orig) + ppuint32(1)
	aafmt = (*(*func(*iqlibc.ppTLS, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_allocate_func})))(cgtls, aafmtsize)

	aap = aafmt_orig
	aaq = aafmt
	for {
		if !(ppint32(*(*ppint8)(iqunsafe.ppPointer(aap))) != ppint32('\000')) {
			break
		}

		switch ppint32(*(*ppint8)(iqunsafe.ppPointer(aap))) {
		case ppint32('a'):
			fallthrough
		case ppint32('A'):
			/* The exact value of the exponent isn't guaranteed in glibc, and it
			   and gmp_printf do slightly different things, so don't compare
			   directly. */
			goto ppdone
			fallthrough
		case ppint32('F'):
			if aap > aafmt_orig && ppint32(*(*ppint8)(iqunsafe.ppPointer(aap - iqlibc.ppUintptrFromInt32(1)))) == ppint32('.') {
				goto ppdone
			} /* don't test the "all digits" cases */
			/* discard 'F' type */
			break
			fallthrough
		case ppint32('Z'):
			/* transmute */
			ccv2 = aaq
			aaq++
			*(*ppint8)(iqunsafe.ppPointer(ccv2)) = ppint8('l')
			break
			fallthrough
		default:
			ccv3 = aaq
			aaq++
			*(*ppint8)(iqunsafe.ppPointer(ccv3)) = *(*ppint8)(iqunsafe.ppPointer(aap))
			break
		}

		goto cg_1
	cg_1:
		;
		aap++
	}
	*(*ppint8)(iqunsafe.ppPointer(aaq)) = ppint8('\000')

	aawant_len = iqlibc.ppInt32FromUint32(Xstrlen(cgtls, aawant))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppUint32FromInt32(aawant_len) < iqlibc.ppUint32FromInt64(1024))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(111), "want_len < sizeof(got)\x00")
	}

	aagot_len = Xvsprintf(cgtls, cgbp, aafmt, aaap)

	if aagot_len != aawant_len || Xstrcmp(cgtls, cgbp, aawant) != 0 {

		Xprintf(cgtls, "wanted data doesn't match plain vsprintf\n\x00", 0)
		Xprintf(cgtls, "  fmt      |%s|\n\x00", iqlibc.ppVaList(cgbp+1032, aafmt))
		Xprintf(cgtls, "  got      |%s|\n\x00", iqlibc.ppVaList(cgbp+1032, cgbp))
		Xprintf(cgtls, "  want     |%s|\n\x00", iqlibc.ppVaList(cgbp+1032, aawant))
		Xprintf(cgtls, "  got_len  %d\n\x00", iqlibc.ppVaList(cgbp+1032, aagot_len))
		Xprintf(cgtls, "  want_len %d\n\x00", iqlibc.ppVaList(cgbp+1032, aawant_len))
		Xabort(cgtls)
	}

	goto ppdone
ppdone:
	;
	(*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t))(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_free_func})))(cgtls, aafmt, aafmtsize)
}

func Xcheck_vsprintf(cgtls *iqlibc.ppTLS, aawant ppuintptr, aafmt ppuintptr, aaap tnva_list) {
	cgbp := cgtls.ppAlloc(1040)
	defer cgtls.ppFree(1040)

	var aagot_len, aawant_len ppint32
	var pp_ /* got at bp+0 */ [1024]ppint8
	pp_, pp_ = aagot_len, aawant_len

	aawant_len = iqlibc.ppInt32FromUint32(Xstrlen(cgtls, aawant))
	aagot_len = X__gmp_vsprintf(cgtls, cgbp, aafmt, aaap)

	if aagot_len != aawant_len || Xstrcmp(cgtls, cgbp, aawant) != 0 {

		Xprintf(cgtls, "gmp_vsprintf wrong\n\x00", 0)
		Xprintf(cgtls, "  fmt      |%s|\n\x00", iqlibc.ppVaList(cgbp+1032, aafmt))
		Xprintf(cgtls, "  got      |%s|\n\x00", iqlibc.ppVaList(cgbp+1032, cgbp))
		Xprintf(cgtls, "  want     |%s|\n\x00", iqlibc.ppVaList(cgbp+1032, aawant))
		Xprintf(cgtls, "  got_len  %d\n\x00", iqlibc.ppVaList(cgbp+1032, aagot_len))
		Xprintf(cgtls, "  want_len %d\n\x00", iqlibc.ppVaList(cgbp+1032, aawant_len))
		Xabort(cgtls)
	}
}

func Xcheck_vfprintf(cgtls *iqlibc.ppTLS, aawant ppuintptr, aafmt ppuintptr, aaap tnva_list) {
	cgbp := cgtls.ppAlloc(1056)
	defer cgtls.ppFree(1056)

	var aafread_len, aaftell_len, aagot_len, aawant_len ppint32
	var pp_ /* got at bp+0 */ [1024]ppint8
	pp_, pp_, pp_, pp_ = aafread_len, aaftell_len, aagot_len, aawant_len

	aawant_len = iqlibc.ppInt32FromUint32(Xstrlen(cgtls, aawant))

	Xrewind(cgtls, Xcheck_vfprintf_fp)
	aagot_len = X__gmp_vfprintf(cgtls, Xcheck_vfprintf_fp, aafmt, aaap)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aagot_len != -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(162), "got_len != -1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xfflush(cgtls, Xcheck_vfprintf_fp) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(163), "fflush (check_vfprintf_fp) == 0\x00")
	}

	aaftell_len = Xftell(cgtls, Xcheck_vfprintf_fp)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaftell_len != ppint32(-iqlibc.ppInt32FromInt32(1)))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(166), "ftell_len != -1\x00")
	}

	Xrewind(cgtls, Xcheck_vfprintf_fp)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppUint32FromInt32(aaftell_len) <= iqlibc.ppUint32FromInt64(1024))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(169), "ftell_len <= sizeof(got)\x00")
	}
	aafread_len = iqlibc.ppInt32FromUint32(Xfread(cgtls, cgbp, ppuint32(1), iqlibc.ppUint32FromInt32(aaftell_len), Xcheck_vfprintf_fp))

	if aagot_len != aawant_len || aaftell_len != ppint32(aawant_len) || aafread_len != aawant_len || Xmemcmp(cgtls, cgbp, aawant, iqlibc.ppUint32FromInt32(aawant_len)) != 0 {

		Xprintf(cgtls, "gmp_vfprintf wrong\n\x00", 0)
		Xprintf(cgtls, "  fmt       |%s|\n\x00", iqlibc.ppVaList(cgbp+1032, aafmt))
		Xprintf(cgtls, "  got       |%.*s|\n\x00", iqlibc.ppVaList(cgbp+1032, aafread_len, cgbp))
		Xprintf(cgtls, "  want      |%s|\n\x00", iqlibc.ppVaList(cgbp+1032, aawant))
		Xprintf(cgtls, "  got_len   %d\n\x00", iqlibc.ppVaList(cgbp+1032, aagot_len))
		Xprintf(cgtls, "  ftell_len %ld\n\x00", iqlibc.ppVaList(cgbp+1032, aaftell_len))
		Xprintf(cgtls, "  fread_len %d\n\x00", iqlibc.ppVaList(cgbp+1032, aafread_len))
		Xprintf(cgtls, "  want_len  %d\n\x00", iqlibc.ppVaList(cgbp+1032, aawant_len))
		Xabort(cgtls)
	}
}

func Xcheck_vsnprintf(cgtls *iqlibc.ppTLS, aawant ppuintptr, aafmt ppuintptr, aaap tnva_list) {
	cgbp := cgtls.ppAlloc(1056)
	defer cgtls.ppFree(1056)

	var aabufsize tnsize_t
	var aagot_len, aaret, aawant_len ppint32
	var ccv4, ccv5, ccv6 ppuint32
	var pp_ /* got at bp+0 */ [1025]ppint8
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aabufsize, aagot_len, aaret, aawant_len, ccv4, ccv5, ccv6

	aawant_len = iqlibc.ppInt32FromUint32(Xstrlen(cgtls, aawant))

	aabufsize = iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))
cg_3:
	;

	/* do 0 to 5, then want-5 to want+5 */
	aabufsize++
	if aabufsize > ppuint32(5) && aabufsize < iqlibc.ppUint32FromInt32(aawant_len-ppint32(5)) {
		aabufsize = iqlibc.ppUint32FromInt32(aawant_len - ppint32(5))
	}
	if aabufsize > iqlibc.ppUint32FromInt32(aawant_len+ppint32(5)) {
		goto cg_1
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aabufsize+iqlibc.ppUint32FromInt32(1) <= iqlibc.ppUint32FromInt64(1025))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(207), "bufsize+1 <= sizeof (got)\x00")
	}

	(*(*[1025]ppint8)(iqunsafe.ppPointer(cgbp)))[aabufsize] = ppint8('!')
	aaret = X__gmp_vsnprintf(cgtls, cgbp, aabufsize, aafmt, aaap)

	if iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)) > aabufsize {
		ccv5 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1))
	} else {
		ccv5 = aabufsize
	}
	if ccv5-ppuint32(1) < iqlibc.ppUint32FromInt32(aawant_len) {
		if iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)) > aabufsize {
			ccv6 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1))
		} else {
			ccv6 = aabufsize
		}
		ccv4 = ccv6 - ppuint32(1)
	} else {
		ccv4 = iqlibc.ppUint32FromInt32(aawant_len)
	}
	aagot_len = iqlibc.ppInt32FromUint32(ccv4)

	if ppint32((*(*[1025]ppint8)(iqunsafe.ppPointer(cgbp)))[aabufsize]) != ppint32('!') {

		Xprintf(cgtls, "gmp_vsnprintf overwrote bufsize sentinel\n\x00", 0)
		goto pperror
	}

	if aaret != aawant_len {

		Xprintf(cgtls, "gmp_vsnprintf return value wrong\n\x00", 0)
		goto pperror
	}

	if !(aabufsize > ppuint32(0)) {
		goto cg_7
	}

	if !(Xmemcmp(cgtls, cgbp, aawant, iqlibc.ppUint32FromInt32(aagot_len)) != 0 || ppint32((*(*[1025]ppint8)(iqunsafe.ppPointer(cgbp)))[aagot_len]) != ppint32('\000')) {
		goto cg_8
	}

	Xprintf(cgtls, "gmp_vsnprintf wrong result string\n\x00", 0)
	goto pperror
pperror:
	;
	Xprintf(cgtls, "  fmt       |%s|\n\x00", iqlibc.ppVaList(cgbp+1040, aafmt))
	Xprintf(cgtls, "  bufsize   %lu\n\x00", iqlibc.ppVaList(cgbp+1040, ppuint32(aabufsize)))
	Xprintf(cgtls, "  got       |%s|\n\x00", iqlibc.ppVaList(cgbp+1040, cgbp))
	Xprintf(cgtls, "  want      |%.*s|\n\x00", iqlibc.ppVaList(cgbp+1040, aagot_len, aawant))
	Xprintf(cgtls, "  want full |%s|\n\x00", iqlibc.ppVaList(cgbp+1040, aawant))
	Xprintf(cgtls, "  ret       %d\n\x00", iqlibc.ppVaList(cgbp+1040, aaret))
	Xprintf(cgtls, "  want_len  %d\n\x00", iqlibc.ppVaList(cgbp+1040, aawant_len))
	Xabort(cgtls)
cg_8:
	;
cg_7:
	;
	goto cg_2
cg_2:
	;
	goto cg_3
	goto cg_1
cg_1:
}

func Xcheck_vasprintf(cgtls *iqlibc.ppTLS, aawant ppuintptr, aafmt ppuintptr, aaap tnva_list) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aagot_len, aawant_len ppint32
	var pp_ /* got at bp+0 */ ppuintptr
	pp_, pp_ = aagot_len, aawant_len

	aawant_len = iqlibc.ppInt32FromUint32(Xstrlen(cgtls, aawant))
	aagot_len = X__gmp_vasprintf(cgtls, cgbp, aafmt, aaap)

	if aagot_len != aawant_len || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp)), aawant) != 0 {

		Xprintf(cgtls, "gmp_vasprintf wrong\n\x00", 0)
		Xprintf(cgtls, "  fmt      |%s|\n\x00", iqlibc.ppVaList(cgbp+16, aafmt))
		Xprintf(cgtls, "  got      |%s|\n\x00", iqlibc.ppVaList(cgbp+16, *(*ppuintptr)(iqunsafe.ppPointer(cgbp))))
		Xprintf(cgtls, "  want     |%s|\n\x00", iqlibc.ppVaList(cgbp+16, aawant))
		Xprintf(cgtls, "  got_len  %d\n\x00", iqlibc.ppVaList(cgbp+16, aagot_len))
		Xprintf(cgtls, "  want_len %d\n\x00", iqlibc.ppVaList(cgbp+16, aawant_len))
		Xabort(cgtls)
	}
	(*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t))(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_free_func})))(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp)), Xstrlen(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp)))+ppuint32(1))
}

func Xcheck_obstack_vprintf(cgtls *iqlibc.ppTLS, aawant ppuintptr, aafmt ppuintptr, aaap tnva_list) {

}

func Xcheck_one(cgtls *iqlibc.ppTLS, aawant ppuintptr, aafmt ppuintptr, cgva ppuintptr) {

	var aaap tnva_list
	pp_ = aaap
	aaap = cgva

	/* simplest first */
	Xcheck_vsprintf(cgtls, aawant, aafmt, aaap)
	Xcheck_vfprintf(cgtls, aawant, aafmt, aaap)
	Xcheck_vsnprintf(cgtls, aawant, aafmt, aaap)
	Xcheck_vasprintf(cgtls, aawant, aafmt, aaap)
	Xcheck_obstack_vprintf(cgtls, aawant, aafmt, aaap)
}

func Xcheck_z(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aai, aaj, ccv10, ccv11, ccv2, ccv8 ppint32
	var aanfmt ppuintptr
	var aansize, aazeros tnmp_size_t
	var ccv3, ccv4, ccv5 ppbool
	var pp_ /* z at bp+0 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aai, aaj, aanfmt, aansize, aazeros, ccv10, ccv11, ccv2, ccv3, ccv4, ccv5, ccv8
	var sndata = [49]struct {
		fdfmt  ppuintptr
		fdz    ppuintptr
		fdwant ppuintptr
	}{
		0: {
			fdfmt:  "%Zd\x00",
			fdz:    "0\x00",
			fdwant: "0\x00",
		},
		1: {
			fdfmt:  "%Zd\x00",
			fdz:    "1\x00",
			fdwant: "1\x00",
		},
		2: {
			fdfmt:  "%Zd\x00",
			fdz:    "123\x00",
			fdwant: "123\x00",
		},
		3: {
			fdfmt:  "%Zd\x00",
			fdz:    "-1\x00",
			fdwant: "-1\x00",
		},
		4: {
			fdfmt:  "%Zd\x00",
			fdz:    "-123\x00",
			fdwant: "-123\x00",
		},
		5: {
			fdfmt:  "%+Zd\x00",
			fdz:    "0\x00",
			fdwant: "+0\x00",
		},
		6: {
			fdfmt:  "%+Zd\x00",
			fdz:    "123\x00",
			fdwant: "+123\x00",
		},
		7: {
			fdfmt:  "%+Zd\x00",
			fdz:    "-123\x00",
			fdwant: "-123\x00",
		},
		8: {
			fdfmt:  "%Zx\x00",
			fdz:    "123\x00",
			fdwant: "7b\x00",
		},
		9: {
			fdfmt:  "%ZX\x00",
			fdz:    "123\x00",
			fdwant: "7B\x00",
		},
		10: {
			fdfmt:  "%Zx\x00",
			fdz:    "-123\x00",
			fdwant: "-7b\x00",
		},
		11: {
			fdfmt:  "%ZX\x00",
			fdz:    "-123\x00",
			fdwant: "-7B\x00",
		},
		12: {
			fdfmt:  "%Zo\x00",
			fdz:    "123\x00",
			fdwant: "173\x00",
		},
		13: {
			fdfmt:  "%Zo\x00",
			fdz:    "-123\x00",
			fdwant: "-173\x00",
		},
		14: {
			fdfmt:  "%#Zx\x00",
			fdz:    "0\x00",
			fdwant: "0\x00",
		},
		15: {
			fdfmt:  "%#ZX\x00",
			fdz:    "0\x00",
			fdwant: "0\x00",
		},
		16: {
			fdfmt:  "%#Zx\x00",
			fdz:    "123\x00",
			fdwant: "0x7b\x00",
		},
		17: {
			fdfmt:  "%#ZX\x00",
			fdz:    "123\x00",
			fdwant: "0X7B\x00",
		},
		18: {
			fdfmt:  "%#Zx\x00",
			fdz:    "-123\x00",
			fdwant: "-0x7b\x00",
		},
		19: {
			fdfmt:  "%#ZX\x00",
			fdz:    "-123\x00",
			fdwant: "-0X7B\x00",
		},
		20: {
			fdfmt:  "%#Zo\x00",
			fdz:    "0\x00",
			fdwant: "0\x00",
		},
		21: {
			fdfmt:  "%#Zo\x00",
			fdz:    "123\x00",
			fdwant: "0173\x00",
		},
		22: {
			fdfmt:  "%#Zo\x00",
			fdz:    "-123\x00",
			fdwant: "-0173\x00",
		},
		23: {
			fdfmt:  "%10Zd\x00",
			fdz:    "0\x00",
			fdwant: "         0\x00",
		},
		24: {
			fdfmt:  "%10Zd\x00",
			fdz:    "123\x00",
			fdwant: "       123\x00",
		},
		25: {
			fdfmt:  "%10Zd\x00",
			fdz:    "-123\x00",
			fdwant: "      -123\x00",
		},
		26: {
			fdfmt:  "%-10Zd\x00",
			fdz:    "0\x00",
			fdwant: "0         \x00",
		},
		27: {
			fdfmt:  "%-10Zd\x00",
			fdz:    "123\x00",
			fdwant: "123       \x00",
		},
		28: {
			fdfmt:  "%-10Zd\x00",
			fdz:    "-123\x00",
			fdwant: "-123      \x00",
		},
		29: {
			fdfmt:  "%+10Zd\x00",
			fdz:    "123\x00",
			fdwant: "      +123\x00",
		},
		30: {
			fdfmt:  "%+-10Zd\x00",
			fdz:    "123\x00",
			fdwant: "+123      \x00",
		},
		31: {
			fdfmt:  "%+10Zd\x00",
			fdz:    "-123\x00",
			fdwant: "      -123\x00",
		},
		32: {
			fdfmt:  "%+-10Zd\x00",
			fdz:    "-123\x00",
			fdwant: "-123      \x00",
		},
		33: {
			fdfmt:  "%08Zd\x00",
			fdz:    "0\x00",
			fdwant: "00000000\x00",
		},
		34: {
			fdfmt:  "%08Zd\x00",
			fdz:    "123\x00",
			fdwant: "00000123\x00",
		},
		35: {
			fdfmt:  "%08Zd\x00",
			fdz:    "-123\x00",
			fdwant: "-0000123\x00",
		},
		36: {
			fdfmt:  "%+08Zd\x00",
			fdz:    "0\x00",
			fdwant: "+0000000\x00",
		},
		37: {
			fdfmt:  "%+08Zd\x00",
			fdz:    "123\x00",
			fdwant: "+0000123\x00",
		},
		38: {
			fdfmt:  "%+08Zd\x00",
			fdz:    "-123\x00",
			fdwant: "-0000123\x00",
		},
		39: {
			fdfmt:  "%#08Zx\x00",
			fdz:    "0\x00",
			fdwant: "00000000\x00",
		},
		40: {
			fdfmt:  "%#08Zx\x00",
			fdz:    "123\x00",
			fdwant: "0x00007b\x00",
		},
		41: {
			fdfmt:  "%#08Zx\x00",
			fdz:    "-123\x00",
			fdwant: "-0x0007b\x00",
		},
		42: {
			fdfmt:  "%+#08Zx\x00",
			fdz:    "0\x00",
			fdwant: "+0000000\x00",
		},
		43: {
			fdfmt:  "%+#08Zx\x00",
			fdz:    "123\x00",
			fdwant: "+0x0007b\x00",
		},
		44: {
			fdfmt:  "%+#08Zx\x00",
			fdz:    "-123\x00",
			fdwant: "-0x0007b\x00",
		},
		45: {
			fdfmt:  "%.0Zd\x00",
			fdz:    "0\x00",
			fdwant: "\x00",
		},
		46: {
			fdfmt:  "%.1Zd\x00",
			fdz:    "0\x00",
			fdwant: "0\x00",
		},
		47: {
			fdfmt:  "%.2Zd\x00",
			fdz:    "0\x00",
			fdwant: "00\x00",
		},
		48: {
			fdfmt:  "%.3Zd\x00",
			fdz:    "0\x00",
			fdwant: "000\x00",
		},
	}

	X__gmpz_init(cgtls, cgbp)

	aai = 0
	for {
		if !(iqlibc.ppUint32FromInt32(aai) < iqlibc.ppUint32FromInt64(588)/iqlibc.ppUint32FromInt64(12)) {
			break
		}

		Xmpz_set_str_or_abort(cgtls, cgbp, sndata[aai].fdz, 0)

		/* don't try negatives or forced sign in hex or octal */

		if ccv5 = X__gmpz_fits_slong_p(cgtls, cgbp) != 0; ccv5 {
			if ccv4 = Xstrchr(cgtls, sndata[aai].fdfmt, ppint32('x')) != iqlibc.ppUintptrFromInt32(0) || Xstrchr(cgtls, sndata[aai].fdfmt, ppint32('X')) != iqlibc.ppUintptrFromInt32(0) || Xstrchr(cgtls, sndata[aai].fdfmt, ppint32('o')) != iqlibc.ppUintptrFromInt32(0); ccv4 {
				if ccv3 = Xstrchr(cgtls, sndata[aai].fdfmt, ppint32('+')) != iqlibc.ppUintptrFromInt32(0); !ccv3 {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size < 0 {
						ccv2 = -ppint32(1)
					} else {
						ccv2 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size > 0)
					}
				}
			}
		}
		if ccv5 && !(ccv4 && (ccv3 || ccv2 < 0)) {

			Xcheck_plain(cgtls, sndata[aai].fdwant, sndata[aai].fdfmt, iqlibc.ppVaList(cgbp+24, X__gmpz_get_si(cgtls, cgbp)))
		}

		Xcheck_one(cgtls, sndata[aai].fdwant, sndata[aai].fdfmt, iqlibc.ppVaList(cgbp+24, cgbp))

		/* Same again, with %N and possibly some high zero limbs */
		aanfmt = X__gmp_allocate_strdup(cgtls, sndata[aai].fdfmt)
		aaj = 0
		for {
			if !(ppint32(*(*ppint8)(iqunsafe.ppPointer(aanfmt + ppuintptr(aaj)))) != ppint32('\000')) {
				break
			}
			if ppint32(*(*ppint8)(iqunsafe.ppPointer(aanfmt + ppuintptr(aaj)))) == ppint32('Z') {
				*(*ppint8)(iqunsafe.ppPointer(aanfmt + ppuintptr(aaj))) = ppint8('N')
			}
			goto cg_6
		cg_6:
			;
			aaj++
		}
		aazeros = 0
		for {
			if !(aazeros <= ppint32(3)) {
				break
			}

			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size >= 0 {
				ccv8 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size
			} else {
				ccv8 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size
			}
			aansize = ppint32(ccv8) + aazeros
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(aansize > ppint32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_alloc)) != 0), 0) != 0 {
				X__gmpz_realloc(cgtls, cgbp, aansize)
			} else {
				pp_ = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_d
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size >= 0 {
				ccv10 = aansize
			} else {
				ccv10 = -aansize
			}
			aansize = ccv10
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size >= 0 {
				ccv11 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size
			} else {
				ccv11 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size
			}
			Xrefmpn_zero(cgtls, (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_d+ppuintptr(ccv11)*4, aazeros)
			Xcheck_one(cgtls, sndata[aai].fdwant, aanfmt, iqlibc.ppVaList(cgbp+24, (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_d, aansize))

			goto cg_7
		cg_7:
			;
			aazeros++
		}
		(*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t))(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_free_func})))(cgtls, aanfmt, Xstrlen(cgtls, aanfmt)+ppuint32(1))

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpz_clear(cgtls, cgbp)
}

func Xcheck_q(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aai ppint32
	var pp_ /* q at bp+0 */ tnmpq_t
	pp_ = aai
	var sndata = [72]struct {
		fdfmt  ppuintptr
		fdq    ppuintptr
		fdwant ppuintptr
	}{
		0: {
			fdfmt:  "%Qd\x00",
			fdq:    "0\x00",
			fdwant: "0\x00",
		},
		1: {
			fdfmt:  "%Qd\x00",
			fdq:    "1\x00",
			fdwant: "1\x00",
		},
		2: {
			fdfmt:  "%Qd\x00",
			fdq:    "123\x00",
			fdwant: "123\x00",
		},
		3: {
			fdfmt:  "%Qd\x00",
			fdq:    "-1\x00",
			fdwant: "-1\x00",
		},
		4: {
			fdfmt:  "%Qd\x00",
			fdq:    "-123\x00",
			fdwant: "-123\x00",
		},
		5: {
			fdfmt:  "%Qd\x00",
			fdq:    "3/2\x00",
			fdwant: "3/2\x00",
		},
		6: {
			fdfmt:  "%Qd\x00",
			fdq:    "-3/2\x00",
			fdwant: "-3/2\x00",
		},
		7: {
			fdfmt:  "%+Qd\x00",
			fdq:    "0\x00",
			fdwant: "+0\x00",
		},
		8: {
			fdfmt:  "%+Qd\x00",
			fdq:    "123\x00",
			fdwant: "+123\x00",
		},
		9: {
			fdfmt:  "%+Qd\x00",
			fdq:    "-123\x00",
			fdwant: "-123\x00",
		},
		10: {
			fdfmt:  "%+Qd\x00",
			fdq:    "5/8\x00",
			fdwant: "+5/8\x00",
		},
		11: {
			fdfmt:  "%+Qd\x00",
			fdq:    "-5/8\x00",
			fdwant: "-5/8\x00",
		},
		12: {
			fdfmt:  "%Qx\x00",
			fdq:    "123\x00",
			fdwant: "7b\x00",
		},
		13: {
			fdfmt:  "%QX\x00",
			fdq:    "123\x00",
			fdwant: "7B\x00",
		},
		14: {
			fdfmt:  "%Qx\x00",
			fdq:    "15/16\x00",
			fdwant: "f/10\x00",
		},
		15: {
			fdfmt:  "%QX\x00",
			fdq:    "15/16\x00",
			fdwant: "F/10\x00",
		},
		16: {
			fdfmt:  "%Qx\x00",
			fdq:    "-123\x00",
			fdwant: "-7b\x00",
		},
		17: {
			fdfmt:  "%QX\x00",
			fdq:    "-123\x00",
			fdwant: "-7B\x00",
		},
		18: {
			fdfmt:  "%Qx\x00",
			fdq:    "-15/16\x00",
			fdwant: "-f/10\x00",
		},
		19: {
			fdfmt:  "%QX\x00",
			fdq:    "-15/16\x00",
			fdwant: "-F/10\x00",
		},
		20: {
			fdfmt:  "%Qo\x00",
			fdq:    "123\x00",
			fdwant: "173\x00",
		},
		21: {
			fdfmt:  "%Qo\x00",
			fdq:    "-123\x00",
			fdwant: "-173\x00",
		},
		22: {
			fdfmt:  "%Qo\x00",
			fdq:    "16/17\x00",
			fdwant: "20/21\x00",
		},
		23: {
			fdfmt:  "%Qo\x00",
			fdq:    "-16/17\x00",
			fdwant: "-20/21\x00",
		},
		24: {
			fdfmt:  "%#Qx\x00",
			fdq:    "0\x00",
			fdwant: "0\x00",
		},
		25: {
			fdfmt:  "%#QX\x00",
			fdq:    "0\x00",
			fdwant: "0\x00",
		},
		26: {
			fdfmt:  "%#Qx\x00",
			fdq:    "123\x00",
			fdwant: "0x7b\x00",
		},
		27: {
			fdfmt:  "%#QX\x00",
			fdq:    "123\x00",
			fdwant: "0X7B\x00",
		},
		28: {
			fdfmt:  "%#Qx\x00",
			fdq:    "5/8\x00",
			fdwant: "0x5/0x8\x00",
		},
		29: {
			fdfmt:  "%#QX\x00",
			fdq:    "5/8\x00",
			fdwant: "0X5/0X8\x00",
		},
		30: {
			fdfmt:  "%#Qx\x00",
			fdq:    "-123\x00",
			fdwant: "-0x7b\x00",
		},
		31: {
			fdfmt:  "%#QX\x00",
			fdq:    "-123\x00",
			fdwant: "-0X7B\x00",
		},
		32: {
			fdfmt:  "%#Qx\x00",
			fdq:    "-5/8\x00",
			fdwant: "-0x5/0x8\x00",
		},
		33: {
			fdfmt:  "%#QX\x00",
			fdq:    "-5/8\x00",
			fdwant: "-0X5/0X8\x00",
		},
		34: {
			fdfmt:  "%#Qo\x00",
			fdq:    "0\x00",
			fdwant: "0\x00",
		},
		35: {
			fdfmt:  "%#Qo\x00",
			fdq:    "123\x00",
			fdwant: "0173\x00",
		},
		36: {
			fdfmt:  "%#Qo\x00",
			fdq:    "-123\x00",
			fdwant: "-0173\x00",
		},
		37: {
			fdfmt:  "%#Qo\x00",
			fdq:    "5/7\x00",
			fdwant: "05/07\x00",
		},
		38: {
			fdfmt:  "%#Qo\x00",
			fdq:    "-5/7\x00",
			fdwant: "-05/07\x00",
		},
		39: {
			fdfmt:  "%#10Qo\x00",
			fdq:    "0/0\x00",
			fdwant: "       0/0\x00",
		},
		40: {
			fdfmt:  "%#10Qd\x00",
			fdq:    "0/0\x00",
			fdwant: "       0/0\x00",
		},
		41: {
			fdfmt:  "%#10Qx\x00",
			fdq:    "0/0\x00",
			fdwant: "       0/0\x00",
		},
		42: {
			fdfmt:  "%#10Qo\x00",
			fdq:    "123/0\x00",
			fdwant: "    0173/0\x00",
		},
		43: {
			fdfmt:  "%#10Qd\x00",
			fdq:    "123/0\x00",
			fdwant: "     123/0\x00",
		},
		44: {
			fdfmt:  "%#10Qx\x00",
			fdq:    "123/0\x00",
			fdwant: "    0x7b/0\x00",
		},
		45: {
			fdfmt:  "%#10QX\x00",
			fdq:    "123/0\x00",
			fdwant: "    0X7B/0\x00",
		},
		46: {
			fdfmt:  "%#10Qo\x00",
			fdq:    "-123/0\x00",
			fdwant: "   -0173/0\x00",
		},
		47: {
			fdfmt:  "%#10Qd\x00",
			fdq:    "-123/0\x00",
			fdwant: "    -123/0\x00",
		},
		48: {
			fdfmt:  "%#10Qx\x00",
			fdq:    "-123/0\x00",
			fdwant: "   -0x7b/0\x00",
		},
		49: {
			fdfmt:  "%#10QX\x00",
			fdq:    "-123/0\x00",
			fdwant: "   -0X7B/0\x00",
		},
		50: {
			fdfmt:  "%10Qd\x00",
			fdq:    "0\x00",
			fdwant: "         0\x00",
		},
		51: {
			fdfmt:  "%-10Qd\x00",
			fdq:    "0\x00",
			fdwant: "0         \x00",
		},
		52: {
			fdfmt:  "%10Qd\x00",
			fdq:    "123\x00",
			fdwant: "       123\x00",
		},
		53: {
			fdfmt:  "%-10Qd\x00",
			fdq:    "123\x00",
			fdwant: "123       \x00",
		},
		54: {
			fdfmt:  "%10Qd\x00",
			fdq:    "-123\x00",
			fdwant: "      -123\x00",
		},
		55: {
			fdfmt:  "%-10Qd\x00",
			fdq:    "-123\x00",
			fdwant: "-123      \x00",
		},
		56: {
			fdfmt:  "%+10Qd\x00",
			fdq:    "123\x00",
			fdwant: "      +123\x00",
		},
		57: {
			fdfmt:  "%+-10Qd\x00",
			fdq:    "123\x00",
			fdwant: "+123      \x00",
		},
		58: {
			fdfmt:  "%+10Qd\x00",
			fdq:    "-123\x00",
			fdwant: "      -123\x00",
		},
		59: {
			fdfmt:  "%+-10Qd\x00",
			fdq:    "-123\x00",
			fdwant: "-123      \x00",
		},
		60: {
			fdfmt:  "%08Qd\x00",
			fdq:    "0\x00",
			fdwant: "00000000\x00",
		},
		61: {
			fdfmt:  "%08Qd\x00",
			fdq:    "123\x00",
			fdwant: "00000123\x00",
		},
		62: {
			fdfmt:  "%08Qd\x00",
			fdq:    "-123\x00",
			fdwant: "-0000123\x00",
		},
		63: {
			fdfmt:  "%+08Qd\x00",
			fdq:    "0\x00",
			fdwant: "+0000000\x00",
		},
		64: {
			fdfmt:  "%+08Qd\x00",
			fdq:    "123\x00",
			fdwant: "+0000123\x00",
		},
		65: {
			fdfmt:  "%+08Qd\x00",
			fdq:    "-123\x00",
			fdwant: "-0000123\x00",
		},
		66: {
			fdfmt:  "%#08Qx\x00",
			fdq:    "0\x00",
			fdwant: "00000000\x00",
		},
		67: {
			fdfmt:  "%#08Qx\x00",
			fdq:    "123\x00",
			fdwant: "0x00007b\x00",
		},
		68: {
			fdfmt:  "%#08Qx\x00",
			fdq:    "-123\x00",
			fdwant: "-0x0007b\x00",
		},
		69: {
			fdfmt:  "%+#08Qx\x00",
			fdq:    "0\x00",
			fdwant: "+0000000\x00",
		},
		70: {
			fdfmt:  "%+#08Qx\x00",
			fdq:    "123\x00",
			fdwant: "+0x0007b\x00",
		},
		71: {
			fdfmt:  "%+#08Qx\x00",
			fdq:    "-123\x00",
			fdwant: "-0x0007b\x00",
		},
	}

	X__gmpq_init(cgtls, cgbp)

	aai = 0
	for {
		if !(iqlibc.ppUint32FromInt32(aai) < iqlibc.ppUint32FromInt64(864)/iqlibc.ppUint32FromInt64(12)) {
			break
		}

		Xmpq_set_str_or_abort(cgtls, cgbp, sndata[aai].fdq, 0)
		Xcheck_one(cgtls, sndata[aai].fdwant, sndata[aai].fdfmt, iqlibc.ppVaList(cgbp+32, cgbp))

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpq_clear(cgtls, cgbp)
}

func Xcheck_f(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aad ppfloat64
	var aai ppint32
	var pp_ /* f at bp+0 */ tnmpf_t
	pp_, pp_ = aad, aai
	var sndata = [94]struct {
		fdfmt  ppuintptr
		fdf    ppuintptr
		fdwant ppuintptr
	}{
		0: {
			fdfmt:  "%Ff\x00",
			fdf:    "0\x00",
			fdwant: "0.000000\x00",
		},
		1: {
			fdfmt:  "%Ff\x00",
			fdf:    "123\x00",
			fdwant: "123.000000\x00",
		},
		2: {
			fdfmt:  "%Ff\x00",
			fdf:    "-123\x00",
			fdwant: "-123.000000\x00",
		},
		3: {
			fdfmt:  "%+Ff\x00",
			fdf:    "0\x00",
			fdwant: "+0.000000\x00",
		},
		4: {
			fdfmt:  "%+Ff\x00",
			fdf:    "123\x00",
			fdwant: "+123.000000\x00",
		},
		5: {
			fdfmt:  "%+Ff\x00",
			fdf:    "-123\x00",
			fdwant: "-123.000000\x00",
		},
		6: {
			fdfmt:  "%.0Ff\x00",
			fdf:    "0\x00",
			fdwant: "0\x00",
		},
		7: {
			fdfmt:  "%.0Ff\x00",
			fdf:    "123\x00",
			fdwant: "123\x00",
		},
		8: {
			fdfmt:  "%.0Ff\x00",
			fdf:    "-123\x00",
			fdwant: "-123\x00",
		},
		9: {
			fdfmt:  "%8.0Ff\x00",
			fdf:    "0\x00",
			fdwant: "       0\x00",
		},
		10: {
			fdfmt:  "%8.0Ff\x00",
			fdf:    "123\x00",
			fdwant: "     123\x00",
		},
		11: {
			fdfmt:  "%8.0Ff\x00",
			fdf:    "-123\x00",
			fdwant: "    -123\x00",
		},
		12: {
			fdfmt:  "%08.0Ff\x00",
			fdf:    "0\x00",
			fdwant: "00000000\x00",
		},
		13: {
			fdfmt:  "%08.0Ff\x00",
			fdf:    "123\x00",
			fdwant: "00000123\x00",
		},
		14: {
			fdfmt:  "%08.0Ff\x00",
			fdf:    "-123\x00",
			fdwant: "-0000123\x00",
		},
		15: {
			fdfmt:  "%10.2Ff\x00",
			fdf:    "0\x00",
			fdwant: "      0.00\x00",
		},
		16: {
			fdfmt:  "%10.2Ff\x00",
			fdf:    "0.25\x00",
			fdwant: "      0.25\x00",
		},
		17: {
			fdfmt:  "%10.2Ff\x00",
			fdf:    "123.25\x00",
			fdwant: "    123.25\x00",
		},
		18: {
			fdfmt:  "%10.2Ff\x00",
			fdf:    "-123.25\x00",
			fdwant: "   -123.25\x00",
		},
		19: {
			fdfmt:  "%-10.2Ff\x00",
			fdf:    "0\x00",
			fdwant: "0.00      \x00",
		},
		20: {
			fdfmt:  "%-10.2Ff\x00",
			fdf:    "0.25\x00",
			fdwant: "0.25      \x00",
		},
		21: {
			fdfmt:  "%-10.2Ff\x00",
			fdf:    "123.25\x00",
			fdwant: "123.25    \x00",
		},
		22: {
			fdfmt:  "%-10.2Ff\x00",
			fdf:    "-123.25\x00",
			fdwant: "-123.25   \x00",
		},
		23: {
			fdfmt:  "%.2Ff\x00",
			fdf:    "0.00000000000001\x00",
			fdwant: "0.00\x00",
		},
		24: {
			fdfmt:  "%.2Ff\x00",
			fdf:    "0.002\x00",
			fdwant: "0.00\x00",
		},
		25: {
			fdfmt:  "%.2Ff\x00",
			fdf:    "0.008\x00",
			fdwant: "0.01\x00",
		},
		26: {
			fdfmt:  "%.0Ff\x00",
			fdf:    "123.00000000000001\x00",
			fdwant: "123\x00",
		},
		27: {
			fdfmt:  "%.0Ff\x00",
			fdf:    "123.2\x00",
			fdwant: "123\x00",
		},
		28: {
			fdfmt:  "%.0Ff\x00",
			fdf:    "123.8\x00",
			fdwant: "124\x00",
		},
		29: {
			fdfmt:  "%.0Ff\x00",
			fdf:    "999999.9\x00",
			fdwant: "1000000\x00",
		},
		30: {
			fdfmt:  "%.0Ff\x00",
			fdf:    "3999999.9\x00",
			fdwant: "4000000\x00",
		},
		31: {
			fdfmt:  "%Fe\x00",
			fdf:    "0\x00",
			fdwant: "0.000000e+00\x00",
		},
		32: {
			fdfmt:  "%Fe\x00",
			fdf:    "1\x00",
			fdwant: "1.000000e+00\x00",
		},
		33: {
			fdfmt:  "%Fe\x00",
			fdf:    "123\x00",
			fdwant: "1.230000e+02\x00",
		},
		34: {
			fdfmt:  "%FE\x00",
			fdf:    "0\x00",
			fdwant: "0.000000E+00\x00",
		},
		35: {
			fdfmt:  "%FE\x00",
			fdf:    "1\x00",
			fdwant: "1.000000E+00\x00",
		},
		36: {
			fdfmt:  "%FE\x00",
			fdf:    "123\x00",
			fdwant: "1.230000E+02\x00",
		},
		37: {
			fdfmt:  "%Fe\x00",
			fdf:    "0\x00",
			fdwant: "0.000000e+00\x00",
		},
		38: {
			fdfmt:  "%Fe\x00",
			fdf:    "1\x00",
			fdwant: "1.000000e+00\x00",
		},
		39: {
			fdfmt:  "%.0Fe\x00",
			fdf:    "10000000000\x00",
			fdwant: "1e+10\x00",
		},
		40: {
			fdfmt:  "%.0Fe\x00",
			fdf:    "-10000000000\x00",
			fdwant: "-1e+10\x00",
		},
		41: {
			fdfmt:  "%.2Fe\x00",
			fdf:    "10000000000\x00",
			fdwant: "1.00e+10\x00",
		},
		42: {
			fdfmt:  "%.2Fe\x00",
			fdf:    "-10000000000\x00",
			fdwant: "-1.00e+10\x00",
		},
		43: {
			fdfmt:  "%8.0Fe\x00",
			fdf:    "10000000000\x00",
			fdwant: "   1e+10\x00",
		},
		44: {
			fdfmt:  "%8.0Fe\x00",
			fdf:    "-10000000000\x00",
			fdwant: "  -1e+10\x00",
		},
		45: {
			fdfmt:  "%-8.0Fe\x00",
			fdf:    "10000000000\x00",
			fdwant: "1e+10   \x00",
		},
		46: {
			fdfmt:  "%-8.0Fe\x00",
			fdf:    "-10000000000\x00",
			fdwant: "-1e+10  \x00",
		},
		47: {
			fdfmt:  "%12.2Fe\x00",
			fdf:    "10000000000\x00",
			fdwant: "    1.00e+10\x00",
		},
		48: {
			fdfmt:  "%12.2Fe\x00",
			fdf:    "-10000000000\x00",
			fdwant: "   -1.00e+10\x00",
		},
		49: {
			fdfmt:  "%012.2Fe\x00",
			fdf:    "10000000000\x00",
			fdwant: "00001.00e+10\x00",
		},
		50: {
			fdfmt:  "%012.2Fe\x00",
			fdf:    "-10000000000\x00",
			fdwant: "-0001.00e+10\x00",
		},
		51: {
			fdfmt:  "%Fg\x00",
			fdf:    "0\x00",
			fdwant: "0\x00",
		},
		52: {
			fdfmt:  "%Fg\x00",
			fdf:    "1\x00",
			fdwant: "1\x00",
		},
		53: {
			fdfmt:  "%Fg\x00",
			fdf:    "-1\x00",
			fdwant: "-1\x00",
		},
		54: {
			fdfmt:  "%.0Fg\x00",
			fdf:    "0\x00",
			fdwant: "0\x00",
		},
		55: {
			fdfmt:  "%.0Fg\x00",
			fdf:    "1\x00",
			fdwant: "1\x00",
		},
		56: {
			fdfmt:  "%.0Fg\x00",
			fdf:    "-1\x00",
			fdwant: "-1\x00",
		},
		57: {
			fdfmt:  "%.1Fg\x00",
			fdf:    "100\x00",
			fdwant: "1e+02\x00",
		},
		58: {
			fdfmt:  "%.2Fg\x00",
			fdf:    "100\x00",
			fdwant: "1e+02\x00",
		},
		59: {
			fdfmt:  "%.3Fg\x00",
			fdf:    "100\x00",
			fdwant: "100\x00",
		},
		60: {
			fdfmt:  "%.4Fg\x00",
			fdf:    "100\x00",
			fdwant: "100\x00",
		},
		61: {
			fdfmt:  "%Fg\x00",
			fdf:    "0.001\x00",
			fdwant: "0.001\x00",
		},
		62: {
			fdfmt:  "%Fg\x00",
			fdf:    "0.0001\x00",
			fdwant: "0.0001\x00",
		},
		63: {
			fdfmt:  "%Fg\x00",
			fdf:    "0.00001\x00",
			fdwant: "1e-05\x00",
		},
		64: {
			fdfmt:  "%Fg\x00",
			fdf:    "0.000001\x00",
			fdwant: "1e-06\x00",
		},
		65: {
			fdfmt:  "%.4Fg\x00",
			fdf:    "1.00000000000001\x00",
			fdwant: "1\x00",
		},
		66: {
			fdfmt:  "%.4Fg\x00",
			fdf:    "100000000000001\x00",
			fdwant: "1e+14\x00",
		},
		67: {
			fdfmt:  "%.4Fg\x00",
			fdf:    "12345678\x00",
			fdwant: "1.235e+07\x00",
		},
		68: {
			fdfmt:  "%Fa\x00",
			fdf:    "0\x00",
			fdwant: "0x0p+0\x00",
		},
		69: {
			fdfmt:  "%FA\x00",
			fdf:    "0\x00",
			fdwant: "0X0P+0\x00",
		},
		70: {
			fdfmt:  "%Fa\x00",
			fdf:    "1\x00",
			fdwant: "0x1p+0\x00",
		},
		71: {
			fdfmt:  "%Fa\x00",
			fdf:    "65535\x00",
			fdwant: "0xf.fffp+12\x00",
		},
		72: {
			fdfmt:  "%Fa\x00",
			fdf:    "65536\x00",
			fdwant: "0x1p+16\x00",
		},
		73: {
			fdfmt:  "%F.10a\x00",
			fdf:    "65536\x00",
			fdwant: "0x1.0000000000p+16\x00",
		},
		74: {
			fdfmt:  "%F.1a\x00",
			fdf:    "65535\x00",
			fdwant: "0x1.0p+16\x00",
		},
		75: {
			fdfmt:  "%F.0a\x00",
			fdf:    "65535\x00",
			fdwant: "0x1p+16\x00",
		},
		76: {
			fdfmt:  "%.2Ff\x00",
			fdf:    "0.99609375\x00",
			fdwant: "1.00\x00",
		},
		77: {
			fdfmt:  "%.Ff\x00",
			fdf:    "0.99609375\x00",
			fdwant: "0.99609375\x00",
		},
		78: {
			fdfmt:  "%.Fe\x00",
			fdf:    "0.99609375\x00",
			fdwant: "9.9609375e-01\x00",
		},
		79: {
			fdfmt:  "%.Fg\x00",
			fdf:    "0.99609375\x00",
			fdwant: "0.99609375\x00",
		},
		80: {
			fdfmt:  "%.20Fg\x00",
			fdf:    "1000000\x00",
			fdwant: "1000000\x00",
		},
		81: {
			fdfmt:  "%.Fg\x00",
			fdf:    "1000000\x00",
			fdwant: "1000000\x00",
		},
		82: {
			fdfmt:  "%#.0Ff\x00",
			fdf:    "1\x00",
			fdwant: "1.\x00",
		},
		83: {
			fdfmt:  "%#.0Fe\x00",
			fdf:    "1\x00",
			fdwant: "1.e+00\x00",
		},
		84: {
			fdfmt:  "%#.0Fg\x00",
			fdf:    "1\x00",
			fdwant: "1.\x00",
		},
		85: {
			fdfmt:  "%#.1Ff\x00",
			fdf:    "1\x00",
			fdwant: "1.0\x00",
		},
		86: {
			fdfmt:  "%#.1Fe\x00",
			fdf:    "1\x00",
			fdwant: "1.0e+00\x00",
		},
		87: {
			fdfmt:  "%#.1Fg\x00",
			fdf:    "1\x00",
			fdwant: "1.\x00",
		},
		88: {
			fdfmt:  "%#.4Ff\x00",
			fdf:    "1234\x00",
			fdwant: "1234.0000\x00",
		},
		89: {
			fdfmt:  "%#.4Fe\x00",
			fdf:    "1234\x00",
			fdwant: "1.2340e+03\x00",
		},
		90: {
			fdfmt:  "%#.4Fg\x00",
			fdf:    "1234\x00",
			fdwant: "1234.\x00",
		},
		91: {
			fdfmt:  "%#.8Ff\x00",
			fdf:    "1234\x00",
			fdwant: "1234.00000000\x00",
		},
		92: {
			fdfmt:  "%#.8Fe\x00",
			fdf:    "1234\x00",
			fdwant: "1.23400000e+03\x00",
		},
		93: {
			fdfmt:  "%#.8Fg\x00",
			fdf:    "1234\x00",
			fdwant: "1234.0000\x00",
		},
	}

	X__gmpf_init2(cgtls, cgbp, ppuint32(256))

	aai = 0
	for {
		if !(iqlibc.ppUint32FromInt32(aai) < iqlibc.ppUint32FromInt64(1128)/iqlibc.ppUint32FromInt64(12)) {
			break
		}

		if ppint32(*(*ppint8)(iqunsafe.ppPointer(sndata[aai].fdf))) == ppint32('0') && ppint32(*(*ppint8)(iqunsafe.ppPointer(sndata[aai].fdf + 1))) == ppint32('x') {
			Xmpf_set_str_or_abort(cgtls, cgbp, sndata[aai].fdf, ppint32(16))
		} else {
			Xmpf_set_str_or_abort(cgtls, cgbp, sndata[aai].fdf, ppint32(10))
		}

		/* if mpf->double doesn't truncate, then expect same result */
		aad = X__gmpf_get_d(cgtls, cgbp)
		if X__gmpf_cmp_d(cgtls, cgbp, aad) == 0 {
			Xcheck_plain(cgtls, sndata[aai].fdwant, sndata[aai].fdfmt, iqlibc.ppVaList(cgbp+24, aad))
		}

		Xcheck_one(cgtls, sndata[aai].fdwant, sndata[aai].fdfmt, iqlibc.ppVaList(cgbp+24, cgbp))

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpf_clear(cgtls, cgbp)
}

func Xcheck_limb(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aai ppint32
	var aalimb tnmp_limb_t
	var aas ppuintptr
	var pp_ /* z at bp+0 */ tnmpz_t
	pp_, pp_, pp_ = aai, aalimb, aas

	Xcheck_one(cgtls, "0\x00", "%Md\x00", iqlibc.ppVaList(cgbp+24, iqlibc.ppUint32FromInt32(0)))
	Xcheck_one(cgtls, "1\x00", "%Md\x00", iqlibc.ppVaList(cgbp+24, iqlibc.ppUint32FromInt32(1)))

	/* "i" many 1 bits, tested against mpz_get_str in decimal and hex */
	aalimb = ppuint32(1)
	X__gmpz_init_set_ui(cgtls, cgbp, ppuint32(1))
	aai = ppint32(1)
	for {
		if !(aai <= ppint32(mvGMP_LIMB_BITS)) {
			break
		}

		aas = X__gmpz_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), ppint32(10), cgbp)
		Xcheck_one(cgtls, aas, "%Mu\x00", iqlibc.ppVaList(cgbp+24, aalimb))
		(*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t))(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_free_func})))(cgtls, aas, Xstrlen(cgtls, aas)+ppuint32(1))

		aas = X__gmpz_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), ppint32(16), cgbp)
		Xcheck_one(cgtls, aas, "%Mx\x00", iqlibc.ppVaList(cgbp+24, aalimb))
		(*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t))(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_free_func})))(cgtls, aas, Xstrlen(cgtls, aas)+ppuint32(1))

		aas = X__gmpz_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), -ppint32(16), cgbp)
		Xcheck_one(cgtls, aas, "%MX\x00", iqlibc.ppVaList(cgbp+24, aalimb))
		(*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t))(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_free_func})))(cgtls, aas, Xstrlen(cgtls, aas)+ppuint32(1))

		aalimb = ppuint32(2)*aalimb + ppuint32(1)
		X__gmpz_mul_2exp(cgtls, cgbp, cgbp, ppuint32(1))
		X__gmpz_add_ui(cgtls, cgbp, cgbp, ppuint32(1))

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpz_clear(cgtls, cgbp)
}

func Xcheck_n(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(1328)
	defer cgtls.ppFree(1328)

	var aa__dst, ccv37 tnmp_ptr
	var aa__gmp_i, aa__i, aa__i1, aa__i2, aa__i3, aa__i4, aa__i5, aa__n, aai, ccv35, ccv39 tnmp_size_t
	var aa__gmp_result, ccv1, ccv10, ccv11, ccv13, ccv15, ccv16, ccv18, ccv20, ccv21, ccv23, ccv25, ccv26, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv34, ccv40, ccv41, ccv5, ccv6, ccv8 ppint32
	var aa__gmp_x, aa__gmp_y, aa__nail, aa__nail1, aa__nail2, aa__nail3, aa__nail4, aa__nail5 tnmp_limb_t
	var ccv12, ccv17, ccv2, ccv22, ccv27, ccv7 ppbool
	var pp_ /* a at bp+1248 */ [5]tnmp_limb_t
	var pp_ /* a_want at bp+1268 */ [5]tnmp_limb_t
	var pp_ /* fmt at bp+154 */ [128]ppint8
	var pp_ /* fmt at bp+24 */ [128]ppint8
	var pp_ /* fmt at bp+296 */ [128]ppint8
	var pp_ /* fmt at bp+440 */ [128]ppint8
	var pp_ /* fmt at bp+584 */ [128]ppint8
	var pp_ /* fmt at bp+720 */ [128]ppint8
	var pp_ /* fmt at bp+852 */ [128]ppint8
	var pp_ /* fmt at bp+992 */ [128]ppint8
	var pp_ /* g at bp+1192 */ tnmpz_t
	var pp_ /* g at bp+1204 */ tnmpz_t
	var pp_ /* n at bp+0 */ ppint32
	var pp_ /* n at bp+4 */ ppint32
	var pp_ /* n at bp+8 */ ppint32
	var pp_ /* x at bp+1120 */ [2]tnmpz_t
	var pp_ /* x at bp+1144 */ [2]tnmpq_t
	var pp_ /* x at bp+1216 */ [2]tnmpf_t
	var pp_ /* x at bp+152 */ [2]ppint8
	var pp_ /* x at bp+16 */ [2]tnmp_limb_t
	var pp_ /* x at bp+288 */ [2]ppint32
	var pp_ /* x at bp+424 */ [2]ppint64
	var pp_ /* x at bp+568 */ [2]tnintmax_t
	var pp_ /* x at bp+712 */ [2]tnptrdiff_t
	var pp_ /* x at bp+848 */ [2]ppint16
	var pp_ /* x at bp+984 */ [2]tnsize_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__dst, aa__gmp_i, aa__gmp_result, aa__gmp_x, aa__gmp_y, aa__i, aa__i1, aa__i2, aa__i3, aa__i4, aa__i5, aa__n, aa__nail, aa__nail1, aa__nail2, aa__nail3, aa__nail4, aa__nail5, aai, ccv1, ccv10, ccv11, ccv12, ccv13, ccv15, ccv16, ccv17, ccv18, ccv2, ccv20, ccv21, ccv22, ccv23, ccv25, ccv26, ccv27, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv34, ccv35, ccv37, ccv39, ccv40, ccv41, ccv5, ccv6, ccv7, ccv8

	*(*ppint32)(iqunsafe.ppPointer(cgbp)) = -ppint32(1)
	Xcheck_one(cgtls, "blah\x00", "%nblah\x00", iqlibc.ppVaList(cgbp+1296, cgbp))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(744), "n == 0\x00")
	}

	*(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) = -ppint32(1)
	Xcheck_one(cgtls, "hello \x00", "hello %n\x00", iqlibc.ppVaList(cgbp+1296, cgbp+4))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) == iqlibc.ppInt32FromInt32(6))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(750), "n == 6\x00")
	}

	*(*ppint32)(iqunsafe.ppPointer(cgbp + 8)) = -ppint32(1)
	Xcheck_one(cgtls, "hello  world\x00", "hello %n world\x00", iqlibc.ppVaList(cgbp+1296, cgbp+8))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 8)) == iqlibc.ppInt32FromInt32(6))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(756), "n == 6\x00")
	}

	(*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 16)))[0] = ^iqlibc.ppUint32FromInt32(0)
	(*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 16)))[ppint32(1)] = ^iqlibc.ppUint32FromInt32(0)
	Xsprintf(cgtls, cgbp+24, "%%d%%%sn%%d\x00", iqlibc.ppVaList(cgbp+1296, "M\x00"))
	Xcheck_one(cgtls, "123456\x00", cgbp+24, iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+16, ppint32(456)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 16)))[0] == iqlibc.ppUint32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(775), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 16)))[ppint32(1)] == ^iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(775), "x[1] == (mp_limb_t) ~ (mp_limb_t) 0\x00")
	}
	(*(*[2]ppint8)(iqunsafe.ppPointer(cgbp + 152)))[0] = ppint8(^ppint32(iqlibc.ppInt8FromInt32(0)))
	(*(*[2]ppint8)(iqunsafe.ppPointer(cgbp + 152)))[ppint32(1)] = ppint8(^ppint32(iqlibc.ppInt8FromInt32(0)))
	Xsprintf(cgtls, cgbp+154, "%%d%%%sn%%d\x00", iqlibc.ppVaList(cgbp+1296, "hh\x00"))
	Xcheck_one(cgtls, "123456\x00", cgbp+154, iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+152, ppint32(456)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*(*[2]ppint8)(iqunsafe.ppPointer(cgbp + 152)))[0]) == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(776), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*(*[2]ppint8)(iqunsafe.ppPointer(cgbp + 152)))[ppint32(1)]) == ppint32(ppint8(^ppint32(iqlibc.ppInt8FromInt32(0)))))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(776), "x[1] == (char) ~ (char) 0\x00")
	}
	(*(*[2]ppint32)(iqunsafe.ppPointer(cgbp + 288)))[0] = ^iqlibc.ppInt32FromInt32(0)
	(*(*[2]ppint32)(iqunsafe.ppPointer(cgbp + 288)))[ppint32(1)] = ^iqlibc.ppInt32FromInt32(0)
	Xsprintf(cgtls, cgbp+296, "%%d%%%sn%%d\x00", iqlibc.ppVaList(cgbp+1296, "l\x00"))
	Xcheck_one(cgtls, "123456\x00", cgbp+296, iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+288, ppint32(456)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]ppint32)(iqunsafe.ppPointer(cgbp + 288)))[0] == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(777), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]ppint32)(iqunsafe.ppPointer(cgbp + 288)))[ppint32(1)] == ^iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(777), "x[1] == (long) ~ (long) 0\x00")
	}
	(*(*[2]ppint64)(iqunsafe.ppPointer(cgbp + 424)))[0] = ^iqlibc.ppInt64FromInt32(0)
	(*(*[2]ppint64)(iqunsafe.ppPointer(cgbp + 424)))[ppint32(1)] = ^iqlibc.ppInt64FromInt32(0)
	Xsprintf(cgtls, cgbp+440, "%%d%%%sn%%d\x00", iqlibc.ppVaList(cgbp+1296, "L\x00"))
	Xcheck_one(cgtls, "123456\x00", cgbp+440, iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+424, ppint32(456)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]ppint64)(iqunsafe.ppPointer(cgbp + 424)))[0] == iqlibc.ppInt64FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(779), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]ppint64)(iqunsafe.ppPointer(cgbp + 424)))[ppint32(1)] == ^iqlibc.ppInt64FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(779), "x[1] == (long long) ~ (long long) 0\x00")
	}
	(*(*[2]tnintmax_t)(iqunsafe.ppPointer(cgbp + 568)))[0] = ^iqlibc.ppInt64FromInt32(0)
	(*(*[2]tnintmax_t)(iqunsafe.ppPointer(cgbp + 568)))[ppint32(1)] = ^iqlibc.ppInt64FromInt32(0)
	Xsprintf(cgtls, cgbp+584, "%%d%%%sn%%d\x00", iqlibc.ppVaList(cgbp+1296, "j\x00"))
	Xcheck_one(cgtls, "123456\x00", cgbp+584, iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+568, ppint32(456)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnintmax_t)(iqunsafe.ppPointer(cgbp + 568)))[0] == iqlibc.ppInt64FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(782), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnintmax_t)(iqunsafe.ppPointer(cgbp + 568)))[ppint32(1)] == ^iqlibc.ppInt64FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(782), "x[1] == (intmax_t) ~ (intmax_t) 0\x00")
	}
	(*(*[2]tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 712)))[0] = ^iqlibc.ppInt32FromInt32(0)
	(*(*[2]tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 712)))[ppint32(1)] = ^iqlibc.ppInt32FromInt32(0)
	Xsprintf(cgtls, cgbp+720, "%%d%%%sn%%d\x00", iqlibc.ppVaList(cgbp+1296, "t\x00"))
	Xcheck_one(cgtls, "123456\x00", cgbp+720, iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+712, ppint32(456)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 712)))[0] == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(785), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 712)))[ppint32(1)] == ^iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(785), "x[1] == (ptrdiff_t) ~ (ptrdiff_t) 0\x00")
	}
	(*(*[2]ppint16)(iqunsafe.ppPointer(cgbp + 848)))[0] = ppint16(^ppint32(iqlibc.ppInt16FromInt32(0)))
	(*(*[2]ppint16)(iqunsafe.ppPointer(cgbp + 848)))[ppint32(1)] = ppint16(^ppint32(iqlibc.ppInt16FromInt32(0)))
	Xsprintf(cgtls, cgbp+852, "%%d%%%sn%%d\x00", iqlibc.ppVaList(cgbp+1296, "h\x00"))
	Xcheck_one(cgtls, "123456\x00", cgbp+852, iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+848, ppint32(456)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*(*[2]ppint16)(iqunsafe.ppPointer(cgbp + 848)))[0]) == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(787), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*(*[2]ppint16)(iqunsafe.ppPointer(cgbp + 848)))[ppint32(1)]) == ppint32(ppint16(^ppint32(iqlibc.ppInt16FromInt32(0)))))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(787), "x[1] == (short) ~ (short) 0\x00")
	}
	(*(*[2]tnsize_t)(iqunsafe.ppPointer(cgbp + 984)))[0] = ^iqlibc.ppUint32FromInt32(0)
	(*(*[2]tnsize_t)(iqunsafe.ppPointer(cgbp + 984)))[ppint32(1)] = ^iqlibc.ppUint32FromInt32(0)
	Xsprintf(cgtls, cgbp+992, "%%d%%%sn%%d\x00", iqlibc.ppVaList(cgbp+1296, "z\x00"))
	Xcheck_one(cgtls, "123456\x00", cgbp+992, iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+984, ppint32(456)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnsize_t)(iqunsafe.ppPointer(cgbp + 984)))[0] == iqlibc.ppUint32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(788), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnsize_t)(iqunsafe.ppPointer(cgbp + 984)))[ppint32(1)] == ^iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(788), "x[1] == (size_t) ~ (size_t) 0\x00")
	}

	X__gmpz_init_set_si(cgtls, cgbp+1120, -ppint32(987))
	X__gmpz_init_set_si(cgtls, cgbp+1120+1*12, ppint32(654))
	Xcheck_one(cgtls, "123456\x00", "%d%Zn%d\x00", iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+1120, ppint32(456)))

	if ccv2 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120)).fd_mp_size == 0; !ccv2 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120)).fd_mp_size >= 0 {
			ccv1 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120)).fd_mp_size
		} else {
			ccv1 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv2 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120)).fd_mp_d + ppuintptr(ccv1-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(795), "((x[0])->_mp_size) == 0 || ((x[0])->_mp_d)[((((x[0])->_mp_size)) >= 0 ? (((x[0])->_mp_size)) : -(((x[0])->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120)).fd_mp_size >= 0 {
		ccv3 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120)).fd_mp_size
	} else {
		ccv3 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120)).fd_mp_alloc >= ccv3)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(795), "((x[0])->_mp_alloc) >= ((((x[0])->_mp_size)) >= 0 ? (((x[0])->_mp_size)) : -(((x[0])->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120)).fd_mp_size >= 0 {
				ccv5 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120)).fd_mp_size
			} else {
				ccv5 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120)).fd_mp_size
			}
			if !(aa__i < ppint32(ccv5)) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120)).fd_mp_d + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(795), "__nail == 0\x00")
			}
			goto cg_4
		cg_4:
			;
			aa__i++
		}
	}

	if ccv7 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120+1*12)).fd_mp_size == 0; !ccv7 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120+1*12)).fd_mp_size >= 0 {
			ccv6 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120 + 1*12)).fd_mp_size
		} else {
			ccv6 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120 + 1*12)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv7 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120+1*12)).fd_mp_d + ppuintptr(ccv6-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(796), "((x[1])->_mp_size) == 0 || ((x[1])->_mp_d)[((((x[1])->_mp_size)) >= 0 ? (((x[1])->_mp_size)) : -(((x[1])->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120+1*12)).fd_mp_size >= 0 {
		ccv8 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120 + 1*12)).fd_mp_size
	} else {
		ccv8 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120 + 1*12)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120+1*12)).fd_mp_alloc >= ccv8)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(796), "((x[1])->_mp_alloc) >= ((((x[1])->_mp_size)) >= 0 ? (((x[1])->_mp_size)) : -(((x[1])->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120+1*12)).fd_mp_size >= 0 {
				ccv10 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120 + 1*12)).fd_mp_size
			} else {
				ccv10 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1120 + 1*12)).fd_mp_size
			}
			if !(aa__i1 < ppint32(ccv10)) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1120+1*12)).fd_mp_d + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(796), "__nail == 0\x00")
			}
			goto cg_9
		cg_9:
			;
			aa__i1++
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+1120, ppuint32(3)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(797), "(__builtin_constant_p (3L) && (3L) == 0 ? ((x[0])->_mp_size < 0 ? -1 : (x[0])->_mp_size > 0) : __gmpz_cmp_ui (x[0],3L)) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+1120+1*12, ppuint32(654)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(798), "(__builtin_constant_p (654L) && (654L) == 0 ? ((x[1])->_mp_size < 0 ? -1 : (x[1])->_mp_size > 0) : __gmpz_cmp_ui (x[1],654L)) == 0\x00")
	}
	X__gmpz_clear(cgtls, cgbp+1120)
	X__gmpz_clear(cgtls, cgbp+1120+1*12)

	X__gmpq_init(cgtls, cgbp+1144)
	X__gmpq_init(cgtls, cgbp+1144+1*24)
	X__gmpq_set_ui(cgtls, cgbp+1144, ppuint32(987), ppuint32(654))
	X__gmpq_set_ui(cgtls, cgbp+1144+1*24, ppuint32(4115), ppuint32(226))
	Xcheck_one(cgtls, "123456\x00", "%d%Qn%d\x00", iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+1144, ppint32(456)))

	if ccv12 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144)).fd_mp_size == 0; !ccv12 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144)).fd_mp_size >= 0 {
			ccv11 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144)).fd_mp_size
		} else {
			ccv11 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv12 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144)).fd_mp_d + ppuintptr(ccv11-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(810), "(((&((x[0])->_mp_num)))->_mp_size) == 0 || (((&((x[0])->_mp_num)))->_mp_d)[(((((&((x[0])->_mp_num)))->_mp_size)) >= 0 ? ((((&((x[0])->_mp_num)))->_mp_size)) : -((((&((x[0])->_mp_num)))->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144)).fd_mp_size >= 0 {
		ccv13 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144)).fd_mp_size
	} else {
		ccv13 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144)).fd_mp_alloc >= ccv13)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(810), "(((&((x[0])->_mp_num)))->_mp_alloc) >= (((((&((x[0])->_mp_num)))->_mp_size)) >= 0 ? ((((&((x[0])->_mp_num)))->_mp_size)) : -((((&((x[0])->_mp_num)))->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i2 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144)).fd_mp_size >= 0 {
				ccv15 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144)).fd_mp_size
			} else {
				ccv15 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144)).fd_mp_size
			}
			if !(aa__i2 < ppint32(ccv15)) {
				break
			}
			aa__nail2 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144)).fd_mp_d + ppuintptr(aa__i2)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(810), "__nail == 0\x00")
			}
			goto cg_14
		cg_14:
			;
			aa__i2++
		}
	}
	if ccv17 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+12)).fd_mp_size == 0; !ccv17 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+12)).fd_mp_size >= 0 {
			ccv16 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 12)).fd_mp_size
		} else {
			ccv16 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 12)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv17 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+12)).fd_mp_d + ppuintptr(ccv16-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(810), "(((&((x[0])->_mp_den)))->_mp_size) == 0 || (((&((x[0])->_mp_den)))->_mp_d)[(((((&((x[0])->_mp_den)))->_mp_size)) >= 0 ? ((((&((x[0])->_mp_den)))->_mp_size)) : -((((&((x[0])->_mp_den)))->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+12)).fd_mp_size >= 0 {
		ccv18 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 12)).fd_mp_size
	} else {
		ccv18 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 12)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+12)).fd_mp_alloc >= ccv18)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(810), "(((&((x[0])->_mp_den)))->_mp_alloc) >= (((((&((x[0])->_mp_den)))->_mp_size)) >= 0 ? ((((&((x[0])->_mp_den)))->_mp_size)) : -((((&((x[0])->_mp_den)))->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i3 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+12)).fd_mp_size >= 0 {
				ccv20 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 12)).fd_mp_size
			} else {
				ccv20 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 12)).fd_mp_size
			}
			if !(aa__i3 < ppint32(ccv20)) {
				break
			}
			aa__nail3 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+12)).fd_mp_d + ppuintptr(aa__i3)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail3 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(810), "__nail == 0\x00")
			}
			goto cg_19
		cg_19:
			;
			aa__i3++
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+12)).fd_mp_size >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(810), "(((&((x[0])->_mp_den)))->_mp_size) >= 1\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144)).fd_mp_size == 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+12)).fd_mp_size == ppint32(1) && *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 12)).fd_mp_d)) == ppuint32(1))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(810), "(((&((x[0])->_mp_den)))->_mp_size) == 1 && (((&((x[0])->_mp_den)))->_mp_d)[0] == 1\x00")
		}
	} else {
		X__gmpz_init(cgtls, cgbp+1192)
		X__gmpz_gcd(cgtls, cgbp+1192, cgbp+1144, cgbp+1144+12)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+1192, ppuint32(1)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(810), "(__builtin_constant_p (1) && (1) == 0 ? ((g)->_mp_size < 0 ? -1 : (g)->_mp_size > 0) : __gmpz_cmp_ui (g,1)) == 0\x00")
		}
		X__gmpz_clear(cgtls, cgbp+1192)
	}

	if ccv22 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24)).fd_mp_size == 0; !ccv22 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24)).fd_mp_size >= 0 {
			ccv21 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24)).fd_mp_size
		} else {
			ccv21 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv22 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24)).fd_mp_d + ppuintptr(ccv21-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(811), "(((&((x[1])->_mp_num)))->_mp_size) == 0 || (((&((x[1])->_mp_num)))->_mp_d)[(((((&((x[1])->_mp_num)))->_mp_size)) >= 0 ? ((((&((x[1])->_mp_num)))->_mp_size)) : -((((&((x[1])->_mp_num)))->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24)).fd_mp_size >= 0 {
		ccv23 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24)).fd_mp_size
	} else {
		ccv23 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24)).fd_mp_alloc >= ccv23)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(811), "(((&((x[1])->_mp_num)))->_mp_alloc) >= (((((&((x[1])->_mp_num)))->_mp_size)) >= 0 ? ((((&((x[1])->_mp_num)))->_mp_size)) : -((((&((x[1])->_mp_num)))->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i4 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24)).fd_mp_size >= 0 {
				ccv25 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24)).fd_mp_size
			} else {
				ccv25 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24)).fd_mp_size
			}
			if !(aa__i4 < ppint32(ccv25)) {
				break
			}
			aa__nail4 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24)).fd_mp_d + ppuintptr(aa__i4)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail4 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(811), "__nail == 0\x00")
			}
			goto cg_24
		cg_24:
			;
			aa__i4++
		}
	}
	if ccv27 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24+12)).fd_mp_size == 0; !ccv27 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24+12)).fd_mp_size >= 0 {
			ccv26 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24 + 12)).fd_mp_size
		} else {
			ccv26 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24 + 12)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv27 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24+12)).fd_mp_d + ppuintptr(ccv26-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(811), "(((&((x[1])->_mp_den)))->_mp_size) == 0 || (((&((x[1])->_mp_den)))->_mp_d)[(((((&((x[1])->_mp_den)))->_mp_size)) >= 0 ? ((((&((x[1])->_mp_den)))->_mp_size)) : -((((&((x[1])->_mp_den)))->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24+12)).fd_mp_size >= 0 {
		ccv28 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24 + 12)).fd_mp_size
	} else {
		ccv28 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24 + 12)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24+12)).fd_mp_alloc >= ccv28)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(811), "(((&((x[1])->_mp_den)))->_mp_alloc) >= (((((&((x[1])->_mp_den)))->_mp_size)) >= 0 ? ((((&((x[1])->_mp_den)))->_mp_size)) : -((((&((x[1])->_mp_den)))->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i5 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24+12)).fd_mp_size >= 0 {
				ccv30 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24 + 12)).fd_mp_size
			} else {
				ccv30 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24 + 12)).fd_mp_size
			}
			if !(aa__i5 < ppint32(ccv30)) {
				break
			}
			aa__nail5 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24+12)).fd_mp_d + ppuintptr(aa__i5)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail5 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(811), "__nail == 0\x00")
			}
			goto cg_29
		cg_29:
			;
			aa__i5++
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24+12)).fd_mp_size >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(811), "(((&((x[1])->_mp_den)))->_mp_size) >= 1\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24)).fd_mp_size == 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1144+1*24+12)).fd_mp_size == ppint32(1) && *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1144 + 1*24 + 12)).fd_mp_d)) == ppuint32(1))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(811), "(((&((x[1])->_mp_den)))->_mp_size) == 1 && (((&((x[1])->_mp_den)))->_mp_d)[0] == 1\x00")
		}
	} else {
		X__gmpz_init(cgtls, cgbp+1204)
		X__gmpz_gcd(cgtls, cgbp+1204, cgbp+1144+1*24, cgbp+1144+1*24+12)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+1204, ppuint32(1)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(811), "(__builtin_constant_p (1) && (1) == 0 ? ((g)->_mp_size < 0 ? -1 : (g)->_mp_size > 0) : __gmpz_cmp_ui (g,1)) == 0\x00")
		}
		X__gmpz_clear(cgtls, cgbp+1204)
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpq_cmp_ui(cgtls, cgbp+1144, ppuint32(3), ppuint32(1)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(812), "(__builtin_constant_p (3L) && (3L) == 0 ? ((x[0])->_mp_num._mp_size < 0 ? -1 : (x[0])->_mp_num._mp_size > 0) : __builtin_constant_p ((3L) == (1L)) && (3L) == (1L) ? __gmpz_cmp ((&((x[0])->_mp_num)), (&((x[0])->_mp_den))) : __gmpq_cmp_ui (x[0],3L,1L)) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpq_cmp_ui(cgtls, cgbp+1144+1*24, ppuint32(4115), ppuint32(226)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(813), "(__builtin_constant_p (4115L) && (4115L) == 0 ? ((x[1])->_mp_num._mp_size < 0 ? -1 : (x[1])->_mp_num._mp_size > 0) : __builtin_constant_p ((4115L) == (226L)) && (4115L) == (226L) ? __gmpz_cmp ((&((x[1])->_mp_num)), (&((x[1])->_mp_den))) : __gmpq_cmp_ui (x[1],4115L,226L)) == 0\x00")
	}
	X__gmpq_clear(cgtls, cgbp+1144)
	X__gmpq_clear(cgtls, cgbp+1144+1*24)

	X__gmpf_init(cgtls, cgbp+1216)
	X__gmpf_init(cgtls, cgbp+1216+1*16)
	X__gmpf_set_ui(cgtls, cgbp+1216, ppuint32(987))
	X__gmpf_set_ui(cgtls, cgbp+1216+1*16, ppuint32(654))
	Xcheck_one(cgtls, "123456\x00", "%d%Fn%d\x00", iqlibc.ppVaList(cgbp+1296, ppint32(123), cgbp+1216, ppint32(456)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216)).fd_mp_prec) >= ppint32((iqlibc.ppInt32FromInt32(53)+iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-iqlibc.ppInt32FromInt32(1))/(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(825), "((x[0])->_mp_prec) >= ((mp_size_t) ((((53) > (53) ? (53) : (53)) + 2 * (32 - 0) - 1) / (32 - 0)))\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216)).fd_mp_size >= 0 {
		ccv31 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1216)).fd_mp_size
	} else {
		ccv31 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1216)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv31 <= (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216)).fd_mp_prec+iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(825), "((((x[0])->_mp_size)) >= 0 ? (((x[0])->_mp_size)) : -(((x[0])->_mp_size))) <= ((x[0])->_mp_prec)+1\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216)).fd_mp_size == 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216)).fd_mp_exp == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(825), "((x[0])->_mp_exp) == 0\x00")
		}
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216)).fd_mp_size != 0 {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216)).fd_mp_size >= 0 {
			ccv32 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1216)).fd_mp_size
		} else {
			ccv32 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1216)).fd_mp_size
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216)).fd_mp_d + ppuintptr(ccv32-ppint32(1))*4)) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(825), "((x[0])->_mp_d)[((((x[0])->_mp_size)) >= 0 ? (((x[0])->_mp_size)) : -(((x[0])->_mp_size))) - 1] != 0\x00")
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216+1*16)).fd_mp_prec) >= ppint32((iqlibc.ppInt32FromInt32(53)+iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-iqlibc.ppInt32FromInt32(1))/(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(826), "((x[1])->_mp_prec) >= ((mp_size_t) ((((53) > (53) ? (53) : (53)) + 2 * (32 - 0) - 1) / (32 - 0)))\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216+1*16)).fd_mp_size >= 0 {
		ccv33 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1216 + 1*16)).fd_mp_size
	} else {
		ccv33 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1216 + 1*16)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv33 <= (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216+1*16)).fd_mp_prec+iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(826), "((((x[1])->_mp_size)) >= 0 ? (((x[1])->_mp_size)) : -(((x[1])->_mp_size))) <= ((x[1])->_mp_prec)+1\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216+1*16)).fd_mp_size == 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216+1*16)).fd_mp_exp == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(826), "((x[1])->_mp_exp) == 0\x00")
		}
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216+1*16)).fd_mp_size != 0 {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216+1*16)).fd_mp_size >= 0 {
			ccv34 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1216 + 1*16)).fd_mp_size
		} else {
			ccv34 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1216 + 1*16)).fd_mp_size
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1216+1*16)).fd_mp_d + ppuintptr(ccv34-ppint32(1))*4)) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(826), "((x[1])->_mp_d)[((((x[1])->_mp_size)) >= 0 ? (((x[1])->_mp_size)) : -(((x[1])->_mp_size))) - 1] != 0\x00")
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpf_cmp_ui(cgtls, cgbp+1216, ppuint32(3)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(827), "__gmpf_cmp_ui (x[0], 3L) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpf_cmp_ui(cgtls, cgbp+1216+1*16, ppuint32(654)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(828), "__gmpf_cmp_ui (x[1], 654L) == 0\x00")
	}
	X__gmpf_clear(cgtls, cgbp+1216)
	X__gmpf_clear(cgtls, cgbp+1216+1*16)

	(*(*[5]tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 1248)))[0] = ppuint32(123)
	Xcheck_one(cgtls, "blah\x00", "bl%Nnah\x00", iqlibc.ppVaList(cgbp+1296, cgbp+1248, iqlibc.ppInt32FromInt32(0)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[5]tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 1248)))[0] == iqlibc.ppUint32FromInt32(123))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(840), "a[0] == 123\x00")
	}

	if iqlibc.ppUint32FromInt64(20)/iqlibc.ppUint32FromInt64(4) != ppuint32(0) {
		aa__dst = cgbp + 1268
		aa__n = iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt64(20) / iqlibc.ppUint32FromInt64(4))
		for {
			ccv37 = aa__dst
			aa__dst += 4
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv37)) = iqlibc.ppUint32FromInt32(0)
			goto cg_36
		cg_36:
			;
			aa__n--
			ccv35 = aa__n
			if !(ccv35 != 0) {
				break
			}
		}
	}
	aai = ppint32(1)
	for {
		if !(iqlibc.ppUint32FromInt32(aai) < ppuint32(iqlibc.ppUint32FromInt64(20)/iqlibc.ppUint32FromInt64(4))) {
			break
		}

		Xcheck_one(cgtls, "blah\x00", "bl%Nnah\x00", iqlibc.ppVaList(cgbp+1296, cgbp+1248, aai))
		(*(*[5]tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 1268)))[0] = ppuint32(2)
		aa__gmp_result = 0
		aa__gmp_i = aai
		for {
			aa__gmp_i--
			ccv39 = aa__gmp_i
			if !(ccv39 >= iqlibc.ppInt32FromInt32(0)) {
				break
			}
			aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 1248 + ppuintptr(aa__gmp_i)*4))
			aa__gmp_y = *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 1268 + ppuintptr(aa__gmp_i)*4))
			if aa__gmp_x != aa__gmp_y {
				if aa__gmp_x > aa__gmp_y {
					ccv40 = ppint32(1)
				} else {
					ccv40 = -ppint32(1)
				}
				aa__gmp_result = ccv40
				break
			}
		}
		ccv41 = aa__gmp_result
		goto cg_42
	cg_42:
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv41 == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(847), "__gmpn_cmp (a, a_want, i) == 0\x00")
		}

		goto cg_38
	cg_38:
		;
		aai++
	}

}

func Xcheck_misc(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaxs ppuintptr
	var pp_ /* f at bp+12 */ tnmpf_t
	var pp_ /* z at bp+0 */ tnmpz_t
	pp_ = aaxs

	X__gmpz_init(cgtls, cgbp)
	X__gmpf_init2(cgtls, cgbp+12, ppuint32(128))

	Xcheck_one(cgtls, "!\x00", "%c\x00", iqlibc.ppVaList(cgbp+40, ppint32('!')))

	Xcheck_one(cgtls, "hello world\x00", "hello %s\x00", iqlibc.ppVaList(cgbp+40, "world\x00"))
	Xcheck_one(cgtls, "hello:\x00", "%s:\x00", iqlibc.ppVaList(cgbp+40, "hello\x00"))
	X__gmpz_set_ui(cgtls, cgbp, ppuint32(0))
	Xcheck_one(cgtls, "hello0\x00", "%s%Zd\x00", iqlibc.ppVaList(cgbp+40, "hello\x00", cgbp, cgbp))

	var snxs [801]ppint8
	Xmemset(cgtls, ppuintptr(iqunsafe.ppPointer(&snxs)), ppint32('x'), iqlibc.ppUint32FromInt64(801)-iqlibc.ppUint32FromInt32(1))
	Xcheck_one(cgtls, ppuintptr(iqunsafe.ppPointer(&snxs)), "%s\x00", iqlibc.ppVaList(cgbp+40, ppuintptr(iqunsafe.ppPointer(&snxs))))

	aaxs = (*(*func(*iqlibc.ppTLS, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_allocate_func})))(cgtls, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMAX_OUTPUT)*iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromInt32(12)))
	Xmemset(cgtls, aaxs, ppint32('%'), iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMAX_OUTPUT)*iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromInt32(14)))
	*(*ppint8)(iqunsafe.ppPointer(aaxs + ppuintptr(iqlibc.ppInt32FromInt32(mvMAX_OUTPUT)*iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromInt32(13)))) = ppint8('\000')
	*(*ppint8)(iqunsafe.ppPointer(aaxs + ppuintptr(iqlibc.ppInt32FromInt32(mvMAX_OUTPUT)*iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromInt32(14)))) = ppint8('x')
	Xcheck_one(cgtls, aaxs+ppuintptr(mvMAX_OUTPUT)-ppuintptr(7), aaxs, iqlibc.ppVaList(cgbp+40, iqlibc.ppUintptrFromInt32(0)))
	(*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t))(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_free_func})))(cgtls, aaxs, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMAX_OUTPUT)*iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromInt32(12)))

	X__gmpz_set_ui(cgtls, cgbp, ppuint32(12345))
	Xcheck_one(cgtls, "     12345\x00", "%*Zd\x00", iqlibc.ppVaList(cgbp+40, ppint32(10), cgbp))
	Xcheck_one(cgtls, "0000012345\x00", "%0*Zd\x00", iqlibc.ppVaList(cgbp+40, ppint32(10), cgbp))
	Xcheck_one(cgtls, "12345     \x00", "%*Zd\x00", iqlibc.ppVaList(cgbp+40, -ppint32(10), cgbp))
	Xcheck_one(cgtls, "12345 and 678\x00", "%Zd and %d\x00", iqlibc.ppVaList(cgbp+40, cgbp, ppint32(678)))
	Xcheck_one(cgtls, "12345,1,12345,2,12345\x00", "%Zd,%d,%Zd,%d,%Zd\x00", iqlibc.ppVaList(cgbp+40, cgbp, ppint32(1), cgbp, ppint32(2), cgbp))

	/* from the glibc info docs */
	X__gmpz_set_si(cgtls, cgbp, 0)
	Xcheck_one(cgtls, "|    0|0    |   +0|+0   |    0|00000|     |   00|0|\x00", "|%5Zd|%-5Zd|%+5Zd|%+-5Zd|% 5Zd|%05Zd|%5.0Zd|%5.2Zd|%Zd|\x00", iqlibc.ppVaList(cgbp+40, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp))
	X__gmpz_set_si(cgtls, cgbp, ppint32(1))
	Xcheck_one(cgtls, "|    1|1    |   +1|+1   |    1|00001|    1|   01|1|\x00", "|%5Zd|%-5Zd|%+5Zd|%+-5Zd|% 5Zd|%05Zd|%5.0Zd|%5.2Zd|%Zd|\x00", iqlibc.ppVaList(cgbp+40, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp))
	X__gmpz_set_si(cgtls, cgbp, -ppint32(1))
	Xcheck_one(cgtls, "|   -1|-1   |   -1|-1   |   -1|-0001|   -1|  -01|-1|\x00", "|%5Zd|%-5Zd|%+5Zd|%+-5Zd|% 5Zd|%05Zd|%5.0Zd|%5.2Zd|%Zd|\x00", iqlibc.ppVaList(cgbp+40, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp))
	X__gmpz_set_si(cgtls, cgbp, ppint32(100000))
	Xcheck_one(cgtls, "|100000|100000|+100000|+100000| 100000|100000|100000|100000|100000|\x00", "|%5Zd|%-5Zd|%+5Zd|%+-5Zd|% 5Zd|%05Zd|%5.0Zd|%5.2Zd|%Zd|\x00", iqlibc.ppVaList(cgbp+40, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp))
	X__gmpz_set_si(cgtls, cgbp, 0)
	Xcheck_one(cgtls, "|    0|    0|    0|    0|    0|    0|  00000000|\x00", "|%5Zo|%5Zx|%5ZX|%#5Zo|%#5Zx|%#5ZX|%#10.8Zx|\x00", iqlibc.ppVaList(cgbp+40, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp))
	X__gmpz_set_si(cgtls, cgbp, ppint32(1))
	Xcheck_one(cgtls, "|    1|    1|    1|   01|  0x1|  0X1|0x00000001|\x00", "|%5Zo|%5Zx|%5ZX|%#5Zo|%#5Zx|%#5ZX|%#10.8Zx|\x00", iqlibc.ppVaList(cgbp+40, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp))
	X__gmpz_set_si(cgtls, cgbp, ppint32(100000))
	Xcheck_one(cgtls, "|303240|186a0|186A0|0303240|0x186a0|0X186A0|0x000186a0|\x00", "|%5Zo|%5Zx|%5ZX|%#5Zo|%#5Zx|%#5ZX|%#10.8Zx|\x00", iqlibc.ppVaList(cgbp+40, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp, cgbp))

	/* %zd for size_t won't be available on old systems, and running something
	   to see if it works might be bad, so only try it on glibc, and only on a
	   new enough version (glibc 2.0 doesn't have %zd) */

	X__gmpz_clear(cgtls, cgbp)
	X__gmpf_clear(cgtls, cgbp+12)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	if aaargc > ppint32(1) && Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*4)), "-s\x00") == 0 {
		Xoption_check_printf = ppint32(1)
	}

	Xtests_start(cgtls)
	Xcheck_vfprintf_fp = Xfopen(cgtls, "t-printf.tmp\x00", "w+\x00")
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xcheck_vfprintf_fp != iqlibc.ppUintptrFromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(942), "check_vfprintf_fp != ((void*)0)\x00")
	}

	Xcheck_z(cgtls)
	Xcheck_q(cgtls)
	Xcheck_f(cgtls)
	Xcheck_limb(cgtls)
	Xcheck_n(cgtls)
	Xcheck_misc(cgtls)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xfclose(cgtls, Xcheck_vfprintf_fp) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-printf.c\x00", ppint32(951), "fclose (check_vfprintf_fp) == 0\x00")
	}
	Xunlink(cgtls, "t-printf.tmp\x00")
	Xtests_end(cgtls)
	Xexit(cgtls, 0)
	return cgr
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

var ___gmp_allocate_func ppuintptr

func ___gmp_allocate_strdup(*iqlibc.ppTLS, ppuintptr) ppuintptr

func ___gmp_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

var ___gmp_free_func ppuintptr

func ___gmp_vasprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func ___gmp_vfprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func ___gmp_vsnprintf(*iqlibc.ppTLS, ppuintptr, ppuint32, ppuintptr, ppuintptr) ppint32

func ___gmp_vsprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func ___gmpf_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_cmp_d(*iqlibc.ppTLS, ppuintptr, ppfloat64) ppint32

func ___gmpf_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint32) ppint32

func ___gmpf_get_d(*iqlibc.ppTLS, ppuintptr) ppfloat64

func ___gmpf_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_init2(*iqlibc.ppTLS, ppuintptr, ppuint32)

func ___gmpf_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32)

func ___gmpq_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint32, ppuint32) ppint32

func ___gmpq_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32, ppuint32)

func ___gmpz_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32)

func ___gmpz_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint32) ppint32

func ___gmpz_fits_slong_p(*iqlibc.ppTLS, ppuintptr) ppint32

func ___gmpz_gcd(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_get_si(*iqlibc.ppTLS, ppuintptr) ppint32

func ___gmpz_get_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr) ppuintptr

func ___gmpz_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_init_set_si(*iqlibc.ppTLS, ppuintptr, ppint32)

func ___gmpz_init_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32)

func ___gmpz_mul_2exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32)

func ___gmpz_realloc(*iqlibc.ppTLS, ppuintptr, ppint32) ppuintptr

func ___gmpz_set_si(*iqlibc.ppTLS, ppuintptr, ppint32)

func ___gmpz_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32)

func _abort(*iqlibc.ppTLS)

func _exit(*iqlibc.ppTLS, ppint32)

func _fclose(*iqlibc.ppTLS, ppuintptr) ppint32

func _fflush(*iqlibc.ppTLS, ppuintptr) ppint32

func _fopen(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _fread(*iqlibc.ppTLS, ppuintptr, ppuint32, ppuint32, ppuintptr) ppuint32

func _ftell(*iqlibc.ppTLS, ppuintptr) ppint32

func _memcmp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32) ppint32

func _memset(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32) ppuintptr

func _mpf_set_str_or_abort(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32)

func _mpq_set_str_or_abort(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32)

func _mpz_set_str_or_abort(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32)

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _refmpn_zero(*iqlibc.ppTLS, ppuintptr, ppint32)

func _rewind(*iqlibc.ppTLS, ppuintptr)

func _sprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _strchr(*iqlibc.ppTLS, ppuintptr, ppint32) ppuintptr

func _strcmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _strlen(*iqlibc.ppTLS, ppuintptr) ppuint32

func _tests_end(*iqlibc.ppTLS)

func _tests_start(*iqlibc.ppTLS)

func _unlink(*iqlibc.ppTLS, ppuintptr) ppint32

func _vsprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
