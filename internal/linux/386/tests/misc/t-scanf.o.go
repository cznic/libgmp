// Code generated for linux/386 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -ignore-unsupported-alignment -DHAVE_CONFIG_H -I. -I../.. -I../.. -I../../tests -DNDEBUG -mlong-double-64 -c -o t-scanf.o.go t-scanf.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvASSERT_FILE = "__FILE__"
const mvASSERT_LINE = "__LINE__"
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBINV_NEWTON_THRESHOLD = 300
const mvBMOD_1_TO_MOD_1_THRESHOLD = 10
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDC_BDIV_Q_THRESHOLD = 180
const mvDC_DIVAPPR_Q_THRESHOLD = 200
const mvDELAYTIMER_MAX = 0x7fffffff
const mvDIVEXACT_1_THRESHOLD = 0
const mvDIVEXACT_BY3_METHOD = 0
const mvDIVEXACT_JEB_THRESHOLD = 25
const mvDOPRNT_CONV_FIXED = 1
const mvDOPRNT_CONV_GENERAL = 3
const mvDOPRNT_CONV_SCIENTIFIC = 2
const mvDOPRNT_JUSTIFY_INTERNAL = 3
const mvDOPRNT_JUSTIFY_LEFT = 1
const mvDOPRNT_JUSTIFY_NONE = 0
const mvDOPRNT_JUSTIFY_RIGHT = 2
const mvDOPRNT_SHOWBASE_NO = 2
const mvDOPRNT_SHOWBASE_NONZERO = 3
const mvDOPRNT_SHOWBASE_YES = 1
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFAC_DSC_THRESHOLD = 400
const mvFAC_ODD_THRESHOLD = 35
const mvFFT_FIRST_K = 4
const mvFIB_TABLE_LIMIT = 47
const mvFIB_TABLE_LUCNUM_LIMIT = 46
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFOPEN_MAX = 1000
const mvF_LOCK = 1
const mvF_OK = 0
const mvF_TEST = 3
const mvF_TLOCK = 2
const mvF_ULOCK = 0
const mvGCDEXT_DC_THRESHOLD = 600
const mvGCD_DC_THRESHOLD = 1000
const mvGET_STR_DC_THRESHOLD = 18
const mvGET_STR_PRECOMPUTE_THRESHOLD = 35
const mvGMP_LIMB_BITS = 32
const mvGMP_LIMB_BYTES = "SIZEOF_MP_LIMB_T"
const mvGMP_MPARAM_H_SUGGEST = "./mpn/generic/gmp-mparam.h"
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_ALARM = 1
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_CONST = 1
const mvHAVE_ATTRIBUTE_MALLOC = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_ATTRIBUTE_NORETURN = 1
const mvHAVE_CLOCK = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_CONFIG_H = 1
const mvHAVE_DECL_FGETC = 1
const mvHAVE_DECL_FSCANF = 1
const mvHAVE_DECL_OPTARG = 1
const mvHAVE_DECL_SYS_ERRLIST = 0
const mvHAVE_DECL_SYS_NERR = 0
const mvHAVE_DECL_UNGETC = 1
const mvHAVE_DECL_VFPRINTF = 1
const mvHAVE_DLFCN_H = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FCNTL_H = 1
const mvHAVE_FLOAT_H = 1
const mvHAVE_GETPAGESIZE = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_HIDDEN_ALIAS = 1
const mvHAVE_HOST_CPU_FAMILY_x86_64 = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTPTR_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LANGINFO_H = 1
const mvHAVE_LIMB_LITTLE_ENDIAN = 1
const mvHAVE_LOCALECONV = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_DOUBLE = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_MEMORY_H = 1
const mvHAVE_MEMSET = 1
const mvHAVE_MMAP = 1
const mvHAVE_MPROTECT = 1
const mvHAVE_NL_LANGINFO = 1
const mvHAVE_NL_TYPES_H = 1
const mvHAVE_POPEN = 1
const mvHAVE_PTRDIFF_T = 1
const mvHAVE_QUAD_T = 1
const mvHAVE_RAISE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGALTSTACK = 1
const mvHAVE_SIGSTACK = 1
const mvHAVE_SPEED_CYCLECOUNTER = 2
const mvHAVE_STACK_T = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDLIB_H = 1
const mvHAVE_STRCHR = 1
const mvHAVE_STRERROR = 1
const mvHAVE_STRINGS_H = 1
const mvHAVE_STRING_H = 1
const mvHAVE_STRNLEN = 1
const mvHAVE_STRTOL = 1
const mvHAVE_STRTOUL = 1
const mvHAVE_SYSCONF = 1
const mvHAVE_SYS_MMAN_H = 1
const mvHAVE_SYS_PARAM_H = 1
const mvHAVE_SYS_RESOURCE_H = 1
const mvHAVE_SYS_STAT_H = 1
const mvHAVE_SYS_SYSINFO_H = 1
const mvHAVE_SYS_TIMES_H = 1
const mvHAVE_SYS_TIME_H = 1
const mvHAVE_SYS_TYPES_H = 1
const mvHAVE_TIMES = 1
const mvHAVE_UINT_LEAST32_T = 1
const mvHAVE_UNISTD_H = 1
const mvHAVE_VSNPRINTF = 1
const mvHGCD_APPR_THRESHOLD = 400
const mvHGCD_REDUCE_THRESHOLD = 1000
const mvHGCD_THRESHOLD = 400
const mvHOST_NAME_MAX = 255
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT32_MAX"
const mvINTPTR_MIN = "INT32_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_HIGHBIT = "INT_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvINV_APPR_THRESHOLD = "INV_NEWTON_THRESHOLD"
const mvINV_NEWTON_THRESHOLD = 200
const mvIOV_MAX = 1024
const mvLIMBS_PER_ULONG = 1
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_HIGHBIT = "LONG_MIN"
const mvLONG_MAX = "__LONG_MAX"
const mvLSYM_PREFIX = ".L"
const mvLT_OBJDIR = ".libs/"
const mvL_INCR = 1
const mvL_SET = 0
const mvL_XTND = 2
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMATRIX22_STRASSEN_THRESHOLD = 30
const mvMB_LEN_MAX = 4
const mvMOD_BKNP1_ONLY3 = 0
const mvMPN_FFT_TABLE_SIZE = 16
const mvMPN_TOOM22_MUL_MINSIZE = 6
const mvMPN_TOOM2_SQR_MINSIZE = 4
const mvMPN_TOOM32_MUL_MINSIZE = 10
const mvMPN_TOOM33_MUL_MINSIZE = 17
const mvMPN_TOOM3_SQR_MINSIZE = 17
const mvMPN_TOOM42_MULMID_MINSIZE = 4
const mvMPN_TOOM42_MUL_MINSIZE = 10
const mvMPN_TOOM43_MUL_MINSIZE = 25
const mvMPN_TOOM44_MUL_MINSIZE = 30
const mvMPN_TOOM4_SQR_MINSIZE = 30
const mvMPN_TOOM53_MUL_MINSIZE = 17
const mvMPN_TOOM54_MUL_MINSIZE = 31
const mvMPN_TOOM63_MUL_MINSIZE = 49
const mvMPN_TOOM6H_MUL_MINSIZE = 46
const mvMPN_TOOM6_SQR_MINSIZE = 46
const mvMPN_TOOM8H_MUL_MINSIZE = 86
const mvMPN_TOOM8_SQR_MINSIZE = 86
const mvMP_BASES_BIG_BASE_CTZ_10 = 9
const mvMP_BASES_CHARS_PER_LIMB_10 = 9
const mvMP_BASES_NORMALIZATION_STEPS_10 = 2
const mvMP_EXP_T_MAX = "MP_SIZE_T_MAX"
const mvMP_EXP_T_MIN = "MP_SIZE_T_MIN"
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMULLO_BASECASE_THRESHOLD = 0
const mvMULLO_BASECASE_THRESHOLD_LIMIT = "MULLO_BASECASE_THRESHOLD"
const mvMULMID_TOOM42_THRESHOLD = "MUL_TOOM22_THRESHOLD"
const mvMULMOD_BNM1_THRESHOLD = 16
const mvMUL_TOOM22_THRESHOLD = 30
const mvMUL_TOOM22_THRESHOLD_LIMIT = "MUL_TOOM22_THRESHOLD"
const mvMUL_TOOM32_TO_TOOM43_THRESHOLD = 100
const mvMUL_TOOM32_TO_TOOM53_THRESHOLD = 110
const mvMUL_TOOM33_THRESHOLD = 100
const mvMUL_TOOM33_THRESHOLD_LIMIT = "MUL_TOOM33_THRESHOLD"
const mvMUL_TOOM42_TO_TOOM53_THRESHOLD = 100
const mvMUL_TOOM42_TO_TOOM63_THRESHOLD = 110
const mvMUL_TOOM43_TO_TOOM54_THRESHOLD = 150
const mvMUL_TOOM44_THRESHOLD = 300
const mvMUL_TOOM6H_THRESHOLD = 350
const mvMUL_TOOM8H_THRESHOLD = 450
const mvMUPI_DIV_QR_THRESHOLD = 200
const mvMU_BDIV_QR_THRESHOLD = 2000
const mvMU_BDIV_Q_THRESHOLD = 2000
const mvMU_DIVAPPR_Q_THRESHOLD = 2000
const mvMU_DIV_QR_THRESHOLD = 2000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvODD_CENTRAL_BINOMIAL_OFFSET = 8
const mvODD_CENTRAL_BINOMIAL_TABLE_LIMIT = 18
const mvODD_DOUBLEFACTORIAL_TABLE_LIMIT = 19
const mvODD_FACTORIAL_EXTTABLE_LIMIT = 34
const mvODD_FACTORIAL_TABLE_LIMIT = 16
const mvPACKAGE = "gmp"
const mvPACKAGE_BUGREPORT = "gmp-bugs@gmplib.org (see https://gmplib.org/manual/Reporting-Bugs.html)"
const mvPACKAGE_NAME = "GNU MP"
const mvPACKAGE_STRING = "GNU MP 6.3.0"
const mvPACKAGE_TARNAME = "gmp"
const mvPACKAGE_URL = "http://www.gnu.org/software/gmp/"
const mvPACKAGE_VERSION = "6.3.0"
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPOSIX_CLOSE_RESTART = 0
const mvPP = 0xC0CFD797
const mvPP_FIRST_OMITTED = 31
const mvPP_INVERTED = 0x53E5645C
const mvPREINV_MOD_1_TO_MOD_1_THRESHOLD = 10
const mvPRIMESIEVE_HIGHEST_PRIME = 5351
const mvPRIMESIEVE_NUMBEROF_TABLE = 56
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT32_MAX"
const mvPTRDIFF_MIN = "INT32_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvREDC_1_TO_REDC_N_THRESHOLD = 100
const mvRETSIGTYPE = "void"
const mvRE_DUP_MAX = 255
const mvR_OK = 4
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEEK_DATA = 3
const mvSEEK_HOLE = 4
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSET_STR_DC_THRESHOLD = 750
const mvSET_STR_PRECOMPUTE_THRESHOLD = 2000
const mvSHRT_HIGHBIT = "SHRT_MIN"
const mvSHRT_MAX = 0x7fff
const mvSIEVESIZE = 512
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZEOF_MP_LIMB_T = 4
const mvSIZEOF_UNSIGNED = 4
const mvSIZEOF_UNSIGNED_LONG = 4
const mvSIZEOF_UNSIGNED_SHORT = 2
const mvSIZEOF_VOID_P = 4
const mvSIZE_MAX = "UINT32_MAX"
const mvSQRLO_BASECASE_THRESHOLD = 0
const mvSQRLO_BASECASE_THRESHOLD_LIMIT = "SQRLO_BASECASE_THRESHOLD"
const mvSQRLO_DC_THRESHOLD = "MULLO_DC_THRESHOLD"
const mvSQRLO_DC_THRESHOLD_LIMIT = "SQRLO_DC_THRESHOLD"
const mvSQRLO_SQR_THRESHOLD = "MULLO_MUL_N_THRESHOLD"
const mvSQRMOD_BNM1_THRESHOLD = 16
const mvSQR_BASECASE_THRESHOLD = 0
const mvSQR_TOOM2_THRESHOLD = 50
const mvSQR_TOOM3_THRESHOLD = 120
const mvSQR_TOOM3_THRESHOLD_LIMIT = "SQR_TOOM3_THRESHOLD"
const mvSQR_TOOM4_THRESHOLD = 400
const mvSQR_TOOM6_THRESHOLD = "MUL_TOOM6H_THRESHOLD"
const mvSQR_TOOM8_THRESHOLD = "MUL_TOOM8H_THRESHOLD"
const mvSSIZE_MAX = "LONG_MAX"
const mvSTDC_HEADERS = 1
const mvSTDERR_FILENO = 2
const mvSTDIN_FILENO = 0
const mvSTDOUT_FILENO = 1
const mvSYMLOOP_MAX = 40
const mvTABLE_LIMIT_2N_MINUS_POPC_2N = 49
const mvTARGET_REGISTER_STARVED = 0
const mvTEMPFILE = "t-scanf.tmp"
const mvTIME_WITH_SYS_TIME = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTUNE_SQR_TOOM2_MAX = "SQR_TOOM2_MAX_GENERIC"
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT32_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSE_LEADING_REGPARM = 0
const mvUSE_PREINV_DIVREM_1 = 1
const mvUSHRT_MAX = 65535
const mvVERSION = "6.3.0"
const mvWANT_FFT = 1
const mvWANT_TMP_ALLOCA = 1
const mvWANT_TMP_DEBUG = 0
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_OK = 2
const mvW_TYPE_SIZE = "GMP_LIMB_BITS"
const mvX_OK = 1
const mv_CS_GNU_LIBC_VERSION = 2
const mv_CS_GNU_LIBPTHREAD_VERSION = 3
const mv_CS_PATH = 0
const mv_CS_POSIX_V5_WIDTH_RESTRICTED_ENVS = 4
const mv_CS_POSIX_V6_ILP32_OFF32_CFLAGS = 1116
const mv_CS_POSIX_V6_ILP32_OFF32_LDFLAGS = 1117
const mv_CS_POSIX_V6_ILP32_OFF32_LIBS = 1118
const mv_CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = 1119
const mv_CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = 1120
const mv_CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = 1121
const mv_CS_POSIX_V6_ILP32_OFFBIG_LIBS = 1122
const mv_CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = 1123
const mv_CS_POSIX_V6_LP64_OFF64_CFLAGS = 1124
const mv_CS_POSIX_V6_LP64_OFF64_LDFLAGS = 1125
const mv_CS_POSIX_V6_LP64_OFF64_LIBS = 1126
const mv_CS_POSIX_V6_LP64_OFF64_LINTFLAGS = 1127
const mv_CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = 1128
const mv_CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = 1129
const mv_CS_POSIX_V6_LPBIG_OFFBIG_LIBS = 1130
const mv_CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = 1131
const mv_CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = 1
const mv_CS_POSIX_V7_ILP32_OFF32_CFLAGS = 1132
const mv_CS_POSIX_V7_ILP32_OFF32_LDFLAGS = 1133
const mv_CS_POSIX_V7_ILP32_OFF32_LIBS = 1134
const mv_CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = 1135
const mv_CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = 1136
const mv_CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = 1137
const mv_CS_POSIX_V7_ILP32_OFFBIG_LIBS = 1138
const mv_CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = 1139
const mv_CS_POSIX_V7_LP64_OFF64_CFLAGS = 1140
const mv_CS_POSIX_V7_LP64_OFF64_LDFLAGS = 1141
const mv_CS_POSIX_V7_LP64_OFF64_LIBS = 1142
const mv_CS_POSIX_V7_LP64_OFF64_LINTFLAGS = 1143
const mv_CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = 1144
const mv_CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = 1145
const mv_CS_POSIX_V7_LPBIG_OFFBIG_LIBS = 1146
const mv_CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = 1147
const mv_CS_POSIX_V7_THREADS_CFLAGS = 1150
const mv_CS_POSIX_V7_THREADS_LDFLAGS = 1151
const mv_CS_POSIX_V7_WIDTH_RESTRICTED_ENVS = 5
const mv_CS_V6_ENV = 1148
const mv_CS_V7_ENV = 1149
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_H_HAVE_VA_LIST = 1
const mv_GMP_IEEE_FLOATS = 1
const mv_GNU_SOURCE = 1
const mv_ILP32 = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_PC_2_SYMLINKS = 20
const mv_PC_ALLOC_SIZE_MIN = 18
const mv_PC_ASYNC_IO = 10
const mv_PC_CHOWN_RESTRICTED = 6
const mv_PC_FILESIZEBITS = 13
const mv_PC_LINK_MAX = 0
const mv_PC_MAX_CANON = 1
const mv_PC_MAX_INPUT = 2
const mv_PC_NAME_MAX = 3
const mv_PC_NO_TRUNC = 7
const mv_PC_PATH_MAX = 4
const mv_PC_PIPE_BUF = 5
const mv_PC_PRIO_IO = 11
const mv_PC_REC_INCR_XFER_SIZE = 14
const mv_PC_REC_MAX_XFER_SIZE = 15
const mv_PC_REC_MIN_XFER_SIZE = 16
const mv_PC_REC_XFER_ALIGN = 17
const mv_PC_SOCK_MAXBUF = 12
const mv_PC_SYMLINK_MAX = 19
const mv_PC_SYNC_IO = 9
const mv_PC_VDISABLE = 8
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_C_BIND = "_POSIX_VERSION"
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX2_VERSION = "_POSIX_VERSION"
const mv_POSIX_ADVISORY_INFO = "_POSIX_VERSION"
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_ASYNCHRONOUS_IO = "_POSIX_VERSION"
const mv_POSIX_BARRIERS = "_POSIX_VERSION"
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CHOWN_RESTRICTED = 1
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_CLOCK_SELECTION = "_POSIX_VERSION"
const mv_POSIX_CPUTIME = "_POSIX_VERSION"
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_FSYNC = "_POSIX_VERSION"
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_IPV6 = "_POSIX_VERSION"
const mv_POSIX_JOB_CONTROL = 1
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAPPED_FILES = "_POSIX_VERSION"
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MEMLOCK = "_POSIX_VERSION"
const mv_POSIX_MEMLOCK_RANGE = "_POSIX_VERSION"
const mv_POSIX_MEMORY_PROTECTION = "_POSIX_VERSION"
const mv_POSIX_MESSAGE_PASSING = "_POSIX_VERSION"
const mv_POSIX_MONOTONIC_CLOCK = "_POSIX_VERSION"
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_NO_TRUNC = 1
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RAW_SOCKETS = "_POSIX_VERSION"
const mv_POSIX_READER_WRITER_LOCKS = "_POSIX_VERSION"
const mv_POSIX_REALTIME_SIGNALS = "_POSIX_VERSION"
const mv_POSIX_REGEXP = 1
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SAVED_IDS = 1
const mv_POSIX_SEMAPHORES = "_POSIX_VERSION"
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SHARED_MEMORY_OBJECTS = "_POSIX_VERSION"
const mv_POSIX_SHELL = 1
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SPAWN = "_POSIX_VERSION"
const mv_POSIX_SPIN_LOCKS = "_POSIX_VERSION"
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREADS = "_POSIX_VERSION"
const mv_POSIX_THREAD_ATTR_STACKADDR = "_POSIX_VERSION"
const mv_POSIX_THREAD_ATTR_STACKSIZE = "_POSIX_VERSION"
const mv_POSIX_THREAD_CPUTIME = "_POSIX_VERSION"
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_PRIORITY_SCHEDULING = "_POSIX_VERSION"
const mv_POSIX_THREAD_PROCESS_SHARED = "_POSIX_VERSION"
const mv_POSIX_THREAD_SAFE_FUNCTIONS = "_POSIX_VERSION"
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMEOUTS = "_POSIX_VERSION"
const mv_POSIX_TIMERS = "_POSIX_VERSION"
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_POSIX_V6_ILP32_OFFBIG = 1
const mv_POSIX_V7_ILP32_OFFBIG = 1
const mv_POSIX_VDISABLE = 0
const mv_POSIX_VERSION = 200809
const mv_REDIR_TIME64 = 1
const mv_SC_2_CHAR_TERM = 95
const mv_SC_2_C_BIND = 47
const mv_SC_2_C_DEV = 48
const mv_SC_2_FORT_DEV = 49
const mv_SC_2_FORT_RUN = 50
const mv_SC_2_LOCALEDEF = 52
const mv_SC_2_PBS = 168
const mv_SC_2_PBS_ACCOUNTING = 169
const mv_SC_2_PBS_CHECKPOINT = 175
const mv_SC_2_PBS_LOCATE = 170
const mv_SC_2_PBS_MESSAGE = 171
const mv_SC_2_PBS_TRACK = 172
const mv_SC_2_SW_DEV = 51
const mv_SC_2_UPE = 97
const mv_SC_2_VERSION = 46
const mv_SC_ADVISORY_INFO = 132
const mv_SC_AIO_LISTIO_MAX = 23
const mv_SC_AIO_MAX = 24
const mv_SC_AIO_PRIO_DELTA_MAX = 25
const mv_SC_ARG_MAX = 0
const mv_SC_ASYNCHRONOUS_IO = 12
const mv_SC_ATEXIT_MAX = 87
const mv_SC_AVPHYS_PAGES = 86
const mv_SC_BARRIERS = 133
const mv_SC_BC_BASE_MAX = 36
const mv_SC_BC_DIM_MAX = 37
const mv_SC_BC_SCALE_MAX = 38
const mv_SC_BC_STRING_MAX = 39
const mv_SC_CHILD_MAX = 1
const mv_SC_CLK_TCK = 2
const mv_SC_CLOCK_SELECTION = 137
const mv_SC_COLL_WEIGHTS_MAX = 40
const mv_SC_CPUTIME = 138
const mv_SC_DELAYTIMER_MAX = 26
const mv_SC_EXPR_NEST_MAX = 42
const mv_SC_FSYNC = 15
const mv_SC_GETGR_R_SIZE_MAX = 69
const mv_SC_GETPW_R_SIZE_MAX = 70
const mv_SC_HOST_NAME_MAX = 180
const mv_SC_IOV_MAX = 60
const mv_SC_IPV6 = 235
const mv_SC_JOB_CONTROL = 7
const mv_SC_LINE_MAX = 43
const mv_SC_LOGIN_NAME_MAX = 71
const mv_SC_MAPPED_FILES = 16
const mv_SC_MEMLOCK = 17
const mv_SC_MEMLOCK_RANGE = 18
const mv_SC_MEMORY_PROTECTION = 19
const mv_SC_MESSAGE_PASSING = 20
const mv_SC_MINSIGSTKSZ = 249
const mv_SC_MONOTONIC_CLOCK = 149
const mv_SC_MQ_OPEN_MAX = 27
const mv_SC_MQ_PRIO_MAX = 28
const mv_SC_NGROUPS_MAX = 3
const mv_SC_NPROCESSORS_CONF = 83
const mv_SC_NPROCESSORS_ONLN = 84
const mv_SC_NZERO = 109
const mv_SC_OPEN_MAX = 4
const mv_SC_PAGESIZE = 30
const mv_SC_PAGE_SIZE = 30
const mv_SC_PASS_MAX = 88
const mv_SC_PHYS_PAGES = 85
const mv_SC_PRIORITIZED_IO = 13
const mv_SC_PRIORITY_SCHEDULING = 10
const mv_SC_RAW_SOCKETS = 236
const mv_SC_READER_WRITER_LOCKS = 153
const mv_SC_REALTIME_SIGNALS = 9
const mv_SC_REGEXP = 155
const mv_SC_RE_DUP_MAX = 44
const mv_SC_RTSIG_MAX = 31
const mv_SC_SAVED_IDS = 8
const mv_SC_SEMAPHORES = 21
const mv_SC_SEM_NSEMS_MAX = 32
const mv_SC_SEM_VALUE_MAX = 33
const mv_SC_SHARED_MEMORY_OBJECTS = 22
const mv_SC_SHELL = 157
const mv_SC_SIGQUEUE_MAX = 34
const mv_SC_SIGSTKSZ = 250
const mv_SC_SPAWN = 159
const mv_SC_SPIN_LOCKS = 154
const mv_SC_SPORADIC_SERVER = 160
const mv_SC_SS_REPL_MAX = 241
const mv_SC_STREAMS = 174
const mv_SC_STREAM_MAX = 5
const mv_SC_SYMLOOP_MAX = 173
const mv_SC_SYNCHRONIZED_IO = 14
const mv_SC_THREADS = 67
const mv_SC_THREAD_ATTR_STACKADDR = 77
const mv_SC_THREAD_ATTR_STACKSIZE = 78
const mv_SC_THREAD_CPUTIME = 139
const mv_SC_THREAD_DESTRUCTOR_ITERATIONS = 73
const mv_SC_THREAD_KEYS_MAX = 74
const mv_SC_THREAD_PRIORITY_SCHEDULING = 79
const mv_SC_THREAD_PRIO_INHERIT = 80
const mv_SC_THREAD_PRIO_PROTECT = 81
const mv_SC_THREAD_PROCESS_SHARED = 82
const mv_SC_THREAD_ROBUST_PRIO_INHERIT = 247
const mv_SC_THREAD_ROBUST_PRIO_PROTECT = 248
const mv_SC_THREAD_SAFE_FUNCTIONS = 68
const mv_SC_THREAD_SPORADIC_SERVER = 161
const mv_SC_THREAD_STACK_MIN = 75
const mv_SC_THREAD_THREADS_MAX = 76
const mv_SC_TIMEOUTS = 164
const mv_SC_TIMERS = 11
const mv_SC_TIMER_MAX = 35
const mv_SC_TRACE = 181
const mv_SC_TRACE_EVENT_FILTER = 182
const mv_SC_TRACE_EVENT_NAME_MAX = 242
const mv_SC_TRACE_INHERIT = 183
const mv_SC_TRACE_LOG = 184
const mv_SC_TRACE_NAME_MAX = 243
const mv_SC_TRACE_SYS_MAX = 244
const mv_SC_TRACE_USER_EVENT_MAX = 245
const mv_SC_TTY_NAME_MAX = 72
const mv_SC_TYPED_MEMORY_OBJECTS = 165
const mv_SC_TZNAME_MAX = 6
const mv_SC_UIO_MAXIOV = 60
const mv_SC_V6_ILP32_OFF32 = 176
const mv_SC_V6_ILP32_OFFBIG = 177
const mv_SC_V6_LP64_OFF64 = 178
const mv_SC_V6_LPBIG_OFFBIG = 179
const mv_SC_V7_ILP32_OFF32 = 237
const mv_SC_V7_ILP32_OFFBIG = 238
const mv_SC_V7_LP64_OFF64 = 239
const mv_SC_V7_LPBIG_OFFBIG = 240
const mv_SC_VERSION = 29
const mv_SC_XBS5_ILP32_OFF32 = 125
const mv_SC_XBS5_ILP32_OFFBIG = 126
const mv_SC_XBS5_LP64_OFF64 = 127
const mv_SC_XBS5_LPBIG_OFFBIG = 128
const mv_SC_XOPEN_CRYPT = 92
const mv_SC_XOPEN_ENH_I18N = 93
const mv_SC_XOPEN_LEGACY = 129
const mv_SC_XOPEN_REALTIME = 130
const mv_SC_XOPEN_REALTIME_THREADS = 131
const mv_SC_XOPEN_SHM = 94
const mv_SC_XOPEN_STREAMS = 246
const mv_SC_XOPEN_UNIX = 91
const mv_SC_XOPEN_VERSION = 89
const mv_SC_XOPEN_XCU_VERSION = 90
const mv_SC_XOPEN_XPG2 = 98
const mv_SC_XOPEN_XPG3 = 99
const mv_SC_XOPEN_XPG4 = 100
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_ENH_I18N = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv_XOPEN_UNIX = 1
const mv_XOPEN_VERSION = 700
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 2
const mv__FLT_EVAL_METHOD__ = 2
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG -mlong-double-64"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__ILP32__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LAHF_SAHF__ = 1
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 2147483647
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "ll"
const mv__PRIPTR = ""
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 12
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__code_model_32__ = 1
const mv__gmp_doprnt_mpf = "__gmp_doprnt_mpf2"
const mv__gnu_linux__ = 1
const mv__i386 = 1
const mv__i386__ = 1
const mv__i686 = 1
const mv__i686__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pentiumpro = 1
const mv__pentiumpro__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_newalloc = "_mpz_realloc"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvbinvert_limb_table = "__gmp_binvert_limb_table"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_init_primesieve = "__gmp_init_primesieve"
const mvgmp_nextprime = "__gmp_nextprime"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_primesieve = "__gmp_primesieve"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvi386 = 1
const mvjacobi_table = "__gmp_jacobi_table"
const mvlinux = 1
const mvmodlimb_invert = "binvert_limb"
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui (x[0], 3L) == 0"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpn_fft_mul = "mpn_nussbaumer_mul"
const mvmpn_get_d = "__gmpn_get_d"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_gcd = "__gmpz_divexact_gcd"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_inp_str_nowhite = "__gmpz_inp_str_nowhite"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucas_mod = "__gmpz_lucas_mod"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_n_pow_ui = "__gmpz_n_pow_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_oddfac_1 = "__gmpz_oddfac_1"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_prodlimbs = "__gmpz_prodlimbs"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_stronglucas = "__gmpz_stronglucas"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvrestrict = "__restrict"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint32

type tnva_list = ppuintptr

type tnwchar_t = ppint32

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnsize_t = ppuint32

type tnptrdiff_t = ppint32

type tnssize_t = ppint32

type tnoff_t = ppint64

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlocale_t = ppuintptr

type tnuintptr_t = ppuint32

type tnintptr_t = ppint32

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tnpid_t = ppint32

type tnuid_t = ppuint32

type tngid_t = ppuint32

type tnuseconds_t = ppuint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

type tngmp_uint_least32_t = ppuint32

type tngmp_intptr_t = ppint32

type tngmp_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tngmp_pi2_t = struct {
	fdinv21 tnmp_limb_t
	fdinv32 tnmp_limb_t
	fdinv53 tnmp_limb_t
}

type tutmp_align_t = struct {
	fdd           [0]ppfloat64
	fdp           [0]ppuintptr
	fdl           tnmp_limb_t
	fd__ccgo_pad3 [4]byte
}

type tstmp_reentrant_t = struct {
	fdnext ppuintptr
	fdsize tnsize_t
}

type tngmp_randfnptr_t = struct {
	fdrandseed_fn  ppuintptr
	fdrandget_fn   ppuintptr
	fdrandclear_fn ppuintptr
	fdrandiset_fn  ppuintptr
}

type tetoom6_flags = ppint32

const ectoom6_all_pos = 0
const ectoom6_vm1_neg = 1
const ectoom6_vm2_neg = 2

type tetoom7_flags = ppint32

const ectoom7_w1_neg = 1
const ectoom7_w3_neg = 2

type tngmp_primesieve_t = struct {
	fdd       ppuint32
	fds0      ppuint32
	fdsqrt_s0 ppuint32
	fds       [513]ppuint8
}

type tsfft_table_nk = struct {
	fd__ccgo0 uint32
}

type tsbases = struct {
	fdchars_per_limb    ppint32
	fdlogb2             tnmp_limb_t
	fdlog2b             tnmp_limb_t
	fdbig_base          tnmp_limb_t
	fdbig_base_inverted tnmp_limb_t
}

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tuieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type tnmp_double_limb_t = struct {
	fdd0 tnmp_limb_t
	fdd1 tnmp_limb_t
}

type tshgcd_matrix1 = struct {
	fdu [2][2]tnmp_limb_t
}

type tshgcd_matrix = struct {
	fdalloc tnmp_size_t
	fdn     tnmp_size_t
	fdp     [2][2]tnmp_ptr
}

type tsgcdext_ctx = struct {
	fdgp    tnmp_ptr
	fdgn    tnmp_size_t
	fdup    tnmp_ptr
	fdusize ppuintptr
	fdun    tnmp_size_t
	fdu0    tnmp_ptr
	fdu1    tnmp_ptr
	fdtp    tnmp_ptr
}

type tspowers = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tnpowers_t = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tsdoprnt_params_t = struct {
	fdbase         ppint32
	fdconv         ppint32
	fdexpfmt       ppuintptr
	fdexptimes4    ppint32
	fdfill         ppint8
	fdjustify      ppint32
	fdprec         ppint32
	fdshowbase     ppint32
	fdshowpoint    ppint32
	fdshowtrailing ppint32
	fdsign         ppint8
	fdwidth        ppint32
}

type tndoprnt_format_t = ppuintptr

type tndoprnt_memory_t = ppuintptr

type tndoprnt_reps_t = ppuintptr

type tndoprnt_final_t = ppuintptr

type tsdoprnt_funs_t = struct {
	fdformat tndoprnt_format_t
	fdmemory tndoprnt_memory_t
	fdreps   tndoprnt_reps_t
	fdfinal  tndoprnt_final_t
}

type tsgmp_asprintf_t = struct {
	fdresult ppuintptr
	fdbuf    ppuintptr
	fdsize   tnsize_t
	fdalloc  tnsize_t
}

type tsgmp_snprintf_t = struct {
	fdbuf  ppuintptr
	fdsize tnsize_t
}

type tngmp_doscan_scan_t = ppuintptr

type tngmp_doscan_step_t = ppuintptr

type tngmp_doscan_get_t = ppuintptr

type tngmp_doscan_unget_t = ppuintptr

type tsgmp_doscan_funs_t = struct {
	fdscan  tngmp_doscan_scan_t
	fdstep  tngmp_doscan_step_t
	fdget   tngmp_doscan_get_t
	fdunget tngmp_doscan_unget_t
}

type tn__jmp_buf = [6]ppuint32

type tnjmp_buf = [1]ts__jmp_buf_tag

type ts__jmp_buf_tag = struct {
	fd__jb tn__jmp_buf
	fd__fl ppuint32
	fd__ss [32]ppuint32
}

type tnsigjmp_buf = [1]ts__jmp_buf_tag

/* Establish ostringstream and istringstream.  Do this here so as to hide
   the conditionals, rather than putting stuff in each test program.

   Oldish versions of g++, like 2.95.2, don't have <sstream>, only
   <strstream>.  Fake up ostringstream and istringstream classes, but not a
   full implementation, just enough for our purposes.  */

var Xoption_libc_scanf ppint32

type tnfun_t = ppuintptr

// C documentation
//
//	/* This problem was seen on powerpc7450-apple-darwin7.0.0, sscanf returns 0
//	   where it should return EOF.  A workaround in gmp_sscanf would be a bit
//	   tedious, and since this is a rather obvious libc bug, quite likely
//	   affecting other programs, we'll just suppress affected tests for now.  */
func Xtest_sscanf_eof_ok(cgtls *iqlibc.ppTLS) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var pp_ /* x at bp+0 */ ppint32
	var snresult = -ppint32(1)

	if snresult == -ppint32(1) {

		if Xsscanf(cgtls, "\x00", "%d\x00", iqlibc.ppVaList(cgbp+16, cgbp)) == -ppint32(1) {

			snresult = ppint32(1)
		} else {

			Xprintf(cgtls, "Warning, sscanf(\"\",\"%%d\",&x) doesn't return EOF.\n\x00", 0)
			Xprintf(cgtls, "This affects gmp_sscanf, tests involving it will be suppressed.\n\x00", 0)
			Xprintf(cgtls, "You should try to get a fix for your libc.\n\x00", 0)
			snresult = 0
		}
	}
	return snresult
}

// C documentation
//
//	/* Convert fmt from a GMP scanf format string to an equivalent for a plain
//	   libc scanf, for example "%Zd" becomes "%ld".  Return 1 if this succeeds,
//	   0 if it cannot (or should not) be done.  */
func Xlibc_scanf_convert(cgtls *iqlibc.ppTLS, aafmt ppuintptr) (cgr ppint32) {

	var aap, ccv2, ccv3 ppuintptr
	pp_, pp_, pp_ = aap, ccv2, ccv3
	aap = aafmt

	if !(Xoption_libc_scanf != 0) {
		return 0
	}

	for {
		if !(ppint32(*(*ppint8)(iqunsafe.ppPointer(aafmt))) != ppint32('\000')) {
			break
		}

		switch ppint32(*(*ppint8)(iqunsafe.ppPointer(aafmt))) {
		case ppint32('F'):
			fallthrough
		case ppint32('Q'):
			fallthrough
		case ppint32('Z'):
			/* transmute */
			ccv2 = aap
			aap++
			*(*ppint8)(iqunsafe.ppPointer(ccv2)) = ppint8('l')
			break
			fallthrough
		default:
			ccv3 = aap
			aap++
			*(*ppint8)(iqunsafe.ppPointer(ccv3)) = *(*ppint8)(iqunsafe.ppPointer(aafmt))
			break
		}

		goto cg_1
	cg_1:
		;
		aafmt++
	}
	*(*ppint8)(iqunsafe.ppPointer(aap)) = ppint8('\000')
	return ppint32(1)
}

var Xgot_ftell ppint32
var Xfromstring_next_c ppint32

// C documentation
//
//	/* Call gmp_fscanf, reading the "input" string data provided. */
func Xfromstring_gmp_fscanf(cgtls *iqlibc.ppTLS, aainput ppuintptr, aafmt ppuintptr, cgva ppuintptr) (cgr ppint32) {

	var aaap tnva_list
	var aafp ppuintptr
	var aaret ppint32
	pp_, pp_, pp_ = aaap, aafp, aaret
	aaap = cgva

	aafp = Xfopen(cgtls, "t-scanf.tmp\x00", "w+\x00")
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aafp != iqlibc.ppUintptrFromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(133), "fp != ((void*)0)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xfputs(cgtls, aainput, aafp) != -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(134), "fputs (input, fp) != (-1)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xfflush(cgtls, aafp) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(135), "fflush (fp) == 0\x00")
	}
	Xrewind(cgtls, aafp)

	aaret = X__gmp_vfscanf(cgtls, aafp, aafmt, aaap)
	Xgot_ftell = Xftell(cgtls, aafp)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xgot_ftell != -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(140), "got_ftell != -1L\x00")
	}

	Xfromstring_next_c = Xgetc(cgtls, aafp)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xfclose(cgtls, aafp) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(144), "fclose (fp) == 0\x00")
	}
	pp_ = aaap
	return aaret
}

func Xfun_gmp_sscanf(cgtls *iqlibc.ppTLS, aainput ppuintptr, aafmt ppuintptr, aaa1 ppuintptr, aaa2 ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	if aaa2 == iqlibc.ppUintptrFromInt32(0) {
		return X__gmp_sscanf(cgtls, aainput, aafmt, iqlibc.ppVaList(cgbp+8, aaa1))
	} else {
		return X__gmp_sscanf(cgtls, aainput, aafmt, iqlibc.ppVaList(cgbp+8, aaa1, aaa2))
	}
	return cgr
}

func Xfun_gmp_fscanf(cgtls *iqlibc.ppTLS, aainput ppuintptr, aafmt ppuintptr, aaa1 ppuintptr, aaa2 ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	if aaa2 == iqlibc.ppUintptrFromInt32(0) {
		return Xfromstring_gmp_fscanf(cgtls, aainput, aafmt, iqlibc.ppVaList(cgbp+8, aaa1))
	} else {
		return Xfromstring_gmp_fscanf(cgtls, aainput, aafmt, iqlibc.ppVaList(cgbp+8, aaa1, aaa2))
	}
	return cgr
}

func Xfun_fscanf(cgtls *iqlibc.ppTLS, aainput ppuintptr, aafmt ppuintptr, aaa1 ppuintptr, aaa2 ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aafp ppuintptr
	var aaret ppint32
	pp_, pp_ = aafp, aaret

	aafp = Xfopen(cgtls, "t-scanf.tmp\x00", "w+\x00")
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aafp != iqlibc.ppUintptrFromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(176), "fp != ((void*)0)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xfputs(cgtls, aainput, aafp) != -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(177), "fputs (input, fp) != (-1)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xfflush(cgtls, aafp) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(178), "fflush (fp) == 0\x00")
	}
	Xrewind(cgtls, aafp)

	if aaa2 == iqlibc.ppUintptrFromInt32(0) {
		aaret = Xfscanf(cgtls, aafp, aafmt, iqlibc.ppVaList(cgbp+8, aaa1))
	} else {
		aaret = Xfscanf(cgtls, aafp, aafmt, iqlibc.ppVaList(cgbp+8, aaa1, aaa2))
	}

	Xgot_ftell = Xftell(cgtls, aafp)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xgot_ftell != -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(187), "got_ftell != -1L\x00")
	}

	Xfromstring_next_c = Xgetc(cgtls, aafp)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xfclose(cgtls, aafp) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(191), "fclose (fp) == 0\x00")
	}
	return aaret
}

/* On various old systems, for instance HP-UX 9, the C library sscanf needs
   to be able to write into the input string.  Ensure that this is possible,
   when gcc is putting the test data into a read-only section.

   Actually we ought to only need this under SSCANF_WRITABLE_INPUT from
   configure, but it's just as easy to do it unconditionally, and in any
   case this code is only executed under the -s option.  */

func Xfun_sscanf(cgtls *iqlibc.ppTLS, aainput ppuintptr, aafmt ppuintptr, aaa1 ppuintptr, aaa2 ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aainput_writable ppuintptr
	var aaret ppint32
	var aasize tnsize_t
	pp_, pp_, pp_ = aainput_writable, aaret, aasize

	aasize = Xstrlen(cgtls, aainput) + ppuint32(1)
	aainput_writable = (*(*func(*iqlibc.ppTLS, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_allocate_func})))(cgtls, aasize)
	Xmemcpy(cgtls, aainput_writable, aainput, aasize)

	if aaa2 == iqlibc.ppUintptrFromInt32(0) {
		aaret = Xsscanf(cgtls, aainput_writable, aafmt, iqlibc.ppVaList(cgbp+8, aaa1))
	} else {
		aaret = Xsscanf(cgtls, aainput_writable, aafmt, iqlibc.ppVaList(cgbp+8, aaa1, aaa2))
	}

	(*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t))(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_free_func})))(cgtls, aainput_writable, aasize)
	return aaret
}

// C documentation
//
//	/* whether the format string consists entirely of ignored fields */
func Xfmt_allignore(cgtls *iqlibc.ppTLS, aafmt ppuintptr) (cgr ppint32) {

	var aasaw_star ppint32
	pp_ = aasaw_star
	aasaw_star = ppint32(1)
	for {
		if !(ppint32(*(*ppint8)(iqunsafe.ppPointer(aafmt))) != ppint32('\000')) {
			break
		}

		switch ppint32(*(*ppint8)(iqunsafe.ppPointer(aafmt))) {
		case ppint32('%'):
			if !(aasaw_star != 0) {
				return 0
			}
			aasaw_star = 0
			break
			fallthrough
		case ppint32('*'):
			aasaw_star = ppint32(1)
			break
		}

		goto cg_1
	cg_1:
		;
		aafmt++
	}
	return ppint32(1)
}

func Xcheck_z(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aa__i tnmp_size_t
	var aa__nail tnmp_limb_t
	var aaerror, aagot_ret, aai, aaignore, aaj, aawant_ftell, aawant_ret, aawant_upto, ccv21, ccv23, ccv25 ppint32
	var aafun tnfun_t
	var aaname ppuintptr
	var ccv22 ppbool
	var pp_ /* fmt at bp+32 */ [128]ppint8
	var pp_ /* got at bp+4 */ tnmpz_t
	var pp_ /* got_l at bp+28 */ ppint32
	var pp_ /* got_upto at bp+0 */ ppint32
	var pp_ /* want at bp+16 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__nail, aaerror, aafun, aagot_ret, aai, aaignore, aaj, aaname, aawant_ftell, aawant_ret, aawant_upto, ccv21, ccv22, ccv23, ccv25
	var sndata = [179]struct {
		fdfmt        ppuintptr
		fdinput      ppuintptr
		fdwant       ppuintptr
		fdwant_ret   ppint32
		fdwant_ftell ppint32
		fdwant_upto  ppint32
		fdnot_glibc  ppint32
	}{
		0: {
			fdfmt:        "%Zd\x00",
			fdinput:      "0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		1: {
			fdfmt:        "%Zd\x00",
			fdinput:      "1\x00",
			fdwant:       "1\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		2: {
			fdfmt:        "%Zd\x00",
			fdinput:      "123\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		3: {
			fdfmt:        "%Zd\x00",
			fdinput:      "+0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		4: {
			fdfmt:        "%Zd\x00",
			fdinput:      "+1\x00",
			fdwant:       "1\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		5: {
			fdfmt:        "%Zd\x00",
			fdinput:      "+123\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		6: {
			fdfmt:        "%Zd\x00",
			fdinput:      "-0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		7: {
			fdfmt:        "%Zd\x00",
			fdinput:      "-1\x00",
			fdwant:       "-1\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		8: {
			fdfmt:        "%Zd\x00",
			fdinput:      "-123\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		9: {
			fdfmt:        "%Zo\x00",
			fdinput:      "0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		10: {
			fdfmt:        "%Zo\x00",
			fdinput:      "173\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		11: {
			fdfmt:        "%Zo\x00",
			fdinput:      "+0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		12: {
			fdfmt:        "%Zo\x00",
			fdinput:      "+173\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		13: {
			fdfmt:        "%Zo\x00",
			fdinput:      "-0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		14: {
			fdfmt:        "%Zo\x00",
			fdinput:      "-173\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		15: {
			fdfmt:        "%Zx\x00",
			fdinput:      "0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		16: {
			fdfmt:        "%Zx\x00",
			fdinput:      "7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		17: {
			fdfmt:        "%Zx\x00",
			fdinput:      "7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		18: {
			fdfmt:        "%Zx\x00",
			fdinput:      "+0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		19: {
			fdfmt:        "%Zx\x00",
			fdinput:      "+7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		20: {
			fdfmt:        "%Zx\x00",
			fdinput:      "+7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		21: {
			fdfmt:        "%Zx\x00",
			fdinput:      "-0\x00",
			fdwant:       "-0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		22: {
			fdfmt:        "%Zx\x00",
			fdinput:      "-7b\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		23: {
			fdfmt:        "%Zx\x00",
			fdinput:      "-7b\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		24: {
			fdfmt:        "%ZX\x00",
			fdinput:      "0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		25: {
			fdfmt:        "%ZX\x00",
			fdinput:      "7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		26: {
			fdfmt:        "%ZX\x00",
			fdinput:      "7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		27: {
			fdfmt:        "%ZX\x00",
			fdinput:      "+0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		28: {
			fdfmt:        "%ZX\x00",
			fdinput:      "+7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		29: {
			fdfmt:        "%ZX\x00",
			fdinput:      "+7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		30: {
			fdfmt:        "%ZX\x00",
			fdinput:      "-0\x00",
			fdwant:       "-0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		31: {
			fdfmt:        "%ZX\x00",
			fdinput:      "-7b\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		32: {
			fdfmt:        "%ZX\x00",
			fdinput:      "-7b\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		33: {
			fdfmt:        "%Zx\x00",
			fdinput:      "0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		34: {
			fdfmt:        "%Zx\x00",
			fdinput:      "7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		35: {
			fdfmt:        "%Zx\x00",
			fdinput:      "7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		36: {
			fdfmt:        "%Zx\x00",
			fdinput:      "+0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		37: {
			fdfmt:        "%Zx\x00",
			fdinput:      "+7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		38: {
			fdfmt:        "%Zx\x00",
			fdinput:      "+7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		39: {
			fdfmt:        "%Zx\x00",
			fdinput:      "-0\x00",
			fdwant:       "-0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		40: {
			fdfmt:        "%Zx\x00",
			fdinput:      "-7B\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		41: {
			fdfmt:        "%Zx\x00",
			fdinput:      "-7B\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		42: {
			fdfmt:        "%ZX\x00",
			fdinput:      "0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		43: {
			fdfmt:        "%ZX\x00",
			fdinput:      "7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		44: {
			fdfmt:        "%ZX\x00",
			fdinput:      "7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		45: {
			fdfmt:        "%ZX\x00",
			fdinput:      "+0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		46: {
			fdfmt:        "%ZX\x00",
			fdinput:      "+7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		47: {
			fdfmt:        "%ZX\x00",
			fdinput:      "+7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		48: {
			fdfmt:        "%ZX\x00",
			fdinput:      "-0\x00",
			fdwant:       "-0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		49: {
			fdfmt:        "%ZX\x00",
			fdinput:      "-7B\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		50: {
			fdfmt:        "%ZX\x00",
			fdinput:      "-7B\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		51: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		52: {
			fdfmt:        "%Zi\x00",
			fdinput:      "1\x00",
			fdwant:       "1\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		53: {
			fdfmt:        "%Zi\x00",
			fdinput:      "123\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		54: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		55: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+1\x00",
			fdwant:       "1\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		56: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+123\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		57: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		58: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-1\x00",
			fdwant:       "-1\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		59: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-123\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		60: {
			fdfmt:        "%Zi\x00",
			fdinput:      "00\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		61: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0173\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		62: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+00\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		63: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0173\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		64: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-00\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		65: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0173\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		66: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0x0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		67: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0x7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		68: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0x7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		69: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0x0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		70: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0x7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		71: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0x7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		72: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0x0\x00",
			fdwant:       "-0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		73: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0x7b\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		74: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0x7b\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		75: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0X0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		76: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0X7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		77: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0X7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		78: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0X0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		79: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0X7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		80: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0X7b\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		81: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0X0\x00",
			fdwant:       "-0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		82: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0X7b\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		83: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0X7b\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		84: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0x0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		85: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0x7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		86: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0x7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		87: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0x0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		88: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0x7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		89: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0x7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		90: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0x0\x00",
			fdwant:       "-0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		91: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0x7B\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		92: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0x7B\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		93: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0X0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		94: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0X7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		95: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0X7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		96: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0X0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		97: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0X7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		98: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0X7B\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		99: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0X0\x00",
			fdwant:       "-0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		100: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0X7B\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		101: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0X7B\x00",
			fdwant:       "-123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		102: {
			fdfmt:        "%Zd\x00",
			fdinput:      " 0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		103: {
			fdfmt:        "%Zd\x00",
			fdinput:      "  0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		104: {
			fdfmt:        "%Zd\x00",
			fdinput:      "   0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		105: {
			fdfmt:        "%Zd\x00",
			fdinput:      "\t0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		106: {
			fdfmt:        "%Zd\x00",
			fdinput:      "\t\t0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		107: {
			fdfmt:        "hello%Zd\x00",
			fdinput:      "hello0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		108: {
			fdfmt:        "hello%Zd\x00",
			fdinput:      "hello 0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		109: {
			fdfmt:        "hello%Zd\x00",
			fdinput:      "hello \t0\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		110: {
			fdfmt:        "hello%Zdworld\x00",
			fdinput:      "hello 0world\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		111: {
			fdfmt:        "hello%*Zd\x00",
			fdinput:      "hello0\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		112: {
			fdfmt:        "hello%*Zd\x00",
			fdinput:      "hello 0\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		113: {
			fdfmt:        "hello%*Zd\x00",
			fdinput:      "hello \t0\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		114: {
			fdfmt:        "hello%*Zdworld\x00",
			fdinput:      "hello 0world\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		115: {
			fdfmt:        "%Zd\x00",
			fdinput:      "\x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(555),
		},
		116: {
			fdfmt:        "%Zd\x00",
			fdinput:      " \x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(555),
		},
		117: {
			fdfmt:        " %Zd\x00",
			fdinput:      "\x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(555),
		},
		118: {
			fdfmt:        "xyz%Zd\x00",
			fdinput:      "\x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(555),
		},
		119: {
			fdfmt:        "%*Zd\x00",
			fdinput:      "\x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(555),
		},
		120: {
			fdfmt:        " %*Zd\x00",
			fdinput:      "\x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(555),
		},
		121: {
			fdfmt:        "xyz%*Zd\x00",
			fdinput:      "\x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(555),
		},
		122: {
			fdfmt:       "%Zd\x00",
			fdinput:     "xyz\x00",
			fdwant:      "0\x00",
			fdwant_upto: -ppint32(555),
		},
		123: {
			fdfmt:        "%Zd\x00",
			fdinput:      "-\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(1),
			fdwant_upto:  -ppint32(555),
		},
		124: {
			fdfmt:        "%Zd\x00",
			fdinput:      "+\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(1),
			fdwant_upto:  -ppint32(555),
		},
		125: {
			fdfmt:        "xyz%Zd\x00",
			fdinput:      "xyz-\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(4),
			fdwant_upto:  -ppint32(555),
		},
		126: {
			fdfmt:        "xyz%Zd\x00",
			fdinput:      "xyz+\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(4),
			fdwant_upto:  -ppint32(555),
		},
		127: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0x\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(2),
			fdwant_upto:  -ppint32(555),
		},
		128: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0X\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(2),
			fdwant_upto:  -ppint32(555),
		},
		129: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0x-\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(2),
			fdwant_upto:  -ppint32(555),
		},
		130: {
			fdfmt:        "%Zi\x00",
			fdinput:      "0X+\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(2),
			fdwant_upto:  -ppint32(555),
		},
		131: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0x\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(3),
			fdwant_upto:  -ppint32(555),
		},
		132: {
			fdfmt:        "%Zi\x00",
			fdinput:      "-0X\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(3),
			fdwant_upto:  -ppint32(555),
		},
		133: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0x\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(3),
			fdwant_upto:  -ppint32(555),
		},
		134: {
			fdfmt:        "%Zi\x00",
			fdinput:      "+0X\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(3),
			fdwant_upto:  -ppint32(555),
		},
		135: {
			fdfmt:        "%1Zi\x00",
			fdinput:      "1234\x00",
			fdwant:       "1\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(1),
			fdwant_upto:  ppint32(1),
		},
		136: {
			fdfmt:        "%2Zi\x00",
			fdinput:      "1234\x00",
			fdwant:       "12\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(2),
			fdwant_upto:  ppint32(2),
		},
		137: {
			fdfmt:        "%3Zi\x00",
			fdinput:      "1234\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(3),
			fdwant_upto:  ppint32(3),
		},
		138: {
			fdfmt:        "%4Zi\x00",
			fdinput:      "1234\x00",
			fdwant:       "1234\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(4),
			fdwant_upto:  ppint32(4),
		},
		139: {
			fdfmt:        "%5Zi\x00",
			fdinput:      "1234\x00",
			fdwant:       "1234\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(4),
			fdwant_upto:  ppint32(4),
		},
		140: {
			fdfmt:        "%6Zi\x00",
			fdinput:      "1234\x00",
			fdwant:       "1234\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(4),
			fdwant_upto:  ppint32(4),
		},
		141: {
			fdfmt:        "%1Zi\x00",
			fdinput:      "01234\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(1),
			fdwant_upto:  ppint32(1),
		},
		142: {
			fdfmt:        "%2Zi\x00",
			fdinput:      "01234\x00",
			fdwant:       "01\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(2),
			fdwant_upto:  ppint32(2),
		},
		143: {
			fdfmt:        "%3Zi\x00",
			fdinput:      "01234\x00",
			fdwant:       "012\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(3),
			fdwant_upto:  ppint32(3),
		},
		144: {
			fdfmt:        "%4Zi\x00",
			fdinput:      "01234\x00",
			fdwant:       "0123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(4),
			fdwant_upto:  ppint32(4),
		},
		145: {
			fdfmt:        "%5Zi\x00",
			fdinput:      "01234\x00",
			fdwant:       "01234\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(5),
			fdwant_upto:  ppint32(5),
		},
		146: {
			fdfmt:        "%6Zi\x00",
			fdinput:      "01234\x00",
			fdwant:       "01234\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(5),
			fdwant_upto:  ppint32(5),
		},
		147: {
			fdfmt:        "%7Zi\x00",
			fdinput:      "01234\x00",
			fdwant:       "01234\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(5),
			fdwant_upto:  ppint32(5),
		},
		148: {
			fdfmt:        "%1Zi\x00",
			fdinput:      "0x1234\x00",
			fdwant:       "0\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(1),
			fdwant_upto:  ppint32(1),
		},
		149: {
			fdfmt:        "%2Zi\x00",
			fdinput:      "0x1234\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(2),
			fdwant_upto:  -ppint32(555),
		},
		150: {
			fdfmt:        "%3Zi\x00",
			fdinput:      "0x1234\x00",
			fdwant:       "0x1\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(3),
			fdwant_upto:  ppint32(3),
		},
		151: {
			fdfmt:        "%4Zi\x00",
			fdinput:      "0x1234\x00",
			fdwant:       "0x12\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(4),
			fdwant_upto:  ppint32(4),
		},
		152: {
			fdfmt:        "%5Zi\x00",
			fdinput:      "0x1234\x00",
			fdwant:       "0x123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(5),
			fdwant_upto:  ppint32(5),
		},
		153: {
			fdfmt:        "%6Zi\x00",
			fdinput:      "0x1234\x00",
			fdwant:       "0x1234\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(6),
			fdwant_upto:  ppint32(6),
		},
		154: {
			fdfmt:        "%7Zi\x00",
			fdinput:      "0x1234\x00",
			fdwant:       "0x1234\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(6),
			fdwant_upto:  ppint32(6),
		},
		155: {
			fdfmt:        "%8Zi\x00",
			fdinput:      "0x1234\x00",
			fdwant:       "0x1234\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(6),
			fdwant_upto:  ppint32(6),
		},
		156: {
			fdfmt:        "%%xyz%Zd\x00",
			fdinput:      "%xyz123\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		157: {
			fdfmt:        "12%%34%Zd\x00",
			fdinput:      "12%34567\x00",
			fdwant:       "567\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		158: {
			fdfmt:        "%%%%%Zd\x00",
			fdinput:      "%%123\x00",
			fdwant:       "123\x00",
			fdwant_ret:   ppint32(1),
			fdwant_ftell: ppint32(-ppint32(1)),
			fdwant_upto:  -ppint32(1),
		},
		159: {
			fdfmt:       "x\x00",
			fdinput:     "\x00",
			fdwant:      "-999\x00",
			fdwant_ret:  -iqlibc.ppInt32FromInt32(1),
			fdwant_upto: -ppint32(555),
		},
		160: {
			fdfmt:       " x\x00",
			fdinput:     "\x00",
			fdwant:      "-999\x00",
			fdwant_ret:  -iqlibc.ppInt32FromInt32(1),
			fdwant_upto: -ppint32(555),
		},
		161: {
			fdfmt:       "xyz\x00",
			fdinput:     "\x00",
			fdwant:      "-999\x00",
			fdwant_ret:  -iqlibc.ppInt32FromInt32(1),
			fdwant_upto: -ppint32(555),
		},
		162: {
			fdfmt:   " \x00",
			fdinput: "\x00",
			fdwant:  "-999\x00",
		},
		163: {
			fdfmt:        " \x00",
			fdinput:      " \x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(1),
			fdwant_upto:  ppint32(1),
		},
		164: {
			fdfmt:       "%*Zd%Zd\x00",
			fdinput:     "\x00",
			fdwant:      "-999\x00",
			fdwant_ret:  -iqlibc.ppInt32FromInt32(1),
			fdwant_upto: -ppint32(555),
		},
		165: {
			fdfmt:        "%*Zd%Zd\x00",
			fdinput:      "123\x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -iqlibc.ppInt32FromInt32(1),
			fdwant_ftell: ppint32(3),
			fdwant_upto:  -ppint32(555),
		},
		166: {
			fdfmt:        "x\x00",
			fdinput:      "x\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(1),
			fdwant_upto:  ppint32(1),
		},
		167: {
			fdfmt:        "xyz\x00",
			fdinput:      "x\x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -iqlibc.ppInt32FromInt32(1),
			fdwant_ftell: ppint32(1),
			fdwant_upto:  -ppint32(555),
		},
		168: {
			fdfmt:        "xyz\x00",
			fdinput:      "xy\x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -iqlibc.ppInt32FromInt32(1),
			fdwant_ftell: ppint32(2),
			fdwant_upto:  -ppint32(555),
		},
		169: {
			fdfmt:        "xyz\x00",
			fdinput:      "xyz\x00",
			fdwant:       "-999\x00",
			fdwant_ftell: ppint32(3),
			fdwant_upto:  ppint32(3),
		},
		170: {
			fdfmt:   "%Zn\x00",
			fdinput: "\x00",
			fdwant:  "0\x00",
		},
		171: {
			fdfmt:   " %Zn\x00",
			fdinput: "\x00",
			fdwant:  "0\x00",
		},
		172: {
			fdfmt:       " x%Zn\x00",
			fdinput:     "\x00",
			fdwant:      "-999\x00",
			fdwant_ret:  -iqlibc.ppInt32FromInt32(1),
			fdwant_upto: -ppint32(555),
		},
		173: {
			fdfmt:       "xyz%Zn\x00",
			fdinput:     "\x00",
			fdwant:      "-999\x00",
			fdwant_ret:  -iqlibc.ppInt32FromInt32(1),
			fdwant_upto: -ppint32(555),
		},
		174: {
			fdfmt:       " x%Zn\x00",
			fdinput:     "\x00",
			fdwant:      "-999\x00",
			fdwant_ret:  -iqlibc.ppInt32FromInt32(1),
			fdwant_upto: -ppint32(555),
		},
		175: {
			fdfmt:        " %Zn x\x00",
			fdinput:      " \x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -iqlibc.ppInt32FromInt32(1),
			fdwant_ftell: ppint32(1),
			fdwant_upto:  -ppint32(555),
		},
		176: {
			fdfmt:        " x\x00",
			fdinput:      " \x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -iqlibc.ppInt32FromInt32(1),
			fdwant_ftell: ppint32(1),
			fdwant_upto:  -ppint32(555),
			fdnot_glibc:  ppint32(1),
		},
		177: {
			fdfmt:        " xyz\x00",
			fdinput:      " \x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -iqlibc.ppInt32FromInt32(1),
			fdwant_ftell: ppint32(1),
			fdwant_upto:  -ppint32(555),
			fdnot_glibc:  ppint32(1),
		},
		178: {
			fdfmt:        " x%Zn\x00",
			fdinput:      " \x00",
			fdwant:       "-999\x00",
			fdwant_ret:   -iqlibc.ppInt32FromInt32(1),
			fdwant_ftell: ppint32(1),
			fdwant_upto:  -ppint32(555),
			fdnot_glibc:  ppint32(1),
		},
	}
	aaerror = 0

	X__gmpz_init(cgtls, cgbp+4)
	X__gmpz_init(cgtls, cgbp+16)

	aai = 0
	for {
		if !(iqlibc.ppUint32FromInt32(aai) < iqlibc.ppUint32FromInt64(5012)/iqlibc.ppUint32FromInt64(28)) {
			break
		}

		Xmpz_set_str_or_abort(cgtls, cgbp+16, sndata[aai].fdwant, 0)

		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xstrlen(cgtls, sndata[aai].fdfmt)+iqlibc.ppUint32FromInt32(2) < iqlibc.ppUint32FromInt64(128))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(478), "strlen (data[i].fmt) + 2 < sizeof (fmt)\x00")
		}
		Xstrcpy(cgtls, cgbp+32, sndata[aai].fdfmt)
		Xstrcat(cgtls, cgbp+32, "%n\x00")

		aaignore = Xfmt_allignore(cgtls, cgbp+32)

		aaj = 0
		for {
			if !(aaj <= ppint32(3)) {
				break
			}

			aawant_ret = sndata[aai].fdwant_ret

			aawant_ftell = sndata[aai].fdwant_ftell
			if aawant_ftell == ppint32(-ppint32(1)) {
				aawant_ftell = iqlibc.ppInt32FromUint32(Xstrlen(cgtls, sndata[aai].fdinput))
			}

			aawant_upto = sndata[aai].fdwant_upto
			if aawant_upto == -ppint32(1) {
				aawant_upto = iqlibc.ppInt32FromUint32(Xstrlen(cgtls, sndata[aai].fdinput))
			}

			switch aaj {
			case 0:
				goto cg_3
			case ppint32(1):
				goto cg_4
			case ppint32(2):
				goto cg_5
			case ppint32(3):
				goto cg_6
			default:
				goto cg_7
			}
			goto cg_8
		cg_3:
			;
			aaname = "gmp_sscanf\x00"
			aafun = pp__ccgo_fp(Xfun_gmp_sscanf)
			goto cg_8
		cg_4:
			;
			aaname = "gmp_fscanf\x00"
			aafun = pp__ccgo_fp(Xfun_gmp_fscanf)
			goto cg_8
		cg_5:
			;
			if !(Xlibc_scanf_convert(cgtls, cgbp+32) != 0) {
				goto cg_2
			}
			aaname = "standard sscanf\x00"
			aafun = pp__ccgo_fp(Xfun_sscanf)
			goto cg_8
		cg_6:
			;
			if !(Xlibc_scanf_convert(cgtls, cgbp+32) != 0) {
				goto cg_2
			}
			aaname = "standard fscanf\x00"
			aafun = pp__ccgo_fp(Xfun_fscanf)
			goto cg_8
		cg_7:
			;
		cg_11:
			;
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppInt32FromInt32(0) != 0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(526), "0\x00")
			}
			goto cg_10
		cg_10:
			;
			if 0 != 0 {
				goto cg_11
			}
			goto cg_9
		cg_9:
			;
			goto cg_8
		cg_8:
			;

			*(*ppint32)(iqunsafe.ppPointer(cgbp)) = -ppint32(555)
			Xgot_ftell = -ppint32(1)

			switch aaj {
			case ppint32(1):
				goto cg_12
			case 0:
				goto cg_13
			case ppint32(3):
				goto cg_14
			case ppint32(2):
				goto cg_15
			default:
				goto cg_16
			}
			goto cg_17
		cg_13:
			;
		cg_12:
			;
			X__gmpz_set_si(cgtls, cgbp+4, -ppint32(999))
			if aaignore != 0 {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+32, cgbp, iqlibc.ppUintptrFromInt32(0))
			} else {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+32, cgbp+4, cgbp)
			}
			goto cg_17
		cg_15:
			;
		cg_14:
			;
			*(*ppint32)(iqunsafe.ppPointer(cgbp + 28)) = -ppint32(999)
			if aaignore != 0 {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+32, cgbp, iqlibc.ppUintptrFromInt32(0))
			} else {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+32, cgbp+28, cgbp)
			}
			X__gmpz_set_si(cgtls, cgbp+4, *(*ppint32)(iqunsafe.ppPointer(cgbp + 28)))
			goto cg_17
		cg_16:
			;
		cg_20:
			;
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppInt32FromInt32(0) != 0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(552), "0\x00")
			}
			goto cg_19
		cg_19:
			;
			if 0 != 0 {
				goto cg_20
			}
			goto cg_18
		cg_18:
			;
			goto cg_17
		cg_17:
			;

			if ccv22 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size == 0; !ccv22 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size >= 0 {
					ccv21 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
				} else {
					ccv21 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv22 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_d + ppuintptr(ccv21-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(556), "((got)->_mp_size) == 0 || ((got)->_mp_d)[((((got)->_mp_size)) >= 0 ? (((got)->_mp_size)) : -(((got)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size >= 0 {
				ccv23 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
			} else {
				ccv23 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_alloc >= ccv23)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(556), "((got)->_mp_alloc) >= ((((got)->_mp_size)) >= 0 ? (((got)->_mp_size)) : -(((got)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size >= 0 {
						ccv25 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
					} else {
						ccv25 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
					}
					if !(aa__i < ppint32(ccv25)) {
						break
					}
					aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_d + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(556), "__nail == 0\x00")
					}
					goto cg_24
				cg_24:
					;
					aa__i++
				}
			}

			if aagot_ret != aawant_ret {

				Xprintf(cgtls, "%s wrong return value\n\x00", iqlibc.ppVaList(cgbp+168, aaname))
				aaerror = ppint32(1)
			}
			if aawant_ret == ppint32(1) && X__gmpz_cmp(cgtls, cgbp+16, cgbp+4) != 0 {

				Xprintf(cgtls, "%s wrong result\n\x00", iqlibc.ppVaList(cgbp+168, aaname))
				aaerror = ppint32(1)
			}
			if *(*ppint32)(iqunsafe.ppPointer(cgbp)) != aawant_upto {

				Xprintf(cgtls, "%s wrong upto\n\x00", iqlibc.ppVaList(cgbp+168, aaname))
				aaerror = ppint32(1)
			}
			if Xgot_ftell != ppint32(-ppint32(1)) && aawant_ftell != ppint32(-ppint32(1)) && Xgot_ftell != aawant_ftell {

				Xprintf(cgtls, "%s wrong ftell\n\x00", iqlibc.ppVaList(cgbp+168, aaname))
				aaerror = ppint32(1)
			}
			if aaerror != 0 {

				Xprintf(cgtls, "  fmt   \"%s\"\n\x00", iqlibc.ppVaList(cgbp+168, sndata[aai].fdfmt))
				Xprintf(cgtls, "  input \"%s\"\n\x00", iqlibc.ppVaList(cgbp+168, sndata[aai].fdinput))
				Xprintf(cgtls, "  ignore %d\n\x00", iqlibc.ppVaList(cgbp+168, aaignore))
				Xprintf(cgtls, "  ret   want=%d\n\x00", iqlibc.ppVaList(cgbp+168, aawant_ret))
				Xprintf(cgtls, "        got =%d\n\x00", iqlibc.ppVaList(cgbp+168, aagot_ret))
				Xmpz_trace(cgtls, "  value want\x00", cgbp+16)
				Xmpz_trace(cgtls, "        got \x00", cgbp+4)
				Xprintf(cgtls, "  upto  want =%d\n\x00", iqlibc.ppVaList(cgbp+168, aawant_upto))
				Xprintf(cgtls, "        got  =%d\n\x00", iqlibc.ppVaList(cgbp+168, *(*ppint32)(iqunsafe.ppPointer(cgbp))))
				if Xgot_ftell != ppint32(-ppint32(1)) {

					Xprintf(cgtls, "  ftell want =%ld\n\x00", iqlibc.ppVaList(cgbp+168, aawant_ftell))
					Xprintf(cgtls, "        got  =%ld\n\x00", iqlibc.ppVaList(cgbp+168, Xgot_ftell))
				}
				Xabort(cgtls)
			}

			goto cg_2
		cg_2:
			;
			aaj++
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpz_clear(cgtls, cgbp+4)
	X__gmpz_clear(cgtls, cgbp+16)
}

func Xcheck_q(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(208)
	defer cgtls.ppFree(208)

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	var aaerror, aagot_ret, aai, aaignore, aaj, aawant_ftell, aawant_ret, aawant_upto, ccv18, ccv22, ccv24, ccv26, ccv27, ccv29, ccv31 ppint32
	var aafun tnfun_t
	var aaname ppuintptr
	var ccv23, ccv28 ppbool
	var pp_ /* fmt at bp+56 */ [128]ppint8
	var pp_ /* got at bp+4 */ tnmpq_t
	var pp_ /* got_l at bp+52 */ ppint32
	var pp_ /* got_upto at bp+0 */ ppint32
	var pp_ /* want at bp+28 */ tnmpq_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aaerror, aafun, aagot_ret, aai, aaignore, aaj, aaname, aawant_ftell, aawant_ret, aawant_upto, ccv18, ccv22, ccv23, ccv24, ccv26, ccv27, ccv28, ccv29, ccv31
	var sndata = [205]struct {
		fdfmt   ppuintptr
		fdinput ppuintptr
		fdwant  ppuintptr
		fdret   ppint32
		fdftell ppint32
	}{
		0: {
			fdfmt:   "%Qd\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		1: {
			fdfmt:   "%Qd\x00",
			fdinput: "1\x00",
			fdwant:  "1\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		2: {
			fdfmt:   "%Qd\x00",
			fdinput: "123\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		3: {
			fdfmt:   "%Qd\x00",
			fdinput: "+0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		4: {
			fdfmt:   "%Qd\x00",
			fdinput: "+1\x00",
			fdwant:  "1\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		5: {
			fdfmt:   "%Qd\x00",
			fdinput: "+123\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		6: {
			fdfmt:   "%Qd\x00",
			fdinput: "-0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		7: {
			fdfmt:   "%Qd\x00",
			fdinput: "-1\x00",
			fdwant:  "-1\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		8: {
			fdfmt:   "%Qd\x00",
			fdinput: "-123\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		9: {
			fdfmt:   "%Qo\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		10: {
			fdfmt:   "%Qo\x00",
			fdinput: "173\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		11: {
			fdfmt:   "%Qo\x00",
			fdinput: "+0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		12: {
			fdfmt:   "%Qo\x00",
			fdinput: "+173\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		13: {
			fdfmt:   "%Qo\x00",
			fdinput: "-0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		14: {
			fdfmt:   "%Qo\x00",
			fdinput: "-173\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		15: {
			fdfmt:   "%Qx\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		16: {
			fdfmt:   "%Qx\x00",
			fdinput: "7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		17: {
			fdfmt:   "%Qx\x00",
			fdinput: "7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		18: {
			fdfmt:   "%Qx\x00",
			fdinput: "+0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		19: {
			fdfmt:   "%Qx\x00",
			fdinput: "+7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		20: {
			fdfmt:   "%Qx\x00",
			fdinput: "+7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		21: {
			fdfmt:   "%Qx\x00",
			fdinput: "-0\x00",
			fdwant:  "-0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		22: {
			fdfmt:   "%Qx\x00",
			fdinput: "-7b\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		23: {
			fdfmt:   "%Qx\x00",
			fdinput: "-7b\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		24: {
			fdfmt:   "%QX\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		25: {
			fdfmt:   "%QX\x00",
			fdinput: "7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		26: {
			fdfmt:   "%QX\x00",
			fdinput: "7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		27: {
			fdfmt:   "%QX\x00",
			fdinput: "+0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		28: {
			fdfmt:   "%QX\x00",
			fdinput: "+7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		29: {
			fdfmt:   "%QX\x00",
			fdinput: "+7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		30: {
			fdfmt:   "%QX\x00",
			fdinput: "-0\x00",
			fdwant:  "-0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		31: {
			fdfmt:   "%QX\x00",
			fdinput: "-7b\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		32: {
			fdfmt:   "%QX\x00",
			fdinput: "-7b\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		33: {
			fdfmt:   "%Qx\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		34: {
			fdfmt:   "%Qx\x00",
			fdinput: "7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		35: {
			fdfmt:   "%Qx\x00",
			fdinput: "7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		36: {
			fdfmt:   "%Qx\x00",
			fdinput: "+0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		37: {
			fdfmt:   "%Qx\x00",
			fdinput: "+7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		38: {
			fdfmt:   "%Qx\x00",
			fdinput: "+7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		39: {
			fdfmt:   "%Qx\x00",
			fdinput: "-0\x00",
			fdwant:  "-0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		40: {
			fdfmt:   "%Qx\x00",
			fdinput: "-7B\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		41: {
			fdfmt:   "%Qx\x00",
			fdinput: "-7B\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		42: {
			fdfmt:   "%QX\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		43: {
			fdfmt:   "%QX\x00",
			fdinput: "7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		44: {
			fdfmt:   "%QX\x00",
			fdinput: "7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		45: {
			fdfmt:   "%QX\x00",
			fdinput: "+0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		46: {
			fdfmt:   "%QX\x00",
			fdinput: "+7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		47: {
			fdfmt:   "%QX\x00",
			fdinput: "+7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		48: {
			fdfmt:   "%QX\x00",
			fdinput: "-0\x00",
			fdwant:  "-0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		49: {
			fdfmt:   "%QX\x00",
			fdinput: "-7B\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		50: {
			fdfmt:   "%QX\x00",
			fdinput: "-7B\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		51: {
			fdfmt:   "%Qi\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		52: {
			fdfmt:   "%Qi\x00",
			fdinput: "1\x00",
			fdwant:  "1\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		53: {
			fdfmt:   "%Qi\x00",
			fdinput: "123\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		54: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		55: {
			fdfmt:   "%Qi\x00",
			fdinput: "+1\x00",
			fdwant:  "1\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		56: {
			fdfmt:   "%Qi\x00",
			fdinput: "+123\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		57: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		58: {
			fdfmt:   "%Qi\x00",
			fdinput: "-1\x00",
			fdwant:  "-1\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		59: {
			fdfmt:   "%Qi\x00",
			fdinput: "-123\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		60: {
			fdfmt:   "%Qi\x00",
			fdinput: "00\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		61: {
			fdfmt:   "%Qi\x00",
			fdinput: "0173\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		62: {
			fdfmt:   "%Qi\x00",
			fdinput: "+00\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		63: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0173\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		64: {
			fdfmt:   "%Qi\x00",
			fdinput: "-00\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		65: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0173\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		66: {
			fdfmt:   "%Qi\x00",
			fdinput: "0x0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		67: {
			fdfmt:   "%Qi\x00",
			fdinput: "0x7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		68: {
			fdfmt:   "%Qi\x00",
			fdinput: "0x7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		69: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0x0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		70: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0x7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		71: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0x7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		72: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0x0\x00",
			fdwant:  "-0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		73: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0x7b\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		74: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0x7b\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		75: {
			fdfmt:   "%Qi\x00",
			fdinput: "0X0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		76: {
			fdfmt:   "%Qi\x00",
			fdinput: "0X7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		77: {
			fdfmt:   "%Qi\x00",
			fdinput: "0X7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		78: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0X0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		79: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0X7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		80: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0X7b\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		81: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0X0\x00",
			fdwant:  "-0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		82: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0X7b\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		83: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0X7b\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		84: {
			fdfmt:   "%Qi\x00",
			fdinput: "0x0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		85: {
			fdfmt:   "%Qi\x00",
			fdinput: "0x7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		86: {
			fdfmt:   "%Qi\x00",
			fdinput: "0x7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		87: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0x0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		88: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0x7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		89: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0x7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		90: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0x0\x00",
			fdwant:  "-0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		91: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0x7B\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		92: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0x7B\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		93: {
			fdfmt:   "%Qi\x00",
			fdinput: "0X0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		94: {
			fdfmt:   "%Qi\x00",
			fdinput: "0X7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		95: {
			fdfmt:   "%Qi\x00",
			fdinput: "0X7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		96: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0X0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		97: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0X7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		98: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0X7B\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		99: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0X0\x00",
			fdwant:  "-0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		100: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0X7B\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		101: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0X7B\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		102: {
			fdfmt:   "%Qd\x00",
			fdinput: " 0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		103: {
			fdfmt:   "%Qd\x00",
			fdinput: "  0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		104: {
			fdfmt:   "%Qd\x00",
			fdinput: "   0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		105: {
			fdfmt:   "%Qd\x00",
			fdinput: "\t0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		106: {
			fdfmt:   "%Qd\x00",
			fdinput: "\t\t0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		107: {
			fdfmt:   "%Qd\x00",
			fdinput: "3/2\x00",
			fdwant:  "3/2\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		108: {
			fdfmt:   "%Qd\x00",
			fdinput: "+3/2\x00",
			fdwant:  "3/2\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		109: {
			fdfmt:   "%Qd\x00",
			fdinput: "-3/2\x00",
			fdwant:  "-3/2\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		110: {
			fdfmt:   "%Qx\x00",
			fdinput: "f/10\x00",
			fdwant:  "15/16\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		111: {
			fdfmt:   "%Qx\x00",
			fdinput: "F/10\x00",
			fdwant:  "15/16\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		112: {
			fdfmt:   "%QX\x00",
			fdinput: "f/10\x00",
			fdwant:  "15/16\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		113: {
			fdfmt:   "%QX\x00",
			fdinput: "F/10\x00",
			fdwant:  "15/16\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		114: {
			fdfmt:   "%Qo\x00",
			fdinput: "20/21\x00",
			fdwant:  "16/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		115: {
			fdfmt:   "%Qo\x00",
			fdinput: "-20/21\x00",
			fdwant:  "-16/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		116: {
			fdfmt:   "%Qi\x00",
			fdinput: "10/11\x00",
			fdwant:  "10/11\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		117: {
			fdfmt:   "%Qi\x00",
			fdinput: "+10/11\x00",
			fdwant:  "10/11\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		118: {
			fdfmt:   "%Qi\x00",
			fdinput: "-10/11\x00",
			fdwant:  "-10/11\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		119: {
			fdfmt:   "%Qi\x00",
			fdinput: "010/11\x00",
			fdwant:  "8/11\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		120: {
			fdfmt:   "%Qi\x00",
			fdinput: "+010/11\x00",
			fdwant:  "8/11\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		121: {
			fdfmt:   "%Qi\x00",
			fdinput: "-010/11\x00",
			fdwant:  "-8/11\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		122: {
			fdfmt:   "%Qi\x00",
			fdinput: "0x10/11\x00",
			fdwant:  "16/11\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		123: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0x10/11\x00",
			fdwant:  "16/11\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		124: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0x10/11\x00",
			fdwant:  "-16/11\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		125: {
			fdfmt:   "%Qi\x00",
			fdinput: "10/011\x00",
			fdwant:  "10/9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		126: {
			fdfmt:   "%Qi\x00",
			fdinput: "+10/011\x00",
			fdwant:  "10/9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		127: {
			fdfmt:   "%Qi\x00",
			fdinput: "-10/011\x00",
			fdwant:  "-10/9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		128: {
			fdfmt:   "%Qi\x00",
			fdinput: "010/011\x00",
			fdwant:  "8/9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		129: {
			fdfmt:   "%Qi\x00",
			fdinput: "+010/011\x00",
			fdwant:  "8/9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		130: {
			fdfmt:   "%Qi\x00",
			fdinput: "-010/011\x00",
			fdwant:  "-8/9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		131: {
			fdfmt:   "%Qi\x00",
			fdinput: "0x10/011\x00",
			fdwant:  "16/9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		132: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0x10/011\x00",
			fdwant:  "16/9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		133: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0x10/011\x00",
			fdwant:  "-16/9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		134: {
			fdfmt:   "%Qi\x00",
			fdinput: "10/0x11\x00",
			fdwant:  "10/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		135: {
			fdfmt:   "%Qi\x00",
			fdinput: "+10/0x11\x00",
			fdwant:  "10/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		136: {
			fdfmt:   "%Qi\x00",
			fdinput: "-10/0x11\x00",
			fdwant:  "-10/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		137: {
			fdfmt:   "%Qi\x00",
			fdinput: "010/0x11\x00",
			fdwant:  "8/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		138: {
			fdfmt:   "%Qi\x00",
			fdinput: "+010/0x11\x00",
			fdwant:  "8/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		139: {
			fdfmt:   "%Qi\x00",
			fdinput: "-010/0x11\x00",
			fdwant:  "-8/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		140: {
			fdfmt:   "%Qi\x00",
			fdinput: "0x10/0x11\x00",
			fdwant:  "16/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		141: {
			fdfmt:   "%Qi\x00",
			fdinput: "+0x10/0x11\x00",
			fdwant:  "16/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		142: {
			fdfmt:   "%Qi\x00",
			fdinput: "-0x10/0x11\x00",
			fdwant:  "-16/17\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		143: {
			fdfmt:   "hello%Qd\x00",
			fdinput: "hello0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		144: {
			fdfmt:   "hello%Qd\x00",
			fdinput: "hello 0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		145: {
			fdfmt:   "hello%Qd\x00",
			fdinput: "hello \t0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		146: {
			fdfmt:   "hello%Qdworld\x00",
			fdinput: "hello 0world\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		147: {
			fdfmt:   "hello%Qd\x00",
			fdinput: "hello3/2\x00",
			fdwant:  "3/2\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		148: {
			fdfmt:   "hello%*Qd\x00",
			fdinput: "hello0\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		149: {
			fdfmt:   "hello%*Qd\x00",
			fdinput: "hello 0\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		150: {
			fdfmt:   "hello%*Qd\x00",
			fdinput: "hello \t0\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		151: {
			fdfmt:   "hello%*Qdworld\x00",
			fdinput: "hello 0world\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		152: {
			fdfmt:   "hello%*Qdworld\x00",
			fdinput: "hello3/2world\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		153: {
			fdfmt:   "%Qd\x00",
			fdinput: "\x00",
			fdwant:  "-999/121\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		154: {
			fdfmt:   "%Qd\x00",
			fdinput: " \x00",
			fdwant:  "-999/121\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		155: {
			fdfmt:   " %Qd\x00",
			fdinput: "\x00",
			fdwant:  "-999/121\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		156: {
			fdfmt:   "xyz%Qd\x00",
			fdinput: "\x00",
			fdwant:  "-999/121\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		157: {
			fdfmt:   "%*Qd\x00",
			fdinput: "\x00",
			fdwant:  "-999/121\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		158: {
			fdfmt:   " %*Qd\x00",
			fdinput: "\x00",
			fdwant:  "-999/121\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		159: {
			fdfmt:   "xyz%*Qd\x00",
			fdinput: "\x00",
			fdwant:  "-999/121\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		160: {
			fdfmt:   "%Qd\x00",
			fdinput: "-\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		161: {
			fdfmt:   "%Qd\x00",
			fdinput: "+\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		162: {
			fdfmt:   "%Qd\x00",
			fdinput: "/-\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		163: {
			fdfmt:   "%Qd\x00",
			fdinput: "/+\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		164: {
			fdfmt:   "%Qd\x00",
			fdinput: "-/\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		165: {
			fdfmt:   "%Qd\x00",
			fdinput: "+/\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		166: {
			fdfmt:   "%Qd\x00",
			fdinput: "-/-\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		167: {
			fdfmt:   "%Qd\x00",
			fdinput: "-/+\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		168: {
			fdfmt:   "%Qd\x00",
			fdinput: "+/+\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		169: {
			fdfmt:   "%Qd\x00",
			fdinput: "/123\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		170: {
			fdfmt:   "%Qd\x00",
			fdinput: "-/123\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		171: {
			fdfmt:   "%Qd\x00",
			fdinput: "+/123\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		172: {
			fdfmt:   "%Qd\x00",
			fdinput: "123/\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		173: {
			fdfmt:   "%Qd\x00",
			fdinput: "123/-\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		174: {
			fdfmt:   "%Qd\x00",
			fdinput: "123/+\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(1),
		},
		175: {
			fdfmt:   "xyz%Qd\x00",
			fdinput: "xyz-\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(4),
		},
		176: {
			fdfmt:   "xyz%Qd\x00",
			fdinput: "xyz+\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(4),
		},
		177: {
			fdfmt:   "%1Qi\x00",
			fdinput: "12/57\x00",
			fdwant:  "1\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(1),
		},
		178: {
			fdfmt:   "%2Qi\x00",
			fdinput: "12/57\x00",
			fdwant:  "12\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(2),
		},
		179: {
			fdfmt:   "%3Qi\x00",
			fdinput: "12/57\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		180: {
			fdfmt:   "%4Qi\x00",
			fdinput: "12/57\x00",
			fdwant:  "12/5\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		181: {
			fdfmt:   "%5Qi\x00",
			fdinput: "12/57\x00",
			fdwant:  "12/57\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(5),
		},
		182: {
			fdfmt:   "%6Qi\x00",
			fdinput: "12/57\x00",
			fdwant:  "12/57\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(5),
		},
		183: {
			fdfmt:   "%7Qi\x00",
			fdinput: "12/57\x00",
			fdwant:  "12/57\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(5),
		},
		184: {
			fdfmt:   "%1Qi\x00",
			fdinput: "012/057\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(1),
		},
		185: {
			fdfmt:   "%2Qi\x00",
			fdinput: "012/057\x00",
			fdwant:  "01\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(2),
		},
		186: {
			fdfmt:   "%3Qi\x00",
			fdinput: "012/057\x00",
			fdwant:  "012\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(3),
		},
		187: {
			fdfmt:   "%4Qi\x00",
			fdinput: "012/057\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		188: {
			fdfmt:   "%5Qi\x00",
			fdinput: "012/057\x00",
			fdwant:  "012/0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(5),
		},
		189: {
			fdfmt:   "%6Qi\x00",
			fdinput: "012/057\x00",
			fdwant:  "012/5\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(6),
		},
		190: {
			fdfmt:   "%7Qi\x00",
			fdinput: "012/057\x00",
			fdwant:  "012/057\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(7),
		},
		191: {
			fdfmt:   "%8Qi\x00",
			fdinput: "012/057\x00",
			fdwant:  "012/057\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(7),
		},
		192: {
			fdfmt:   "%9Qi\x00",
			fdinput: "012/057\x00",
			fdwant:  "012/057\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(7),
		},
		193: {
			fdfmt:   "%1Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(1),
		},
		194: {
			fdfmt:   "%2Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		195: {
			fdfmt:   "%3Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "0x1\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(3),
		},
		196: {
			fdfmt:   "%4Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "0x12\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		197: {
			fdfmt:   "%5Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(5),
		},
		198: {
			fdfmt:   "%6Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "0x12/0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(6),
		},
		199: {
			fdfmt:   "%7Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "-999/121\x00",
			fdftell: ppint32(7),
		},
		200: {
			fdfmt:   "%8Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "0x12/0x5\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(8),
		},
		201: {
			fdfmt:   "%9Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "0x12/0x57\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(9),
		},
		202: {
			fdfmt:   "%10Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "0x12/0x57\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(9),
		},
		203: {
			fdfmt:   "%11Qi\x00",
			fdinput: "0x12/0x57\x00",
			fdwant:  "0x12/0x57\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(9),
		},
		204: {
			fdfmt:   "%Qd\x00",
			fdinput: "xyz\x00",
			fdwant:  "0\x00",
		},
	}
	aaerror = 0

	X__gmpq_init(cgtls, cgbp+4)
	X__gmpq_init(cgtls, cgbp+28)

	aai = 0
	for {
		if !(iqlibc.ppUint32FromInt32(aai) < iqlibc.ppUint32FromInt64(4100)/iqlibc.ppUint32FromInt64(20)) {
			break
		}

		Xmpq_set_str_or_abort(cgtls, cgbp+28, sndata[aai].fdwant, 0)

		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xstrlen(cgtls, sndata[aai].fdfmt)+iqlibc.ppUint32FromInt32(2) < iqlibc.ppUint32FromInt64(128))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(859), "strlen (data[i].fmt) + 2 < sizeof (fmt)\x00")
		}
		Xstrcpy(cgtls, cgbp+56, sndata[aai].fdfmt)
		Xstrcat(cgtls, cgbp+56, "%n\x00")

		aaignore = iqlibc.ppBoolInt32(Xstrchr(cgtls, cgbp+56, ppint32('*')) != iqlibc.ppUintptrFromInt32(0))

		aaj = 0
		for {
			if !(aaj <= ppint32(3)) {
				break
			}

			aawant_ret = sndata[aai].fdret

			aawant_ftell = sndata[aai].fdftell
			if aawant_ftell == ppint32(-ppint32(1)) {
				aawant_ftell = iqlibc.ppInt32FromUint32(Xstrlen(cgtls, sndata[aai].fdinput))
			}
			aawant_upto = ppint32(aawant_ftell)

			if aawant_ret == -ppint32(1) || aawant_ret == 0 && !(aaignore != 0) {

				aawant_ftell = ppint32(-ppint32(1))
				aawant_upto = -ppint32(555)
			}

			switch aaj {
			case 0:
				goto cg_3
			case ppint32(1):
				goto cg_4
			case ppint32(2):
				goto cg_5
			case ppint32(3):
				goto cg_6
			default:
				goto cg_7
			}
			goto cg_8
		cg_3:
			;
			aaname = "gmp_sscanf\x00"
			aafun = pp__ccgo_fp(Xfun_gmp_sscanf)
			goto cg_8
		cg_4:
			;
			aaname = "gmp_fscanf\x00"
			aafun = pp__ccgo_fp(Xfun_gmp_fscanf)
			goto cg_8
		cg_5:
			;
			if Xstrchr(cgtls, sndata[aai].fdinput, ppint32('/')) != iqlibc.ppUintptrFromInt32(0) {
				goto cg_2
			}
			if !(Xlibc_scanf_convert(cgtls, cgbp+56) != 0) {
				goto cg_2
			}
			aaname = "standard sscanf\x00"
			aafun = pp__ccgo_fp(Xfun_sscanf)
			goto cg_8
		cg_6:
			;
			if Xstrchr(cgtls, sndata[aai].fdinput, ppint32('/')) != iqlibc.ppUintptrFromInt32(0) {
				goto cg_2
			}
			if !(Xlibc_scanf_convert(cgtls, cgbp+56) != 0) {
				goto cg_2
			}
			aaname = "standard fscanf\x00"
			aafun = pp__ccgo_fp(Xfun_fscanf)
			goto cg_8
		cg_7:
			;
		cg_11:
			;
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppInt32FromInt32(0) != 0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(906), "0\x00")
			}
			goto cg_10
		cg_10:
			;
			if 0 != 0 {
				goto cg_11
			}
			goto cg_9
		cg_9:
			;
			goto cg_8
		cg_8:
			;

			*(*ppint32)(iqunsafe.ppPointer(cgbp)) = -ppint32(555)
			Xgot_ftell = ppint32(-ppint32(1))

			switch aaj {
			case ppint32(1):
				goto cg_12
			case 0:
				goto cg_13
			case ppint32(3):
				goto cg_14
			case ppint32(2):
				goto cg_15
			default:
				goto cg_16
			}
			goto cg_17
		cg_13:
			;
		cg_12:
			;
			X__gmpq_set_si(cgtls, cgbp+4, -ppint32(999), ppuint32(121))
			if aaignore != 0 {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+56, cgbp, iqlibc.ppUintptrFromInt32(0))
			} else {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+56, cgbp+4, cgbp)
			}
			goto cg_17
		cg_15:
			;
		cg_14:
			;
			*(*ppint32)(iqunsafe.ppPointer(cgbp + 52)) = -ppint32(999)
			if aaignore != 0 {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+56, cgbp, iqlibc.ppUintptrFromInt32(0))
			} else {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+56, cgbp+52, cgbp)
			}
			if *(*ppint32)(iqunsafe.ppPointer(cgbp + 52)) == -ppint32(999) {
				ccv18 = ppint32(121)
			} else {
				ccv18 = ppint32(1)
			}
			X__gmpq_set_si(cgtls, cgbp+4, *(*ppint32)(iqunsafe.ppPointer(cgbp + 52)), iqlibc.ppUint32FromInt32(ccv18))
			goto cg_17
		cg_16:
			;
		cg_21:
			;
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppInt32FromInt32(0) != 0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(932), "0\x00")
			}
			goto cg_20
		cg_20:
			;
			if 0 != 0 {
				goto cg_21
			}
			goto cg_19
		cg_19:
			;
			goto cg_17
		cg_17:
			;

			if ccv23 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size == 0; !ccv23 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size >= 0 {
					ccv22 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
				} else {
					ccv22 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv23 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_d + ppuintptr(ccv22-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(936), "(((&((got)->_mp_num)))->_mp_size) == 0 || (((&((got)->_mp_num)))->_mp_d)[(((((&((got)->_mp_num)))->_mp_size)) >= 0 ? ((((&((got)->_mp_num)))->_mp_size)) : -((((&((got)->_mp_num)))->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size >= 0 {
				ccv24 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
			} else {
				ccv24 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_alloc >= ccv24)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(936), "(((&((got)->_mp_num)))->_mp_alloc) >= (((((&((got)->_mp_num)))->_mp_size)) >= 0 ? ((((&((got)->_mp_num)))->_mp_size)) : -((((&((got)->_mp_num)))->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size >= 0 {
						ccv26 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
					} else {
						ccv26 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
					}
					if !(aa__i < ppint32(ccv26)) {
						break
					}
					aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_d + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(936), "__nail == 0\x00")
					}
					goto cg_25
				cg_25:
					;
					aa__i++
				}
			}

			if ccv28 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4+12)).fd_mp_size == 0; !ccv28 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4+12)).fd_mp_size >= 0 {
					ccv27 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4 + 12)).fd_mp_size
				} else {
					ccv27 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4 + 12)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv28 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4+12)).fd_mp_d + ppuintptr(ccv27-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(937), "(((&((got)->_mp_den)))->_mp_size) == 0 || (((&((got)->_mp_den)))->_mp_d)[(((((&((got)->_mp_den)))->_mp_size)) >= 0 ? ((((&((got)->_mp_den)))->_mp_size)) : -((((&((got)->_mp_den)))->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4+12)).fd_mp_size >= 0 {
				ccv29 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4 + 12)).fd_mp_size
			} else {
				ccv29 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4 + 12)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4+12)).fd_mp_alloc >= ccv29)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(937), "(((&((got)->_mp_den)))->_mp_alloc) >= (((((&((got)->_mp_den)))->_mp_size)) >= 0 ? ((((&((got)->_mp_den)))->_mp_size)) : -((((&((got)->_mp_den)))->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i1 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4+12)).fd_mp_size >= 0 {
						ccv31 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4 + 12)).fd_mp_size
					} else {
						ccv31 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 4 + 12)).fd_mp_size
					}
					if !(aa__i1 < ppint32(ccv31)) {
						break
					}
					aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+4+12)).fd_mp_d + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(937), "__nail == 0\x00")
					}
					goto cg_30
				cg_30:
					;
					aa__i1++
				}
			}

			if aagot_ret != aawant_ret {

				Xprintf(cgtls, "%s wrong return value\n\x00", iqlibc.ppVaList(cgbp+192, aaname))
				aaerror = ppint32(1)
			}
			/* use direct mpz compares, since some of the test data is
			   non-canonical and can trip ASSERTs in mpq_equal */
			if aawant_ret == ppint32(1) && !(X__gmpz_cmp(cgtls, cgbp+28, cgbp+4) == 0 && X__gmpz_cmp(cgtls, cgbp+28+12, cgbp+4+12) == 0) {

				Xprintf(cgtls, "%s wrong result\n\x00", iqlibc.ppVaList(cgbp+192, aaname))
				aaerror = ppint32(1)
			}
			if *(*ppint32)(iqunsafe.ppPointer(cgbp)) != aawant_upto {

				Xprintf(cgtls, "%s wrong upto\n\x00", iqlibc.ppVaList(cgbp+192, aaname))
				aaerror = ppint32(1)
			}
			if Xgot_ftell != ppint32(-ppint32(1)) && aawant_ftell != ppint32(-ppint32(1)) && Xgot_ftell != aawant_ftell {

				Xprintf(cgtls, "%s wrong ftell\n\x00", iqlibc.ppVaList(cgbp+192, aaname))
				aaerror = ppint32(1)
			}
			if aaerror != 0 {

				Xprintf(cgtls, "  fmt   \"%s\"\n\x00", iqlibc.ppVaList(cgbp+192, sndata[aai].fdfmt))
				Xprintf(cgtls, "  input \"%s\"\n\x00", iqlibc.ppVaList(cgbp+192, sndata[aai].fdinput))
				Xprintf(cgtls, "  ret   want=%d\n\x00", iqlibc.ppVaList(cgbp+192, aawant_ret))
				Xprintf(cgtls, "        got =%d\n\x00", iqlibc.ppVaList(cgbp+192, aagot_ret))
				Xmpq_trace(cgtls, "  value want\x00", cgbp+28)
				Xmpq_trace(cgtls, "        got \x00", cgbp+4)
				Xprintf(cgtls, "  upto  want=%d\n\x00", iqlibc.ppVaList(cgbp+192, aawant_upto))
				Xprintf(cgtls, "        got =%d\n\x00", iqlibc.ppVaList(cgbp+192, *(*ppint32)(iqunsafe.ppPointer(cgbp))))
				if Xgot_ftell != ppint32(-ppint32(1)) {

					Xprintf(cgtls, "  ftell want =%ld\n\x00", iqlibc.ppVaList(cgbp+192, aawant_ftell))
					Xprintf(cgtls, "        got  =%ld\n\x00", iqlibc.ppVaList(cgbp+192, Xgot_ftell))
				}
				Xabort(cgtls)
			}

			goto cg_2
		cg_2:
			;
			aaj++
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpq_clear(cgtls, cgbp+4)
	X__gmpq_clear(cgtls, cgbp+28)
}

func Xcheck_f(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(192)
	defer cgtls.ppFree(192)

	var aaerror, aagot_ret, aai, aaignore, aaj, aawant_ftell, aawant_ret, aawant_upto, ccv21, ccv22 ppint32
	var aafun tnfun_t
	var aaname ppuintptr
	var pp_ /* fmt at bp+48 */ [128]ppint8
	var pp_ /* got at bp+4 */ tnmpf_t
	var pp_ /* got_d at bp+40 */ ppfloat64
	var pp_ /* got_upto at bp+0 */ ppint32
	var pp_ /* want at bp+20 */ tnmpf_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaerror, aafun, aagot_ret, aai, aaignore, aaj, aaname, aawant_ftell, aawant_ret, aawant_upto, ccv21, ccv22
	var sndata = [135]struct {
		fdfmt   ppuintptr
		fdinput ppuintptr
		fdwant  ppuintptr
		fdret   ppint32
		fdftell ppint32
	}{
		0: {
			fdfmt:   "%Ff\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		1: {
			fdfmt:   "%Fe\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		2: {
			fdfmt:   "%FE\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		3: {
			fdfmt:   "%Fg\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		4: {
			fdfmt:   "%FG\x00",
			fdinput: "0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		5: {
			fdfmt:   "%Ff\x00",
			fdinput: "123\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		6: {
			fdfmt:   "%Ff\x00",
			fdinput: "+123\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		7: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		8: {
			fdfmt:   "%Ff\x00",
			fdinput: "123.\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		9: {
			fdfmt:   "%Ff\x00",
			fdinput: "+123.\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		10: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123.\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		11: {
			fdfmt:   "%Ff\x00",
			fdinput: "123.0\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		12: {
			fdfmt:   "%Ff\x00",
			fdinput: "+123.0\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		13: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123.0\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		14: {
			fdfmt:   "%Ff\x00",
			fdinput: "0123\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		15: {
			fdfmt:   "%Ff\x00",
			fdinput: "-0123\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		16: {
			fdfmt:   "%Ff\x00",
			fdinput: "123.456e3\x00",
			fdwant:  "123456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		17: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123.456e3\x00",
			fdwant:  "-123456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		18: {
			fdfmt:   "%Ff\x00",
			fdinput: "123.456e+3\x00",
			fdwant:  "123456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		19: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123.456e+3\x00",
			fdwant:  "-123456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		20: {
			fdfmt:   "%Ff\x00",
			fdinput: "123000e-3\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		21: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123000e-3\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		22: {
			fdfmt:   "%Ff\x00",
			fdinput: "123000.e-3\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		23: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123000.e-3\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		24: {
			fdfmt:   "%Ff\x00",
			fdinput: "123.456E3\x00",
			fdwant:  "123456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		25: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123.456E3\x00",
			fdwant:  "-123456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		26: {
			fdfmt:   "%Ff\x00",
			fdinput: "123.456E+3\x00",
			fdwant:  "123456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		27: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123.456E+3\x00",
			fdwant:  "-123456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		28: {
			fdfmt:   "%Ff\x00",
			fdinput: "123000E-3\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		29: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123000E-3\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		30: {
			fdfmt:   "%Ff\x00",
			fdinput: "123000.E-3\x00",
			fdwant:  "123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		31: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123000.E-3\x00",
			fdwant:  "-123\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		32: {
			fdfmt:   "%Ff\x00",
			fdinput: ".456e3\x00",
			fdwant:  "456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		33: {
			fdfmt:   "%Ff\x00",
			fdinput: "-.456e3\x00",
			fdwant:  "-456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		34: {
			fdfmt:   "%Ff\x00",
			fdinput: ".456e+3\x00",
			fdwant:  "456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		35: {
			fdfmt:   "%Ff\x00",
			fdinput: "-.456e+3\x00",
			fdwant:  "-456\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		36: {
			fdfmt:   "%Ff\x00",
			fdinput: " 0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		37: {
			fdfmt:   "%Ff\x00",
			fdinput: "  0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		38: {
			fdfmt:   "%Ff\x00",
			fdinput: "   0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		39: {
			fdfmt:   "%Ff\x00",
			fdinput: "\t0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		40: {
			fdfmt:   "%Ff\x00",
			fdinput: "\t\t0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		41: {
			fdfmt:   "hello%Fg\x00",
			fdinput: "hello0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		42: {
			fdfmt:   "hello%Fg\x00",
			fdinput: "hello 0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		43: {
			fdfmt:   "hello%Fg\x00",
			fdinput: "hello \t0\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		44: {
			fdfmt:   "hello%Fgworld\x00",
			fdinput: "hello 0world\x00",
			fdwant:  "0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		45: {
			fdfmt:   "hello%Fg\x00",
			fdinput: "hello3.0\x00",
			fdwant:  "3.0\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		46: {
			fdfmt:   "hello%*Fg\x00",
			fdinput: "hello0\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		47: {
			fdfmt:   "hello%*Fg\x00",
			fdinput: "hello 0\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		48: {
			fdfmt:   "hello%*Fg\x00",
			fdinput: "hello \t0\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		49: {
			fdfmt:   "hello%*Fgworld\x00",
			fdinput: "hello 0world\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		50: {
			fdfmt:   "hello%*Fgworld\x00",
			fdinput: "hello3.0world\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(-ppint32(1)),
		},
		51: {
			fdfmt:   "%Ff\x00",
			fdinput: "\x00",
			fdwant:  "-999\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		52: {
			fdfmt:   "%Ff\x00",
			fdinput: " \x00",
			fdwant:  "-999\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		53: {
			fdfmt:   "%Ff\x00",
			fdinput: "\t\x00",
			fdwant:  "-999\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		54: {
			fdfmt:   "%Ff\x00",
			fdinput: " \t\x00",
			fdwant:  "-999\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		55: {
			fdfmt:   " %Ff\x00",
			fdinput: "\x00",
			fdwant:  "-999\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		56: {
			fdfmt:   "xyz%Ff\x00",
			fdinput: "\x00",
			fdwant:  "-999\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		57: {
			fdfmt:   "%*Ff\x00",
			fdinput: "\x00",
			fdwant:  "-999\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		58: {
			fdfmt:   " %*Ff\x00",
			fdinput: "\x00",
			fdwant:  "-999\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		59: {
			fdfmt:   "xyz%*Ff\x00",
			fdinput: "\x00",
			fdwant:  "-999\x00",
			fdret:   -ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		60: {
			fdfmt:   "%Ff\x00",
			fdinput: "xyz\x00",
			fdwant:  "0\x00",
		},
		61: {
			fdfmt:   "%Ff\x00",
			fdinput: "-\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(1),
		},
		62: {
			fdfmt:   "%Ff\x00",
			fdinput: "+\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(1),
		},
		63: {
			fdfmt:   "xyz%Ff\x00",
			fdinput: "xyz-\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		64: {
			fdfmt:   "xyz%Ff\x00",
			fdinput: "xyz+\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		65: {
			fdfmt:   "%Ff\x00",
			fdinput: "-.\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		66: {
			fdfmt:   "%Ff\x00",
			fdinput: "+.\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		67: {
			fdfmt:   "%Ff\x00",
			fdinput: ".e\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(1),
		},
		68: {
			fdfmt:   "%Ff\x00",
			fdinput: "-.e\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		69: {
			fdfmt:   "%Ff\x00",
			fdinput: "+.e\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		70: {
			fdfmt:   "%Ff\x00",
			fdinput: ".E\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(1),
		},
		71: {
			fdfmt:   "%Ff\x00",
			fdinput: "-.E\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		72: {
			fdfmt:   "%Ff\x00",
			fdinput: "+.E\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		73: {
			fdfmt:   "%Ff\x00",
			fdinput: ".e123\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(1),
		},
		74: {
			fdfmt:   "%Ff\x00",
			fdinput: "-.e123\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		75: {
			fdfmt:   "%Ff\x00",
			fdinput: "+.e123\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		76: {
			fdfmt:   "%Ff\x00",
			fdinput: "123e\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		77: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123e\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(5),
		},
		78: {
			fdfmt:   "%Ff\x00",
			fdinput: "123e-\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(5),
		},
		79: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123e-\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(6),
		},
		80: {
			fdfmt:   "%Ff\x00",
			fdinput: "123e+\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(5),
		},
		81: {
			fdfmt:   "%Ff\x00",
			fdinput: "-123e+\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(6),
		},
		82: {
			fdfmt:   "%Ff\x00",
			fdinput: "123e-Z\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(5),
		},
		83: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x123p0\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		84: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x123P0\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		85: {
			fdfmt:   "%Ff\x00",
			fdinput: "0X123p0\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		86: {
			fdfmt:   "%Ff\x00",
			fdinput: "0X123P0\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		87: {
			fdfmt:   "%Ff\x00",
			fdinput: "-0x123p0\x00",
			fdwant:  "-291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		88: {
			fdfmt:   "%Ff\x00",
			fdinput: "+0x123p0\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		89: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x123.p0\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		90: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x12.3p4\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		91: {
			fdfmt:   "%Ff\x00",
			fdinput: "-0x12.3p4\x00",
			fdwant:  "-291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		92: {
			fdfmt:   "%Ff\x00",
			fdinput: "+0x12.3p4\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		93: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x1230p-4\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		94: {
			fdfmt:   "%Ff\x00",
			fdinput: "-0x1230p-4\x00",
			fdwant:  "-291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		95: {
			fdfmt:   "%Ff\x00",
			fdinput: "+0x1230p-4\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		96: {
			fdfmt:   "%Ff\x00",
			fdinput: "+0x.1230p12\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		97: {
			fdfmt:   "%Ff\x00",
			fdinput: "+0x123000p-12\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		98: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x123 p12\x00",
			fdwant:  "291\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(5),
		},
		99: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x9 9\x00",
			fdwant:  "9\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(3),
		},
		100: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x01\x00",
			fdwant:  "1\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		101: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x23\x00",
			fdwant:  "35\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		102: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x45\x00",
			fdwant:  "69\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		103: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x67\x00",
			fdwant:  "103\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		104: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x89\x00",
			fdwant:  "137\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		105: {
			fdfmt:   "%Ff\x00",
			fdinput: "0xAB\x00",
			fdwant:  "171\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		106: {
			fdfmt:   "%Ff\x00",
			fdinput: "0xCD\x00",
			fdwant:  "205\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		107: {
			fdfmt:   "%Ff\x00",
			fdinput: "0xEF\x00",
			fdwant:  "239\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		108: {
			fdfmt:   "%Ff\x00",
			fdinput: "0xab\x00",
			fdwant:  "171\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		109: {
			fdfmt:   "%Ff\x00",
			fdinput: "0xcd\x00",
			fdwant:  "205\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		110: {
			fdfmt:   "%Ff\x00",
			fdinput: "0xef\x00",
			fdwant:  "239\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(4),
		},
		111: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x100p0A\x00",
			fdwant:  "256\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(7),
		},
		112: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x1p9\x00",
			fdwant:  "512\x00",
			fdret:   ppint32(1),
			fdftell: ppint32(-ppint32(1)),
		},
		113: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		114: {
			fdfmt:   "%Ff\x00",
			fdinput: "-0x\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(3),
		},
		115: {
			fdfmt:   "%Ff\x00",
			fdinput: "+0x\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(3),
		},
		116: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x-\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		117: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x+\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		118: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x.\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(3),
		},
		119: {
			fdfmt:   "%Ff\x00",
			fdinput: "-0x.\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		120: {
			fdfmt:   "%Ff\x00",
			fdinput: "+0x.\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		121: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x.p\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(3),
		},
		122: {
			fdfmt:   "%Ff\x00",
			fdinput: "-0x.p\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		123: {
			fdfmt:   "%Ff\x00",
			fdinput: "+0x.p\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		124: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x.P\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(3),
		},
		125: {
			fdfmt:   "%Ff\x00",
			fdinput: "-0x.P\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		126: {
			fdfmt:   "%Ff\x00",
			fdinput: "+0x.P\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		127: {
			fdfmt:   "%Ff\x00",
			fdinput: ".p123\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(1),
		},
		128: {
			fdfmt:   "%Ff\x00",
			fdinput: "-.p123\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		129: {
			fdfmt:   "%Ff\x00",
			fdinput: "+.p123\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(2),
		},
		130: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x1p\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(4),
		},
		131: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x1p-\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(5),
		},
		132: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x1p+\x00",
			fdwant:  "-999\x00",
			fdftell: ppint32(5),
		},
		133: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x123p 12\x00",
			fdwant:  "291\x00",
			fdftell: ppint32(6),
		},
		134: {
			fdfmt:   "%Ff\x00",
			fdinput: "0x 123p12\x00",
			fdwant:  "291\x00",
			fdftell: ppint32(2),
		},
	}
	aaerror = 0

	X__gmpf_init(cgtls, cgbp+4)
	X__gmpf_init(cgtls, cgbp+20)

	aai = 0
	for {
		if !(iqlibc.ppUint32FromInt32(aai) < iqlibc.ppUint32FromInt64(2700)/iqlibc.ppUint32FromInt64(20)) {
			break
		}

		Xmpf_set_str_or_abort(cgtls, cgbp+20, sndata[aai].fdwant, ppint32(10))

		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xstrlen(cgtls, sndata[aai].fdfmt)+iqlibc.ppUint32FromInt32(2) < iqlibc.ppUint32FromInt64(128))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1169), "strlen (data[i].fmt) + 2 < sizeof (fmt)\x00")
		}
		Xstrcpy(cgtls, cgbp+48, sndata[aai].fdfmt)
		Xstrcat(cgtls, cgbp+48, "%n\x00")

		aaignore = iqlibc.ppBoolInt32(Xstrchr(cgtls, cgbp+48, ppint32('*')) != iqlibc.ppUintptrFromInt32(0))

		aaj = 0
		for {
			if !(aaj <= ppint32(3)) {
				break
			}

			aawant_ret = sndata[aai].fdret

			aawant_ftell = sndata[aai].fdftell
			if aawant_ftell == ppint32(-ppint32(1)) {
				aawant_ftell = iqlibc.ppInt32FromUint32(Xstrlen(cgtls, sndata[aai].fdinput))
			}
			aawant_upto = ppint32(aawant_ftell)

			if aawant_ret == -ppint32(1) || aawant_ret == 0 && !(aaignore != 0) {
				aawant_upto = -ppint32(555)
			}

			switch aaj {
			case 0:
				goto cg_3
			case ppint32(1):
				goto cg_4
			case ppint32(2):
				goto cg_5
			case ppint32(3):
				goto cg_6
			default:
				goto cg_7
			}
			goto cg_8
		cg_3:
			;
			aaname = "gmp_sscanf\x00"
			aafun = pp__ccgo_fp(Xfun_gmp_sscanf)
			goto cg_8
		cg_4:
			;
			aaname = "gmp_fscanf\x00"
			aafun = pp__ccgo_fp(Xfun_gmp_fscanf)
			goto cg_8
		cg_5:
			;
			if !(Xlibc_scanf_convert(cgtls, cgbp+48) != 0) {
				goto cg_2
			}
			aaname = "standard sscanf\x00"
			aafun = pp__ccgo_fp(Xfun_sscanf)
			goto cg_8
		cg_6:
			;
			if !(Xlibc_scanf_convert(cgtls, cgbp+48) != 0) {
				goto cg_2
			}
			aaname = "standard fscanf\x00"
			aafun = pp__ccgo_fp(Xfun_fscanf)
			goto cg_8
		cg_7:
			;
		cg_11:
			;
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppInt32FromInt32(0) != 0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1209), "0\x00")
			}
			goto cg_10
		cg_10:
			;
			if 0 != 0 {
				goto cg_11
			}
			goto cg_9
		cg_9:
			;
			goto cg_8
		cg_8:
			;

			*(*ppint32)(iqunsafe.ppPointer(cgbp)) = -ppint32(555)
			Xgot_ftell = ppint32(-ppint32(1))

			switch aaj {
			case ppint32(1):
				goto cg_12
			case 0:
				goto cg_13
			case ppint32(3):
				goto cg_14
			case ppint32(2):
				goto cg_15
			default:
				goto cg_16
			}
			goto cg_17
		cg_13:
			;
		cg_12:
			;
			X__gmpf_set_si(cgtls, cgbp+4, -ppint32(999))
			if aaignore != 0 {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+48, cgbp, iqlibc.ppUintptrFromInt32(0))
			} else {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+48, cgbp+4, cgbp)
			}
			goto cg_17
		cg_15:
			;
		cg_14:
			;
			*(*ppfloat64)(iqunsafe.ppPointer(cgbp + 40)) = ppfloat64(-iqlibc.ppInt32FromInt32(999))
			if aaignore != 0 {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+48, cgbp, iqlibc.ppUintptrFromInt32(0))
			} else {
				aagot_ret = (*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafun})))(cgtls, sndata[aai].fdinput, cgbp+48, cgbp+40, cgbp)
			}
			X__gmpf_set_d(cgtls, cgbp+4, *(*ppfloat64)(iqunsafe.ppPointer(cgbp + 40)))
			goto cg_17
		cg_16:
			;
		cg_20:
			;
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppInt32FromInt32(0) != 0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1235), "0\x00")
			}
			goto cg_19
		cg_19:
			;
			if 0 != 0 {
				goto cg_20
			}
			goto cg_18
		cg_18:
			;
			goto cg_17
		cg_17:
			;

			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_prec) >= ppint32((iqlibc.ppInt32FromInt32(53)+iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-iqlibc.ppInt32FromInt32(1))/(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1239), "((got)->_mp_prec) >= ((mp_size_t) ((((53) > (53) ? (53) : (53)) + 2 * (32 - 0) - 1) / (32 - 0)))\x00")
			}
			if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size >= 0 {
				ccv21 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
			} else {
				ccv21 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv21 <= (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_prec+iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1239), "((((got)->_mp_size)) >= 0 ? (((got)->_mp_size)) : -(((got)->_mp_size))) <= ((got)->_mp_prec)+1\x00")
			}
			if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size == 0 {
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_exp == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1239), "((got)->_mp_exp) == 0\x00")
				}
			}
			if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size != 0 {
				if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_size >= 0 {
					ccv22 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
				} else {
					ccv22 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 4)).fd_mp_size
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+4)).fd_mp_d + ppuintptr(ccv22-ppint32(1))*4)) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1239), "((got)->_mp_d)[((((got)->_mp_size)) >= 0 ? (((got)->_mp_size)) : -(((got)->_mp_size))) - 1] != 0\x00")
				}
			}

			if aagot_ret != aawant_ret {

				Xprintf(cgtls, "%s wrong return value\n\x00", iqlibc.ppVaList(cgbp+184, aaname))
				aaerror = ppint32(1)
			}
			if aawant_ret == ppint32(1) && X__gmpf_cmp(cgtls, cgbp+20, cgbp+4) != 0 {

				Xprintf(cgtls, "%s wrong result\n\x00", iqlibc.ppVaList(cgbp+184, aaname))
				aaerror = ppint32(1)
			}
			if *(*ppint32)(iqunsafe.ppPointer(cgbp)) != aawant_upto {

				Xprintf(cgtls, "%s wrong upto\n\x00", iqlibc.ppVaList(cgbp+184, aaname))
				aaerror = ppint32(1)
			}
			if Xgot_ftell != ppint32(-ppint32(1)) && aawant_ftell != ppint32(-ppint32(1)) && Xgot_ftell != aawant_ftell {

				Xprintf(cgtls, "%s wrong ftell\n\x00", iqlibc.ppVaList(cgbp+184, aaname))
				aaerror = ppint32(1)
			}
			if aaerror != 0 {

				Xprintf(cgtls, "  fmt   \"%s\"\n\x00", iqlibc.ppVaList(cgbp+184, sndata[aai].fdfmt))
				Xprintf(cgtls, "  input \"%s\"\n\x00", iqlibc.ppVaList(cgbp+184, sndata[aai].fdinput))
				Xprintf(cgtls, "  ret   want=%d\n\x00", iqlibc.ppVaList(cgbp+184, aawant_ret))
				Xprintf(cgtls, "        got =%d\n\x00", iqlibc.ppVaList(cgbp+184, aagot_ret))
				Xmpf_trace(cgtls, "  value want\x00", cgbp+20)
				Xmpf_trace(cgtls, "        got \x00", cgbp+4)
				Xprintf(cgtls, "  upto  want=%d\n\x00", iqlibc.ppVaList(cgbp+184, aawant_upto))
				Xprintf(cgtls, "        got =%d\n\x00", iqlibc.ppVaList(cgbp+184, *(*ppint32)(iqunsafe.ppPointer(cgbp))))
				if Xgot_ftell != ppint32(-ppint32(1)) {

					Xprintf(cgtls, "  ftell want =%ld\n\x00", iqlibc.ppVaList(cgbp+184, aawant_ftell))
					Xprintf(cgtls, "        got  =%ld\n\x00", iqlibc.ppVaList(cgbp+184, Xgot_ftell))
				}
				Xabort(cgtls)
			}

			goto cg_2
		cg_2:
			;
			aaj++
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpf_clear(cgtls, cgbp+4)
	X__gmpf_clear(cgtls, cgbp+20)
}

func Xcheck_n(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(1184)
	defer cgtls.ppFree(1184)

	var aa__i, aa__i1, aa__i2, aa__i3, aa__i4, aa__i5 tnmp_size_t
	var aa__nail, aa__nail1, aa__nail2, aa__nail3, aa__nail4, aa__nail5 tnmp_limb_t
	var aaret, aaret1, aaret2, aaret3, aaret4, aaret5, aaret6, aaret7, ccv1, ccv10, ccv11, ccv13, ccv15, ccv16, ccv18, ccv20, ccv21, ccv23, ccv25, ccv26, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv34, ccv5, ccv6, ccv8 ppint32
	var ccv12, ccv17, ccv2, ccv22, ccv27, ccv7 ppbool
	var pp_ /* fmt at bp+10 */ [128]ppint8
	var pp_ /* fmt at bp+152 */ [128]ppint8
	var pp_ /* fmt at bp+296 */ [128]ppint8
	var pp_ /* fmt at bp+440 */ [128]ppint8
	var pp_ /* fmt at bp+576 */ [128]ppint8
	var pp_ /* fmt at bp+708 */ [128]ppint8
	var pp_ /* fmt at bp+848 */ [128]ppint8
	var pp_ /* g at bp+1060 */ tnmpz_t
	var pp_ /* g at bp+1072 */ tnmpz_t
	var pp_ /* n at bp+0 */ ppint32
	var pp_ /* n at bp+4 */ ppint32
	var pp_ /* x at bp+1000 */ tnmpz_t
	var pp_ /* x at bp+1012 */ [2]tnmpq_t
	var pp_ /* x at bp+1084 */ tnmpq_t
	var pp_ /* x at bp+1108 */ [2]tnmpf_t
	var pp_ /* x at bp+1140 */ tnmpf_t
	var pp_ /* x at bp+144 */ [2]ppint32
	var pp_ /* x at bp+280 */ [2]ppint64
	var pp_ /* x at bp+424 */ [2]tnintmax_t
	var pp_ /* x at bp+568 */ [2]tnptrdiff_t
	var pp_ /* x at bp+704 */ [2]ppint16
	var pp_ /* x at bp+8 */ [2]ppint8
	var pp_ /* x at bp+840 */ [2]tnsize_t
	var pp_ /* x at bp+976 */ [2]tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__i2, aa__i3, aa__i4, aa__i5, aa__nail, aa__nail1, aa__nail2, aa__nail3, aa__nail4, aa__nail5, aaret, aaret1, aaret2, aaret3, aaret4, aaret5, aaret6, aaret7, ccv1, ccv10, ccv11, ccv12, ccv13, ccv15, ccv16, ccv17, ccv18, ccv2, ccv20, ccv21, ccv22, ccv23, ccv25, ccv26, ccv27, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv34, ccv5, ccv6, ccv7, ccv8

	/* %n suppressed */

	*(*ppint32)(iqunsafe.ppPointer(cgbp)) = ppint32(123)
	X__gmp_sscanf(cgtls, "   \x00", " %*n\x00", iqlibc.ppVaList(cgbp+1168, cgbp))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp)) == iqlibc.ppInt32FromInt32(123))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1295), "n == 123\x00")
	}

	*(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) = ppint32(123)
	Xfromstring_gmp_fscanf(cgtls, "   \x00", " %*n\x00", iqlibc.ppVaList(cgbp+1168, cgbp+4))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) == iqlibc.ppInt32FromInt32(123))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1300), "n == 123\x00")
	}

	(*(*[2]ppint8)(iqunsafe.ppPointer(cgbp + 8)))[0] = ppint8(^ppint32(iqlibc.ppInt8FromInt32(0)))
	(*(*[2]ppint8)(iqunsafe.ppPointer(cgbp + 8)))[ppint32(1)] = ppint8(^ppint32(iqlibc.ppInt8FromInt32(0)))
	Xsprintf(cgtls, cgbp+10, "abc%%%sn\x00", iqlibc.ppVaList(cgbp+1168, "hh\x00"))
	aaret1 = X__gmp_sscanf(cgtls, "abc\x00", cgbp+10, iqlibc.ppVaList(cgbp+1168, cgbp+8))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret1 == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1323), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*(*[2]ppint8)(iqunsafe.ppPointer(cgbp + 8)))[0]) == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1323), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*(*[2]ppint8)(iqunsafe.ppPointer(cgbp + 8)))[ppint32(1)]) == ppint32(ppint8(^ppint32(iqlibc.ppInt8FromInt32(0)))))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1323), "x[1] == (char) ~ (char) 0\x00")
	}
	(*(*[2]ppint32)(iqunsafe.ppPointer(cgbp + 144)))[0] = ^iqlibc.ppInt32FromInt32(0)
	(*(*[2]ppint32)(iqunsafe.ppPointer(cgbp + 144)))[ppint32(1)] = ^iqlibc.ppInt32FromInt32(0)
	Xsprintf(cgtls, cgbp+152, "abc%%%sn\x00", iqlibc.ppVaList(cgbp+1168, "l\x00"))
	aaret2 = X__gmp_sscanf(cgtls, "abc\x00", cgbp+152, iqlibc.ppVaList(cgbp+1168, cgbp+144))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret2 == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1324), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]ppint32)(iqunsafe.ppPointer(cgbp + 144)))[0] == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1324), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]ppint32)(iqunsafe.ppPointer(cgbp + 144)))[ppint32(1)] == ^iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1324), "x[1] == (long) ~ (long) 0\x00")
	}
	(*(*[2]ppint64)(iqunsafe.ppPointer(cgbp + 280)))[0] = ^iqlibc.ppInt64FromInt32(0)
	(*(*[2]ppint64)(iqunsafe.ppPointer(cgbp + 280)))[ppint32(1)] = ^iqlibc.ppInt64FromInt32(0)
	Xsprintf(cgtls, cgbp+296, "abc%%%sn\x00", iqlibc.ppVaList(cgbp+1168, "L\x00"))
	aaret3 = X__gmp_sscanf(cgtls, "abc\x00", cgbp+296, iqlibc.ppVaList(cgbp+1168, cgbp+280))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret3 == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1326), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]ppint64)(iqunsafe.ppPointer(cgbp + 280)))[0] == iqlibc.ppInt64FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1326), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]ppint64)(iqunsafe.ppPointer(cgbp + 280)))[ppint32(1)] == ^iqlibc.ppInt64FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1326), "x[1] == (long long) ~ (long long) 0\x00")
	}
	(*(*[2]tnintmax_t)(iqunsafe.ppPointer(cgbp + 424)))[0] = ^iqlibc.ppInt64FromInt32(0)
	(*(*[2]tnintmax_t)(iqunsafe.ppPointer(cgbp + 424)))[ppint32(1)] = ^iqlibc.ppInt64FromInt32(0)
	Xsprintf(cgtls, cgbp+440, "abc%%%sn\x00", iqlibc.ppVaList(cgbp+1168, "j\x00"))
	aaret4 = X__gmp_sscanf(cgtls, "abc\x00", cgbp+440, iqlibc.ppVaList(cgbp+1168, cgbp+424))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret4 == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1329), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnintmax_t)(iqunsafe.ppPointer(cgbp + 424)))[0] == iqlibc.ppInt64FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1329), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnintmax_t)(iqunsafe.ppPointer(cgbp + 424)))[ppint32(1)] == ^iqlibc.ppInt64FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1329), "x[1] == (intmax_t) ~ (intmax_t) 0\x00")
	}
	(*(*[2]tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 568)))[0] = ^iqlibc.ppInt32FromInt32(0)
	(*(*[2]tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 568)))[ppint32(1)] = ^iqlibc.ppInt32FromInt32(0)
	Xsprintf(cgtls, cgbp+576, "abc%%%sn\x00", iqlibc.ppVaList(cgbp+1168, "t\x00"))
	aaret5 = X__gmp_sscanf(cgtls, "abc\x00", cgbp+576, iqlibc.ppVaList(cgbp+1168, cgbp+568))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret5 == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1332), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 568)))[0] == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1332), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 568)))[ppint32(1)] == ^iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1332), "x[1] == (ptrdiff_t) ~ (ptrdiff_t) 0\x00")
	}
	(*(*[2]ppint16)(iqunsafe.ppPointer(cgbp + 704)))[0] = ppint16(^ppint32(iqlibc.ppInt16FromInt32(0)))
	(*(*[2]ppint16)(iqunsafe.ppPointer(cgbp + 704)))[ppint32(1)] = ppint16(^ppint32(iqlibc.ppInt16FromInt32(0)))
	Xsprintf(cgtls, cgbp+708, "abc%%%sn\x00", iqlibc.ppVaList(cgbp+1168, "h\x00"))
	aaret6 = X__gmp_sscanf(cgtls, "abc\x00", cgbp+708, iqlibc.ppVaList(cgbp+1168, cgbp+704))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret6 == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1334), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*(*[2]ppint16)(iqunsafe.ppPointer(cgbp + 704)))[0]) == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1334), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*(*[2]ppint16)(iqunsafe.ppPointer(cgbp + 704)))[ppint32(1)]) == ppint32(ppint16(^ppint32(iqlibc.ppInt16FromInt32(0)))))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1334), "x[1] == (short) ~ (short) 0\x00")
	}
	(*(*[2]tnsize_t)(iqunsafe.ppPointer(cgbp + 840)))[0] = ^iqlibc.ppUint32FromInt32(0)
	(*(*[2]tnsize_t)(iqunsafe.ppPointer(cgbp + 840)))[ppint32(1)] = ^iqlibc.ppUint32FromInt32(0)
	Xsprintf(cgtls, cgbp+848, "abc%%%sn\x00", iqlibc.ppVaList(cgbp+1168, "z\x00"))
	aaret7 = X__gmp_sscanf(cgtls, "abc\x00", cgbp+848, iqlibc.ppVaList(cgbp+1168, cgbp+840))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret7 == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1335), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnsize_t)(iqunsafe.ppPointer(cgbp + 840)))[0] == iqlibc.ppUint32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1335), "x[0] == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*(*[2]tnsize_t)(iqunsafe.ppPointer(cgbp + 840)))[ppint32(1)] == ^iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1335), "x[1] == (size_t) ~ (size_t) 0\x00")
	}

	/* %Zn */

	X__gmpz_init_set_si(cgtls, cgbp+976, -ppint32(987))
	X__gmpz_init_set_si(cgtls, cgbp+976+1*12, ppint32(654))
	aaret = X__gmp_sscanf(cgtls, "xyz   \x00", "xyz%Zn\x00", iqlibc.ppVaList(cgbp+1168, cgbp+976))

	if ccv2 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976)).fd_mp_size == 0; !ccv2 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976)).fd_mp_size >= 0 {
			ccv1 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976)).fd_mp_size
		} else {
			ccv1 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv2 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976)).fd_mp_d + ppuintptr(ccv1-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1343), "((x[0])->_mp_size) == 0 || ((x[0])->_mp_d)[((((x[0])->_mp_size)) >= 0 ? (((x[0])->_mp_size)) : -(((x[0])->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976)).fd_mp_size >= 0 {
		ccv3 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976)).fd_mp_size
	} else {
		ccv3 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976)).fd_mp_alloc >= ccv3)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1343), "((x[0])->_mp_alloc) >= ((((x[0])->_mp_size)) >= 0 ? (((x[0])->_mp_size)) : -(((x[0])->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976)).fd_mp_size >= 0 {
				ccv5 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976)).fd_mp_size
			} else {
				ccv5 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976)).fd_mp_size
			}
			if !(aa__i < ppint32(ccv5)) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976)).fd_mp_d + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1343), "__nail == 0\x00")
			}
			goto cg_4
		cg_4:
			;
			aa__i++
		}
	}

	if ccv7 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976+1*12)).fd_mp_size == 0; !ccv7 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976+1*12)).fd_mp_size >= 0 {
			ccv6 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976 + 1*12)).fd_mp_size
		} else {
			ccv6 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976 + 1*12)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv7 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976+1*12)).fd_mp_d + ppuintptr(ccv6-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1344), "((x[1])->_mp_size) == 0 || ((x[1])->_mp_d)[((((x[1])->_mp_size)) >= 0 ? (((x[1])->_mp_size)) : -(((x[1])->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976+1*12)).fd_mp_size >= 0 {
		ccv8 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976 + 1*12)).fd_mp_size
	} else {
		ccv8 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976 + 1*12)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976+1*12)).fd_mp_alloc >= ccv8)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1344), "((x[1])->_mp_alloc) >= ((((x[1])->_mp_size)) >= 0 ? (((x[1])->_mp_size)) : -(((x[1])->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976+1*12)).fd_mp_size >= 0 {
				ccv10 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976 + 1*12)).fd_mp_size
			} else {
				ccv10 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 976 + 1*12)).fd_mp_size
			}
			if !(aa__i1 < ppint32(ccv10)) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+976+1*12)).fd_mp_d + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1344), "__nail == 0\x00")
			}
			goto cg_9
		cg_9:
			;
			aa__i1++
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1345), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+976, ppuint32(3)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1346), "(__builtin_constant_p (3L) && (3L) == 0 ? ((x[0])->_mp_size < 0 ? -1 : (x[0])->_mp_size > 0) : __gmpz_cmp_ui (x[0],3L)) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+976+1*12, ppuint32(654)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1347), "(__builtin_constant_p (654L) && (654L) == 0 ? ((x[1])->_mp_size < 0 ? -1 : (x[1])->_mp_size > 0) : __gmpz_cmp_ui (x[1],654L)) == 0\x00")
	}
	X__gmpz_clear(cgtls, cgbp+976)
	X__gmpz_clear(cgtls, cgbp+976+1*12)

	X__gmpz_init(cgtls, cgbp+1000)
	aaret = Xfromstring_gmp_fscanf(cgtls, "xyz   \x00", "xyz%Zn\x00", iqlibc.ppVaList(cgbp+1168, cgbp+1000))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1355), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+1000, ppuint32(3)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1356), "(__builtin_constant_p (3L) && (3L) == 0 ? ((x)->_mp_size < 0 ? -1 : (x)->_mp_size > 0) : __gmpz_cmp_ui (x,3L)) == 0\x00")
	}
	X__gmpz_clear(cgtls, cgbp+1000)

	/* %Qn */

	X__gmpq_init(cgtls, cgbp+1012)
	X__gmpq_init(cgtls, cgbp+1012+1*24)
	X__gmpq_set_ui(cgtls, cgbp+1012, ppuint32(987), ppuint32(654))
	X__gmpq_set_ui(cgtls, cgbp+1012+1*24, ppuint32(4115), ppuint32(226))
	aaret = X__gmp_sscanf(cgtls, "xyz   \x00", "xyz%Qn\x00", iqlibc.ppVaList(cgbp+1168, cgbp+1012))

	if ccv12 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012)).fd_mp_size == 0; !ccv12 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012)).fd_mp_size >= 0 {
			ccv11 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012)).fd_mp_size
		} else {
			ccv11 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv12 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012)).fd_mp_d + ppuintptr(ccv11-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1368), "(((&((x[0])->_mp_num)))->_mp_size) == 0 || (((&((x[0])->_mp_num)))->_mp_d)[(((((&((x[0])->_mp_num)))->_mp_size)) >= 0 ? ((((&((x[0])->_mp_num)))->_mp_size)) : -((((&((x[0])->_mp_num)))->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012)).fd_mp_size >= 0 {
		ccv13 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012)).fd_mp_size
	} else {
		ccv13 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012)).fd_mp_alloc >= ccv13)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1368), "(((&((x[0])->_mp_num)))->_mp_alloc) >= (((((&((x[0])->_mp_num)))->_mp_size)) >= 0 ? ((((&((x[0])->_mp_num)))->_mp_size)) : -((((&((x[0])->_mp_num)))->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i2 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012)).fd_mp_size >= 0 {
				ccv15 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012)).fd_mp_size
			} else {
				ccv15 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012)).fd_mp_size
			}
			if !(aa__i2 < ppint32(ccv15)) {
				break
			}
			aa__nail2 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012)).fd_mp_d + ppuintptr(aa__i2)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1368), "__nail == 0\x00")
			}
			goto cg_14
		cg_14:
			;
			aa__i2++
		}
	}
	if ccv17 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+12)).fd_mp_size == 0; !ccv17 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+12)).fd_mp_size >= 0 {
			ccv16 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 12)).fd_mp_size
		} else {
			ccv16 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 12)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv17 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+12)).fd_mp_d + ppuintptr(ccv16-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1368), "(((&((x[0])->_mp_den)))->_mp_size) == 0 || (((&((x[0])->_mp_den)))->_mp_d)[(((((&((x[0])->_mp_den)))->_mp_size)) >= 0 ? ((((&((x[0])->_mp_den)))->_mp_size)) : -((((&((x[0])->_mp_den)))->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+12)).fd_mp_size >= 0 {
		ccv18 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 12)).fd_mp_size
	} else {
		ccv18 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 12)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+12)).fd_mp_alloc >= ccv18)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1368), "(((&((x[0])->_mp_den)))->_mp_alloc) >= (((((&((x[0])->_mp_den)))->_mp_size)) >= 0 ? ((((&((x[0])->_mp_den)))->_mp_size)) : -((((&((x[0])->_mp_den)))->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i3 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+12)).fd_mp_size >= 0 {
				ccv20 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 12)).fd_mp_size
			} else {
				ccv20 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 12)).fd_mp_size
			}
			if !(aa__i3 < ppint32(ccv20)) {
				break
			}
			aa__nail3 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+12)).fd_mp_d + ppuintptr(aa__i3)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail3 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1368), "__nail == 0\x00")
			}
			goto cg_19
		cg_19:
			;
			aa__i3++
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+12)).fd_mp_size >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1368), "(((&((x[0])->_mp_den)))->_mp_size) >= 1\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012)).fd_mp_size == 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+12)).fd_mp_size == ppint32(1) && *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 12)).fd_mp_d)) == ppuint32(1))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1368), "(((&((x[0])->_mp_den)))->_mp_size) == 1 && (((&((x[0])->_mp_den)))->_mp_d)[0] == 1\x00")
		}
	} else {
		X__gmpz_init(cgtls, cgbp+1060)
		X__gmpz_gcd(cgtls, cgbp+1060, cgbp+1012, cgbp+1012+12)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+1060, ppuint32(1)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1368), "(__builtin_constant_p (1) && (1) == 0 ? ((g)->_mp_size < 0 ? -1 : (g)->_mp_size > 0) : __gmpz_cmp_ui (g,1)) == 0\x00")
		}
		X__gmpz_clear(cgtls, cgbp+1060)
	}

	if ccv22 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24)).fd_mp_size == 0; !ccv22 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24)).fd_mp_size >= 0 {
			ccv21 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24)).fd_mp_size
		} else {
			ccv21 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv22 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24)).fd_mp_d + ppuintptr(ccv21-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1369), "(((&((x[1])->_mp_num)))->_mp_size) == 0 || (((&((x[1])->_mp_num)))->_mp_d)[(((((&((x[1])->_mp_num)))->_mp_size)) >= 0 ? ((((&((x[1])->_mp_num)))->_mp_size)) : -((((&((x[1])->_mp_num)))->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24)).fd_mp_size >= 0 {
		ccv23 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24)).fd_mp_size
	} else {
		ccv23 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24)).fd_mp_alloc >= ccv23)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1369), "(((&((x[1])->_mp_num)))->_mp_alloc) >= (((((&((x[1])->_mp_num)))->_mp_size)) >= 0 ? ((((&((x[1])->_mp_num)))->_mp_size)) : -((((&((x[1])->_mp_num)))->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i4 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24)).fd_mp_size >= 0 {
				ccv25 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24)).fd_mp_size
			} else {
				ccv25 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24)).fd_mp_size
			}
			if !(aa__i4 < ppint32(ccv25)) {
				break
			}
			aa__nail4 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24)).fd_mp_d + ppuintptr(aa__i4)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail4 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1369), "__nail == 0\x00")
			}
			goto cg_24
		cg_24:
			;
			aa__i4++
		}
	}
	if ccv27 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24+12)).fd_mp_size == 0; !ccv27 {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24+12)).fd_mp_size >= 0 {
			ccv26 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24 + 12)).fd_mp_size
		} else {
			ccv26 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24 + 12)).fd_mp_size
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv27 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24+12)).fd_mp_d + ppuintptr(ccv26-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1369), "(((&((x[1])->_mp_den)))->_mp_size) == 0 || (((&((x[1])->_mp_den)))->_mp_d)[(((((&((x[1])->_mp_den)))->_mp_size)) >= 0 ? ((((&((x[1])->_mp_den)))->_mp_size)) : -((((&((x[1])->_mp_den)))->_mp_size))) - 1] != 0\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24+12)).fd_mp_size >= 0 {
		ccv28 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24 + 12)).fd_mp_size
	} else {
		ccv28 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24 + 12)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24+12)).fd_mp_alloc >= ccv28)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1369), "(((&((x[1])->_mp_den)))->_mp_alloc) >= (((((&((x[1])->_mp_den)))->_mp_size)) >= 0 ? ((((&((x[1])->_mp_den)))->_mp_size)) : -((((&((x[1])->_mp_den)))->_mp_size)))\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i5 = 0
		for {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24+12)).fd_mp_size >= 0 {
				ccv30 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24 + 12)).fd_mp_size
			} else {
				ccv30 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24 + 12)).fd_mp_size
			}
			if !(aa__i5 < ppint32(ccv30)) {
				break
			}
			aa__nail5 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24+12)).fd_mp_d + ppuintptr(aa__i5)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail5 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1369), "__nail == 0\x00")
			}
			goto cg_29
		cg_29:
			;
			aa__i5++
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24+12)).fd_mp_size >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1369), "(((&((x[1])->_mp_den)))->_mp_size) >= 1\x00")
	}
	if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24)).fd_mp_size == 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+1012+1*24+12)).fd_mp_size == ppint32(1) && *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 1012 + 1*24 + 12)).fd_mp_d)) == ppuint32(1))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1369), "(((&((x[1])->_mp_den)))->_mp_size) == 1 && (((&((x[1])->_mp_den)))->_mp_d)[0] == 1\x00")
		}
	} else {
		X__gmpz_init(cgtls, cgbp+1072)
		X__gmpz_gcd(cgtls, cgbp+1072, cgbp+1012+1*24, cgbp+1012+1*24+12)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+1072, ppuint32(1)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1369), "(__builtin_constant_p (1) && (1) == 0 ? ((g)->_mp_size < 0 ? -1 : (g)->_mp_size > 0) : __gmpz_cmp_ui (g,1)) == 0\x00")
		}
		X__gmpz_clear(cgtls, cgbp+1072)
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1370), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpq_cmp_ui(cgtls, cgbp+1012, ppuint32(3), ppuint32(1)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1371), "(__builtin_constant_p (3L) && (3L) == 0 ? ((x[0])->_mp_num._mp_size < 0 ? -1 : (x[0])->_mp_num._mp_size > 0) : __builtin_constant_p ((3L) == (1L)) && (3L) == (1L) ? __gmpz_cmp ((&((x[0])->_mp_num)), (&((x[0])->_mp_den))) : __gmpq_cmp_ui (x[0],3L,1L)) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpq_cmp_ui(cgtls, cgbp+1012+1*24, ppuint32(4115), ppuint32(226)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1372), "(__builtin_constant_p (4115L) && (4115L) == 0 ? ((x[1])->_mp_num._mp_size < 0 ? -1 : (x[1])->_mp_num._mp_size > 0) : __builtin_constant_p ((4115L) == (226L)) && (4115L) == (226L) ? __gmpz_cmp ((&((x[1])->_mp_num)), (&((x[1])->_mp_den))) : __gmpq_cmp_ui (x[1],4115L,226L)) == 0\x00")
	}
	X__gmpq_clear(cgtls, cgbp+1012)
	X__gmpq_clear(cgtls, cgbp+1012+1*24)

	X__gmpq_init(cgtls, cgbp+1084)
	aaret = Xfromstring_gmp_fscanf(cgtls, "xyz   \x00", "xyz%Qn\x00", iqlibc.ppVaList(cgbp+1168, cgbp+1084))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1380), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpq_cmp_ui(cgtls, cgbp+1084, ppuint32(3), ppuint32(1)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1381), "(__builtin_constant_p (3L) && (3L) == 0 ? ((x)->_mp_num._mp_size < 0 ? -1 : (x)->_mp_num._mp_size > 0) : __builtin_constant_p ((3L) == (1L)) && (3L) == (1L) ? __gmpz_cmp ((&((x)->_mp_num)), (&((x)->_mp_den))) : __gmpq_cmp_ui (x,3L,1L)) == 0\x00")
	}
	X__gmpq_clear(cgtls, cgbp+1084)

	/* %Fn */

	X__gmpf_init(cgtls, cgbp+1108)
	X__gmpf_init(cgtls, cgbp+1108+1*16)
	X__gmpf_set_ui(cgtls, cgbp+1108, ppuint32(987))
	X__gmpf_set_ui(cgtls, cgbp+1108+1*16, ppuint32(654))
	aaret = X__gmp_sscanf(cgtls, "xyz   \x00", "xyz%Fn\x00", iqlibc.ppVaList(cgbp+1168, cgbp+1108))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108)).fd_mp_prec) >= ppint32((iqlibc.ppInt32FromInt32(53)+iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-iqlibc.ppInt32FromInt32(1))/(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1393), "((x[0])->_mp_prec) >= ((mp_size_t) ((((53) > (53) ? (53) : (53)) + 2 * (32 - 0) - 1) / (32 - 0)))\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108)).fd_mp_size >= 0 {
		ccv31 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1108)).fd_mp_size
	} else {
		ccv31 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1108)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv31 <= (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108)).fd_mp_prec+iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1393), "((((x[0])->_mp_size)) >= 0 ? (((x[0])->_mp_size)) : -(((x[0])->_mp_size))) <= ((x[0])->_mp_prec)+1\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108)).fd_mp_size == 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108)).fd_mp_exp == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1393), "((x[0])->_mp_exp) == 0\x00")
		}
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108)).fd_mp_size != 0 {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108)).fd_mp_size >= 0 {
			ccv32 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1108)).fd_mp_size
		} else {
			ccv32 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1108)).fd_mp_size
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108)).fd_mp_d + ppuintptr(ccv32-ppint32(1))*4)) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1393), "((x[0])->_mp_d)[((((x[0])->_mp_size)) >= 0 ? (((x[0])->_mp_size)) : -(((x[0])->_mp_size))) - 1] != 0\x00")
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppint32((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108+1*16)).fd_mp_prec) >= ppint32((iqlibc.ppInt32FromInt32(53)+iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-iqlibc.ppInt32FromInt32(1))/(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1394), "((x[1])->_mp_prec) >= ((mp_size_t) ((((53) > (53) ? (53) : (53)) + 2 * (32 - 0) - 1) / (32 - 0)))\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108+1*16)).fd_mp_size >= 0 {
		ccv33 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1108 + 1*16)).fd_mp_size
	} else {
		ccv33 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1108 + 1*16)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv33 <= (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108+1*16)).fd_mp_prec+iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1394), "((((x[1])->_mp_size)) >= 0 ? (((x[1])->_mp_size)) : -(((x[1])->_mp_size))) <= ((x[1])->_mp_prec)+1\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108+1*16)).fd_mp_size == 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108+1*16)).fd_mp_exp == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1394), "((x[1])->_mp_exp) == 0\x00")
		}
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108+1*16)).fd_mp_size != 0 {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108+1*16)).fd_mp_size >= 0 {
			ccv34 = (*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1108 + 1*16)).fd_mp_size
		} else {
			ccv34 = -(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 1108 + 1*16)).fd_mp_size
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpf_struct)(iqunsafe.ppPointer(cgbp+1108+1*16)).fd_mp_d + ppuintptr(ccv34-ppint32(1))*4)) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1394), "((x[1])->_mp_d)[((((x[1])->_mp_size)) >= 0 ? (((x[1])->_mp_size)) : -(((x[1])->_mp_size))) - 1] != 0\x00")
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1395), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpf_cmp_ui(cgtls, cgbp+1108, ppuint32(3)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1396), "__gmpf_cmp_ui (x[0], 3L) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpf_cmp_ui(cgtls, cgbp+1108+1*16, ppuint32(654)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1397), "__gmpf_cmp_ui (x[1], 654L) == 0\x00")
	}
	X__gmpf_clear(cgtls, cgbp+1108)
	X__gmpf_clear(cgtls, cgbp+1108+1*16)

	X__gmpf_init(cgtls, cgbp+1140)
	aaret = Xfromstring_gmp_fscanf(cgtls, "xyz   \x00", "xyz%Fn\x00", iqlibc.ppVaList(cgbp+1168, cgbp+1140))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1405), "ret == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpf_cmp_ui(cgtls, cgbp+1140, ppuint32(3)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1406), "__gmpf_cmp_ui (x, 3L) == 0\x00")
	}
	X__gmpf_clear(cgtls, cgbp+1140)

}

func Xcheck_misc(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(288)
	defer cgtls.ppFree(288)

	var aacmp, aaret ppint32
	var pp_ /* a at bp+0 */ ppint32
	var pp_ /* a at bp+28 */ ppint32
	var pp_ /* a at bp+56 */ ppint32
	var pp_ /* a at bp+76 */ ppint32
	var pp_ /* b at bp+32 */ ppint32
	var pp_ /* b at bp+4 */ ppint32
	var pp_ /* buf at bp+96 */ [128]ppint8
	var pp_ /* c at bp+36 */ ppint32
	var pp_ /* c at bp+8 */ ppint32
	var pp_ /* n at bp+12 */ ppint32
	var pp_ /* n at bp+40 */ ppint32
	var pp_ /* n at bp+60 */ ppint32
	var pp_ /* n at bp+80 */ ppint32
	var pp_ /* x at bp+224 */ ppint32
	var pp_ /* x at bp+228 */ tnmpz_t
	var pp_ /* z at bp+16 */ tnmpz_t
	var pp_ /* z at bp+44 */ tnmpz_t
	var pp_ /* z at bp+64 */ tnmpz_t
	var pp_ /* z at bp+84 */ tnmpz_t
	pp_, pp_ = aacmp, aaret

	*(*ppint32)(iqunsafe.ppPointer(cgbp)) = ppint32(9)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) = ppint32(8)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 8)) = ppint32(7)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 12)) = ppint32(66)
	X__gmpz_init(cgtls, cgbp+16)
	aaret = X__gmp_sscanf(cgtls, "1 2 3 4\x00", "%d %d %d %Zd%n\x00", iqlibc.ppVaList(cgbp+248, cgbp, cgbp+4, cgbp+8, cgbp+16, cgbp+12))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(4))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1422), "ret == 4\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp)) == iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1423), "a == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) == iqlibc.ppInt32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1424), "b == 2\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 8)) == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1425), "c == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 12)) == iqlibc.ppInt32FromInt32(7))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1426), "n == 7\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+16, ppuint32(4)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1427), "(__builtin_constant_p (4L) && (4L) == 0 ? ((z)->_mp_size < 0 ? -1 : (z)->_mp_size > 0) : __gmpz_cmp_ui (z,4L)) == 0\x00")
	}
	X__gmpz_clear(cgtls, cgbp+16)

	*(*ppint32)(iqunsafe.ppPointer(cgbp + 28)) = ppint32(9)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 32)) = ppint32(8)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 36)) = ppint32(7)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 40)) = ppint32(66)
	X__gmpz_init(cgtls, cgbp+44)
	aaret = Xfromstring_gmp_fscanf(cgtls, "1 2 3 4\x00", "%d %d %d %Zd%n\x00", iqlibc.ppVaList(cgbp+248, cgbp+28, cgbp+32, cgbp+36, cgbp+44, cgbp+40))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(4))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1436), "ret == 4\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 28)) == iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1437), "a == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 32)) == iqlibc.ppInt32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1438), "b == 2\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 36)) == iqlibc.ppInt32FromInt32(3))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1439), "c == 3\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+44, ppuint32(4)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1440), "(__builtin_constant_p (4L) && (4L) == 0 ? ((z)->_mp_size < 0 ? -1 : (z)->_mp_size > 0) : __gmpz_cmp_ui (z,4L)) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 40)) == iqlibc.ppInt32FromInt32(7))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1441), "n == 7\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xgot_ftell == iqlibc.ppInt32FromInt32(7))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1442), "got_ftell == 7\x00")
	}
	X__gmpz_clear(cgtls, cgbp+44)

	*(*ppint32)(iqunsafe.ppPointer(cgbp + 56)) = ppint32(9)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 60)) = ppint32(8)
	X__gmpz_init(cgtls, cgbp+64)
	aaret = X__gmp_sscanf(cgtls, "1 2 3 4\x00", "%d %*d %*d %Zd%n\x00", iqlibc.ppVaList(cgbp+248, cgbp+56, cgbp+64, cgbp+60))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1451), "ret == 2\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 56)) == iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1452), "a == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+64, ppuint32(4)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1453), "(__builtin_constant_p (4L) && (4L) == 0 ? ((z)->_mp_size < 0 ? -1 : (z)->_mp_size > 0) : __gmpz_cmp_ui (z,4L)) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 60)) == iqlibc.ppInt32FromInt32(7))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1454), "n == 7\x00")
	}
	X__gmpz_clear(cgtls, cgbp+64)

	*(*ppint32)(iqunsafe.ppPointer(cgbp + 76)) = ppint32(9)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 80)) = ppint32(8)
	X__gmpz_init(cgtls, cgbp+84)
	aaret = Xfromstring_gmp_fscanf(cgtls, "1 2 3 4\x00", "%d %*d %*d %Zd%n\x00", iqlibc.ppVaList(cgbp+248, cgbp+76, cgbp+84, cgbp+80))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == iqlibc.ppInt32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1463), "ret == 2\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 76)) == iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1464), "a == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(X__gmpz_cmp_ui(cgtls, cgbp+84, ppuint32(4)) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1465), "(__builtin_constant_p (4L) && (4L) == 0 ? ((z)->_mp_size < 0 ? -1 : (z)->_mp_size > 0) : __gmpz_cmp_ui (z,4L)) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 80)) == iqlibc.ppInt32FromInt32(7))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1466), "n == 7\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xgot_ftell == iqlibc.ppInt32FromInt32(7))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1467), "got_ftell == 7\x00")
	}
	X__gmpz_clear(cgtls, cgbp+84)

	/* EOF for no matching */

	aaret = X__gmp_sscanf(cgtls, "   \x00", "%s\x00", iqlibc.ppVaList(cgbp+248, cgbp+96))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1475), "ret == (-1)\x00")
	}
	aaret = Xfromstring_gmp_fscanf(cgtls, "   \x00", "%s\x00", iqlibc.ppVaList(cgbp+248, cgbp+96))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1477), "ret == (-1)\x00")
	}
	if Xoption_libc_scanf != 0 {

		aaret = Xsscanf(cgtls, "   \x00", "%s\x00", iqlibc.ppVaList(cgbp+248, cgbp+96))
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1481), "ret == (-1)\x00")
		}
		aaret = Xfun_fscanf(cgtls, "   \x00", "%s\x00", cgbp+96, iqlibc.ppUintptrFromInt32(0))
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1483), "ret == (-1)\x00")
		}
	}

	/* suppressed field, then eof */

	if Xtest_sscanf_eof_ok(cgtls) != 0 {

		aaret = X__gmp_sscanf(cgtls, "123\x00", "%*d%d\x00", iqlibc.ppVaList(cgbp+248, cgbp+224))
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1493), "ret == (-1)\x00")
		}
	}
	aaret = Xfromstring_gmp_fscanf(cgtls, "123\x00", "%*d%d\x00", iqlibc.ppVaList(cgbp+248, cgbp+224))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1496), "ret == (-1)\x00")
	}
	if Xoption_libc_scanf != 0 {

		aaret = Xsscanf(cgtls, "123\x00", "%*d%d\x00", iqlibc.ppVaList(cgbp+248, cgbp+224))
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1500), "ret == (-1)\x00")
		}
		aaret = Xfun_fscanf(cgtls, "123\x00", "%*d%d\x00", cgbp+224, iqlibc.ppUintptrFromInt32(0))
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1502), "ret == (-1)\x00")
		}
	}

	X__gmpz_init(cgtls, cgbp+228)
	aaret = X__gmp_sscanf(cgtls, "123\x00", "%*Zd%Zd\x00", iqlibc.ppVaList(cgbp+248, cgbp+228))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1509), "ret == (-1)\x00")
	}
	aaret = Xfromstring_gmp_fscanf(cgtls, "123\x00", "%*Zd%Zd\x00", iqlibc.ppVaList(cgbp+248, cgbp+228))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaret == -iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "t-scanf.c\x00", ppint32(1511), "ret == (-1)\x00")
	}
	X__gmpz_clear(cgtls, cgbp+228)

	/* %[...], glibc only */

	/* %zd etc won't be accepted by sscanf on old systems, and running
	   something to see if they work might be bad, so only try it on glibc,
	   and only on a new enough version (glibc 2.0 doesn't have %zd) */
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	if aaargc > ppint32(1) && Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*4)), "-s\x00") == 0 {
		Xoption_libc_scanf = ppint32(1)
	}

	Xtests_start(cgtls)

	Xmp_trace_base = ppint32(16)

	Xcheck_z(cgtls)
	Xcheck_q(cgtls)
	Xcheck_f(cgtls)
	Xcheck_n(cgtls)
	Xcheck_misc(cgtls)

	Xunlink(cgtls, "t-scanf.tmp\x00")
	Xtests_end(cgtls)
	Xexit(cgtls, 0)
	return cgr
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

var ___gmp_allocate_func ppuintptr

func ___gmp_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

var ___gmp_free_func ppuintptr

func ___gmp_sscanf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func ___gmp_vfscanf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func ___gmpf_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_cmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmpf_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint32) ppint32

func ___gmpf_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_set_d(*iqlibc.ppTLS, ppuintptr, ppfloat64)

func ___gmpf_set_si(*iqlibc.ppTLS, ppuintptr, ppint32)

func ___gmpf_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32)

func ___gmpq_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint32, ppuint32) ppint32

func ___gmpq_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_set_si(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32)

func ___gmpq_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32, ppuint32)

func ___gmpz_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_cmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmpz_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint32) ppint32

func ___gmpz_gcd(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_init_set_si(*iqlibc.ppTLS, ppuintptr, ppint32)

func ___gmpz_set_si(*iqlibc.ppTLS, ppuintptr, ppint32)

func _abort(*iqlibc.ppTLS)

func _exit(*iqlibc.ppTLS, ppint32)

func _fclose(*iqlibc.ppTLS, ppuintptr) ppint32

func _fflush(*iqlibc.ppTLS, ppuintptr) ppint32

func _fopen(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _fputs(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _fscanf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _ftell(*iqlibc.ppTLS, ppuintptr) ppint32

func _getc(*iqlibc.ppTLS, ppuintptr) ppint32

func _memcpy(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32) ppuintptr

var _mp_trace_base ppint32

func _mpf_set_str_or_abort(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32)

func _mpf_trace(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpq_set_str_or_abort(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32)

func _mpq_trace(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpz_set_str_or_abort(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32)

func _mpz_trace(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _rewind(*iqlibc.ppTLS, ppuintptr)

func _sprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _sscanf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _strcat(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _strchr(*iqlibc.ppTLS, ppuintptr, ppint32) ppuintptr

func _strcmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _strcpy(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _strlen(*iqlibc.ppTLS, ppuintptr) ppuint32

func _tests_end(*iqlibc.ppTLS)

func _tests_start(*iqlibc.ppTLS)

func _unlink(*iqlibc.ppTLS, ppuintptr) ppint32

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
