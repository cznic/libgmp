// Code generated for linux/386 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -ignore-unsupported-alignment -DHAVE_CONFIG_H -I. -I.. -I.. -DNDEBUG -mlong-double-64 -c refmpn.c -o refmpn.o.go', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvASSERT_FILE = "__FILE__"
const mvASSERT_LINE = "__LINE__"
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBINV_NEWTON_THRESHOLD = 300
const mvBMOD_1_TO_MOD_1_THRESHOLD = 10
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDC_BDIV_Q_THRESHOLD = 180
const mvDC_DIVAPPR_Q_THRESHOLD = 200
const mvDELAYTIMER_MAX = 0x7fffffff
const mvDIVEXACT_1_THRESHOLD = 0
const mvDIVEXACT_BY3_METHOD = 0
const mvDIVEXACT_JEB_THRESHOLD = 25
const mvDOPRNT_CONV_FIXED = 1
const mvDOPRNT_CONV_GENERAL = 3
const mvDOPRNT_CONV_SCIENTIFIC = 2
const mvDOPRNT_JUSTIFY_INTERNAL = 3
const mvDOPRNT_JUSTIFY_LEFT = 1
const mvDOPRNT_JUSTIFY_NONE = 0
const mvDOPRNT_JUSTIFY_RIGHT = 2
const mvDOPRNT_SHOWBASE_NO = 2
const mvDOPRNT_SHOWBASE_NONZERO = 3
const mvDOPRNT_SHOWBASE_YES = 1
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFAC_DSC_THRESHOLD = 400
const mvFAC_ODD_THRESHOLD = 35
const mvFFT_FIRST_K = 4
const mvFIB_TABLE_LIMIT = 47
const mvFIB_TABLE_LUCNUM_LIMIT = 46
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFOPEN_MAX = 1000
const mvGCDEXT_DC_THRESHOLD = 600
const mvGCD_DC_THRESHOLD = 1000
const mvGET_STR_DC_THRESHOLD = 18
const mvGET_STR_PRECOMPUTE_THRESHOLD = 35
const mvGMP_LIMB_BITS = 32
const mvGMP_LIMB_BYTES = "SIZEOF_MP_LIMB_T"
const mvGMP_MPARAM_H_SUGGEST = "./mpn/generic/gmp-mparam.h"
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_ALARM = 1
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_CONST = 1
const mvHAVE_ATTRIBUTE_MALLOC = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_ATTRIBUTE_NORETURN = 1
const mvHAVE_CLOCK = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_CONFIG_H = 1
const mvHAVE_DECL_FGETC = 1
const mvHAVE_DECL_FSCANF = 1
const mvHAVE_DECL_OPTARG = 1
const mvHAVE_DECL_SYS_ERRLIST = 0
const mvHAVE_DECL_SYS_NERR = 0
const mvHAVE_DECL_UNGETC = 1
const mvHAVE_DECL_VFPRINTF = 1
const mvHAVE_DLFCN_H = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FCNTL_H = 1
const mvHAVE_FLOAT_H = 1
const mvHAVE_GETPAGESIZE = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_HIDDEN_ALIAS = 1
const mvHAVE_HOST_CPU_FAMILY_x86_64 = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTPTR_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LANGINFO_H = 1
const mvHAVE_LIMB_LITTLE_ENDIAN = 1
const mvHAVE_LOCALECONV = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_DOUBLE = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_MEMORY_H = 1
const mvHAVE_MEMSET = 1
const mvHAVE_MMAP = 1
const mvHAVE_MPROTECT = 1
const mvHAVE_NL_LANGINFO = 1
const mvHAVE_NL_TYPES_H = 1
const mvHAVE_POPEN = 1
const mvHAVE_PTRDIFF_T = 1
const mvHAVE_QUAD_T = 1
const mvHAVE_RAISE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGALTSTACK = 1
const mvHAVE_SIGSTACK = 1
const mvHAVE_SPEED_CYCLECOUNTER = 2
const mvHAVE_STACK_T = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDLIB_H = 1
const mvHAVE_STRCHR = 1
const mvHAVE_STRERROR = 1
const mvHAVE_STRINGS_H = 1
const mvHAVE_STRING_H = 1
const mvHAVE_STRNLEN = 1
const mvHAVE_STRTOL = 1
const mvHAVE_STRTOUL = 1
const mvHAVE_SYSCONF = 1
const mvHAVE_SYS_MMAN_H = 1
const mvHAVE_SYS_PARAM_H = 1
const mvHAVE_SYS_RESOURCE_H = 1
const mvHAVE_SYS_STAT_H = 1
const mvHAVE_SYS_SYSINFO_H = 1
const mvHAVE_SYS_TIMES_H = 1
const mvHAVE_SYS_TIME_H = 1
const mvHAVE_SYS_TYPES_H = 1
const mvHAVE_TIMES = 1
const mvHAVE_UINT_LEAST32_T = 1
const mvHAVE_UNISTD_H = 1
const mvHAVE_VSNPRINTF = 1
const mvHGCD_APPR_THRESHOLD = 400
const mvHGCD_REDUCE_THRESHOLD = 1000
const mvHGCD_THRESHOLD = 400
const mvHOST_NAME_MAX = 255
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT32_MAX"
const mvINTPTR_MIN = "INT32_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_HIGHBIT = "INT_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvINV_APPR_THRESHOLD = "INV_NEWTON_THRESHOLD"
const mvINV_NEWTON_THRESHOLD = 200
const mvIOV_MAX = 1024
const mvLIMBS_PER_ULONG = 1
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_HIGHBIT = "LONG_MIN"
const mvLONG_MAX = "__LONG_MAX"
const mvLSYM_PREFIX = ".L"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMATRIX22_STRASSEN_THRESHOLD = 30
const mvMB_LEN_MAX = 4
const mvMOD_BKNP1_ONLY3 = 0
const mvMPN_FFT_TABLE_SIZE = 16
const mvMPN_TOOM22_MUL_MINSIZE = 6
const mvMPN_TOOM2_SQR_MINSIZE = 4
const mvMPN_TOOM32_MUL_MINSIZE = 10
const mvMPN_TOOM33_MUL_MINSIZE = 17
const mvMPN_TOOM3_SQR_MINSIZE = 17
const mvMPN_TOOM42_MULMID_MINSIZE = 4
const mvMPN_TOOM42_MUL_MINSIZE = 10
const mvMPN_TOOM43_MUL_MINSIZE = 25
const mvMPN_TOOM44_MUL_MINSIZE = 30
const mvMPN_TOOM4_SQR_MINSIZE = 30
const mvMPN_TOOM53_MUL_MINSIZE = 17
const mvMPN_TOOM54_MUL_MINSIZE = 31
const mvMPN_TOOM63_MUL_MINSIZE = 49
const mvMPN_TOOM6H_MUL_MINSIZE = 46
const mvMPN_TOOM6_SQR_MINSIZE = 46
const mvMPN_TOOM8H_MUL_MINSIZE = 86
const mvMPN_TOOM8_SQR_MINSIZE = 86
const mvMP_BASES_BIG_BASE_CTZ_10 = 9
const mvMP_BASES_CHARS_PER_LIMB_10 = 9
const mvMP_BASES_NORMALIZATION_STEPS_10 = 2
const mvMP_EXP_T_MAX = "MP_SIZE_T_MAX"
const mvMP_EXP_T_MIN = "MP_SIZE_T_MIN"
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMULLO_BASECASE_THRESHOLD = 0
const mvMULLO_BASECASE_THRESHOLD_LIMIT = "MULLO_BASECASE_THRESHOLD"
const mvMULMID_TOOM42_THRESHOLD = "MUL_TOOM22_THRESHOLD"
const mvMULMOD_BNM1_THRESHOLD = 16
const mvMUL_TOOM22_THRESHOLD = 30
const mvMUL_TOOM22_THRESHOLD_LIMIT = "MUL_TOOM22_THRESHOLD"
const mvMUL_TOOM32_TO_TOOM43_THRESHOLD = 100
const mvMUL_TOOM32_TO_TOOM53_THRESHOLD = 110
const mvMUL_TOOM33_THRESHOLD = 100
const mvMUL_TOOM33_THRESHOLD_LIMIT = "MUL_TOOM33_THRESHOLD"
const mvMUL_TOOM42_TO_TOOM53_THRESHOLD = 100
const mvMUL_TOOM42_TO_TOOM63_THRESHOLD = 110
const mvMUL_TOOM43_TO_TOOM54_THRESHOLD = 150
const mvMUL_TOOM44_THRESHOLD = 300
const mvMUL_TOOM6H_THRESHOLD = 350
const mvMUL_TOOM8H_THRESHOLD = 450
const mvMUPI_DIV_QR_THRESHOLD = 200
const mvMU_BDIV_QR_THRESHOLD = 2000
const mvMU_BDIV_Q_THRESHOLD = 2000
const mvMU_DIVAPPR_Q_THRESHOLD = 2000
const mvMU_DIV_QR_THRESHOLD = 2000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvODD_CENTRAL_BINOMIAL_OFFSET = 8
const mvODD_CENTRAL_BINOMIAL_TABLE_LIMIT = 18
const mvODD_DOUBLEFACTORIAL_TABLE_LIMIT = 19
const mvODD_FACTORIAL_EXTTABLE_LIMIT = 34
const mvODD_FACTORIAL_TABLE_LIMIT = 16
const mvPACKAGE = "gmp"
const mvPACKAGE_BUGREPORT = "gmp-bugs@gmplib.org (see https://gmplib.org/manual/Reporting-Bugs.html)"
const mvPACKAGE_NAME = "GNU MP"
const mvPACKAGE_STRING = "GNU MP 6.3.0"
const mvPACKAGE_TARNAME = "gmp"
const mvPACKAGE_URL = "http://www.gnu.org/software/gmp/"
const mvPACKAGE_VERSION = "6.3.0"
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPP = 0xC0CFD797
const mvPP_FIRST_OMITTED = 31
const mvPP_INVERTED = 0x53E5645C
const mvPREINV_MOD_1_TO_MOD_1_THRESHOLD = 10
const mvPRIMESIEVE_HIGHEST_PRIME = 5351
const mvPRIMESIEVE_NUMBEROF_TABLE = 56
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT32_MAX"
const mvPTRDIFF_MIN = "INT32_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvREDC_1_TO_REDC_N_THRESHOLD = 100
const mvRETSIGTYPE = "void"
const mvRE_DUP_MAX = 255
const mvRUN_MODULUS = 32
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSET_STR_DC_THRESHOLD = 750
const mvSET_STR_PRECOMPUTE_THRESHOLD = 2000
const mvSHRT_HIGHBIT = "SHRT_MIN"
const mvSHRT_MAX = 0x7fff
const mvSIEVESIZE = 512
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZEOF_MP_LIMB_T = 4
const mvSIZEOF_UNSIGNED = 4
const mvSIZEOF_UNSIGNED_LONG = 4
const mvSIZEOF_UNSIGNED_SHORT = 2
const mvSIZEOF_VOID_P = 4
const mvSIZE_MAX = "UINT32_MAX"
const mvSQRLO_BASECASE_THRESHOLD = 0
const mvSQRLO_BASECASE_THRESHOLD_LIMIT = "SQRLO_BASECASE_THRESHOLD"
const mvSQRLO_DC_THRESHOLD = "MULLO_DC_THRESHOLD"
const mvSQRLO_DC_THRESHOLD_LIMIT = "SQRLO_DC_THRESHOLD"
const mvSQRLO_SQR_THRESHOLD = "MULLO_MUL_N_THRESHOLD"
const mvSQRMOD_BNM1_THRESHOLD = 16
const mvSQR_BASECASE_THRESHOLD = 0
const mvSQR_TOOM2_THRESHOLD = 50
const mvSQR_TOOM3_THRESHOLD = 120
const mvSQR_TOOM3_THRESHOLD_LIMIT = "SQR_TOOM3_THRESHOLD"
const mvSQR_TOOM4_THRESHOLD = 400
const mvSQR_TOOM6_THRESHOLD = "MUL_TOOM6H_THRESHOLD"
const mvSQR_TOOM8_THRESHOLD = "MUL_TOOM8H_THRESHOLD"
const mvSSIZE_MAX = "LONG_MAX"
const mvSTDC_HEADERS = 1
const mvSYMLOOP_MAX = 40
const mvTABLE_LIMIT_2N_MINUS_POPC_2N = 49
const mvTARGET_REGISTER_STARVED = 0
const mvTIME_WITH_SYS_TIME = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTUNE_SQR_TOOM2_MAX = "SQR_TOOM2_MAX_GENERIC"
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUDIV_NEEDS_NORMALIZATION = 1
const mvUDIV_PREINV_ALWAYS = 0
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT32_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSE_LEADING_REGPARM = 0
const mvUSE_PREINV_DIVREM_1 = 1
const mvUSHRT_MAX = 65535
const mvVERSION = "6.3.0"
const mvWANT_ASSERT = 1
const mvWANT_FFT = 1
const mvWANT_TMP_ALLOCA = 1
const mvWANT_TMP_DEBUG = 0
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_LIMB_BITS"
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_IEEE_FLOATS = 1
const mv_GNU_SOURCE = 1
const mv_ILP32 = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_REDIR_TIME64 = 1
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 2
const mv__FLT_EVAL_METHOD__ = 2
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG -mlong-double-64"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__ILP32__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LAHF_SAHF__ = 1
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 0x7fffffff
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "ll"
const mv__PRIPTR = ""
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 12
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__code_model_32__ = 1
const mv__gnu_linux__ = 1
const mv__i386 = 1
const mv__i386__ = 1
const mv__i686 = 1
const mv__i686__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pentiumpro = 1
const mv__pentiumpro__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvbinvert_limb_table = "__gmp_binvert_limb_table"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_init_primesieve = "__gmp_init_primesieve"
const mvgmp_nextprime = "__gmp_nextprime"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_primesieve = "__gmp_primesieve"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvi386 = 1
const mvjacobi_table = "__gmp_jacobi_table"
const mvlinux = 1
const mvmodlimb_invert = "binvert_limb"
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpn_fft_mul = "mpn_nussbaumer_mul"
const mvmpn_get_d = "__gmpn_get_d"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_gcd = "__gmpz_divexact_gcd"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_inp_str_nowhite = "__gmpz_inp_str_nowhite"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucas_mod = "__gmpz_lucas_mod"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_n_pow_ui = "__gmpz_n_pow_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_oddfac_1 = "__gmpz_oddfac_1"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_prodlimbs = "__gmpz_prodlimbs"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_stronglucas = "__gmpz_stronglucas"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvrestrict = "__restrict"
const mvudiv_qrnnd = "__udiv_qrnnd_c"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint32

type tnsize_t = ppuint32

type tnssize_t = ppint32

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

type tnuintptr_t = ppuint32

type tnintptr_t = ppint32

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tngmp_uint_least32_t = ppuint32

type tngmp_intptr_t = ppint32

type tngmp_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tngmp_pi2_t = struct {
	fdinv21 tnmp_limb_t
	fdinv32 tnmp_limb_t
	fdinv53 tnmp_limb_t
}

type tutmp_align_t = struct {
	fdd           [0]ppfloat64
	fdp           [0]ppuintptr
	fdl           tnmp_limb_t
	fd__ccgo_pad3 [4]byte
}

type tstmp_reentrant_t = struct {
	fdnext ppuintptr
	fdsize tnsize_t
}

type tngmp_randfnptr_t = struct {
	fdrandseed_fn  ppuintptr
	fdrandget_fn   ppuintptr
	fdrandclear_fn ppuintptr
	fdrandiset_fn  ppuintptr
}

type tetoom6_flags = ppint32

const ectoom6_all_pos = 0
const ectoom6_vm1_neg = 1
const ectoom6_vm2_neg = 2

type tetoom7_flags = ppint32

const ectoom7_w1_neg = 1
const ectoom7_w3_neg = 2

type tngmp_primesieve_t = struct {
	fdd       ppuint32
	fds0      ppuint32
	fdsqrt_s0 ppuint32
	fds       [513]ppuint8
}

type tsfft_table_nk = struct {
	fd__ccgo0 uint32
}

type tsbases = struct {
	fdchars_per_limb    ppint32
	fdlogb2             tnmp_limb_t
	fdlog2b             tnmp_limb_t
	fdbig_base          tnmp_limb_t
	fdbig_base_inverted tnmp_limb_t
}

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tuieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type tnmp_double_limb_t = struct {
	fdd0 tnmp_limb_t
	fdd1 tnmp_limb_t
}

type tshgcd_matrix1 = struct {
	fdu [2][2]tnmp_limb_t
}

type tshgcd_matrix = struct {
	fdalloc tnmp_size_t
	fdn     tnmp_size_t
	fdp     [2][2]tnmp_ptr
}

type tsgcdext_ctx = struct {
	fdgp    tnmp_ptr
	fdgn    tnmp_size_t
	fdup    tnmp_ptr
	fdusize ppuintptr
	fdun    tnmp_size_t
	fdu0    tnmp_ptr
	fdu1    tnmp_ptr
	fdtp    tnmp_ptr
}

type tspowers = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tnpowers_t = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tsdoprnt_params_t = struct {
	fdbase         ppint32
	fdconv         ppint32
	fdexpfmt       ppuintptr
	fdexptimes4    ppint32
	fdfill         ppint8
	fdjustify      ppint32
	fdprec         ppint32
	fdshowbase     ppint32
	fdshowpoint    ppint32
	fdshowtrailing ppint32
	fdsign         ppint8
	fdwidth        ppint32
}

type tngmp_doscan_scan_t = ppuintptr

type tngmp_doscan_step_t = ppuintptr

type tngmp_doscan_get_t = ppuintptr

type tngmp_doscan_unget_t = ppuintptr

type tsgmp_doscan_funs_t = struct {
	fdscan  tngmp_doscan_scan_t
	fdstep  tngmp_doscan_step_t
	fdget   tngmp_doscan_get_t
	fdunget tngmp_doscan_unget_t
}

type tn__jmp_buf = [6]ppuint32

type tnjmp_buf = [1]ts__jmp_buf_tag

type ts__jmp_buf_tag = struct {
	fd__jb tn__jmp_buf
	fd__fl ppuint32
	fd__ss [32]ppuint32
}

type tnsigjmp_buf = [1]ts__jmp_buf_tag

/* Establish ostringstream and istringstream.  Do this here so as to hide
   the conditionals, rather than putting stuff in each test program.

   Oldish versions of g++, like 2.95.2, don't have <sstream>, only
   <strstream>.  Fake up ostringstream and istringstream classes, but not a
   full implementation, just enough for our purposes.  */

// C documentation
//
//	/* Return non-zero if regions {xp,xsize} and {yp,ysize} overlap, with sizes
//	   in bytes. */
func Xbyte_overlap_p(cgtls *iqlibc.ppTLS, aav_xp ppuintptr, aaxsize tnmp_size_t, aav_yp ppuintptr, aaysize tnmp_size_t) (cgr ppint32) {

	var aaxp, aayp ppuintptr
	pp_, pp_ = aaxp, aayp
	aaxp = aav_xp
	aayp = aav_yp

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaxsize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(50), "xsize >= 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaysize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(51), "ysize >= 0\x00")
	}

	/* no wraparounds */
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaxp+ppuintptr(aaxsize) >= aaxp)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(54), "xp+xsize >= xp\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aayp+ppuintptr(aaysize) >= aayp)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(55), "yp+ysize >= yp\x00")
	}

	if aaxp+ppuintptr(aaxsize) <= aayp {
		return 0
	}

	if aayp+ppuintptr(aaysize) <= aaxp {
		return 0
	}

	return ppint32(1)
}

// C documentation
//
//	/* Return non-zero if limb regions {xp,xsize} and {yp,ysize} overlap. */
func Xrefmpn_overlap_p(cgtls *iqlibc.ppTLS, aaxp tnmp_srcptr, aaxsize tnmp_size_t, aayp tnmp_srcptr, aaysize tnmp_size_t) (cgr ppint32) {

	return Xbyte_overlap_p(cgtls, aaxp, aaxsize*ppint32(mvSIZEOF_MP_LIMB_T), aayp, aaysize*ppint32(mvSIZEOF_MP_LIMB_T))
}

// C documentation
//
//	/* Check overlap for a routine defined to work low to high. */
func Xrefmpn_overlap_low_to_high_p(cgtls *iqlibc.ppTLS, aadst tnmp_srcptr, aasrc tnmp_srcptr, aasize tnmp_size_t) (cgr ppint32) {

	return iqlibc.ppBoolInt32(aadst <= aasrc || !(Xrefmpn_overlap_p(cgtls, aadst, aasize, aasrc, aasize) != 0))
}

// C documentation
//
//	/* Check overlap for a routine defined to work high to low. */
func Xrefmpn_overlap_high_to_low_p(cgtls *iqlibc.ppTLS, aadst tnmp_srcptr, aasrc tnmp_srcptr, aasize tnmp_size_t) (cgr ppint32) {

	return iqlibc.ppBoolInt32(aadst >= aasrc || !(Xrefmpn_overlap_p(cgtls, aadst, aasize, aasrc, aasize) != 0))
}

// C documentation
//
//	/* Check overlap for a standard routine requiring equal or separate. */
func Xrefmpn_overlap_fullonly_p(cgtls *iqlibc.ppTLS, aadst tnmp_srcptr, aasrc tnmp_srcptr, aasize tnmp_size_t) (cgr ppint32) {

	return iqlibc.ppBoolInt32(aadst == aasrc || !(Xrefmpn_overlap_p(cgtls, aadst, aasize, aasrc, aasize) != 0))
}

func Xrefmpn_overlap_fullonly_two_p(cgtls *iqlibc.ppTLS, aadst tnmp_srcptr, aasrc1 tnmp_srcptr, aasrc2 tnmp_srcptr, aasize tnmp_size_t) (cgr ppint32) {

	return iqlibc.ppBoolInt32(Xrefmpn_overlap_fullonly_p(cgtls, aadst, aasrc1, aasize) != 0 && Xrefmpn_overlap_fullonly_p(cgtls, aadst, aasrc2, aasize) != 0)
}

func Xrefmpn_malloc_limbs(cgtls *iqlibc.ppTLS, aasize tnmp_size_t) (cgr tnmp_ptr) {

	var aap tnmp_ptr
	pp_ = aap
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(107), "size >= 0\x00")
	}
	if aasize == 0 {
		aasize = ppint32(1)
	}
	aap = Xmalloc(cgtls, iqlibc.ppUint32FromInt32(aasize*iqlibc.ppInt32FromInt32(mvSIZEOF_MP_LIMB_T)))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aap != iqlibc.ppUintptrFromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(111), "p != ((void*)0)\x00")
	}
	return aap
}

// C documentation
//
//	/* Free limbs allocated by refmpn_malloc_limbs. NOTE: Can't free
//	 * memory allocated by refmpn_malloc_limbs_aligned. */
func Xrefmpn_free_limbs(cgtls *iqlibc.ppTLS, aap tnmp_ptr) {

	Xfree(cgtls, aap)
}

func Xrefmpn_memdup_limbs(cgtls *iqlibc.ppTLS, aaptr tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_ptr) {

	var aap tnmp_ptr
	pp_ = aap
	aap = Xrefmpn_malloc_limbs(cgtls, aasize)
	Xrefmpn_copyi(cgtls, aap, aaptr, aasize)
	return aap
}

// C documentation
//
//	/* malloc n limbs on a multiple of m bytes boundary */
func Xrefmpn_malloc_limbs_aligned(cgtls *iqlibc.ppTLS, aan tnmp_size_t, aam tnsize_t) (cgr tnmp_ptr) {

	return Xalign_pointer(cgtls, Xrefmpn_malloc_limbs(cgtls, iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(aan)+ppuint32(aam)-ppuint32(1))), aam)
}

func Xrefmpn_fill(cgtls *iqlibc.ppTLS, aaptr tnmp_ptr, aasize tnmp_size_t, aavalue tnmp_limb_t) {

	var aai tnmp_size_t
	pp_ = aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(144), "size >= 0\x00")
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aai)*4)) = aavalue
		goto cg_1
	cg_1:
		;
		aai++
	}
}

func Xrefmpn_zero(cgtls *iqlibc.ppTLS, aaptr tnmp_ptr, aasize tnmp_size_t) {

	Xrefmpn_fill(cgtls, aaptr, aasize, iqlibc.ppUint32FromInt32(0))
}

func Xrefmpn_zero_extend(cgtls *iqlibc.ppTLS, aaptr tnmp_ptr, aaoldsize tnmp_size_t, aanewsize tnmp_size_t) {

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aanewsize >= aaoldsize)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(158), "newsize >= oldsize\x00")
	}
	Xrefmpn_zero(cgtls, aaptr+ppuintptr(aaoldsize)*4, aanewsize-aaoldsize)
}

func Xrefmpn_zero_p(cgtls *iqlibc.ppTLS, aaptr tnmp_srcptr, aasize tnmp_size_t) (cgr ppint32) {

	var aai tnmp_size_t
	pp_ = aai
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aai)*4)) != ppuint32(0) {
			return 0
		}
		goto cg_1
	cg_1:
		;
		aai++
	}
	return ppint32(1)
}

func Xrefmpn_normalize(cgtls *iqlibc.ppTLS, aaptr tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_size_t) {

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(175), "size >= 0\x00")
	}
	for aasize > 0 && *(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aasize-ppint32(1))*4)) == ppuint32(0) {
		aasize--
	}
	return aasize
}

// C documentation
//
//	/* the highest one bit in x */
func Xrefmpn_msbone(cgtls *iqlibc.ppTLS, aax tnmp_limb_t) (cgr tnmp_limb_t) {

	var aan tnmp_limb_t
	pp_ = aan
	aan = iqlibc.ppUint32FromInt32(1) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(1))

	for aan != ppuint32(0) {

		if aax&aan != 0 {
			break
		}

		aan >>= ppuint32(1)
	}
	return aan
}

// C documentation
//
//	/* a mask of the highest one bit plus and all bits below */
func Xrefmpn_msbone_mask(cgtls *iqlibc.ppTLS, aax tnmp_limb_t) (cgr tnmp_limb_t) {

	if aax == ppuint32(0) {
		return ppuint32(0)
	}

	return Xrefmpn_msbone(cgtls, aax)<<iqlibc.ppInt32FromInt32(1) - ppuint32(1)
}

// C documentation
//
//	/* How many digits in the given base will fit in a limb.
//	   Notice that the product b is allowed to be equal to the limit
//	   2^GMP_NUMB_BITS, this ensures the result for base==2 will be
//	   GMP_NUMB_BITS (and similarly other powers of 2).  */
func Xrefmpn_chars_per_limb(cgtls *iqlibc.ppTLS, aabase ppint32) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aachars_per_limb ppint32
	var pp_ /* b at bp+8 */ [2]tnmp_limb_t
	var pp_ /* limit at bp+0 */ [2]tnmp_limb_t
	pp_ = aachars_per_limb

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aabase >= iqlibc.ppInt32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(216), "base >= 2\x00")
	}

	(*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp)))[0] = ppuint32(0) /* limit = 2^GMP_NUMB_BITS */
	(*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp)))[ppint32(1)] = ppuint32(1)
	(*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 8)))[0] = ppuint32(1) /* b = 1 */
	(*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 8)))[ppint32(1)] = ppuint32(0)

	aachars_per_limb = 0
	for {

		if Xrefmpn_mul_1(cgtls, cgbp+8, cgbp+8, iqlibc.ppInt32FromInt32(2), iqlibc.ppUint32FromInt32(aabase)) != 0 {
			break
		}
		if Xrefmpn_cmp(cgtls, cgbp+8, cgbp, iqlibc.ppInt32FromInt32(2)) > 0 {
			break
		}
		aachars_per_limb++

		goto cg_1
	cg_1:
	}
	return aachars_per_limb
}

// C documentation
//
//	/* The biggest value base**n which fits in GMP_NUMB_BITS. */
func Xrefmpn_big_base(cgtls *iqlibc.ppTLS, aabase ppint32) (cgr tnmp_limb_t) {

	var aabb tnmp_limb_t
	var aachars_per_limb, aai ppint32
	pp_, pp_, pp_ = aabb, aachars_per_limb, aai
	aachars_per_limb = Xrefmpn_chars_per_limb(cgtls, aabase)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aabase >= iqlibc.ppInt32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(243), "base >= 2\x00")
	}
	aabb = ppuint32(1)
	aai = 0
	for {
		if !(aai < aachars_per_limb) {
			break
		}

		aabb *= iqlibc.ppUint32FromInt32(aabase)
		goto cg_1
	cg_1:
		;
		aai++
	}
	return aabb
}

func Xrefmpn_setbit(cgtls *iqlibc.ppTLS, aaptr tnmp_ptr, aabit ppuint32) {

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aabit/iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*4)) |= iqlibc.ppUint32FromInt32(1) << (aabit % iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
}

func Xrefmpn_clrbit(cgtls *iqlibc.ppTLS, aaptr tnmp_ptr, aabit ppuint32) {

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aabit/iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*4)) &= ^(iqlibc.ppUint32FromInt32(1) << (aabit % iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))
}

func Xrefmpn_tstbit(cgtls *iqlibc.ppTLS, aaptr tnmp_srcptr, aabit ppuint32) (cgr ppint32) {

	return iqlibc.ppBoolInt32(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aabit/iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*4))&(iqlibc.ppUint32FromInt32(1)<<(aabit%iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != ppuint32(0))
}

func Xrefmpn_scan0(cgtls *iqlibc.ppTLS, aaptr tnmp_srcptr, aabit ppuint32) (cgr ppuint32) {

	for iqlibc.ppBoolInt32(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aabit/iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*4))&(iqlibc.ppUint32FromInt32(1)<<(aabit%iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != ppuint32(0)) != 0 {
		aabit++
	}
	return aabit
}

func Xrefmpn_scan1(cgtls *iqlibc.ppTLS, aaptr tnmp_srcptr, aabit ppuint32) (cgr ppuint32) {

	for iqlibc.ppBoolInt32(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aabit/iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*4))&(iqlibc.ppUint32FromInt32(1)<<(aabit%iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != ppuint32(0)) == 0 {
		aabit++
	}
	return aabit
}

func Xrefmpn_copy(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t) {

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(291), "refmpn_overlap_fullonly_p (rp, sp, size)\x00")
	}
	Xrefmpn_copyi(cgtls, aarp, aasp, aasize)
}

func Xrefmpn_copyi(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t) {

	var aai tnmp_size_t
	pp_ = aai

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_low_to_high_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(300), "refmpn_overlap_low_to_high_p (rp, sp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(301), "size >= 0\x00")
	}

	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4))
		goto cg_1
	cg_1:
		;
		aai++
	}
}

func Xrefmpn_copyd(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t) {

	var aai tnmp_size_t
	pp_ = aai

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_high_to_low_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(312), "refmpn_overlap_high_to_low_p (rp, sp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(313), "size >= 0\x00")
	}

	aai = aasize - ppint32(1)
	for {
		if !(aai >= 0) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4))
		goto cg_1
	cg_1:
		;
		aai--
	}
}

// C documentation
//
//	/* Copy {xp,xsize} to {wp,wsize}.  If x is shorter, then pad w with low
//	   zeros to wsize.  If x is longer, then copy just the high wsize limbs.  */
func Xrefmpn_copy_extend(cgtls *iqlibc.ppTLS, aawp tnmp_ptr, aawsize tnmp_size_t, aaxp tnmp_srcptr, aaxsize tnmp_size_t) {

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aawsize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(324), "wsize >= 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaxsize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(325), "xsize >= 0\x00")
	}

	/* high part of x if x bigger than w */
	if aaxsize > aawsize {

		aaxp += ppuintptr(aaxsize-aawsize) * 4
		aaxsize = aawsize
	}

	Xrefmpn_copy(cgtls, aawp+ppuintptr(aawsize)*4-ppuintptr(aaxsize)*4, aaxp, aaxsize)
	Xrefmpn_zero(cgtls, aawp, aawsize-aaxsize)
}

func Xrefmpn_cmp(cgtls *iqlibc.ppTLS, aaxp tnmp_srcptr, aayp tnmp_srcptr, aasize tnmp_size_t) (cgr ppint32) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(343), "size >= 1\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(344), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(345), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	aai = aasize - ppint32(1)
	for {
		if !(aai >= 0) {
			break
		}

		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aai)*4)) > *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aai)*4)) {
			return ppint32(1)
		}
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aai)*4)) < *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aai)*4)) {
			return -ppint32(1)
		}

		goto cg_3
	cg_3:
		;
		aai--
	}
	return 0
}

func Xrefmpn_cmp_allowzero(cgtls *iqlibc.ppTLS, aaxp tnmp_srcptr, aayp tnmp_srcptr, aasize tnmp_size_t) (cgr ppint32) {

	if aasize == 0 {
		return 0
	} else {
		return Xrefmpn_cmp(cgtls, aaxp, aayp, aasize)
	}
	return cgr
}

func Xrefmpn_cmp_twosizes(cgtls *iqlibc.ppTLS, aaxp tnmp_srcptr, aaxsize tnmp_size_t, aayp tnmp_srcptr, aaysize tnmp_size_t) (cgr ppint32) {

	var aa__i, aa__i1, aa__mp_size_t_swap__tmp tnmp_size_t
	var aa__mp_srcptr_swap__tmp tnmp_srcptr
	var aa__nail, aa__nail1 tnmp_limb_t
	var aacmp, aaopp, ccv3 ppint32
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__mp_size_t_swap__tmp, aa__mp_srcptr_swap__tmp, aa__nail, aa__nail1, aacmp, aaopp, ccv3

	/* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aaxsize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(370), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aaysize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(371), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	aaopp = iqlibc.ppBoolInt32(aaxsize < aaysize)
	if aaopp != 0 {
		aa__mp_srcptr_swap__tmp = aaxp
		aaxp = aayp
		aayp = aa__mp_srcptr_swap__tmp
		aa__mp_size_t_swap__tmp = aaxsize
		aaxsize = aaysize
		aaysize = aa__mp_size_t_swap__tmp
	}

	if !(Xrefmpn_zero_p(cgtls, aaxp+ppuintptr(aaysize)*4, aaxsize-aaysize) != 0) {
		aacmp = ppint32(1)
	} else {
		aacmp = Xrefmpn_cmp(cgtls, aaxp, aayp, aaysize)
	}

	if aaopp != 0 {
		ccv3 = -aacmp
	} else {
		ccv3 = aacmp
	}
	return ccv3
}

func Xrefmpn_equal_anynail(cgtls *iqlibc.ppTLS, aaxp tnmp_srcptr, aayp tnmp_srcptr, aasize tnmp_size_t) (cgr ppint32) {

	var aai tnmp_size_t
	pp_ = aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(389), "size >= 0\x00")
	}

	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aai)*4)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aai)*4)) {
			return 0
		}
		goto cg_1
	cg_1:
		;
		aai++
	}
	return ppint32(1)
}

func Xrefmpn_and_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(414), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(414), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(414), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(414), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)) & *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4))
		goto cg_3
	cg_3:
		;
		aai++
	}
}

func Xrefmpn_andn_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(419), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(419), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(419), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(419), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)) & ^*(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4))
		goto cg_3
	cg_3:
		;
		aai++
	}
}

func Xrefmpn_nand_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(424), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(424), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(424), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(424), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4))&*(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)) ^ ^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0))>>iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)
		goto cg_3
	cg_3:
		;
		aai++
	}
}

func Xrefmpn_ior_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(429), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(429), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(429), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(429), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)) | *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4))
		goto cg_3
	cg_3:
		;
		aai++
	}
}

func Xrefmpn_iorn_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(434), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(434), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(434), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(434), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)) | (*(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)) ^ ^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0))>>iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
		goto cg_3
	cg_3:
		;
		aai++
	}
}

func Xrefmpn_nior_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(439), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(439), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(439), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(439), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)) | *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)) ^ ^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0))>>iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)
		goto cg_3
	cg_3:
		;
		aai++
	}
}

func Xrefmpn_xor_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(444), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(444), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(444), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(444), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)) ^ *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4))
		goto cg_3
	cg_3:
		;
		aai++
	}
}

func Xrefmpn_xnor_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(449), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(449), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(449), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(449), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)) ^ *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)) ^ ^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0))>>iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)
		goto cg_3
	cg_3:
		;
		aai++
	}
}

// C documentation
//
//	/* set *dh,*dl to mh:ml - sh:sl, in full limbs */
func Xrefmpn_sub_ddmmss(cgtls *iqlibc.ppTLS, aadh ppuintptr, aadl ppuintptr, aamh tnmp_limb_t, aaml tnmp_limb_t, aash tnmp_limb_t, aasl tnmp_limb_t) {

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aadl)) = aaml - aasl
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aadh)) = aamh - aash - iqlibc.ppBoolUint32(aaml < aasl)
}

// C documentation
//
//	/* set *w to x+y, return 0 or 1 carry */
func Xref_addc_limb(cgtls *iqlibc.ppTLS, aaw ppuintptr, aax tnmp_limb_t, aay tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__nail, aa__nail1, aacy, aasum tnmp_limb_t
	pp_, pp_, pp_, pp_ = aa__nail, aa__nail1, aacy, aasum

	aa__nail = aax & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(469), "__nail == 0\x00")
	}
	aa__nail1 = aay & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(470), "__nail == 0\x00")
	}

	aasum = aax + aay
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaw)) = aasum
	aacy = iqlibc.ppBoolUint32(aasum < aax)
	return aacy
}

// C documentation
//
//	/* set *w to x-y, return 0 or 1 borrow */
func Xref_subc_limb(cgtls *iqlibc.ppTLS, aaw ppuintptr, aax tnmp_limb_t, aay tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__nail, aa__nail1, aacy, aadiff tnmp_limb_t
	pp_, pp_, pp_, pp_ = aa__nail, aa__nail1, aacy, aadiff

	aa__nail = aax & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(489), "__nail == 0\x00")
	}
	aa__nail1 = aay & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(490), "__nail == 0\x00")
	}

	aadiff = aax - aay
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaw)) = aadiff
	aacy = iqlibc.ppBoolUint32(aadiff > aax)
	return aacy
}

// C documentation
//
//	/* set *w to x+y+c (where c is 0 or 1), return 0 or 1 carry */
func Xadc(cgtls *iqlibc.ppTLS, aaw ppuintptr, aax tnmp_limb_t, aay tnmp_limb_t, aac tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__nail, aa__nail1, aar tnmp_limb_t
	pp_, pp_, pp_ = aa__nail, aa__nail1, aar

	aa__nail = aax & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(509), "__nail == 0\x00")
	}
	aa__nail1 = aay & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(510), "__nail == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aac == ppuint32(0) || aac == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(511), "c == 0 || c == 1\x00")
	}

	aar = Xref_addc_limb(cgtls, aaw, aax, aay)
	return aar + Xref_addc_limb(cgtls, aaw, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaw)), aac)
}

// C documentation
//
//	/* set *w to x-y-c (where c is 0 or 1), return 0 or 1 borrow */
func Xsbb(cgtls *iqlibc.ppTLS, aaw ppuintptr, aax tnmp_limb_t, aay tnmp_limb_t, aac tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__nail, aa__nail1, aar tnmp_limb_t
	pp_, pp_, pp_ = aa__nail, aa__nail1, aar

	aa__nail = aax & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(523), "__nail == 0\x00")
	}
	aa__nail1 = aay & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(524), "__nail == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aac == ppuint32(0) || aac == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(525), "c == 0 || c == 1\x00")
	}

	aar = Xref_subc_limb(cgtls, aaw, aax, aay)
	return aar + Xref_subc_limb(cgtls, aaw, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaw)), aac)
}

func Xrefmpn_add_1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aan tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_ = aa__i, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(549), "refmpn_overlap_fullonly_p (rp, sp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(549), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(549), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	aa__nail1 = aan & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(549), "__nail == 0\x00")
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aan = Xref_addc_limb(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4)), aan)
		goto cg_2
	cg_2:
		;
		aai++
	}
	return aan
	return cgr
}

func Xrefmpn_sub_1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aan tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_ = aa__i, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(554), "refmpn_overlap_fullonly_p (rp, sp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(554), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(554), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	aa__nail1 = aan & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(554), "__nail == 0\x00")
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aan = Xref_subc_limb(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4)), aan)
		goto cg_2
	cg_2:
		;
		aai++
	}
	return aan
	return cgr
}

func Xrefmpn_add_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(576), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry == ppuint32(0) || aacarry == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(576), "carry == 0 || carry == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(576), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(576), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(576), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aacarry = Xadc(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)), aacarry)
		goto cg_3
	cg_3:
		;
		aai++
	}
	return aacarry
	return cgr
}

func Xrefmpn_sub_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(582), "refmpn_overlap_fullonly_two_p (rp, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry == ppuint32(0) || aacarry == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(582), "carry == 0 || carry == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(582), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(582), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(582), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aacarry = Xsbb(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)), aacarry)
		goto cg_3
	cg_3:
		;
		aai++
	}
	return aacarry
	return cgr
}

func Xrefmpn_add_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_add_nc(cgtls, aarp, aas1p, aas2p, aasize, iqlibc.ppUint32FromInt32(0))
}

func Xrefmpn_sub_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_sub_nc(cgtls, aarp, aas1p, aas2p, aasize, iqlibc.ppUint32FromInt32(0))
}

func Xrefmpn_cnd_add_n(cgtls *iqlibc.ppTLS, aacnd tnmp_limb_t, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_limb_t) {

	if aacnd != ppuint32(0) {
		return Xrefmpn_add_n(cgtls, aarp, aas1p, aas2p, aasize)
	} else {

		Xrefmpn_copyi(cgtls, aarp, aas1p, aasize)
		return ppuint32(0)
	}
	return cgr
}

func Xrefmpn_cnd_sub_n(cgtls *iqlibc.ppTLS, aacnd tnmp_limb_t, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_limb_t) {

	if aacnd != ppuint32(0) {
		return Xrefmpn_sub_n(cgtls, aarp, aas1p, aas2p, aasize)
	} else {

		Xrefmpn_copyi(cgtls, aarp, aas1p, aasize)
		return ppuint32(0)
	}
	return cgr
}

func Xrefmpn_add_err1_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aaep tnmp_ptr, aayp tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1, aa__i2, aai tnmp_size_t
	var aa__nail, aa__nail1, aa__nail2, aacarry2, ccv4 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__i2, aa__nail, aa__nail1, aa__nail2, aacarry2, aai, ccv4
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "refmpn_overlap_fullonly_p (rp, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "refmpn_overlap_fullonly_p (rp, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aayp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "! refmpn_overlap_p (rp, size, yp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(2), aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "! refmpn_overlap_p (ep, 2, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(2), aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "! refmpn_overlap_p (ep, 2, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(2), aayp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "! refmpn_overlap_p (ep, 2, yp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(2), aarp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "! refmpn_overlap_p (ep, 2, rp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry == ppuint32(0) || aacarry == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "carry == 0 || carry == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i2 = 0
		for {
			if !(aa__i2 < aasize) {
				break
			}
			aa__nail2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aa__i2)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "__nail == 0\x00")
			}
			goto cg_3
		cg_3:
			;
			aa__i2++
		}
	}
	ccv4 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)) = ccv4
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)) = ccv4
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aacarry = Xadc(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)), aacarry)
		if aacarry == ppuint32(1) {
			aacarry2 = Xref_addc_limb(cgtls, aaep, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+1*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(660), "carry2 == 0\x00")
			}
		}
		goto cg_5
	cg_5:
		;
		aai++
	}
	return aacarry
	return cgr
}

func Xrefmpn_sub_err1_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aaep tnmp_ptr, aayp tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1, aa__i2, aai tnmp_size_t
	var aa__nail, aa__nail1, aa__nail2, aacarry2, ccv4 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__i2, aa__nail, aa__nail1, aa__nail2, aacarry2, aai, ccv4
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "refmpn_overlap_fullonly_p (rp, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "refmpn_overlap_fullonly_p (rp, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aayp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "! refmpn_overlap_p (rp, size, yp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(2), aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "! refmpn_overlap_p (ep, 2, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(2), aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "! refmpn_overlap_p (ep, 2, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(2), aayp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "! refmpn_overlap_p (ep, 2, yp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(2), aarp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "! refmpn_overlap_p (ep, 2, rp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry == ppuint32(0) || aacarry == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "carry == 0 || carry == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i2 = 0
		for {
			if !(aa__i2 < aasize) {
				break
			}
			aa__nail2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aa__i2)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "__nail == 0\x00")
			}
			goto cg_3
		cg_3:
			;
			aa__i2++
		}
	}
	ccv4 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)) = ccv4
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)) = ccv4
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aacarry = Xsbb(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)), aacarry)
		if aacarry == ppuint32(1) {
			aacarry2 = Xref_addc_limb(cgtls, aaep, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+1*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(667), "carry2 == 0\x00")
			}
		}
		goto cg_5
	cg_5:
		;
		aai++
	}
	return aacarry
	return cgr
}

func Xrefmpn_add_err2_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aaep tnmp_ptr, aay1p tnmp_srcptr, aay2p tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1, aa__i2, aa__i3, aai tnmp_size_t
	var aa__nail, aa__nail1, aa__nail2, aa__nail3, aacarry2, ccv5, ccv6 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__i2, aa__i3, aa__nail, aa__nail1, aa__nail2, aa__nail3, aacarry2, aai, ccv5, ccv6
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "refmpn_overlap_fullonly_p (rp, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "refmpn_overlap_fullonly_p (rp, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "! refmpn_overlap_p (rp, size, y1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "! refmpn_overlap_p (rp, size, y2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "! refmpn_overlap_p (ep, 4, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "! refmpn_overlap_p (ep, 4, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aay1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "! refmpn_overlap_p (ep, 4, y1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aay2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "! refmpn_overlap_p (ep, 4, y2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aarp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "! refmpn_overlap_p (ep, 4, rp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry == ppuint32(0) || aacarry == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "carry == 0 || carry == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i2 = 0
		for {
			if !(aa__i2 < aasize) {
				break
			}
			aa__nail2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay1p + ppuintptr(aa__i2)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "__nail == 0\x00")
			}
			goto cg_3
		cg_3:
			;
			aa__i2++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i3 = 0
		for {
			if !(aa__i3 < aasize) {
				break
			}
			aa__nail3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay2p + ppuintptr(aa__i3)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail3 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "__nail == 0\x00")
			}
			goto cg_4
		cg_4:
			;
			aa__i3++
		}
	}
	ccv5 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)) = ccv5
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)) = ccv5
	ccv6 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 3*4)) = ccv6
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 2*4)) = ccv6
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aacarry = Xadc(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)), aacarry)
		if aacarry == ppuint32(1) {
			aacarry2 = Xref_addc_limb(cgtls, aaep, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay1p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+1*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "carry2 == 0\x00")
			}
			aacarry2 = Xref_addc_limb(cgtls, aaep+2*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 2*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay2p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+3*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 3*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(717), "carry2 == 0\x00")
			}
		}
		goto cg_7
	cg_7:
		;
		aai++
	}
	return aacarry
	return cgr
}

func Xrefmpn_sub_err2_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aaep tnmp_ptr, aay1p tnmp_srcptr, aay2p tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1, aa__i2, aa__i3, aai tnmp_size_t
	var aa__nail, aa__nail1, aa__nail2, aa__nail3, aacarry2, ccv5, ccv6 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__i2, aa__i3, aa__nail, aa__nail1, aa__nail2, aa__nail3, aacarry2, aai, ccv5, ccv6
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "refmpn_overlap_fullonly_p (rp, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "refmpn_overlap_fullonly_p (rp, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "! refmpn_overlap_p (rp, size, y1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "! refmpn_overlap_p (rp, size, y2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "! refmpn_overlap_p (ep, 4, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "! refmpn_overlap_p (ep, 4, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aay1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "! refmpn_overlap_p (ep, 4, y1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aay2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "! refmpn_overlap_p (ep, 4, y2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(4), aarp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "! refmpn_overlap_p (ep, 4, rp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry == ppuint32(0) || aacarry == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "carry == 0 || carry == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i2 = 0
		for {
			if !(aa__i2 < aasize) {
				break
			}
			aa__nail2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay1p + ppuintptr(aa__i2)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "__nail == 0\x00")
			}
			goto cg_3
		cg_3:
			;
			aa__i2++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i3 = 0
		for {
			if !(aa__i3 < aasize) {
				break
			}
			aa__nail3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay2p + ppuintptr(aa__i3)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail3 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "__nail == 0\x00")
			}
			goto cg_4
		cg_4:
			;
			aa__i3++
		}
	}
	ccv5 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)) = ccv5
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)) = ccv5
	ccv6 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 3*4)) = ccv6
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 2*4)) = ccv6
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aacarry = Xsbb(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)), aacarry)
		if aacarry == ppuint32(1) {
			aacarry2 = Xref_addc_limb(cgtls, aaep, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay1p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+1*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "carry2 == 0\x00")
			}
			aacarry2 = Xref_addc_limb(cgtls, aaep+2*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 2*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay2p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+3*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 3*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(724), "carry2 == 0\x00")
			}
		}
		goto cg_7
	cg_7:
		;
		aai++
	}
	return aacarry
	return cgr
}

func Xrefmpn_add_err3_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aaep tnmp_ptr, aay1p tnmp_srcptr, aay2p tnmp_srcptr, aay3p tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1, aa__i2, aa__i3, aa__i4, aai tnmp_size_t
	var aa__nail, aa__nail1, aa__nail2, aa__nail3, aa__nail4, aacarry2, ccv6, ccv7, ccv8 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__i2, aa__i3, aa__i4, aa__nail, aa__nail1, aa__nail2, aa__nail3, aa__nail4, aacarry2, aai, ccv6, ccv7, ccv8
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "refmpn_overlap_fullonly_p (rp, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "refmpn_overlap_fullonly_p (rp, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "! refmpn_overlap_p (rp, size, y1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "! refmpn_overlap_p (rp, size, y2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay3p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "! refmpn_overlap_p (rp, size, y3p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "! refmpn_overlap_p (ep, 6, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "! refmpn_overlap_p (ep, 6, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aay1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "! refmpn_overlap_p (ep, 6, y1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aay2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "! refmpn_overlap_p (ep, 6, y2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aay3p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "! refmpn_overlap_p (ep, 6, y3p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aarp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "! refmpn_overlap_p (ep, 6, rp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry == ppuint32(0) || aacarry == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "carry == 0 || carry == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i2 = 0
		for {
			if !(aa__i2 < aasize) {
				break
			}
			aa__nail2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay1p + ppuintptr(aa__i2)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "__nail == 0\x00")
			}
			goto cg_3
		cg_3:
			;
			aa__i2++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i3 = 0
		for {
			if !(aa__i3 < aasize) {
				break
			}
			aa__nail3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay2p + ppuintptr(aa__i3)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail3 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "__nail == 0\x00")
			}
			goto cg_4
		cg_4:
			;
			aa__i3++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i4 = 0
		for {
			if !(aa__i4 < aasize) {
				break
			}
			aa__nail4 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay3p + ppuintptr(aa__i4)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail4 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "__nail == 0\x00")
			}
			goto cg_5
		cg_5:
			;
			aa__i4++
		}
	}
	ccv6 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)) = ccv6
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)) = ccv6
	ccv7 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 3*4)) = ccv7
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 2*4)) = ccv7
	ccv8 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 5*4)) = ccv8
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 4*4)) = ccv8
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aacarry = Xadc(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)), aacarry)
		if aacarry == ppuint32(1) {
			aacarry2 = Xref_addc_limb(cgtls, aaep, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay1p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+1*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "carry2 == 0\x00")
			}
			aacarry2 = Xref_addc_limb(cgtls, aaep+2*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 2*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay2p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+3*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 3*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "carry2 == 0\x00")
			}
			aacarry2 = Xref_addc_limb(cgtls, aaep+4*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 4*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay3p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+5*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 5*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(781), "carry2 == 0\x00")
			}
		}
		goto cg_9
	cg_9:
		;
		aai++
	}
	return aacarry
	return cgr
}

func Xrefmpn_sub_err3_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aaep tnmp_ptr, aay1p tnmp_srcptr, aay2p tnmp_srcptr, aay3p tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1, aa__i2, aa__i3, aa__i4, aai tnmp_size_t
	var aa__nail, aa__nail1, aa__nail2, aa__nail3, aa__nail4, aacarry2, ccv6, ccv7, ccv8 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__i2, aa__i3, aa__i4, aa__nail, aa__nail1, aa__nail2, aa__nail3, aa__nail4, aacarry2, aai, ccv6, ccv7, ccv8
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "refmpn_overlap_fullonly_p (rp, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "refmpn_overlap_fullonly_p (rp, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "! refmpn_overlap_p (rp, size, y1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "! refmpn_overlap_p (rp, size, y2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aasize, aay3p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "! refmpn_overlap_p (rp, size, y3p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aas1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "! refmpn_overlap_p (ep, 6, s1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "! refmpn_overlap_p (ep, 6, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aay1p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "! refmpn_overlap_p (ep, 6, y1p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aay2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "! refmpn_overlap_p (ep, 6, y2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aay3p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "! refmpn_overlap_p (ep, 6, y3p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaep, ppint32(6), aarp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "! refmpn_overlap_p (ep, 6, rp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry == ppuint32(0) || aacarry == ppuint32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "carry == 0 || carry == 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "size >= 1\x00")
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i2 = 0
		for {
			if !(aa__i2 < aasize) {
				break
			}
			aa__nail2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay1p + ppuintptr(aa__i2)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "__nail == 0\x00")
			}
			goto cg_3
		cg_3:
			;
			aa__i2++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i3 = 0
		for {
			if !(aa__i3 < aasize) {
				break
			}
			aa__nail3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay2p + ppuintptr(aa__i3)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail3 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "__nail == 0\x00")
			}
			goto cg_4
		cg_4:
			;
			aa__i3++
		}
	} /* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i4 = 0
		for {
			if !(aa__i4 < aasize) {
				break
			}
			aa__nail4 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aay3p + ppuintptr(aa__i4)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail4 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "__nail == 0\x00")
			}
			goto cg_5
		cg_5:
			;
			aa__i4++
		}
	}
	ccv6 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)) = ccv6
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)) = ccv6
	ccv7 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 3*4)) = ccv7
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 2*4)) = ccv7
	ccv8 = iqlibc.ppUint32FromInt32(0)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 5*4)) = ccv8
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 4*4)) = ccv8
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		aacarry = Xsbb(cgtls, aarp+ppuintptr(aai)*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aai)*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aai)*4)), aacarry)
		if aacarry == ppuint32(1) {
			aacarry2 = Xref_addc_limb(cgtls, aaep, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay1p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+1*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 1*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "carry2 == 0\x00")
			}
			aacarry2 = Xref_addc_limb(cgtls, aaep+2*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 2*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay2p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+3*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 3*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "carry2 == 0\x00")
			}
			aacarry2 = Xref_addc_limb(cgtls, aaep+4*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 4*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aay3p + ppuintptr(aasize-ppint32(1)-aai)*4)))
			aacarry2 = Xref_addc_limb(cgtls, aaep+5*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaep + 5*4)), aacarry2)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(788), "carry2 == 0\x00")
			}
		}
		goto cg_9
	cg_9:
		;
		aai++
	}
	return aacarry
	return cgr
}

func Xrefmpn_addlsh_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32) (cgr tnmp_limb_t) {

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1, aacy tnmp_limb_t
	var aatp tnmp_ptr
	pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aacy, aatp

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aaup, aavp, aan) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(799), "refmpn_overlap_fullonly_two_p (rp, up, vp, n)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aan >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(800), "n >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppuint32(0) < aas && aas < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(801), "0 < s && s < (32 - 0)\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aan) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(802), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aan) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(803), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	aatp = Xrefmpn_malloc_limbs(cgtls, aan)
	aacy = Xrefmpn_lshift(cgtls, aatp, aavp, aan, aas)

	aacy += Xrefmpn_add_n(cgtls, aarp, aaup, aatp, aan)
	Xfree(cgtls, aatp)
	return aacy
}

func Xrefmpn_addlsh1_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_n(cgtls, aarp, aaup, aavp, aan, ppuint32(1))
}

func Xrefmpn_addlsh2_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_n(cgtls, aarp, aaup, aavp, aan, ppuint32(2))
}

func Xrefmpn_addlsh_n_ip1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_n(cgtls, aarp, aarp, aavp, aan, aas)
}

func Xrefmpn_addlsh1_n_ip1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_n(cgtls, aarp, aarp, aavp, aan, ppuint32(1))
}

func Xrefmpn_addlsh2_n_ip1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_n(cgtls, aarp, aarp, aavp, aan, ppuint32(2))
}

func Xrefmpn_addlsh_n_ip2(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_n(cgtls, aarp, aavp, aarp, aan, aas)
}

func Xrefmpn_addlsh1_n_ip2(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_n(cgtls, aarp, aavp, aarp, aan, ppuint32(1))
}

func Xrefmpn_addlsh2_n_ip2(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_n(cgtls, aarp, aavp, aarp, aan, ppuint32(2))
}

func Xrefmpn_addlsh_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aacy tnmp_limb_t
	pp_ = aacy

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry <= iqlibc.ppUint32FromInt32(1)<<aas)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(857), "carry <= (((mp_limb_t) 1L) << s)\x00")
	}

	aacy = Xrefmpn_addlsh_n(cgtls, aarp, aaup, aavp, aan, aas)

	aacy += Xrefmpn_add_1(cgtls, aarp, aarp, aan, aacarry)
	return aacy
}

func Xrefmpn_addlsh1_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_nc(cgtls, aarp, aaup, aavp, aan, ppuint32(1), aacarry)
}

func Xrefmpn_addlsh2_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_addlsh_nc(cgtls, aarp, aaup, aavp, aan, ppuint32(2), aacarry)
}

func Xrefmpn_sublsh_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32) (cgr tnmp_limb_t) {

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1, aacy tnmp_limb_t
	var aatp tnmp_ptr
	pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aacy, aatp

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aaup, aavp, aan) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(881), "refmpn_overlap_fullonly_two_p (rp, up, vp, n)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aan >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(882), "n >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppuint32(0) < aas && aas < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(883), "0 < s && s < (32 - 0)\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aan) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(884), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aan) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(885), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	aatp = Xrefmpn_malloc_limbs(cgtls, aan)
	aacy = X__gmpn_lshift(cgtls, aatp, aavp, aan, aas)

	aacy += X__gmpn_sub_n(cgtls, aarp, aaup, aatp, aan)
	Xfree(cgtls, aatp)
	return aacy
}

func Xrefmpn_sublsh1_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_n(cgtls, aarp, aaup, aavp, aan, ppuint32(1))
}

func Xrefmpn_sublsh2_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_n(cgtls, aarp, aaup, aavp, aan, ppuint32(2))
}

func Xrefmpn_sublsh_n_ip1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_n(cgtls, aarp, aarp, aavp, aan, aas)
}

func Xrefmpn_sublsh1_n_ip1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_n(cgtls, aarp, aarp, aavp, aan, ppuint32(1))
}

func Xrefmpn_sublsh2_n_ip1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_n(cgtls, aarp, aarp, aavp, aan, ppuint32(2))
}

func Xrefmpn_sublsh_n_ip2(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_n(cgtls, aarp, aavp, aarp, aan, aas)
}

func Xrefmpn_sublsh1_n_ip2(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_n(cgtls, aarp, aavp, aarp, aan, ppuint32(1))
}

func Xrefmpn_sublsh2_n_ip2(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_n(cgtls, aarp, aavp, aarp, aan, ppuint32(2))
}

func Xrefmpn_sublsh_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aacy tnmp_limb_t
	pp_ = aacy

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry <= iqlibc.ppUint32FromInt32(1)<<aas)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(939), "carry <= (((mp_limb_t) 1L) << s)\x00")
	}

	aacy = Xrefmpn_sublsh_n(cgtls, aarp, aaup, aavp, aan, aas)

	aacy += Xrefmpn_sub_1(cgtls, aarp, aarp, aan, aacarry)
	return aacy
}

func Xrefmpn_sublsh1_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_nc(cgtls, aarp, aaup, aavp, aan, ppuint32(1), aacarry)
}

func Xrefmpn_sublsh2_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_sublsh_nc(cgtls, aarp, aaup, aavp, aan, ppuint32(2), aacarry)
}

func Xrefmpn_rsblsh_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32) (cgr tnmp_limb_signed_t) {

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	var aacy tnmp_limb_signed_t
	var aatp tnmp_ptr
	pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aacy, aatp

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aaup, aavp, aan) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(963), "refmpn_overlap_fullonly_two_p (rp, up, vp, n)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aan >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(964), "n >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ppuint32(0) < aas && aas < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(965), "0 < s && s < (32 - 0)\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aan) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(966), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aan) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(967), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	aatp = Xrefmpn_malloc_limbs(cgtls, aan)
	aacy = iqlibc.ppInt32FromUint32(X__gmpn_lshift(cgtls, aatp, aavp, aan, aas))

	aacy = tnmp_limb_signed_t(ppuint32(aacy) - X__gmpn_sub_n(cgtls, aarp, aatp, aaup, aan))
	Xfree(cgtls, aatp)
	return aacy
}

func Xrefmpn_rsblsh1_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_signed_t) {

	return Xrefmpn_rsblsh_n(cgtls, aarp, aaup, aavp, aan, ppuint32(1))
}

func Xrefmpn_rsblsh2_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_signed_t) {

	return Xrefmpn_rsblsh_n(cgtls, aarp, aaup, aavp, aan, ppuint32(2))
}

func Xrefmpn_rsblsh_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aas ppuint32, aacarry tnmp_limb_signed_t) (cgr tnmp_limb_signed_t) {

	var aacy tnmp_limb_signed_t
	pp_ = aacy

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry == ppint32(-ppint32(1)) || aacarry>>aas == 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(991), "carry == -1 || (carry >> s) == 0\x00")
	}

	aacy = Xrefmpn_rsblsh_n(cgtls, aarp, aaup, aavp, aan, aas)
	if aacarry > 0 {

		aacy = tnmp_limb_signed_t(ppuint32(aacy) + Xrefmpn_add_1(cgtls, aarp, aarp, aan, iqlibc.ppUint32FromInt32(aacarry)))
	} else {

		aacy = tnmp_limb_signed_t(ppuint32(aacy) - Xrefmpn_sub_1(cgtls, aarp, aarp, aan, iqlibc.ppUint32FromInt32(-aacarry)))
	}
	return aacy
}

func Xrefmpn_rsblsh1_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aacarry tnmp_limb_signed_t) (cgr tnmp_limb_signed_t) {

	return Xrefmpn_rsblsh_nc(cgtls, aarp, aaup, aavp, aan, ppuint32(1), aacarry)
}

func Xrefmpn_rsblsh2_nc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aacarry tnmp_limb_signed_t) (cgr tnmp_limb_signed_t) {

	return Xrefmpn_rsblsh_nc(cgtls, aarp, aaup, aavp, aan, ppuint32(2), aacarry)
}

func Xrefmpn_rsh1add_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1, aacya, aacys tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aacya, aacys

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aaup, aavp, aan) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1016), "refmpn_overlap_fullonly_two_p (rp, up, vp, n)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aan >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1017), "n >= 1\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aan) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1018), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aan) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1019), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	aacya = X__gmpn_add_n(cgtls, aarp, aaup, aavp, aan)
	aacys = X__gmpn_rshift(cgtls, aarp, aarp, aan, ppuint32(1)) >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) - iqlibc.ppInt32FromInt32(1))

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aan-ppint32(1))*4)) |= aacya << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) - iqlibc.ppInt32FromInt32(1))
	return aacys
}

func Xrefmpn_rsh1sub_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1, aacya, aacys tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aacya, aacys

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aarp, aaup, aavp, aan) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1031), "refmpn_overlap_fullonly_two_p (rp, up, vp, n)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aan >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1032), "n >= 1\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aan) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1033), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aan) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1034), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	aacya = X__gmpn_sub_n(cgtls, aarp, aaup, aavp, aan)
	aacys = X__gmpn_rshift(cgtls, aarp, aarp, aan, ppuint32(1)) >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) - iqlibc.ppInt32FromInt32(1))

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aan-ppint32(1))*4)) |= aacya << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) - iqlibc.ppInt32FromInt32(1))
	return aacys
}

// C documentation
//
//	/* Twos complement, return borrow. */
func Xrefmpn_neg(cgtls *iqlibc.ppTLS, aadst tnmp_ptr, aasrc tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_limb_t) {

	var aaret tnmp_limb_t
	var aazeros tnmp_ptr
	pp_, pp_ = aaret, aazeros

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1049), "size >= 1\x00")
	}

	aazeros = Xrefmpn_malloc_limbs(cgtls, aasize)
	Xrefmpn_fill(cgtls, aazeros, aasize, iqlibc.ppUint32FromInt32(0))
	aaret = Xrefmpn_sub_n(cgtls, aadst, aazeros, aasrc, aasize)
	Xfree(cgtls, aazeros)
	return aaret
}

func Xrefmpn_add(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas1size tnmp_size_t, aas2p tnmp_srcptr, aas2size tnmp_size_t) (cgr tnmp_limb_t) {

	var aac tnmp_limb_t
	pp_ = aac
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aas1size >= aas2size)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1074), "s1size >= s2size\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aas2size >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1074), "s2size >= 1\x00")
	}
	aac = Xrefmpn_add_n(cgtls, aarp, aas1p, aas2p, aas2size)
	if aas1size-aas2size != 0 {
		aac = Xrefmpn_add_1(cgtls, aarp+ppuintptr(aas2size)*4, aas1p+ppuintptr(aas2size)*4, aas1size-aas2size, aac)
	}
	return aac
	return cgr
}

func Xrefmpn_sub(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aas1p tnmp_srcptr, aas1size tnmp_size_t, aas2p tnmp_srcptr, aas2size tnmp_size_t) (cgr tnmp_limb_t) {

	var aac tnmp_limb_t
	pp_ = aac
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aas1size >= aas2size)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1081), "s1size >= s2size\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aas2size >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1081), "s2size >= 1\x00")
	}
	aac = Xrefmpn_sub_n(cgtls, aarp, aas1p, aas2p, aas2size)
	if aas1size-aas2size != 0 {
		aac = Xrefmpn_sub_1(cgtls, aarp+ppuintptr(aas2size)*4, aas1p+ppuintptr(aas2size)*4, aas1size-aas2size, aac)
	}
	return aac
	return cgr
}

// C documentation
//
//	/* Set return:*lo to x*y, using full limbs not nails. */
func Xrefmpn_umul_ppmm(cgtls *iqlibc.ppTLS, aalo ppuintptr, aax tnmp_limb_t, aay tnmp_limb_t) (cgr tnmp_limb_t) {

	var aahi, aas tnmp_limb_t
	pp_, pp_ = aahi, aas

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aalo)) = aax & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) * (aay & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)))
	aahi = aax & ((iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))) >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)) * (aay & ((iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))) >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)))

	aas = aax & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) * (aay & ((iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))) >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)))

	aahi += aas & ((iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))) >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))
	aas = aas & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aalo)) += aas

	aahi += iqlibc.ppBoolUint32(*(*tnmp_limb_t)(iqunsafe.ppPointer(aalo)) < aas)

	aas = aax & ((iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))) >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)) * (aay & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)))

	aahi += aas & ((iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))) >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))
	aas = aas & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aalo)) += aas

	aahi += iqlibc.ppBoolUint32(*(*tnmp_limb_t)(iqunsafe.ppPointer(aalo)) < aas)

	return aahi
}

func Xrefmpn_umul_ppmm_r(cgtls *iqlibc.ppTLS, aax tnmp_limb_t, aay tnmp_limb_t, aalo ppuintptr) (cgr tnmp_limb_t) {

	return Xrefmpn_umul_ppmm(cgtls, aalo, aax, aay)
}

func Xrefmpn_mul_1c(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamultiplier tnmp_limb_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aa__i, aai tnmp_size_t
	var aa__nail, aa__nail1, aa__nail2 tnmp_limb_t
	var pp_ /* hi at bp+0 */ tnmp_limb_t
	var pp_ /* lo at bp+4 */ tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__nail, aa__nail1, aa__nail2, aai

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_low_to_high_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1131), "refmpn_overlap_low_to_high_p (rp, sp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1132), "size >= 1\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1133), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	aa__nail1 = aamultiplier & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1134), "__nail == 0\x00")
	}
	aa__nail2 = aacarry & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1135), "__nail == 0\x00")
	}

	aamultiplier <<= ppuint32(mvGMP_NAIL_BITS)
	aai = 0
	for {
		if !(aai < aasize) {
			break
		}

		*(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp)) = Xrefmpn_umul_ppmm(cgtls, cgbp+4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4)), aamultiplier)

		*(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 4)) >>= ppuint32(mvGMP_NAIL_BITS)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xref_addc_limb(cgtls, cgbp, *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp)), Xref_addc_limb(cgtls, cgbp+4, *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 4)), aacarry)) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1142), "(ref_addc_limb (&hi, hi, ref_addc_limb (&lo, lo, carry))) == 0\x00")
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 4))
		aacarry = *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp))

		goto cg_2
	cg_2:
		;
		aai++
	}
	return aacarry
}

func Xrefmpn_mul_1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamultiplier tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_mul_1c(cgtls, aarp, aasp, aasize, aamultiplier, iqlibc.ppUint32FromInt32(0))
}

func Xrefmpn_mul_N(cgtls *iqlibc.ppTLS, aadst tnmp_ptr, aasrc tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr, aamsize tnmp_size_t) (cgr tnmp_limb_t) {

	var aa__i, aai tnmp_size_t
	var aa__nail, aaret tnmp_limb_t
	var aasrc_copy tnmp_ptr
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__nail, aai, aaret, aasrc_copy

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aadst, aasrc, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1164), "refmpn_overlap_fullonly_p (dst, src, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aadst, aasize+aamsize-ppint32(1), aamult, aamsize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1165), "! refmpn_overlap_p (dst, size+msize-1, mult, msize)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= aamsize)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1166), "size >= msize\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aamsize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aamult + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1167), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}

	/* in case dst==src */
	aasrc_copy = Xrefmpn_malloc_limbs(cgtls, aasize)
	Xrefmpn_copyi(cgtls, aasrc_copy, aasrc, aasize)
	aasrc = aasrc_copy

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aadst + ppuintptr(aasize)*4)) = Xrefmpn_mul_1(cgtls, aadst, aasrc, aasize, *(*tnmp_limb_t)(iqunsafe.ppPointer(aamult)))
	aai = ppint32(1)
	for {
		if !(aai < aamsize-ppint32(1)) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aadst + ppuintptr(aasize+aai)*4)) = Xrefmpn_addmul_1(cgtls, aadst+ppuintptr(aai)*4, aasrc, aasize, *(*tnmp_limb_t)(iqunsafe.ppPointer(aamult + ppuintptr(aai)*4)))
		goto cg_2
	cg_2:
		;
		aai++
	}
	aaret = Xrefmpn_addmul_1(cgtls, aadst+ppuintptr(aai)*4, aasrc, aasize, *(*tnmp_limb_t)(iqunsafe.ppPointer(aamult + ppuintptr(aai)*4)))

	Xfree(cgtls, aasrc_copy)
	return aaret
}

func Xrefmpn_mul_2(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_mul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(2))
}

func Xrefmpn_mul_3(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_mul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(3))
}

func Xrefmpn_mul_4(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_mul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(4))
}

func Xrefmpn_mul_5(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_mul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(5))
}

func Xrefmpn_mul_6(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_mul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(6))
}

func Xrefmpn_addmul_1c(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamultiplier tnmp_limb_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aap tnmp_ptr
	var aaret tnmp_limb_t
	pp_, pp_ = aap, aaret
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1228), "refmpn_overlap_fullonly_p (rp, sp, size)\x00")
	}
	aap = Xrefmpn_malloc_limbs(cgtls, aasize)
	aaret = Xrefmpn_mul_1c(cgtls, aap, aasp, aasize, aamultiplier, aacarry)
	aaret += Xrefmpn_add_n(cgtls, aarp, aarp, aap, aasize)
	Xfree(cgtls, aap)
	return aaret
	return cgr
}

func Xrefmpn_submul_1c(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamultiplier tnmp_limb_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aap tnmp_ptr
	var aaret tnmp_limb_t
	pp_, pp_ = aap, aaret
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1234), "refmpn_overlap_fullonly_p (rp, sp, size)\x00")
	}
	aap = Xrefmpn_malloc_limbs(cgtls, aasize)
	aaret = Xrefmpn_mul_1c(cgtls, aap, aasp, aasize, aamultiplier, aacarry)
	aaret += Xrefmpn_sub_n(cgtls, aarp, aarp, aap, aasize)
	Xfree(cgtls, aap)
	return aaret
	return cgr
}

func Xrefmpn_addmul_1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamultiplier tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_addmul_1c(cgtls, aarp, aasp, aasize, aamultiplier, iqlibc.ppUint32FromInt32(0))
}

func Xrefmpn_submul_1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamultiplier tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_submul_1c(cgtls, aarp, aasp, aasize, aamultiplier, iqlibc.ppUint32FromInt32(0))
}

func Xrefmpn_addmul_N(cgtls *iqlibc.ppTLS, aadst tnmp_ptr, aasrc tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr, aamsize tnmp_size_t) (cgr tnmp_limb_t) {

	var aa__i, aai tnmp_size_t
	var aa__nail, aaret tnmp_limb_t
	var aasrc_copy tnmp_ptr
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__nail, aai, aaret, aasrc_copy

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aadst == aasrc || !(Xrefmpn_overlap_p(cgtls, aadst, aasize+aamsize-ppint32(1), aasrc, aasize) != 0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1258), "dst == src || ! refmpn_overlap_p (dst, size+msize-1, src, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aadst, aasize+aamsize-ppint32(1), aamult, aamsize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1259), "! refmpn_overlap_p (dst, size+msize-1, mult, msize)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= aamsize)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1260), "size >= msize\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aamsize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aamult + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1261), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}

	/* in case dst==src */
	aasrc_copy = Xrefmpn_malloc_limbs(cgtls, aasize)
	Xrefmpn_copyi(cgtls, aasrc_copy, aasrc, aasize)
	aasrc = aasrc_copy

	aai = 0
	for {
		if !(aai < aamsize-ppint32(1)) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aadst + ppuintptr(aasize+aai)*4)) = Xrefmpn_addmul_1(cgtls, aadst+ppuintptr(aai)*4, aasrc, aasize, *(*tnmp_limb_t)(iqunsafe.ppPointer(aamult + ppuintptr(aai)*4)))
		goto cg_2
	cg_2:
		;
		aai++
	}
	aaret = Xrefmpn_addmul_1(cgtls, aadst+ppuintptr(aai)*4, aasrc, aasize, *(*tnmp_limb_t)(iqunsafe.ppPointer(aamult + ppuintptr(aai)*4)))

	Xfree(cgtls, aasrc_copy)
	return aaret
}

func Xrefmpn_addmul_2(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_addmul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(2))
}

func Xrefmpn_addmul_3(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_addmul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(3))
}

func Xrefmpn_addmul_4(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_addmul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(4))
}

func Xrefmpn_addmul_5(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_addmul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(5))
}

func Xrefmpn_addmul_6(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_addmul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(6))
}

func Xrefmpn_addmul_7(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_addmul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(7))
}

func Xrefmpn_addmul_8(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aamult tnmp_srcptr) (cgr tnmp_limb_t) {

	return Xrefmpn_addmul_N(cgtls, aarp, aasp, aasize, aamult, iqlibc.ppInt32FromInt32(8))
}

func Xrefmpn_add_n_sub_nc(cgtls *iqlibc.ppTLS, aar1p tnmp_ptr, aar2p tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aaacy, aascy tnmp_limb_t
	var aap tnmp_ptr
	pp_, pp_, pp_ = aaacy, aap, aascy

	/* Destinations can't overlap. */
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aar1p, aasize, aar2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1321), "! refmpn_overlap_p (r1p, size, r2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aar1p, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1322), "refmpn_overlap_fullonly_two_p (r1p, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_two_p(cgtls, aar2p, aas1p, aas2p, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1323), "refmpn_overlap_fullonly_two_p (r2p, s1p, s2p, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1324), "size >= 1\x00")
	}

	/* in case r1p==s1p or r1p==s2p */
	aap = Xrefmpn_malloc_limbs(cgtls, aasize)

	aaacy = Xrefmpn_add_nc(cgtls, aap, aas1p, aas2p, aasize, aacarry>>ppint32(1))
	aascy = Xrefmpn_sub_nc(cgtls, aar2p, aas1p, aas2p, aasize, aacarry&ppuint32(1))
	Xrefmpn_copyi(cgtls, aar1p, aap, aasize)

	Xfree(cgtls, aap)
	return ppuint32(2)*aaacy + aascy
}

func Xrefmpn_add_n_sub_n(cgtls *iqlibc.ppTLS, aar1p tnmp_ptr, aar2p tnmp_ptr, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_add_n_sub_nc(cgtls, aar1p, aar2p, aas1p, aas2p, aasize, iqlibc.ppUint32FromInt32(0))
}

// C documentation
//
//	/* Right shift hi,lo and return the low limb of the result.
//	   Note a shift by GMP_LIMB_BITS isn't assumed to work (doesn't on x86). */
func Xrshift_make(cgtls *iqlibc.ppTLS, aahi tnmp_limb_t, aalo tnmp_limb_t, aashift ppuint32) (cgr tnmp_limb_t) {

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aashift < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1350), "shift < (32 - 0)\x00")
	}
	if aashift == ppuint32(0) {
		return aalo
	} else {
		return (aahi<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-aashift) | aalo>>aashift) & (^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	}
	return cgr
}

// C documentation
//
//	/* Left shift hi,lo and return the high limb of the result.
//	   Note a shift by GMP_LIMB_BITS isn't assumed to work (doesn't on x86). */
func Xlshift_make(cgtls *iqlibc.ppTLS, aahi tnmp_limb_t, aalo tnmp_limb_t, aashift ppuint32) (cgr tnmp_limb_t) {

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aashift < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1362), "shift < (32 - 0)\x00")
	}
	if aashift == ppuint32(0) {
		return aahi
	} else {
		return (aahi<<aashift | aalo>>(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-aashift)) & (^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	}
	return cgr
}

func Xrefmpn_rshift(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aashift ppuint32) (cgr tnmp_limb_t) {

	var aa__i, aai tnmp_size_t
	var aa__nail, aaret tnmp_limb_t
	pp_, pp_, pp_, pp_ = aa__i, aa__nail, aai, aaret

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_low_to_high_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1376), "refmpn_overlap_low_to_high_p (rp, sp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1377), "size >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aashift >= ppuint32(1) && aashift < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1378), "shift >= 1 && shift < (32 - 0)\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1379), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}

	aaret = Xrshift_make(cgtls, *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp)), iqlibc.ppUint32FromInt32(0), aashift)

	aai = 0
	for {
		if !(aai < aasize-ppint32(1)) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = Xrshift_make(cgtls, *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai+ppint32(1))*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4)), aashift)
		goto cg_2
	cg_2:
		;
		aai++
	}

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = Xrshift_make(cgtls, iqlibc.ppUint32FromInt32(0), *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4)), aashift)
	return aaret
}

func Xrefmpn_lshift(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aashift ppuint32) (cgr tnmp_limb_t) {

	var aa__i, aai tnmp_size_t
	var aa__nail, aaret tnmp_limb_t
	pp_, pp_, pp_, pp_ = aa__i, aa__nail, aai, aaret

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_high_to_low_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1396), "refmpn_overlap_high_to_low_p (rp, sp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1397), "size >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aashift >= ppuint32(1) && aashift < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1398), "shift >= 1 && shift < (32 - 0)\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1399), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}

	aaret = Xlshift_make(cgtls, iqlibc.ppUint32FromInt32(0), *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aasize-ppint32(1))*4)), aashift)

	aai = aasize - ppint32(2)
	for {
		if !(aai >= 0) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai+ppint32(1))*4)) = Xlshift_make(cgtls, *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai+ppint32(1))*4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4)), aashift)
		goto cg_2
	cg_2:
		;
		aai--
	}

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai+ppint32(1))*4)) = Xlshift_make(cgtls, *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai+ppint32(1))*4)), iqlibc.ppUint32FromInt32(0), aashift)
	return aaret
}

func Xrefmpn_com(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t) {

	var aai tnmp_size_t
	pp_ = aai

	/* We work downwards since mpn_lshiftc needs that. */
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_high_to_low_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1416), "refmpn_overlap_high_to_low_p (rp, sp, size)\x00")
	}

	aai = aasize - ppint32(1)
	for {
		if !(aai >= 0) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = ^*(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4)) & (^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
		goto cg_1
	cg_1:
		;
		aai--
	}
}

func Xrefmpn_lshiftc(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aashift ppuint32) (cgr tnmp_limb_t) {

	var aares tnmp_limb_t
	pp_ = aares

	/* No asserts here, refmpn_lshift will assert what we need. */

	aares = Xrefmpn_lshift(cgtls, aarp, aasp, aasize, aashift)
	Xrefmpn_com(cgtls, aarp, aarp, aasize)
	return aares
}

// C documentation
//
//	/* accepting shift==0 and doing a plain copyi or copyd in that case */
func Xrefmpn_rshift_or_copy(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aashift ppuint32) (cgr tnmp_limb_t) {

	if aashift == ppuint32(0) {

		Xrefmpn_copyi(cgtls, aarp, aasp, aasize)
		return ppuint32(0)
	} else {

		return Xrefmpn_rshift(cgtls, aarp, aasp, aasize, aashift)
	}
	return cgr
}

func Xrefmpn_lshift_or_copy(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aashift ppuint32) (cgr tnmp_limb_t) {

	if aashift == ppuint32(0) {

		Xrefmpn_copyd(cgtls, aarp, aasp, aasize)
		return ppuint32(0)
	} else {

		return Xrefmpn_lshift(cgtls, aarp, aasp, aasize, aashift)
	}
	return cgr
}

// C documentation
//
//	/* accepting size==0 too */
func Xrefmpn_rshift_or_copy_any(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aashift ppuint32) (cgr tnmp_limb_t) {

	var ccv1 ppuint32
	pp_ = ccv1
	if aasize == 0 {
		ccv1 = ppuint32(0)
	} else {
		ccv1 = Xrefmpn_rshift_or_copy(cgtls, aarp, aasp, aasize, aashift)
	}
	return ccv1
}

func Xrefmpn_lshift_or_copy_any(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aashift ppuint32) (cgr tnmp_limb_t) {

	var ccv1 ppuint32
	pp_ = ccv1
	if aasize == 0 {
		ccv1 = ppuint32(0)
	} else {
		ccv1 = Xrefmpn_lshift_or_copy(cgtls, aarp, aasp, aasize, aashift)
	}
	return ccv1
}

// C documentation
//
//	/* Divide h,l by d, return quotient, store remainder to *rp.
//	   Operates on full limbs, not nails.
//	   Must have h < d.
//	   __udiv_qrnnd_c isn't simple, and it's a bit slow, but it works. */
func Xrefmpn_udiv_qrnnd(cgtls *iqlibc.ppTLS, aarp ppuintptr, aah tnmp_limb_t, aal tnmp_limb_t, aad tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__d0, aa__d1, aa__m, aa__q0, aa__q1, aa__r0, aa__r1 tnUWtype
	var aan ppint32
	var aaq, aar tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__d0, aa__d1, aa__m, aa__q0, aa__q1, aa__r0, aa__r1, aan, aaq, aar

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aad != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1486), "d != 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aah < aad)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1487), "h < d\x00")
	}

	aan = iqlibc.ppInt32FromUint32(Xrefmpn_count_leading_zeros(cgtls, aad))

	aad <<= iqlibc.ppUint32FromInt32(aan)

	if aan != 0 {

		aah = aah<<aan | aal>>(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-aan)

		aal <<= iqlibc.ppUint32FromInt32(aan)
	}

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aad != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1504), "(d) != 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aah < aad)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1504), "(h) < (d)\x00")
	}
	aa__d1 = aad >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))
	aa__d0 = aad & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1))
	aa__q1 = aah / aa__d1
	aa__r1 = aah - aa__q1*aa__d1
	aa__m = aa__q1 * aa__d0
	aa__r1 = aa__r1*(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))) | aal>>(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))
	if aa__r1 < aa__m {
		aa__q1--
		aa__r1 += aad
		if aa__r1 >= aad { /* i.e. we didn't get carry when adding to __r1 */
			if aa__r1 < aa__m {
				aa__q1--
				aa__r1 += aad
			}
		}
	}
	aa__r1 -= aa__m
	aa__q0 = aa__r1 / aa__d1
	aa__r0 = aa__r1 - aa__q0*aa__d1
	aa__m = aa__q0 * aa__d0
	aa__r0 = aa__r0*(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))) | aal&(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))-iqlibc.ppUint32FromInt32(1))
	if aa__r0 < aa__m {
		aa__q0--
		aa__r0 += aad
		if aa__r0 >= aad {
			if aa__r0 < aa__m {
				aa__q0--
				aa__r0 += aad
			}
		}
	}
	aa__r0 -= aa__m
	aaq = aa__q1*(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))) | aa__q0
	aar = aa__r0

	aar >>= iqlibc.ppUint32FromInt32(aan)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp)) = aar
	return aaq
}

func Xrefmpn_udiv_qrnnd_r(cgtls *iqlibc.ppTLS, aah tnmp_limb_t, aal tnmp_limb_t, aad tnmp_limb_t, aarp ppuintptr) (cgr tnmp_limb_t) {

	return Xrefmpn_udiv_qrnnd(cgtls, aarp, aah, aal, aad)
}

// C documentation
//
//	/* This little subroutine avoids some bad code generation from i386 gcc 3.0
//	   -fPIC -O2 -fomit-frame-pointer (%ebp being used uninitialized).  */
func sirefmpn_divmod_1c_workaround(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aadivisor tnmp_limb_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aai tnmp_size_t
	var pp_ /* rem at bp+0 */ [1]tnmp_limb_t
	pp_ = aai
	aai = aasize - ppint32(1)
	for {
		if !(aai >= 0) {
			break
		}

		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aai)*4)) = Xrefmpn_udiv_qrnnd(cgtls, cgbp, aacarry, *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4))<<mvGMP_NAIL_BITS, aadivisor<<mvGMP_NAIL_BITS)
		aacarry = *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp)) >> mvGMP_NAIL_BITS

		goto cg_1
	cg_1:
		;
		aai--
	}
	return aacarry
}

func Xrefmpn_divmod_1c(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aadivisor tnmp_limb_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i tnmp_size_t
	var aa__nail, aa__nail1, aa__nail2, aacarry_orig tnmp_limb_t
	var aaprod, aasp_orig tnmp_ptr
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__nail, aa__nail1, aa__nail2, aacarry_orig, aaprod, aasp_orig

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1542), "refmpn_overlap_fullonly_p (rp, sp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1543), "size >= 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry < aadivisor)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1544), "carry < divisor\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1545), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	aa__nail1 = aadivisor & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1546), "__nail == 0\x00")
	}
	aa__nail2 = aacarry & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1547), "__nail == 0\x00")
	}

	if aasize == 0 {
		return aacarry
	}

	aasp_orig = Xrefmpn_memdup_limbs(cgtls, aasp, aasize)
	aaprod = Xrefmpn_malloc_limbs(cgtls, aasize)
	aacarry_orig = aacarry

	aacarry = sirefmpn_divmod_1c_workaround(cgtls, aarp, aasp, aasize, aadivisor, aacarry)

	/* check by multiplying back */
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_mul_1c(cgtls, aaprod, aarp, aasize, aadivisor, aacarry) == aacarry_orig)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1567), "refmpn_mul_1c (prod, rp, size, divisor, carry) == carry_orig\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_cmp(cgtls, aaprod, aasp_orig, aasize) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1568), "refmpn_cmp (prod, sp_orig, size) == 0\x00")
	}
	Xfree(cgtls, aasp_orig)
	Xfree(cgtls, aaprod)

	return aacarry
}

func Xrefmpn_divmod_1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aadivisor tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_divmod_1c(cgtls, aarp, aasp, aasize, aadivisor, iqlibc.ppUint32FromInt32(0))
}

func Xrefmpn_mod_1c(cgtls *iqlibc.ppTLS, aasp tnmp_srcptr, aasize tnmp_size_t, aadivisor tnmp_limb_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aap tnmp_ptr
	pp_ = aap
	aap = Xrefmpn_malloc_limbs(cgtls, aasize)
	aacarry = Xrefmpn_divmod_1c(cgtls, aap, aasp, aasize, aadivisor, aacarry)
	Xfree(cgtls, aap)
	return aacarry
}

func Xrefmpn_mod_1(cgtls *iqlibc.ppTLS, aasp tnmp_srcptr, aasize tnmp_size_t, aadivisor tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_mod_1c(cgtls, aasp, aasize, aadivisor, iqlibc.ppUint32FromInt32(0))
}

func Xrefmpn_preinv_mod_1(cgtls *iqlibc.ppTLS, aasp tnmp_srcptr, aasize tnmp_size_t, aadivisor tnmp_limb_t, aainverse tnmp_limb_t) (cgr tnmp_limb_t) {

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aadivisor&(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1))) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1602), "divisor & (((mp_limb_t) 1L) << ((32 - 0)-1))\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aainverse == Xrefmpn_invert_limb(cgtls, aadivisor))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1603), "inverse == refmpn_invert_limb (divisor)\x00")
	}
	return Xrefmpn_mod_1(cgtls, aasp, aasize, aadivisor)
}

// C documentation
//
//	/* This implementation will be rather slow, but has the advantage of being
//	   in a different style than the libgmp versions.  */
func Xrefmpn_mod_34lsub1(cgtls *iqlibc.ppTLS, aap tnmp_srcptr, aan tnmp_size_t) (cgr tnmp_limb_t) {

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!((iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))%iqlibc.ppInt32FromInt32(4) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1612), "((32 - 0) % 4) == 0\x00")
	}
	return X__gmpn_mod_1(cgtls, aap, aan, iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(3)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))/iqlibc.ppInt32FromInt32(4))-iqlibc.ppUint32FromInt32(1))
}

func Xrefmpn_divrem_1c(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaxsize tnmp_size_t, aasp tnmp_srcptr, aasize tnmp_size_t, aadivisor tnmp_limb_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aaz tnmp_ptr
	pp_ = aaz

	aaz = Xrefmpn_malloc_limbs(cgtls, aaxsize)
	Xrefmpn_fill(cgtls, aaz, aaxsize, iqlibc.ppUint32FromInt32(0))

	aacarry = Xrefmpn_divmod_1c(cgtls, aarp+ppuintptr(aaxsize)*4, aasp, aasize, aadivisor, aacarry)
	aacarry = Xrefmpn_divmod_1c(cgtls, aarp, aaz, aaxsize, aadivisor, aacarry)

	Xfree(cgtls, aaz)
	return aacarry
}

func Xrefmpn_divrem_1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaxsize tnmp_size_t, aasp tnmp_srcptr, aasize tnmp_size_t, aadivisor tnmp_limb_t) (cgr tnmp_limb_t) {

	return Xrefmpn_divrem_1c(cgtls, aarp, aaxsize, aasp, aasize, aadivisor, iqlibc.ppUint32FromInt32(0))
}

func Xrefmpn_preinv_divrem_1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaxsize tnmp_size_t, aasp tnmp_srcptr, aasize tnmp_size_t, aadivisor tnmp_limb_t, aainverse tnmp_limb_t, aashift ppuint32) (cgr tnmp_limb_t) {

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1646), "size >= 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aashift == Xrefmpn_count_leading_zeros(cgtls, aadivisor))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1647), "shift == refmpn_count_leading_zeros (divisor)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aainverse == Xrefmpn_invert_limb(cgtls, aadivisor<<aashift))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1648), "inverse == refmpn_invert_limb (divisor << shift)\x00")
	}

	return Xrefmpn_divrem_1(cgtls, aarp, aaxsize, aasp, aasize, aadivisor)
}

func Xrefmpn_divrem_2(cgtls *iqlibc.ppTLS, aaqp tnmp_ptr, aaqxn tnmp_size_t, aanp tnmp_ptr, aann tnmp_size_t, aadp tnmp_srcptr) (cgr tnmp_limb_t) {

	var aaqh tnmp_limb_t
	var aatp tnmp_ptr
	pp_, pp_ = aaqh, aatp

	aatp = Xrefmpn_malloc_limbs(cgtls, aann+aaqxn)
	Xrefmpn_zero(cgtls, aatp, aaqxn)
	Xrefmpn_copyi(cgtls, aatp+ppuintptr(aaqxn)*4, aanp, aann)
	aaqh = Xrefmpn_sb_div_qr(cgtls, aaqp, aatp, aann+aaqxn, aadp, ppint32(2))
	Xrefmpn_copyi(cgtls, aanp, aatp, ppint32(2))
	Xfree(cgtls, aatp)
	return aaqh
}

/* Inverse is floor((b*(b-d)-1) / d), per division by invariant integers
   paper, figure 8.1 m', where b=2^GMP_LIMB_BITS.  Note that -d-1 < d
   since d has the high bit set. */

func Xrefmpn_invert_limb(cgtls *iqlibc.ppTLS, aad tnmp_limb_t) (cgr tnmp_limb_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var pp_ /* r at bp+0 */ tnmp_limb_t
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aad&(^iqlibc.ppUint32FromInt32(0)^^iqlibc.ppUint32FromInt32(0)>>iqlibc.ppInt32FromInt32(1)) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1678), "d & ((~ (mp_limb_t) 0) ^ ((~ (mp_limb_t) 0) >> 1))\x00")
	}
	return Xrefmpn_udiv_qrnnd(cgtls, cgbp, -aad-ppuint32(1), ^iqlibc.ppUint32FromInt32(0), aad)
}

func Xrefmpn_invert(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aan tnmp_size_t, aascratch tnmp_ptr) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aa__dst, aaqp, aatp, ccv5, ccv6 tnmp_ptr
	var aa__gmp_c, aa__gmp_r, aa__gmp_x, ccv9 tnmp_limb_t
	var aa__gmp_i, aa__gmp_j, aa__gmp_j1, aa__n, ccv3, ccv8 tnmp_size_t
	var ccv1, ccv2 ppuintptr
	var ccv7 tnmp_srcptr
	var pp_ /* __tmp_marker at bp+0 */ ppuintptr
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__dst, aa__gmp_c, aa__gmp_i, aa__gmp_j, aa__gmp_j1, aa__gmp_r, aa__gmp_x, aa__n, aaqp, aatp, ccv1, ccv2, ccv3, ccv5, ccv6, ccv7, ccv8, ccv9
	cgtls.AllocaEntry()
	defer cgtls.AllocaExit()
	*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) = ppuintptr(0)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)*aan)*ppuint32(4) <= ppuint32(0x7f00)) != 0), ppint32(1)) != 0 {
		ccv1 = X__builtin_alloca(cgtls, ppuint32(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)*aan)*ppuint32(4)))
	} else {
		ccv1 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, ppuint32(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)*aan)*ppuint32(4)))
	}
	aatp = ccv1
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(iqlibc.ppUint32FromInt32(aan+iqlibc.ppInt32FromInt32(1))*ppuint32(4) <= ppuint32(0x7f00)) != 0), ppint32(1)) != 0 {
		ccv2 = X__builtin_alloca(cgtls, ppuint32(iqlibc.ppUint32FromInt32(aan+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
	} else {
		ccv2 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, ppuint32(iqlibc.ppUint32FromInt32(aan+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
	}
	aaqp = ccv2

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppInt32FromInt32(2)*aan >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1692), "(2 * n) >= 0\x00")
	}
	if ppint32(2)*aan != 0 {
		aa__dst = aatp
		aa__n = iqlibc.ppInt32FromInt32(2) * aan
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__n > iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1692), "__n > 0\x00")
		}
		for {
			ccv5 = aa__dst
			aa__dst += 4
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv5)) = iqlibc.ppUint32FromInt32(0)
			goto cg_4
		cg_4:
			;
			aa__n--
			ccv3 = aa__n
			if !(ccv3 != 0) {
				break
			}
		}
	}
	ccv6 = aatp
	ccv7 = aatp
	ccv8 = ppint32(2) * aan
	ccv9 = ppuint32(1)
	aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv7))
	aa__gmp_r = aa__gmp_x - ccv9
	*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv6)) = aa__gmp_r
	if aa__gmp_x < ccv9 {
		aa__gmp_c = ppuint32(1)
		aa__gmp_i = iqlibc.ppInt32FromInt32(1)
		for {
			if !(aa__gmp_i < ccv8) {
				break
			}
			aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv7 + ppuintptr(aa__gmp_i)*4))
			aa__gmp_r = aa__gmp_x - ppuint32(1)
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv6 + ppuintptr(aa__gmp_i)*4)) = aa__gmp_r
			aa__gmp_i++
			if !(aa__gmp_x < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1))) {
				if ccv7 != ccv6 {
					aa__gmp_j = aa__gmp_i
					for {
						if !(aa__gmp_j < ccv8) {
							break
						}
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv6 + ppuintptr(aa__gmp_j)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv7 + ppuintptr(aa__gmp_j)*4))
						goto cg_11
					cg_11:
						;
						aa__gmp_j++
					}
				}
				aa__gmp_c = ppuint32(0)
				break
			}
			goto cg_10
		cg_10:
		}
	} else {
		if ccv7 != ccv6 {
			aa__gmp_j1 = ppint32(iqlibc.ppInt32FromInt32(1))
			for {
				if !(aa__gmp_j1 < ccv8) {
					break
				}
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv6 + ppuintptr(aa__gmp_j1)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv7 + ppuintptr(aa__gmp_j1)*4))
				goto cg_12
			cg_12:
				;
				aa__gmp_j1++
			}
		}
		aa__gmp_c = ppuint32(0)
	}
	pp_ = aa__gmp_c
	goto cg_13
cg_13:
	;

	Xrefmpn_tdiv_qr(cgtls, aaqp, aarp, 0, aatp, ppint32(2)*aan, aaup, aan)
	Xrefmpn_copyi(cgtls, aarp, aaqp, aan)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) != ppuintptr(0)) != 0), 0) != 0 {
		X__gmp_tmp_reentrant_free(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp)))
	}
}

func Xrefmpn_binvert(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aan tnmp_size_t, aascratch tnmp_ptr) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aa__dst, aatp, ccv4 tnmp_ptr
	var aa__inv, aa__n1, aabinv tnmp_limb_t
	var aa__invbits, ccv7 ppint32
	var aa__n, ccv2, ccv5, ccv6 tnmp_size_t
	var ccv1 ppuintptr
	var ccv9 ppbool
	var pp_ /* __tmp_marker at bp+0 */ ppuintptr
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__dst, aa__inv, aa__invbits, aa__n, aa__n1, aabinv, aatp, ccv1, ccv2, ccv4, ccv5, ccv6, ccv7, ccv9
	cgtls.AllocaEntry()
	defer cgtls.AllocaExit()
	*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) = ppuintptr(0)

	/* We use the library mpn_sbpi1_bdiv_q here, which isn't kosher in testing
	   code.  To make up for it, we check that the inverse is correct using a
	   multiply.  */

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)*aan)*ppuint32(4) <= ppuint32(0x7f00)) != 0), ppint32(1)) != 0 {
		ccv1 = X__builtin_alloca(cgtls, ppuint32(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)*aan)*ppuint32(4)))
	} else {
		ccv1 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, ppuint32(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)*aan)*ppuint32(4)))
	}
	aatp = ccv1

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aan >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1714), "(n) >= 0\x00")
	}
	if aan != 0 {
		aa__dst = aatp
		aa__n = aan
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__n > iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1714), "__n > 0\x00")
		}
		for {
			ccv4 = aa__dst
			aa__dst += 4
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv4)) = iqlibc.ppUint32FromInt32(0)
			goto cg_3
		cg_3:
			;
			aa__n--
			ccv2 = aa__n
			if !(ccv2 != 0) {
				break
			}
		}
	}
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aatp)) = ppuint32(1)
	aa__n1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__n1&iqlibc.ppUint32FromInt32(1) == iqlibc.ppUint32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1716), "(__n & 1) == 1\x00")
	}
	aa__inv = ppuint32(X__gmp_binvert_limb_table[aa__n1/ppuint32(2)&ppuint32(0x7F)]) /*  8 */
	if iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) > ppint32(8) {
		aa__inv = ppuint32(2)*aa__inv - aa__inv*aa__inv*aa__n1
	}
	if iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) > ppint32(16) {
		aa__inv = ppuint32(2)*aa__inv - aa__inv*aa__inv*aa__n1
	}
	if iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) > ppint32(32) {
		aa__inv = ppuint32(2)*aa__inv - aa__inv*aa__inv*aa__n1
	}
	if iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) > ppint32(64) {
		aa__invbits = ppint32(64)
		for cgcond := true; cgcond; cgcond = aa__invbits < iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) {
			aa__inv = ppuint32(2)*aa__inv - aa__inv*aa__inv*aa__n1
			aa__invbits *= ppint32(2)
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__inv*aa__n1&(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0))>>iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) == iqlibc.ppUint32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1716), "(__inv * __n & ((~ ((mp_limb_t) (0))) >> 0)) == 1\x00")
	}
	aabinv = aa__inv & (^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	X__gmpn_sbpi1_bdiv_q(cgtls, aarp, aatp, aan, aaup, aan, -aabinv)

	Xrefmpn_mul_n(cgtls, aatp, aarp, aaup, aan)

	if ccv9 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aatp)) == ppuint32(1); ccv9 {
		ccv5 = aan - ppint32(1)
		for cgcond := true; cgcond; cgcond = ccv5 != iqlibc.ppInt32FromInt32(0) {
			ccv5--
			ccv6 = ccv5
			if *(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(1)*4 + ppuintptr(ccv6)*4)) != iqlibc.ppUint32FromInt32(0) {
				ccv7 = 0
				goto cg_8
			}
		}
		ccv7 = ppint32(1)
		goto cg_8
	cg_8:
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(ccv9 && ccv7 != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1720), "tp[0] == 1 && __gmpn_zero_p (tp + 1, n - 1)\x00")
	}

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) != ppuintptr(0)) != 0), 0) != 0 {
		X__gmp_tmp_reentrant_free(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp)))
	}
}

/* The aim is to produce a dst quotient and return a remainder c, satisfying
   c*b^n + src-i == 3*dst, where i is the incoming carry.

   Some value c==0, c==1 or c==2 will satisfy, so just try each.

   If GMP_NUMB_BITS is even then 2^GMP_NUMB_BITS==1mod3 and a non-zero
   remainder from the first division attempt determines the correct
   remainder (3-c), but don't bother with that, since we can't guarantee
   anything about GMP_NUMB_BITS when using nails.

   If the initial src-i produces a borrow then refmpn_sub_1 leaves a twos
   complement negative, ie. b^n+a-i, and the calculation produces c1
   satisfying c1*b^n + b^n+src-i == 3*dst, from which clearly c=c1+1.  This
   means it's enough to just add any borrow back at the end.

   A borrow only occurs when a==0 or a==1, and, by the same reasoning as in
   mpn/generic/diveby3.c, the c1 that results in those cases will only be 0
   or 1 respectively, so with 1 added the final return value is still in the
   prescribed range 0 to 2. */

func Xrefmpn_divexact_by3c(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t, aacarry tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i tnmp_size_t
	var aa__nail, aac, aacs tnmp_limb_t
	var aaspcopy tnmp_ptr
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__nail, aac, aacs, aaspcopy

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aarp, aasp, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1751), "refmpn_overlap_fullonly_p (rp, sp, size)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1752), "size >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacarry <= iqlibc.ppUint32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1753), "carry <= 2\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1754), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}

	aaspcopy = Xrefmpn_malloc_limbs(cgtls, aasize)
	aacs = Xrefmpn_sub_1(cgtls, aaspcopy, aasp, aasize, aacarry)

	aac = ppuint32(0)
	for {
		if !(aac <= ppuint32(2)) {
			break
		}
		if Xrefmpn_divmod_1c(cgtls, aarp, aaspcopy, aasize, iqlibc.ppUint32FromInt32(3), aac) == ppuint32(0) {
			goto ppdone
		}
		goto cg_2
	cg_2:
		;
		aac++
	}
	X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1762), "no value of c satisfies\x00")

	goto ppdone
ppdone:
	;

	aac += aacs
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aac <= iqlibc.ppUint32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1766), "c <= 2\x00")
	}

	Xfree(cgtls, aaspcopy)
	return aac
}

func Xrefmpn_divexact_by3(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aasp tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_limb_t) {

	return Xrefmpn_divexact_by3c(cgtls, aarp, aasp, aasize, iqlibc.ppUint32FromInt32(0))
}

// C documentation
//
//	/* The same as mpn/generic/mul_basecase.c, but using refmpn functions. */
func Xrefmpn_mul_basecase(cgtls *iqlibc.ppTLS, aaprodp tnmp_ptr, aaup tnmp_srcptr, aausize tnmp_size_t, aavp tnmp_srcptr, aavsize tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aai

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaprodp, aausize+aavsize, aaup, aausize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1787), "! refmpn_overlap_p (prodp, usize+vsize, up, usize)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaprodp, aausize+aavsize, aavp, aavsize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1788), "! refmpn_overlap_p (prodp, usize+vsize, vp, vsize)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aausize >= aavsize)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1789), "usize >= vsize\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aavsize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1790), "vsize >= 1\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aausize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1791), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aavsize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1792), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aaprodp + ppuintptr(aausize)*4)) = Xrefmpn_mul_1(cgtls, aaprodp, aaup, aausize, *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp)))
	aai = ppint32(1)
	for {
		if !(aai < aavsize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aaprodp + ppuintptr(aausize+aai)*4)) = Xrefmpn_addmul_1(cgtls, aaprodp+ppuintptr(aai)*4, aaup, aausize, *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aai)*4)))
		goto cg_3
	cg_3:
		;
		aai++
	}
}

// C documentation
//
//	/* The same as mpn/generic/mulmid_basecase.c, but using refmpn functions. */
func Xrefmpn_mulmid_basecase(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aaun tnmp_size_t, aavp tnmp_srcptr, aavn tnmp_size_t) {

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1, aacy tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aacy, aai

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaun >= aavn)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1809), "un >= vn\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aavn >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1810), "vn >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aaun-aavn+ppint32(3), aaup, aaun) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1811), "! refmpn_overlap_p (rp, un - vn + 3, up, un)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aarp, aaun-aavn+ppint32(3), aavp, aavn) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1812), "! refmpn_overlap_p (rp, un - vn + 3, vp, vn)\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aaun) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1813), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aavn) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1814), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aaun-aavn+ppint32(1))*4)) = Xrefmpn_mul_1(cgtls, aarp, aaup+ppuintptr(aavn)*4-ppuintptr(1)*4, aaun-aavn+ppint32(1), *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp)))
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aaun-aavn+ppint32(2))*4)) = iqlibc.ppUint32FromInt32(0)
	aai = ppint32(1)
	for {
		if !(aai < aavn) {
			break
		}

		aacy = Xrefmpn_addmul_1(cgtls, aarp, aaup+ppuintptr(aavn)*4-ppuintptr(aai)*4-ppuintptr(1)*4, aaun-aavn+ppint32(1), *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aai)*4)))
		aacy = Xref_addc_limb(cgtls, aarp+ppuintptr(aaun-aavn+ppint32(1))*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aaun-aavn+ppint32(1))*4)), aacy)
		aacy = Xref_addc_limb(cgtls, aarp+ppuintptr(aaun-aavn+ppint32(2))*4, *(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aaun-aavn+ppint32(2))*4)), aacy)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aacy == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1823), "cy == 0\x00")
		}

		goto cg_3
	cg_3:
		;
		aai++
	}
}

func Xrefmpn_toom42_mulmid(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t, aascratch tnmp_ptr) {

	Xrefmpn_mulmid_basecase(cgtls, aarp, aaup, ppint32(2)*aan-ppint32(1), aavp, aan)
}

func Xrefmpn_mulmid_n(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aan tnmp_size_t) {

	/* FIXME: this could be made faster by using refmpn_mul and then subtracting
	   off products near the middle product region boundary */
	Xrefmpn_mulmid_basecase(cgtls, aarp, aaup, ppint32(2)*aan-ppint32(1), aavp, aan)
}

func Xrefmpn_mulmid(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_srcptr, aaun tnmp_size_t, aavp tnmp_srcptr, aavn tnmp_size_t) {

	/* FIXME: this could be made faster by using refmpn_mul and then subtracting
	   off products near the middle product region boundary */
	Xrefmpn_mulmid_basecase(cgtls, aarp, aaup, aaun, aavp, aavn)
}

func Xrefmpn_mul(cgtls *iqlibc.ppTLS, aawp tnmp_ptr, aaup tnmp_srcptr, aaun tnmp_size_t, aavp tnmp_srcptr, aavn tnmp_size_t) {

	var aa__dst, aa__dst1, aarp, aatp, ccv3, ccv6 tnmp_ptr
	var aa__n, aa__n1, aaestimatedN, aatn, ccv1, ccv4, ccv7 tnmp_size_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__dst, aa__dst1, aa__n, aa__n1, aaestimatedN, aarp, aatn, aatp, ccv1, ccv3, ccv4, ccv6, ccv7

	if aavn < ppint32(iqlibc.ppInt32FromInt32(mvSQR_TOOM3_THRESHOLD)) {

		/* In the mpn_mul_basecase and toom2 ranges, use our own mul_basecase. */
		if aavn != 0 {
			Xrefmpn_mul_basecase(cgtls, aawp, aaup, aaun, aavp, aavn)
		} else {
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaun >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1874), "(un) >= 0\x00")
			}
			if aaun != 0 {
				aa__dst = aawp
				aa__n = aaun
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__n > iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1874), "__n > 0\x00")
				}
				for {
					ccv3 = aa__dst
					aa__dst += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv3)) = iqlibc.ppUint32FromInt32(0)
					goto cg_2
				cg_2:
					;
					aa__n--
					ccv1 = aa__n
					if !(ccv1 != 0) {
						break
					}
				}
			}
		}
		return
	}

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aavn >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1878), "(vn) >= 0\x00")
	}
	if aavn != 0 {
		aa__dst1 = aawp
		aa__n1 = aavn
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__n1 > iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1878), "__n > 0\x00")
		}
		for {
			ccv6 = aa__dst1
			aa__dst1 += 4
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv6)) = iqlibc.ppUint32FromInt32(0)
			goto cg_5
		cg_5:
			;
			aa__n1--
			ccv4 = aa__n1
			if !(ccv4 != 0) {
				break
			}
		}
	}
	aarp = Xrefmpn_malloc_limbs(cgtls, ppint32(2)*aavn)

	if aavn < ppint32(iqlibc.ppInt32FromInt32(mvSQR_TOOM4_THRESHOLD)) {
		aatn = iqlibc.ppInt32FromInt32(2) * (aavn + ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
	} else {
		if aavn < ppint32(iqlibc.ppInt32FromInt32(mvMUL_TOOM6H_THRESHOLD)) {
			aatn = iqlibc.ppInt32FromInt32(3)*aavn + ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
		} else {
			if aavn < ppint32(iqlibc.ppInt32FromInt32(mvSQR_TOOM3_THRESHOLD)*iqlibc.ppInt32FromInt32(3)*iqlibc.ppInt32FromInt32(10)) {
				aatn = iqlibc.ppInt32FromInt32(3)*aavn + ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			} else {
				aaestimatedN = iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(aavn+aavn)/ppuint32(iqlibc.ppUint32FromInt32(10)) + ppuint32(1))
				ccv7 = (aaestimatedN*iqlibc.ppInt32FromInt32(6)-ppint32(iqlibc.ppInt32FromInt32(mvMUL_TOOM6H_THRESHOLD)))*iqlibc.ppInt32FromInt32(2) + ppint32(iqlibc.ppInt32FromInt32(3)*iqlibc.ppInt32FromInt32(mvMUL_TOOM6H_THRESHOLD)+(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
				goto cg_8
			cg_8:
				aatn = ccv7
			}
		}
	}
	aatp = Xrefmpn_malloc_limbs(cgtls, aatn)

	for aaun >= aavn {

		if aavn < ppint32(iqlibc.ppInt32FromInt32(mvSQR_TOOM4_THRESHOLD)) {
			/* In the toom3 range, use mpn_toom22_mul.  */
			X__gmpn_toom22_mul(cgtls, aarp, aaup, aavn, aavp, aavn, aatp)
		} else {
			if aavn < ppint32(iqlibc.ppInt32FromInt32(mvMUL_TOOM6H_THRESHOLD)) {
				/* In the toom4 range, use mpn_toom33_mul.  */
				X__gmpn_toom33_mul(cgtls, aarp, aaup, aavn, aavp, aavn, aatp)
			} else {
				if aavn < ppint32(iqlibc.ppInt32FromInt32(mvSQR_TOOM3_THRESHOLD)*iqlibc.ppInt32FromInt32(3)*iqlibc.ppInt32FromInt32(10)) {
					/* In the toom6 range, use mpn_toom44_mul.  */
					X__gmpn_toom44_mul(cgtls, aarp, aaup, aavn, aavp, aavn, aatp)
				} else {
					/* For the largest operands, use mpn_toom6h_mul.  */
					X__gmpn_toom6h_mul(cgtls, aarp, aaup, aavn, aavp, aavn, aatp)
				}
			}
		}

		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_add(cgtls, aawp, aarp, ppint32(2)*aavn, aawp, aavn) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1906), "(refmpn_add (wp, rp, 2 * vn, wp, vn)) == 0\x00")
		}

		aawp += ppuintptr(aavn) * 4

		aaup += ppuintptr(aavn) * 4

		aaun -= aavn
	}

	Xfree(cgtls, aatp)

	if aaun != 0 {

		Xrefmpn_mul(cgtls, aarp, aavp, aavn, aaup, aaun)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_add(cgtls, aawp, aarp, aaun+aavn, aawp, aavn) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1918), "(refmpn_add (wp, rp, un + vn, wp, vn)) == 0\x00")
		}
	}
	Xfree(cgtls, aarp)
}

func Xrefmpn_mul_n(cgtls *iqlibc.ppTLS, aaprodp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aasize tnmp_size_t) {

	Xrefmpn_mul(cgtls, aaprodp, aaup, aasize, aavp, aasize)
}

func Xrefmpn_mullo_n(cgtls *iqlibc.ppTLS, aaprodp tnmp_ptr, aaup tnmp_srcptr, aavp tnmp_srcptr, aasize tnmp_size_t) {

	var aatp tnmp_ptr
	pp_ = aatp
	aatp = Xrefmpn_malloc_limbs(cgtls, ppint32(2)*aasize)
	Xrefmpn_mul(cgtls, aatp, aaup, aasize, aavp, aasize)
	Xrefmpn_copyi(cgtls, aaprodp, aatp, aasize)
	Xfree(cgtls, aatp)
}

func Xrefmpn_sqr(cgtls *iqlibc.ppTLS, aadst tnmp_ptr, aasrc tnmp_srcptr, aasize tnmp_size_t) {

	Xrefmpn_mul(cgtls, aadst, aasrc, aasize, aasrc, aasize)
}

func Xrefmpn_sqrlo(cgtls *iqlibc.ppTLS, aadst tnmp_ptr, aasrc tnmp_srcptr, aasize tnmp_size_t) {

	Xrefmpn_mullo_n(cgtls, aadst, aasrc, aasrc, aasize)
}

// C documentation
//
//	/* Allowing usize<vsize, usize==0 or vsize==0. */
func Xrefmpn_mul_any(cgtls *iqlibc.ppTLS, aaprodp tnmp_ptr, aaup tnmp_srcptr, aausize tnmp_size_t, aavp tnmp_srcptr, aavsize tnmp_size_t) {

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaprodp, aausize+aavsize, aaup, aausize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1956), "! refmpn_overlap_p (prodp, usize+vsize, up, usize)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaprodp, aausize+aavsize, aavp, aavsize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1957), "! refmpn_overlap_p (prodp, usize+vsize, vp, vsize)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aausize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1958), "usize >= 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aavsize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1959), "vsize >= 0\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aausize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1960), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aavsize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aavp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1961), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	if aausize == 0 {

		Xrefmpn_fill(cgtls, aaprodp, aavsize, iqlibc.ppUint32FromInt32(0))
		return
	}

	if aavsize == 0 {

		Xrefmpn_fill(cgtls, aaprodp, aausize, iqlibc.ppUint32FromInt32(0))
		return
	}

	if aausize >= aavsize {
		Xrefmpn_mul(cgtls, aaprodp, aaup, aausize, aavp, aavsize)
	} else {
		Xrefmpn_mul(cgtls, aaprodp, aavp, aavsize, aaup, aausize)
	}
}

func Xrefmpn_gcd_11(cgtls *iqlibc.ppTLS, aax tnmp_limb_t, aay tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__mp_limb_t_swap__tmp tnmp_limb_t
	pp_ = aa__mp_limb_t_swap__tmp
	/* The non-ref function also requires input operands to be odd, but
	   below refmpn_gcd_1 doesn't guarantee that. */
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aax > iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1987), "x > 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aay > iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(1988), "y > 0\x00")
	}
	for cgcond := true; cgcond; cgcond = aax != ppuint32(0) {

		for aax&ppuint32(1) == ppuint32(0) {
			aax >>= ppuint32(1)
		}
		for aay&ppuint32(1) == ppuint32(0) {
			aay >>= ppuint32(1)
		}

		if aax < aay {
			aa__mp_limb_t_swap__tmp = aax
			aax = aay
			aay = aa__mp_limb_t_swap__tmp
		}

		aax -= aay
	}

	return aay
}

func Xrefmpn_gcd_22(cgtls *iqlibc.ppTLS, aax1 tnmp_limb_t, aax0 tnmp_limb_t, aay1 tnmp_limb_t, aay0 tnmp_limb_t) (cgr tnmp_double_limb_t) {

	var aacy, aat tnmp_limb_t
	var aag tnmp_double_limb_t
	pp_, pp_, pp_ = aacy, aag, aat
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aax0&iqlibc.ppUint32FromInt32(1) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2007), "(x0 & 1) != 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aay0&iqlibc.ppUint32FromInt32(1) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2008), "(y0 & 1) != 0\x00")
	}

	for cgcond := true; cgcond; cgcond = aax1|aax0 != ppuint32(0) {

		for aax0&ppuint32(1) == ppuint32(0) {

			aax0 = aax1<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1)) | aax0>>iqlibc.ppInt32FromInt32(1)

			aax1 >>= ppuint32(1)
		}
		for aay0&ppuint32(1) == ppuint32(0) {

			aay0 = aay1<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1)) | aay0>>iqlibc.ppInt32FromInt32(1)

			aay1 >>= ppuint32(1)
		}

		if aax1 < aay1 || aax1 == aay1 && aax0 < aay0 {

			aat = aax1
			aax1 = aay1
			aay1 = aat
			aat = aax0
			aax0 = aay0
			aay0 = aat
		}

		aacy = iqlibc.ppBoolUint32(aax0 < aay0)

		aax0 -= aay0

		aax1 -= aay1 + aacy
	}

	aag.fdd1 = aay1
	aag.fdd0 = aay0
	return aag
}

func Xrefmpn_gcd_1(cgtls *iqlibc.ppTLS, aaxp tnmp_srcptr, aaxsize tnmp_size_t, aay tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i tnmp_size_t
	var aa__nail, aa__nail1, aax tnmp_limb_t
	var aatwos ppint32
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__nail, aa__nail1, aatwos, aax

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aay != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2050), "y != 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_zero_p(cgtls, aaxp, aaxsize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2051), "! refmpn_zero_p (xp, xsize)\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aaxsize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2052), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	aa__nail1 = aay & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2053), "__nail == 0\x00")
	}

	aax = Xrefmpn_mod_1(cgtls, aaxp, aaxsize, aay)
	if aax == ppuint32(0) {
		return aay
	}

	aatwos = 0
	for aax&ppuint32(1) == ppuint32(0) && aay&ppuint32(1) == ppuint32(0) {

		aax >>= ppuint32(1)

		aay >>= ppuint32(1)
		aatwos++
	}

	return Xrefmpn_gcd_11(cgtls, aax, aay) << aatwos
}

// C documentation
//
//	/* Based on the full limb x, not nails. */
func Xrefmpn_count_leading_zeros(cgtls *iqlibc.ppTLS, aax tnmp_limb_t) (cgr ppuint32) {

	var aan ppuint32
	pp_ = aan
	aan = ppuint32(0)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aax != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2077), "x != 0\x00")
	}

	for aax&(^iqlibc.ppUint32FromInt32(0)^^iqlibc.ppUint32FromInt32(0)>>iqlibc.ppInt32FromInt32(1)) == ppuint32(0) {

		aax <<= ppuint32(1)
		aan++
	}
	return aan
}

// C documentation
//
//	/* Full limbs allowed, not limited to nails. */
func Xrefmpn_count_trailing_zeros(cgtls *iqlibc.ppTLS, aax tnmp_limb_t) (cgr ppuint32) {

	var aa__nail tnmp_limb_t
	var aan ppuint32
	pp_, pp_ = aa__nail, aan
	aan = ppuint32(0)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aax != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2093), "x != 0\x00")
	}
	aa__nail = aax & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2094), "__nail == 0\x00")
	}

	for aax&ppuint32(1) == ppuint32(0) {

		aax >>= ppuint32(1)
		aan++
	}
	return aan
}

// C documentation
//
//	/* Strip factors of two (low zero bits) from {p,size} by right shifting.
//	   The return value is the number of twos stripped.  */
func Xrefmpn_strip_twos(cgtls *iqlibc.ppTLS, aap tnmp_ptr, aasize tnmp_size_t) (cgr tnmp_size_t) {

	var aa__i, aalimbs tnmp_size_t
	var aa__nail tnmp_limb_t
	var aashift ppuint32
	pp_, pp_, pp_, pp_ = aa__i, aa__nail, aalimbs, aashift

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2112), "size >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_zero_p(cgtls, aap, aasize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2113), "! refmpn_zero_p (p, size)\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aap + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2114), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}

	aalimbs = 0
	for {
		if !(*(*tnmp_limb_t)(iqunsafe.ppPointer(aap)) == ppuint32(0)) {
			break
		}

		Xrefmpn_copyi(cgtls, aap, aap+ppuintptr(1)*4, aasize-ppint32(1))
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aap + ppuintptr(aasize-ppint32(1))*4)) = ppuint32(0)

		goto cg_2
	cg_2:
		;
		aalimbs++
	}

	aashift = Xrefmpn_count_trailing_zeros(cgtls, *(*tnmp_limb_t)(iqunsafe.ppPointer(aap)))
	if aashift != 0 {
		Xrefmpn_rshift(cgtls, aap, aap, aasize, aashift)
	}

	return iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(aalimbs*ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))) + ppuint32(aashift))
}

func Xrefmpn_gcd(cgtls *iqlibc.ppTLS, aagp tnmp_ptr, aaxp tnmp_ptr, aaxsize tnmp_size_t, aayp tnmp_ptr, aaysize tnmp_size_t) (cgr tnmp_limb_t) {

	var aa__i, aa__i1, aa__mp_size_t_swap__tmp tnmp_size_t
	var aa__mp_ptr_swap__tmp tnmp_ptr
	var aa__nail, aa__nail1 tnmp_limb_t
	var aacmp ppint32
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__mp_ptr_swap__tmp, aa__mp_size_t_swap__tmp, aa__nail, aa__nail1, aacmp

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaysize >= iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2134), "ysize >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaxsize >= aaysize)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2135), "xsize >= ysize\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp))&iqlibc.ppUint32FromInt32(1) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2136), "(xp[0] & 1) != 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aayp))&iqlibc.ppUint32FromInt32(1) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2137), "(yp[0] & 1) != 0\x00")
	}
	/* ASSERT (xp[xsize-1] != 0); */ /* don't think x needs to be odd */
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aaysize-ppint32(1))*4)) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2139), "yp[ysize-1] != 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aagp, aaxp, aaxsize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2140), "refmpn_overlap_fullonly_p (gp, xp, xsize)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_overlap_fullonly_p(cgtls, aagp, aayp, aaysize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2141), "refmpn_overlap_fullonly_p (gp, yp, ysize)\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aaxp, aaxsize, aayp, aaysize) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2142), "! refmpn_overlap_p (xp, xsize, yp, ysize)\x00")
	}
	if aaxsize == aaysize {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_msbone(cgtls, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aaxsize-ppint32(1))*4))) >= Xrefmpn_msbone(cgtls, *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aaysize-ppint32(1))*4))))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2144), "refmpn_msbone (xp[xsize-1]) >= refmpn_msbone (yp[ysize-1])\x00")
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aaxsize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2145), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aaysize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2146), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	Xrefmpn_strip_twos(cgtls, aaxp, aaxsize)
	for aaxsize > 0 {
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aaxsize-ppint32(1))*4)) != ppuint32(0) {
			break
		}
		aaxsize--
	}
	for aaysize > 0 {
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aayp + ppuintptr(aaysize-ppint32(1))*4)) != ppuint32(0) {
			break
		}
		aaysize--
	}

	for {

		aacmp = Xrefmpn_cmp_twosizes(cgtls, aaxp, aaxsize, aayp, aaysize)
		if aacmp == 0 {
			break
		}
		if aacmp < 0 {
			aa__mp_ptr_swap__tmp = aaxp
			aaxp = aayp
			aayp = aa__mp_ptr_swap__tmp
			aa__mp_size_t_swap__tmp = aaxsize
			aaxsize = aaysize
			aaysize = aa__mp_size_t_swap__tmp
		}

		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_sub(cgtls, aaxp, aaxp, aaxsize, aayp, aaysize) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2160), "(refmpn_sub (xp, xp, xsize, yp, ysize)) == 0\x00")
		}

		Xrefmpn_strip_twos(cgtls, aaxp, aaxsize)
		for aaxsize > 0 {
			if *(*tnmp_limb_t)(iqunsafe.ppPointer(aaxp + ppuintptr(aaxsize-ppint32(1))*4)) != ppuint32(0) {
				break
			}
			aaxsize--
		}

		goto cg_3
	cg_3:
	}

	Xrefmpn_copyi(cgtls, aagp, aaxp, aaxsize)
	return iqlibc.ppUint32FromInt32(aaxsize)
}

func Xref_popc_limb(cgtls *iqlibc.ppTLS, aasrc tnmp_limb_t) (cgr ppuint32) {

	var aacount ppuint32
	var aai ppint32
	pp_, pp_ = aacount, aai

	aacount = ppuint32(0)
	aai = 0
	for {
		if !(aai < ppint32(mvGMP_LIMB_BITS)) {
			break
		}

		aacount += aasrc & ppuint32(1)

		aasrc >>= ppuint32(1)

		goto cg_1
	cg_1:
		;
		aai++
	}
	return aacount
}

func Xrefmpn_popcount(cgtls *iqlibc.ppTLS, aasp tnmp_srcptr, aasize tnmp_size_t) (cgr ppuint32) {

	var aa__i, aai tnmp_size_t
	var aa__nail tnmp_limb_t
	var aacount ppuint32
	pp_, pp_, pp_, pp_ = aa__i, aa__nail, aacount, aai
	aacount = ppuint32(0)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2191), "size >= 0\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2192), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}

	aai = 0
	for {
		if !(aai < aasize) {
			break
		}

		aacount += Xref_popc_limb(cgtls, *(*tnmp_limb_t)(iqunsafe.ppPointer(aasp + ppuintptr(aai)*4)))
		goto cg_2
	cg_2:
		;
		aai++
	}
	return aacount
}

func Xrefmpn_hamdist(cgtls *iqlibc.ppTLS, aas1p tnmp_srcptr, aas2p tnmp_srcptr, aasize tnmp_size_t) (cgr ppuint32) {

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	var aacount ppuint32
	var aad tnmp_ptr
	pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aacount, aad

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2205), "size >= 0\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas1p + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2206), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aasize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aas2p + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2207), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	if aasize == 0 {
		return ppuint32(0)
	}

	aad = Xrefmpn_malloc_limbs(cgtls, aasize)
	Xrefmpn_xor_n(cgtls, aad, aas1p, aas2p, aasize)
	aacount = Xrefmpn_popcount(cgtls, aad, aasize)
	Xfree(cgtls, aad)
	return aacount
}

// C documentation
//
//	/* set r to a%d */
func Xrefmpn_mod2(cgtls *iqlibc.ppTLS, aar ppuintptr, aaa ppuintptr, aad ppuintptr) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	var aan ppint32
	var pp_ /* D at bp+0 */ [2]tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aan

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xrefmpn_overlap_p(cgtls, aar, iqlibc.ppInt32FromInt32(2), aad, iqlibc.ppInt32FromInt32(2)) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2227), "! refmpn_overlap_p (r, (mp_size_t) 2, d, (mp_size_t) 2)\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < ppint32(iqlibc.ppInt32FromInt32(2))) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaa + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2228), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < ppint32(iqlibc.ppInt32FromInt32(2))) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aad + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2229), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	(*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp)))[ppint32(1)] = *(*tnmp_limb_t)(iqunsafe.ppPointer(aad + 1*4))

	(*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp)))[0] = *(*tnmp_limb_t)(iqunsafe.ppPointer(aad))
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aar + 1*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaa + 1*4))
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aar)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaa))
	aan = 0

	for {

		if (*(*[2]tnmp_limb_t)(iqunsafe.ppPointer(cgbp)))[ppint32(1)]&(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1))) != 0 {
			break
		}
		if Xrefmpn_cmp(cgtls, aar, cgbp, iqlibc.ppInt32FromInt32(2)) <= 0 {
			break
		}
		Xrefmpn_lshift(cgtls, cgbp, cgbp, iqlibc.ppInt32FromInt32(2), ppuint32(1))
		aan++
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aan <= iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2243), "n <= (32 - 0)\x00")
		}

		goto cg_3
	cg_3:
	}

	for aan >= 0 {

		if Xrefmpn_cmp(cgtls, aar, cgbp, iqlibc.ppInt32FromInt32(2)) >= 0 {
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_sub_n(cgtls, aar, aar, cgbp, iqlibc.ppInt32FromInt32(2)) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2249), "(refmpn_sub_n (r, r, D, (mp_size_t) 2)) == 0\x00")
			}
		}
		Xrefmpn_rshift(cgtls, cgbp, cgbp, iqlibc.ppInt32FromInt32(2), ppuint32(1))
		aan--
	}

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_cmp(cgtls, aar, aad, iqlibc.ppInt32FromInt32(2)) < iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2254), "refmpn_cmp (r, d, (mp_size_t) 2) < 0\x00")
	}
}

// C documentation
//
//	/* Similar to the old mpn/generic/sb_divrem_mn.c, but somewhat simplified, in
//	   particular the trial quotient is allowed to be 2 too big. */
func Xrefmpn_sb_div_qr(cgtls *iqlibc.ppTLS, aaqp tnmp_ptr, aanp tnmp_ptr, aansize tnmp_size_t, aadp tnmp_srcptr, aadsize tnmp_size_t) (cgr tnmp_limb_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aa__i, aa__i1, aai tnmp_size_t
	var aa__nail, aa__nail1, aad1, aan0, aan1, aaq, aaretval tnmp_limb_t
	var aamp, aanp_orig tnmp_ptr
	var pp_ /* dummy_r at bp+0 */ tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aad1, aai, aamp, aan0, aan1, aanp_orig, aaq, aaretval
	aaretval = ppuint32(0)
	aad1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aadp + ppuintptr(aadsize-ppint32(1))*4))
	aanp_orig = Xrefmpn_memdup_limbs(cgtls, aanp, aansize)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aansize >= aadsize)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2271), "nsize >= dsize\x00")
	}
	/* ASSERT (dsize > 2); */
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aadsize >= iqlibc.ppInt32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2273), "dsize >= 2\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aadp + ppuintptr(aadsize-ppint32(1))*4))&(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1))) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2274), "dp[dsize-1] & (((mp_limb_t) 1L) << ((32 - 0)-1))\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(!(Xrefmpn_overlap_p(cgtls, aaqp, aansize-aadsize, aanp, aansize) != 0) || aaqp+ppuintptr(aadsize)*4 >= aanp)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2275), "! refmpn_overlap_p (qp, nsize-dsize, np, nsize) || qp+dsize >= np\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aansize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aanp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2276), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aadsize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aadp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2277), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}

	aai = aansize - aadsize
	if Xrefmpn_cmp(cgtls, aanp+ppuintptr(aai)*4, aadp, aadsize) >= 0 {

		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_sub_n(cgtls, aanp+ppuintptr(aai)*4, aanp+ppuintptr(aai)*4, aadp, aadsize) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2282), "(refmpn_sub_n (np+i, np+i, dp, dsize)) == 0\x00")
		}
		aaretval = ppuint32(1)
	}

	aai--
	for {
		if !(aai >= 0) {
			break
		}

		aan0 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aanp + ppuintptr(aai+aadsize)*4))
		aan1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aanp + ppuintptr(aai+aadsize-ppint32(1))*4))

		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aan0 <= aad1)) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2292), "n0 <= d1\x00")
		}
		if aan0 == aad1 {
			aaq = ^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)
		} else {
			aaq = Xrefmpn_udiv_qrnnd(cgtls, cgbp, aan0, aan1<<mvGMP_NAIL_BITS, aad1<<mvGMP_NAIL_BITS)
		}

		aan0 -= Xrefmpn_submul_1(cgtls, aanp+ppuintptr(aai)*4, aadp, aadsize, aaq)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aan0 == ppuint32(0) || aan0 == ^iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2300), "n0 == 0 || n0 == (~ (mp_limb_t) 0)\x00")
		}
		if aan0 != 0 {

			aaq--
			if !(Xrefmpn_add_n(cgtls, aanp+ppuintptr(aai)*4, aanp+ppuintptr(aai)*4, aadp, aadsize) != 0) {

				aaq--
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_add_n(cgtls, aanp+ppuintptr(aai)*4, aanp+ppuintptr(aai)*4, aadp, aadsize) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2307), "(refmpn_add_n (np+i, np+i, dp, dsize)) != 0\x00")
				}
			}
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aanp + ppuintptr(aai+aadsize)*4)) = ppuint32(0)

		*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aai)*4)) = aaq

		goto cg_3
	cg_3:
		;
		aai--
	}

	/* remainder < divisor */

	/* multiply back to original */

	aamp = Xrefmpn_malloc_limbs(cgtls, aansize)

	Xrefmpn_mul_any(cgtls, aamp, aaqp, aansize-aadsize, aadp, aadsize)
	if aaretval != 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_add_n(cgtls, aamp+ppuintptr(aansize)*4-ppuintptr(aadsize)*4, aamp+ppuintptr(aansize)*4-ppuintptr(aadsize)*4, aadp, aadsize) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2326), "(refmpn_add_n (mp+nsize-dsize,mp+nsize-dsize, dp, dsize)) == 0\x00")
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_add(cgtls, aamp, aamp, aansize, aanp, aadsize) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2327), "(refmpn_add (mp, mp, nsize, np, dsize)) == 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_cmp(cgtls, aamp, aanp_orig, aansize) == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2328), "refmpn_cmp (mp, np_orig, nsize) == 0\x00")
	}

	Xfree(cgtls, aamp)

	Xfree(cgtls, aanp_orig)
	return aaretval
}

// C documentation
//
//	/* Similar to the old mpn/generic/sb_divrem_mn.c, but somewhat simplified, in
//	   particular the trial quotient is allowed to be 2 too big. */
func Xrefmpn_tdiv_qr(cgtls *iqlibc.ppTLS, aaqp tnmp_ptr, aarp tnmp_ptr, aaqxn tnmp_size_t, aanp tnmp_ptr, aansize tnmp_size_t, aadp tnmp_srcptr, aadsize tnmp_size_t) {

	var aa__i, aa__i1 tnmp_size_t
	var aa__nail, aa__nail1 tnmp_limb_t
	var aad2p, aan2p tnmp_ptr
	var aanorm ppint32
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__i, aa__i1, aa__nail, aa__nail1, aad2p, aan2p, aanorm
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaqxn == iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2344), "qxn == 0\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aansize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aanp + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2345), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i1 = 0
		for {
			if !(aa__i1 < aadsize) {
				break
			}
			aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aadp + ppuintptr(aa__i1)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2346), "__nail == 0\x00")
			}
			goto cg_2
		cg_2:
			;
			aa__i1++
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aadsize > iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2347), "dsize > 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aadp + ppuintptr(aadsize-ppint32(1))*4)) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2348), "dp[dsize-1] != 0\x00")
	}

	if aadsize == ppint32(1) {

		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp)) = Xrefmpn_divmod_1(cgtls, aaqp, aanp, aansize, *(*tnmp_limb_t)(iqunsafe.ppPointer(aadp)))
		return
	} else {

		aan2p = Xrefmpn_malloc_limbs(cgtls, aansize+ppint32(1))
		aad2p = Xrefmpn_malloc_limbs(cgtls, aadsize)
		aanorm = iqlibc.ppInt32FromUint32(Xrefmpn_count_leading_zeros(cgtls, *(*tnmp_limb_t)(iqunsafe.ppPointer(aadp + ppuintptr(aadsize-ppint32(1))*4))) - ppuint32(mvGMP_NAIL_BITS))

		*(*tnmp_limb_t)(iqunsafe.ppPointer(aan2p + ppuintptr(aansize)*4)) = Xrefmpn_lshift_or_copy(cgtls, aan2p, aanp, aansize, iqlibc.ppUint32FromInt32(aanorm))
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_lshift_or_copy(cgtls, aad2p, aadp, aadsize, iqlibc.ppUint32FromInt32(aanorm)) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2362), "(refmpn_lshift_or_copy (d2p, dp, dsize, norm)) == 0\x00")
		}

		Xrefmpn_sb_div_qr(cgtls, aaqp, aan2p, aansize+ppint32(1), aad2p, aadsize)
		Xrefmpn_rshift_or_copy(cgtls, aarp, aan2p, aadsize, iqlibc.ppUint32FromInt32(aanorm))

		/* ASSERT (refmpn_zero_p (tp+dsize, nsize-dsize)); */
		Xfree(cgtls, aan2p)
		Xfree(cgtls, aad2p)
	}
}

func Xrefmpn_redc_1(cgtls *iqlibc.ppTLS, aarp tnmp_ptr, aaup tnmp_ptr, aamp tnmp_srcptr, aan tnmp_size_t, aainvm tnmp_limb_t) (cgr tnmp_limb_t) {

	var aa__i, aaj tnmp_size_t
	var aa__nail, aacy tnmp_limb_t
	pp_, pp_, pp_, pp_ = aa__i, aa__nail, aacy, aaj

	/* let whole loop go dead when no nails */
	if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < ppint32(2)*aan) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2379), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}
	/* ASSERT about directed overlap rp, up */
	/* ASSERT about overlap rp, mp */
	/* ASSERT about overlap up, mp */

	aaj = aan - ppint32(1)
	for {
		if !(aaj >= 0) {
			break
		}

		*(*tnmp_limb_t)(iqunsafe.ppPointer(aaup)) = Xrefmpn_addmul_1(cgtls, aaup, aamp, aan, *(*tnmp_limb_t)(iqunsafe.ppPointer(aaup))*aainvm&(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0))>>iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
		aaup += 4

		goto cg_2
	cg_2:
		;
		aaj--
	}
	aacy = X__gmpn_add_n(cgtls, aarp, aaup, aaup-ppuintptr(aan)*4, aan)
	return aacy
}

func Xrefmpn_get_str(cgtls *iqlibc.ppTLS, aadst ppuintptr, aabase ppint32, aasrc tnmp_ptr, aasize tnmp_size_t) (cgr tnsize_t) {

	var aa__a, aa__u, aa__v, aa__x0, aa__x1, aa__x2, aa__x3, aa__xr tnUWtype
	var aa__cnt, aa__lb_base, ccv2, ccv3, ccv4 ppint32
	var aa__i tnmp_size_t
	var aa__nail, aa_dummy, aa_ph tnmp_limb_t
	var aa__totbits, aa_nbits, aadsize, aai tnsize_t
	var aa__uh, aa__ul, aa__vh, aa__vl tnUHWtype
	var aad ppuintptr
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__a, aa__cnt, aa__i, aa__lb_base, aa__nail, aa__totbits, aa__u, aa__uh, aa__ul, aa__v, aa__vh, aa__vl, aa__x0, aa__x1, aa__x2, aa__x3, aa__xr, aa_dummy, aa_nbits, aa_ph, aad, aadsize, aai, ccv2, ccv3, ccv4

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2399), "size >= 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aabase >= iqlibc.ppInt32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2400), "base >= 2\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppUint32FromInt32(aabase) < iqlibc.ppUint32FromInt64(5140)/iqlibc.ppUint32FromInt64(20))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2401), "base < (sizeof (__gmpn_bases) / sizeof ((__gmpn_bases)[0]))\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize == 0 || *(*tnmp_limb_t)(iqunsafe.ppPointer(aasrc + ppuintptr(aasize-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2402), "size == 0 || src[size-1] != 0\x00")
	}
	/* let whole loop go dead when no nails */ if mvGMP_NAIL_BITS != 0 {
		aa__i = 0
		for {
			if !(aa__i < aasize) {
				break
			}
			aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasrc + ppuintptr(aa__i)*4)) & ^(^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2403), "__nail == 0\x00")
			}
			goto cg_1
		cg_1:
			;
			aa__i++
		}
	}

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aasize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2405), "(size) >= 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aabase >= iqlibc.ppInt32FromInt32(2))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2405), "(base) >= 2\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(iqlibc.ppUint32FromInt32(aabase) < iqlibc.ppUint32FromInt64(5140)/iqlibc.ppUint32FromInt64(20))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2405), "(base) < (sizeof (__gmpn_bases) / sizeof ((__gmpn_bases)[0]))\x00")
	} /* Special case for X == 0.  */
	if aasize == 0 {
		aadsize = ppuint32(1)
	} else {
		aa__xr = *(*tnmp_limb_t)(iqunsafe.ppPointer(aasrc + ppuintptr(aasize-ppint32(1))*4))
		if true {
			if aa__xr < iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(4))) {
				if aa__xr < iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(4)) {
					ccv3 = ppint32(1)
				} else {
					ccv3 = iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(4) + iqlibc.ppInt32FromInt32(1)
				}
				ccv2 = ccv3
			} else {
				if aa__xr < iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(3)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(4))) {
					ccv4 = iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(4)) + iqlibc.ppInt32FromInt32(1)
				} else {
					ccv4 = iqlibc.ppInt32FromInt32(3)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(4)) + iqlibc.ppInt32FromInt32(1)
				}
				ccv2 = ccv4
			}
			aa__a = iqlibc.ppUint32FromInt32(ccv2)
		} else {
			aa__a = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(8))
			for {
				if !(aa__a > ppuint32(0)) {
					break
				}
				if aa__xr>>aa__a&ppuint32(0xff) != ppuint32(0) {
					break
				}
				goto cg_5
			cg_5:
				;
				aa__a -= ppuint32(8)
			}
			aa__a++
		}
		aa__cnt = iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)+iqlibc.ppInt32FromInt32(1)) - aa__a - ppuint32(X__gmpn_clz_tab[aa__xr>>aa__a]))
		aa__totbits = iqlibc.ppUint32FromInt32(aasize)*iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) - iqlibc.ppUint32FromInt32(aa__cnt-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
		if aabase&(aabase-ppint32(1)) == 0 {
			aa__lb_base = iqlibc.ppInt32FromUint32(X__gmpn_bases[aabase].fdbig_base)
			aadsize = (aa__totbits + iqlibc.ppUint32FromInt32(aa__lb_base) - ppuint32(1)) / iqlibc.ppUint32FromInt32(aa__lb_base)
		} else {
			aa_nbits = aa__totbits
			aa__u = X__gmpn_bases[aabase].fdlogb2 + iqlibc.ppUint32FromInt32(1)
			aa__v = ppuint32(aa_nbits)
			aa__ul = ppuint32(aa__u & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)))
			aa__uh = ppuint32(aa__u >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)))
			aa__vl = ppuint32(aa__v & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)))
			aa__vh = ppuint32(aa__v >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)))
			aa__x0 = ppuint32(aa__ul) * ppuint32(aa__vl)
			aa__x1 = ppuint32(aa__ul) * ppuint32(aa__vh)
			aa__x2 = ppuint32(aa__uh) * ppuint32(aa__vl)
			aa__x3 = ppuint32(aa__uh) * ppuint32(aa__vh)
			aa__x1 += aa__x0 >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)) /* this can't give carry */
			aa__x1 += aa__x2                                                                            /* but this indeed can */
			if aa__x1 < aa__x2 {                                                                        /* did we get it? */
				aa__x3 += iqlibc.ppUint32FromInt32(1) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))
			} /* yes, add it in the proper pos. */
			aa_ph = aa__x3 + aa__x1>>(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))
			aa_dummy = aa__x1<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) + aa__x0&(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))-iqlibc.ppUint32FromInt32(1))
			aadsize = ppuint32(aa_ph + ppuint32(1))
		}
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aadsize >= iqlibc.ppUint32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2406), "dsize >= 1\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(Xbyte_overlap_p(cgtls, aadst, iqlibc.ppInt32FromUint32(aadsize), aasrc, aasize*ppint32(mvSIZEOF_MP_LIMB_T)) != 0)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2407), "! byte_overlap_p (dst, (mp_size_t) dsize, src, size * 4)\x00")
	}

	if aasize == 0 {

		*(*ppuint8)(iqunsafe.ppPointer(aadst)) = ppuint8(0)
		return ppuint32(1)
	}

	/* don't clobber input for power of 2 bases */
	if aabase&(aabase-ppint32(1)) == 0 {
		aasrc = Xrefmpn_memdup_limbs(cgtls, aasrc, aasize)
	}

	aad = aadst + ppuintptr(aadsize)
	for cgcond := true; cgcond; cgcond = aasize != 0 {

		aad--
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aad >= aadst)) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2423), "d >= dst\x00")
		}
		*(*ppuint8)(iqunsafe.ppPointer(aad)) = ppuint8(Xrefmpn_divrem_1(cgtls, aasrc, iqlibc.ppInt32FromInt32(0), aasrc, aasize, iqlibc.ppUint32FromInt32(aabase)))

		aasize -= iqlibc.ppBoolInt32(*(*tnmp_limb_t)(iqunsafe.ppPointer(aasrc + ppuintptr(aasize-ppint32(1))*4)) == iqlibc.ppUint32FromInt32(0))
	}

	/* Move result back and decrement dsize if we didn't generate
	   the maximum possible digits.  */
	if aad != aadst {

		aadsize -= iqlibc.ppUint32FromInt32(ppint32(aad) - ppint32(aadst))
		aai = ppuint32(0)
		for {
			if !(aai < aadsize) {
				break
			}
			*(*ppuint8)(iqunsafe.ppPointer(aadst + ppuintptr(aai))) = *(*ppuint8)(iqunsafe.ppPointer(aad + ppuintptr(aai)))
			goto cg_6
		cg_6:
			;
			aai++
		}
	}

	if aabase&(aabase-ppint32(1)) == 0 {
		Xfree(cgtls, aasrc)
	}

	return aadsize
}

func Xref_bswap_limb(cgtls *iqlibc.ppTLS, aasrc tnmp_limb_t) (cgr tnmp_limb_t) {

	var aadst tnmp_limb_t
	var aai ppint32
	pp_, pp_ = aadst, aai

	aadst = ppuint32(0)
	aai = 0
	for {
		if !(aai < ppint32(mvSIZEOF_MP_LIMB_T)) {
			break
		}

		aadst = aadst<<iqlibc.ppInt32FromInt32(8) + aasrc&ppuint32(0xFF)

		aasrc >>= ppuint32(8)

		goto cg_1
	cg_1:
		;
		aai++
	}
	return aadst
}

/* These random functions are mostly for transitional purposes while adding
   nail support, since they're independent of the normal mpn routines.  They
   can probably be removed when those normal routines are reliable, though
   perhaps something independent would still be useful at times.  */

var Xrefmpn_random_seed tnmp_limb_t

func Xrefmpn_random_half(cgtls *iqlibc.ppTLS) (cgr tnmp_limb_t) {

	Xrefmpn_random_seed = Xrefmpn_random_seed*iqlibc.ppUint32FromInt32(0x29CF535) + ppuint32(1)
	return Xrefmpn_random_seed >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))
}

func Xrefmpn_random_limb(cgtls *iqlibc.ppTLS) (cgr tnmp_limb_t) {

	return (Xrefmpn_random_half(cgtls)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) | Xrefmpn_random_half(cgtls)) & (^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
}

func Xrefmpn_random(cgtls *iqlibc.ppTLS, aaptr tnmp_ptr, aasize tnmp_size_t) {

	var aai tnmp_size_t
	pp_ = aai
	if true {

		X__gmpn_random(cgtls, aaptr, aasize)
		return
	}

	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aai)*4)) = Xrefmpn_random_limb(cgtls)
		goto cg_1
	cg_1:
		;
		aai++
	}
}

func Xrefmpn_random2(cgtls *iqlibc.ppTLS, aaptr tnmp_ptr, aasize tnmp_size_t) {

	var aabit, aalimb, aamask tnmp_limb_t
	var aai tnmp_size_t
	var aarun ppint32
	pp_, pp_, pp_, pp_, pp_ = aabit, aai, aalimb, aamask, aarun

	if true {

		X__gmpn_random2(cgtls, aaptr, aasize)
		return
	}

	/* start with ones at a random pos in the high limb */
	aabit = iqlibc.ppUint32FromInt32(1) << (Xrefmpn_random_half(cgtls) % iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
	aamask = ppuint32(0)
	aarun = 0

	aai = aasize - ppint32(1)
	for {
		if !(aai >= 0) {
			break
		}

		aalimb = ppuint32(0)
		for cgcond := true; cgcond; cgcond = aabit != ppuint32(0) {

			if aarun == 0 {

				aarun = iqlibc.ppInt32FromUint32(Xrefmpn_random_half(cgtls)%ppuint32(mvRUN_MODULUS) + ppuint32(1))
				aamask = ^aamask
			}

			aalimb |= aabit & aamask

			aabit >>= ppuint32(1)
			aarun--
		}

		*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aai)*4)) = aalimb
		aabit = iqlibc.ppUint32FromInt32(1) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) - iqlibc.ppInt32FromInt32(1))

		goto cg_1
	cg_1:
		;
		aai--
	}
}

// C documentation
//
//	/* This is a simple bitwise algorithm working high to low across "s" and
//	   testing each time whether setting the bit would make s^2 exceed n.  */
func Xrefmpn_sqrtrem(cgtls *iqlibc.ppTLS, aasp tnmp_ptr, aarp tnmp_ptr, aanp tnmp_srcptr, aansize tnmp_size_t) (cgr tnmp_size_t) {

	var aac tnmp_limb_t
	var aadp, aatp tnmp_ptr
	var aadsize, aailimbs, aaret, aassize, aatalloc, aatsize tnmp_size_t
	var aai ppint32
	var aaibit ppuint32
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aac, aadp, aadsize, aai, aaibit, aailimbs, aaret, aassize, aatalloc, aatp, aatsize

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aansize >= iqlibc.ppInt32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2557), "nsize >= 0\x00")
	}

	/* If n==0, then s=0 and r=0.  */
	if aansize == 0 {
		return 0
	}

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aanp + ppuintptr(aansize-ppint32(1))*4)) != iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2563), "np[nsize - 1] != 0\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aarp == iqlibc.ppUintptrFromInt32(0) || (aanp == aarp || !(aanp+ppuintptr(aansize)*4 > aarp && aarp+ppuintptr(aansize)*4 > aanp)))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2564), "rp == ((void*)0) || ((np) == (rp) || ! ((np) + (nsize) > (rp) && (rp) + (nsize) > (np)))\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aarp == iqlibc.ppUintptrFromInt32(0) || !(aasp+ppuintptr((aansize+iqlibc.ppInt32FromInt32(1))/iqlibc.ppInt32FromInt32(2))*4 > aarp && aarp+ppuintptr(aansize)*4 > aasp))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2565), "rp == ((void*)0) || ! ((sp) + ((nsize + 1) / 2) > (rp) && (rp) + (nsize) > (sp))\x00")
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!!(aasp+ppuintptr((aansize+iqlibc.ppInt32FromInt32(1))/iqlibc.ppInt32FromInt32(2))*4 > aanp && aanp+ppuintptr(aansize)*4 > aasp)) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2566), "! ((sp) + ((nsize + 1) / 2) > (np) && (np) + (nsize) > (sp))\x00")
	}

	/* root */
	aassize = (aansize + ppint32(1)) / ppint32(2)
	Xrefmpn_zero(cgtls, aasp, aassize)

	/* the remainder so far */
	aadp = Xrefmpn_memdup_limbs(cgtls, aanp, aansize)
	aadsize = aansize

	/* temporary */
	aatalloc = ppint32(2)*aassize + ppint32(1)
	aatp = Xrefmpn_malloc_limbs(cgtls, aatalloc)

	aai = ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*aassize - ppint32(1)
	for {
		if !(aai >= 0) {
			break
		}

		/* t = 2*s*2^i + 2^(2*i), being the amount s^2 will increase by if 2^i
		is added to it */

		aailimbs = (aai + ppint32(1)) / ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
		aaibit = iqlibc.ppUint32FromInt32((aai + ppint32(1)) % ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
		Xrefmpn_zero(cgtls, aatp, aailimbs)
		aac = Xrefmpn_lshift_or_copy(cgtls, aatp+ppuintptr(aailimbs)*4, aasp, aassize, aaibit)
		aatsize = aailimbs + aassize
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(aatsize)*4)) = aac

		aatsize += iqlibc.ppBoolInt32(aac != iqlibc.ppUint32FromInt32(0))

		aailimbs = ppint32(2) * aai / ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
		aaibit = iqlibc.ppUint32FromInt32(ppint32(2) * aai % ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
		if aailimbs+ppint32(1) > aatsize {

			Xrefmpn_zero_extend(cgtls, aatp, aatsize, aailimbs+ppint32(1))
			aatsize = aailimbs + ppint32(1)
		}
		aac = Xrefmpn_add_1(cgtls, aatp+ppuintptr(aailimbs)*4, aatp+ppuintptr(aailimbs)*4, aatsize-aailimbs, iqlibc.ppUint32FromInt32(1)<<aaibit)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aatsize < aatalloc)) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2602), "tsize < talloc\x00")
		}
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(aatsize)*4)) = aac

		aatsize += iqlibc.ppBoolInt32(aac != iqlibc.ppUint32FromInt32(0))

		if Xrefmpn_cmp_twosizes(cgtls, aadp, aadsize, aatp, aatsize) >= 0 {

			/* set this bit in s and subtract from the remainder */
			Xrefmpn_setbit(cgtls, aasp, iqlibc.ppUint32FromInt32(aai))

			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(Xrefmpn_sub_n(cgtls, aadp, aadp, aatp, aadsize) == iqlibc.ppUint32FromInt32(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2611), "(refmpn_sub_n (dp, dp, tp, dsize)) == 0\x00")
			}
			aadsize = Xrefmpn_normalize(cgtls, aadp, aadsize)
		}

		goto cg_1
	cg_1:
		;
		aai--
	}

	if aarp == iqlibc.ppUintptrFromInt32(0) {

		aaret = iqlibc.ppBoolInt32(!(Xrefmpn_zero_p(cgtls, aadp, aadsize) != 0))
	} else {

		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aadsize == 0 || *(*tnmp_limb_t)(iqunsafe.ppPointer(aadp + ppuintptr(aadsize-ppint32(1))*4)) != ppuint32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpn.c\x00", ppint32(2622), "dsize == 0 || dp[dsize-1] != 0\x00")
		}
		Xrefmpn_copy(cgtls, aarp, aadp, aadsize)
		aaret = aadsize
	}

	Xfree(cgtls, aadp)
	Xfree(cgtls, aatp)
	return aaret
}

func ___builtin_alloca(*iqlibc.ppTLS, ppuint32) ppuintptr

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___gmp_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

var ___gmp_binvert_limb_table [128]ppuint8

func ___gmp_tmp_reentrant_alloc(*iqlibc.ppTLS, ppuintptr, ppuint32) ppuintptr

func ___gmp_tmp_reentrant_free(*iqlibc.ppTLS, ppuintptr)

func ___gmpn_add_n(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppuint32

var ___gmpn_bases [257]tsbases

var ___gmpn_clz_tab [129]ppuint8

func ___gmpn_lshift(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuint32) ppuint32

func ___gmpn_mod_1(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32) ppuint32

func ___gmpn_random(*iqlibc.ppTLS, ppuintptr, ppint32)

func ___gmpn_random2(*iqlibc.ppTLS, ppuintptr, ppint32)

func ___gmpn_rshift(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuint32) ppuint32

func ___gmpn_sbpi1_bdiv_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuint32)

func ___gmpn_sub_n(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppuint32

func ___gmpn_toom22_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr)

func ___gmpn_toom33_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr)

func ___gmpn_toom44_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr)

func ___gmpn_toom6h_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr)

func _align_pointer(*iqlibc.ppTLS, ppuintptr, ppuint32) ppuintptr

func _free(*iqlibc.ppTLS, ppuintptr)

func _malloc(*iqlibc.ppTLS, ppuint32) ppuintptr

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
