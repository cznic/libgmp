// Code generated for linux/arm by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -ignore-unsupported-alignment -DHAVE_CONFIG_H -I. -I../.. -I../.. -I../../tests -DNDEBUG -c -o t-div.o.go t-div.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvASSERT_FILE = "__FILE__"
const mvASSERT_LINE = "__LINE__"
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBINV_NEWTON_THRESHOLD = 300
const mvBMOD_1_TO_MOD_1_THRESHOLD = 10
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvCOUNT = 200
const mvDC_BDIV_Q_THRESHOLD = 180
const mvDC_DIVAPPR_Q_THRESHOLD = 200
const mvDELAYTIMER_MAX = 0x7fffffff
const mvDIVEXACT_1_THRESHOLD = 0
const mvDIVEXACT_BY3_METHOD = 0
const mvDIVEXACT_JEB_THRESHOLD = 25
const mvDOPRNT_CONV_FIXED = 1
const mvDOPRNT_CONV_GENERAL = 3
const mvDOPRNT_CONV_SCIENTIFIC = 2
const mvDOPRNT_JUSTIFY_INTERNAL = 3
const mvDOPRNT_JUSTIFY_LEFT = 1
const mvDOPRNT_JUSTIFY_NONE = 0
const mvDOPRNT_JUSTIFY_RIGHT = 2
const mvDOPRNT_SHOWBASE_NO = 2
const mvDOPRNT_SHOWBASE_NONZERO = 3
const mvDOPRNT_SHOWBASE_YES = 1
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFAC_DSC_THRESHOLD = 400
const mvFAC_ODD_THRESHOLD = 35
const mvFFT_FIRST_K = 4
const mvFIB_TABLE_LIMIT = 47
const mvFIB_TABLE_LUCNUM_LIMIT = 46
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFOPEN_MAX = 1000
const mvGCDEXT_DC_THRESHOLD = 600
const mvGCD_DC_THRESHOLD = 1000
const mvGET_STR_DC_THRESHOLD = 18
const mvGET_STR_PRECOMPUTE_THRESHOLD = 35
const mvGMP_LIMB_BITS = 32
const mvGMP_LIMB_BYTES = "SIZEOF_MP_LIMB_T"
const mvGMP_MPARAM_H_SUGGEST = "./mpn/generic/gmp-mparam.h"
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_ALARM = 1
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_CONST = 1
const mvHAVE_ATTRIBUTE_MALLOC = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_ATTRIBUTE_NORETURN = 1
const mvHAVE_CLOCK = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_CONFIG_H = 1
const mvHAVE_DECL_FGETC = 1
const mvHAVE_DECL_FSCANF = 1
const mvHAVE_DECL_OPTARG = 1
const mvHAVE_DECL_SYS_ERRLIST = 0
const mvHAVE_DECL_SYS_NERR = 0
const mvHAVE_DECL_UNGETC = 1
const mvHAVE_DECL_VFPRINTF = 1
const mvHAVE_DLFCN_H = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FCNTL_H = 1
const mvHAVE_FLOAT_H = 1
const mvHAVE_GETPAGESIZE = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_HIDDEN_ALIAS = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTPTR_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LANGINFO_H = 1
const mvHAVE_LIMB_LITTLE_ENDIAN = 1
const mvHAVE_LOCALECONV = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_DOUBLE = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_MEMORY_H = 1
const mvHAVE_MEMSET = 1
const mvHAVE_MMAP = 1
const mvHAVE_MPROTECT = 1
const mvHAVE_NL_LANGINFO = 1
const mvHAVE_NL_TYPES_H = 1
const mvHAVE_POPEN = 1
const mvHAVE_PTRDIFF_T = 1
const mvHAVE_QUAD_T = 1
const mvHAVE_RAISE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGALTSTACK = 1
const mvHAVE_SIGSTACK = 1
const mvHAVE_STACK_T = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDLIB_H = 1
const mvHAVE_STRCHR = 1
const mvHAVE_STRERROR = 1
const mvHAVE_STRINGS_H = 1
const mvHAVE_STRING_H = 1
const mvHAVE_STRNLEN = 1
const mvHAVE_STRTOL = 1
const mvHAVE_STRTOUL = 1
const mvHAVE_SYSCONF = 1
const mvHAVE_SYS_MMAN_H = 1
const mvHAVE_SYS_PARAM_H = 1
const mvHAVE_SYS_RESOURCE_H = 1
const mvHAVE_SYS_STAT_H = 1
const mvHAVE_SYS_SYSINFO_H = 1
const mvHAVE_SYS_TIMES_H = 1
const mvHAVE_SYS_TIME_H = 1
const mvHAVE_SYS_TYPES_H = 1
const mvHAVE_TIMES = 1
const mvHAVE_UINT_LEAST32_T = 1
const mvHAVE_UNISTD_H = 1
const mvHAVE_VSNPRINTF = 1
const mvHGCD_APPR_THRESHOLD = 400
const mvHGCD_REDUCE_THRESHOLD = 1000
const mvHGCD_THRESHOLD = 400
const mvHOST_NAME_MAX = 255
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT32_MAX"
const mvINTPTR_MIN = "INT32_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_HIGHBIT = "INT_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvINV_APPR_THRESHOLD = "INV_NEWTON_THRESHOLD"
const mvINV_NEWTON_THRESHOLD = 200
const mvIOV_MAX = 1024
const mvLIMBS_PER_ULONG = 1
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_HIGHBIT = "LONG_MIN"
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMATRIX22_STRASSEN_THRESHOLD = 30
const mvMB_LEN_MAX = 4
const mvMOD_BKNP1_ONLY3 = 0
const mvMPN_FFT_TABLE_SIZE = 16
const mvMPN_TOOM22_MUL_MINSIZE = 6
const mvMPN_TOOM2_SQR_MINSIZE = 4
const mvMPN_TOOM32_MUL_MINSIZE = 10
const mvMPN_TOOM33_MUL_MINSIZE = 17
const mvMPN_TOOM3_SQR_MINSIZE = 17
const mvMPN_TOOM42_MULMID_MINSIZE = 4
const mvMPN_TOOM42_MUL_MINSIZE = 10
const mvMPN_TOOM43_MUL_MINSIZE = 25
const mvMPN_TOOM44_MUL_MINSIZE = 30
const mvMPN_TOOM4_SQR_MINSIZE = 30
const mvMPN_TOOM53_MUL_MINSIZE = 17
const mvMPN_TOOM54_MUL_MINSIZE = 31
const mvMPN_TOOM63_MUL_MINSIZE = 49
const mvMPN_TOOM6H_MUL_MINSIZE = 46
const mvMPN_TOOM6_SQR_MINSIZE = 46
const mvMPN_TOOM8H_MUL_MINSIZE = 86
const mvMPN_TOOM8_SQR_MINSIZE = 86
const mvMP_BASES_BIG_BASE_CTZ_10 = 9
const mvMP_BASES_CHARS_PER_LIMB_10 = 9
const mvMP_BASES_NORMALIZATION_STEPS_10 = 2
const mvMP_EXP_T_MAX = "MP_SIZE_T_MAX"
const mvMP_EXP_T_MIN = "MP_SIZE_T_MIN"
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMULLO_BASECASE_THRESHOLD = 0
const mvMULLO_BASECASE_THRESHOLD_LIMIT = "MULLO_BASECASE_THRESHOLD"
const mvMULMID_TOOM42_THRESHOLD = "MUL_TOOM22_THRESHOLD"
const mvMULMOD_BNM1_THRESHOLD = 16
const mvMUL_TOOM22_THRESHOLD = 30
const mvMUL_TOOM22_THRESHOLD_LIMIT = "MUL_TOOM22_THRESHOLD"
const mvMUL_TOOM32_TO_TOOM43_THRESHOLD = 100
const mvMUL_TOOM32_TO_TOOM53_THRESHOLD = 110
const mvMUL_TOOM33_THRESHOLD = 100
const mvMUL_TOOM33_THRESHOLD_LIMIT = "MUL_TOOM33_THRESHOLD"
const mvMUL_TOOM42_TO_TOOM53_THRESHOLD = 100
const mvMUL_TOOM42_TO_TOOM63_THRESHOLD = 110
const mvMUL_TOOM43_TO_TOOM54_THRESHOLD = 150
const mvMUL_TOOM44_THRESHOLD = 300
const mvMUL_TOOM6H_THRESHOLD = 350
const mvMUL_TOOM8H_THRESHOLD = 450
const mvMUPI_DIV_QR_THRESHOLD = 200
const mvMU_BDIV_QR_THRESHOLD = 2000
const mvMU_BDIV_Q_THRESHOLD = 2000
const mvMU_DIVAPPR_Q_THRESHOLD = 2000
const mvMU_DIV_QR_THRESHOLD = 2000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvODD_CENTRAL_BINOMIAL_OFFSET = 8
const mvODD_CENTRAL_BINOMIAL_TABLE_LIMIT = 18
const mvODD_DOUBLEFACTORIAL_TABLE_LIMIT = 19
const mvODD_FACTORIAL_EXTTABLE_LIMIT = 34
const mvODD_FACTORIAL_TABLE_LIMIT = 16
const mvPACKAGE = "gmp"
const mvPACKAGE_BUGREPORT = "gmp-bugs@gmplib.org (see https://gmplib.org/manual/Reporting-Bugs.html)"
const mvPACKAGE_NAME = "GNU MP"
const mvPACKAGE_STRING = "GNU MP 6.3.0"
const mvPACKAGE_TARNAME = "gmp"
const mvPACKAGE_URL = "http://www.gnu.org/software/gmp/"
const mvPACKAGE_VERSION = "6.3.0"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPP = 0xC0CFD797
const mvPP_FIRST_OMITTED = 31
const mvPP_INVERTED = 0x53E5645C
const mvPREINV_MOD_1_TO_MOD_1_THRESHOLD = 10
const mvPRIMESIEVE_HIGHEST_PRIME = 5351
const mvPRIMESIEVE_NUMBEROF_TABLE = 56
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT32_MAX"
const mvPTRDIFF_MIN = "INT32_MIN"
const mvP_tmpdir = "/tmp"
const mvRANDFUNC = "mpz_rrandomb"
const mvRAND_MAX = 0x7fffffff
const mvREDC_1_TO_REDC_N_THRESHOLD = 100
const mvRETSIGTYPE = "void"
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSET_STR_DC_THRESHOLD = 750
const mvSET_STR_PRECOMPUTE_THRESHOLD = 2000
const mvSHRT_HIGHBIT = "SHRT_MIN"
const mvSHRT_MAX = 0x7fff
const mvSIEVESIZE = 512
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZEOF_MP_LIMB_T = 4
const mvSIZEOF_UNSIGNED = 4
const mvSIZEOF_UNSIGNED_LONG = 4
const mvSIZEOF_UNSIGNED_SHORT = 2
const mvSIZEOF_VOID_P = 4
const mvSIZE_LOG = 17
const mvSIZE_MAX = "UINT32_MAX"
const mvSQRLO_BASECASE_THRESHOLD = 0
const mvSQRLO_BASECASE_THRESHOLD_LIMIT = "SQRLO_BASECASE_THRESHOLD"
const mvSQRLO_DC_THRESHOLD = "MULLO_DC_THRESHOLD"
const mvSQRLO_DC_THRESHOLD_LIMIT = "SQRLO_DC_THRESHOLD"
const mvSQRLO_SQR_THRESHOLD = "MULLO_MUL_N_THRESHOLD"
const mvSQRMOD_BNM1_THRESHOLD = 16
const mvSQR_BASECASE_THRESHOLD = 0
const mvSQR_TOOM2_THRESHOLD = 50
const mvSQR_TOOM3_THRESHOLD = 120
const mvSQR_TOOM3_THRESHOLD_LIMIT = "SQR_TOOM3_THRESHOLD"
const mvSQR_TOOM4_THRESHOLD = 400
const mvSQR_TOOM6_THRESHOLD = "MUL_TOOM6H_THRESHOLD"
const mvSQR_TOOM8_THRESHOLD = "MUL_TOOM8H_THRESHOLD"
const mvSSIZE_MAX = "LONG_MAX"
const mvSTDC_HEADERS = 1
const mvSYMLOOP_MAX = 40
const mvTABLE_LIMIT_2N_MINUS_POPC_2N = 49
const mvTARGET_REGISTER_STARVED = 0
const mvTIME_WITH_SYS_TIME = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTUNE_SQR_TOOM2_MAX = "SQR_TOOM2_MAX_GENERIC"
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUDIV_NEEDS_NORMALIZATION = 1
const mvUDIV_PREINV_ALWAYS = 0
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT32_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSE_LEADING_REGPARM = 0
const mvUSE_PREINV_DIVREM_1 = 1
const mvUSHRT_MAX = 65535
const mvVERSION = "6.3.0"
const mvWANT_FFT = 1
const mvWANT_TMP_ALLOCA = 1
const mvWANT_TMP_DEBUG = 0
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_LIMB_BITS"
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_IEEE_FLOATS = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_REDIR_TIME64 = 1
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ACCUM_EPSILON__ = "0x1P-15K"
const mv__ACCUM_FBIT__ = 15
const mv__ACCUM_IBIT__ = 16
const mv__ACCUM_MAX__ = "0X7FFFFFFFP-15K"
const mv__APCS_32__ = 1
const mv__ARMEL__ = 1
const mv__ARM_32BIT_STATE = 1
const mv__ARM_ARCH = 6
const mv__ARM_ARCH_6__ = 1
const mv__ARM_ARCH_ISA_ARM = 1
const mv__ARM_ARCH_ISA_THUMB = 1
const mv__ARM_EABI__ = 1
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_COPROC = 15
const mv__ARM_FEATURE_DSP = 1
const mv__ARM_FEATURE_LDREX = 4
const mv__ARM_FEATURE_QBIT = 1
const mv__ARM_FEATURE_SAT = 1
const mv__ARM_FEATURE_SIMD32 = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 12
const mv__ARM_PCS_VFP = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 8
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DA_FBIT__ = 31
const mv__DA_IBIT__ = 32
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__DQ_FBIT__ = 63
const mv__DQ_IBIT__ = 0
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.2204460492503131e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.7976931348623157e+308
const mv__FLT32X_MIN__ = 2.2250738585072014e-308
const mv__FLT32X_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.1920928955078125e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.4028234663852886e+38
const mv__FLT32_MIN__ = 1.1754943508222875e-38
const mv__FLT32_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.2204460492503131e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.7976931348623157e+308
const mv__FLT64_MIN__ = 2.2250738585072014e-308
const mv__FLT64_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.1920928955078125e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.4028234663852886e+38
const mv__FLT_MIN__ = 1.1754943508222875e-38
const mv__FLT_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT_RADIX__ = 2
const mv__FRACT_EPSILON__ = "0x1P-15R"
const mv__FRACT_FBIT__ = 15
const mv__FRACT_IBIT__ = 0
const mv__FRACT_MAX__ = "0X7FFFP-15R"
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 1
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 1
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 1
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__GXX_TYPEINFO_EQUALITY_INLINE = 0
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HA_FBIT__ = 7
const mv__HA_IBIT__ = 8
const mv__HQ_FBIT__ = 15
const mv__HQ_IBIT__ = 0
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LACCUM_EPSILON__ = "0x1P-31LK"
const mv__LACCUM_FBIT__ = 31
const mv__LACCUM_IBIT__ = 32
const mv__LACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LK"
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.2204460492503131e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.7976931348623157e+308
const mv__LDBL_MIN__ = 2.2250738585072014e-308
const mv__LDBL_NORM_MAX__ = 1.7976931348623157e+308
const mv__LFRACT_EPSILON__ = "0x1P-31LR"
const mv__LFRACT_FBIT__ = 31
const mv__LFRACT_IBIT__ = 0
const mv__LFRACT_MAX__ = "0X7FFFFFFFP-31LR"
const mv__LITTLE_ENDIAN = 1234
const mv__LLACCUM_EPSILON__ = "0x1P-31LLK"
const mv__LLACCUM_FBIT__ = 31
const mv__LLACCUM_IBIT__ = 32
const mv__LLACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LLK"
const mv__LLFRACT_EPSILON__ = "0x1P-63LLR"
const mv__LLFRACT_FBIT__ = 63
const mv__LLFRACT_IBIT__ = 0
const mv__LLFRACT_MAX__ = "0X7FFFFFFFFFFFFFFFP-63LLR"
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 0x7fffffff
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "ll"
const mv__PRIPTR = ""
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__QQ_FBIT__ = 7
const mv__QQ_IBIT__ = 0
const mv__SACCUM_EPSILON__ = "0x1P-7HK"
const mv__SACCUM_FBIT__ = 7
const mv__SACCUM_IBIT__ = 8
const mv__SACCUM_MAX__ = "0X7FFFP-7HK"
const mv__SA_FBIT__ = 15
const mv__SA_IBIT__ = 16
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SFRACT_EPSILON__ = "0x1P-7HR"
const mv__SFRACT_FBIT__ = 7
const mv__SFRACT_IBIT__ = 0
const mv__SFRACT_MAX__ = "0X7FP-7HR"
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__SQ_FBIT__ = 31
const mv__SQ_IBIT__ = 0
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__TA_FBIT__ = 63
const mv__TA_IBIT__ = 64
const mv__THUMB_INTERWORK__ = 1
const mv__TQ_FBIT__ = 127
const mv__TQ_IBIT__ = 0
const mv__UACCUM_EPSILON__ = "0x1P-16UK"
const mv__UACCUM_FBIT__ = 16
const mv__UACCUM_IBIT__ = 16
const mv__UACCUM_MAX__ = "0XFFFFFFFFP-16UK"
const mv__UACCUM_MIN__ = "0.0UK"
const mv__UDA_FBIT__ = 32
const mv__UDA_IBIT__ = 32
const mv__UDQ_FBIT__ = 64
const mv__UDQ_IBIT__ = 0
const mv__UFRACT_EPSILON__ = "0x1P-16UR"
const mv__UFRACT_FBIT__ = 16
const mv__UFRACT_IBIT__ = 0
const mv__UFRACT_MAX__ = "0XFFFFP-16UR"
const mv__UFRACT_MIN__ = "0.0UR"
const mv__UHA_FBIT__ = 8
const mv__UHA_IBIT__ = 8
const mv__UHQ_FBIT__ = 16
const mv__UHQ_IBIT__ = 0
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__ULACCUM_EPSILON__ = "0x1P-32ULK"
const mv__ULACCUM_FBIT__ = 32
const mv__ULACCUM_IBIT__ = 32
const mv__ULACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULK"
const mv__ULACCUM_MIN__ = "0.0ULK"
const mv__ULFRACT_EPSILON__ = "0x1P-32ULR"
const mv__ULFRACT_FBIT__ = 32
const mv__ULFRACT_IBIT__ = 0
const mv__ULFRACT_MAX__ = "0XFFFFFFFFP-32ULR"
const mv__ULFRACT_MIN__ = "0.0ULR"
const mv__ULLACCUM_EPSILON__ = "0x1P-32ULLK"
const mv__ULLACCUM_FBIT__ = 32
const mv__ULLACCUM_IBIT__ = 32
const mv__ULLACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULLK"
const mv__ULLACCUM_MIN__ = "0.0ULLK"
const mv__ULLFRACT_EPSILON__ = "0x1P-64ULLR"
const mv__ULLFRACT_FBIT__ = 64
const mv__ULLFRACT_IBIT__ = 0
const mv__ULLFRACT_MAX__ = "0XFFFFFFFFFFFFFFFFP-64ULLR"
const mv__ULLFRACT_MIN__ = "0.0ULLR"
const mv__UQQ_FBIT__ = 8
const mv__UQQ_IBIT__ = 0
const mv__USACCUM_EPSILON__ = "0x1P-8UHK"
const mv__USACCUM_FBIT__ = 8
const mv__USACCUM_IBIT__ = 8
const mv__USACCUM_MAX__ = "0XFFFFP-8UHK"
const mv__USACCUM_MIN__ = "0.0UHK"
const mv__USA_FBIT__ = 16
const mv__USA_IBIT__ = 16
const mv__USE_TIME_BITS64 = 1
const mv__USFRACT_EPSILON__ = "0x1P-8UHR"
const mv__USFRACT_FBIT__ = 8
const mv__USFRACT_IBIT__ = 0
const mv__USFRACT_MAX__ = "0XFFP-8UHR"
const mv__USFRACT_MIN__ = "0.0UHR"
const mv__USQ_FBIT__ = 32
const mv__USQ_IBIT__ = 0
const mv__UTA_FBIT__ = 64
const mv__UTA_IBIT__ = 64
const mv__UTQ_FBIT__ = 128
const mv__UTQ_IBIT__ = 0
const mv__VERSION__ = "12.2.0"
const mv__VFP_FP__ = 1
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__arm__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_newalloc = "_mpz_realloc"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvbinvert_limb_table = "__gmp_binvert_limb_table"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_init_primesieve = "__gmp_init_primesieve"
const mvgmp_nextprime = "__gmp_nextprime"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_primesieve = "__gmp_primesieve"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvjacobi_table = "__gmp_jacobi_table"
const mvlinux = 1
const mvmodlimb_invert = "binvert_limb"
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpn_fft_mul = "mpn_nussbaumer_mul"
const mvmpn_get_d = "__gmpn_get_d"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_gcd = "__gmpz_divexact_gcd"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_inp_str_nowhite = "__gmpz_inp_str_nowhite"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucas_mod = "__gmpz_lucas_mod"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_n_pow_ui = "__gmpz_n_pow_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_oddfac_1 = "__gmpz_oddfac_1"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_prodlimbs = "__gmpz_prodlimbs"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_stronglucas = "__gmpz_stronglucas"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvrestrict = "__restrict"
const mvudiv_qrnnd = "__udiv_qrnnd_c"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint32

type tnwchar_t = ppuint32

type tnsize_t = ppuint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fd__ccgo_align [0]ppuint32
	fdquot         ppint64
	fdrem          ppint64
}

type tnssize_t = ppint32

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__ccgo_align [0]ppuint32
	fd__lldata     [0]ppint64
	fd__align      [0]ppfloat64
	fd__opaque     [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnmax_align_t = struct {
	fd__ccgo_align [0]ppuint32
	fd__ll         ppint64
	fd__ld         ppfloat64
}

type tnptrdiff_t = ppint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

type tnuintptr_t = ppuint32

type tnintptr_t = ppint32

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fd__ccgo_align [0]ppuint32
	fdquot         tnintmax_t
	fdrem          tnintmax_t
}

type tngmp_uint_least32_t = ppuint32

type tngmp_intptr_t = ppint32

type tngmp_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tngmp_pi2_t = struct {
	fdinv21 tnmp_limb_t
	fdinv32 tnmp_limb_t
	fdinv53 tnmp_limb_t
}

type tutmp_align_t = struct {
	fd__ccgo_align [0]ppuint32
	fdd            [0]ppfloat64
	fdp            [0]ppuintptr
	fdl            tnmp_limb_t
	fd__ccgo_pad3  [4]byte
}

type tstmp_reentrant_t = struct {
	fdnext ppuintptr
	fdsize tnsize_t
}

type tngmp_randfnptr_t = struct {
	fdrandseed_fn  ppuintptr
	fdrandget_fn   ppuintptr
	fdrandclear_fn ppuintptr
	fdrandiset_fn  ppuintptr
}

type tetoom6_flags = ppint32

const ectoom6_all_pos = 0
const ectoom6_vm1_neg = 1
const ectoom6_vm2_neg = 2

type tetoom7_flags = ppint32

const ectoom7_w1_neg = 1
const ectoom7_w3_neg = 2

type tngmp_primesieve_t = struct {
	fdd       ppuint32
	fds0      ppuint32
	fdsqrt_s0 ppuint32
	fds       [513]ppuint8
}

type tsfft_table_nk = struct {
	fd__ccgo0 uint32
}

type tsbases = struct {
	fdchars_per_limb    ppint32
	fdlogb2             tnmp_limb_t
	fdlog2b             tnmp_limb_t
	fdbig_base          tnmp_limb_t
	fdbig_base_inverted tnmp_limb_t
}

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tuieee_double_extract = struct {
	fd__ccgo_align [0]ppuint32
	fdd            [0]ppfloat64
	fds            struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type tnmp_double_limb_t = struct {
	fdd0 tnmp_limb_t
	fdd1 tnmp_limb_t
}

type tshgcd_matrix1 = struct {
	fdu [2][2]tnmp_limb_t
}

type tshgcd_matrix = struct {
	fdalloc tnmp_size_t
	fdn     tnmp_size_t
	fdp     [2][2]tnmp_ptr
}

type tsgcdext_ctx = struct {
	fdgp    tnmp_ptr
	fdgn    tnmp_size_t
	fdup    tnmp_ptr
	fdusize ppuintptr
	fdun    tnmp_size_t
	fdu0    tnmp_ptr
	fdu1    tnmp_ptr
	fdtp    tnmp_ptr
}

type tspowers = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tnpowers_t = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tsdoprnt_params_t = struct {
	fdbase         ppint32
	fdconv         ppint32
	fdexpfmt       ppuintptr
	fdexptimes4    ppint32
	fdfill         ppuint8
	fdjustify      ppint32
	fdprec         ppint32
	fdshowbase     ppint32
	fdshowpoint    ppint32
	fdshowtrailing ppint32
	fdsign         ppuint8
	fdwidth        ppint32
}

type tngmp_doscan_scan_t = ppuintptr

type tngmp_doscan_step_t = ppuintptr

type tngmp_doscan_get_t = ppuintptr

type tngmp_doscan_unget_t = ppuintptr

type tsgmp_doscan_funs_t = struct {
	fdscan  tngmp_doscan_scan_t
	fdstep  tngmp_doscan_step_t
	fdget   tngmp_doscan_get_t
	fdunget tngmp_doscan_unget_t
}

type tn__jmp_buf = [32]ppuint64

type tnjmp_buf = [1]ts__jmp_buf_tag

type ts__jmp_buf_tag = struct {
	fd__ccgo_align [0]ppuint32
	fd__jb         tn__jmp_buf
	fd__fl         ppuint32
	fd__ss         [32]ppuint32
	fd__ccgo_pad3  [4]byte
}

type tnsigjmp_buf = [1]ts__jmp_buf_tag

/* Establish ostringstream and istringstream.  Do this here so as to hide
   the conditionals, rather than putting stuff in each test program.

   Oldish versions of g++, like 2.95.2, don't have <sstream>, only
   <strstream>.  Fake up ostringstream and istringstream classes, but not a
   full implementation, just enough for our purposes.  */

func sidumpy(cgtls *iqlibc.ppTLS, aap tnmp_srcptr, aan tnmp_size_t) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aai tnmp_size_t
	var ccv3, ccv5 ppuintptr
	pp_, pp_, pp_ = aai, ccv3, ccv5
	if aan > ppint32(20) {

		aai = aan - ppint32(1)
		for {
			if !(aai >= aan-ppint32(4)) {
				break
			}

			Xprintf(cgtls, "%0*lx\x00", iqlibc.ppVaList(cgbp+8, iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(2)*iqlibc.ppUint32FromInt64(4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aap + ppuintptr(aai)*4))))
			Xprintf(cgtls, " \x00", 0)

			goto cg_1
		cg_1:
			;
			aai--
		}
		Xprintf(cgtls, "... \x00", 0)
		aai = ppint32(3)
		for {
			if !(aai >= 0) {
				break
			}

			Xprintf(cgtls, "%0*lx\x00", iqlibc.ppVaList(cgbp+8, iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(2)*iqlibc.ppUint32FromInt64(4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aap + ppuintptr(aai)*4))))
			if aai == 0 {
				ccv3 = "\x00"
			} else {
				ccv3 = " \x00"
			}
			Xprintf(cgtls, ccv3, 0)

			goto cg_2
		cg_2:
			;
			aai--
		}
	} else {

		aai = aan - ppint32(1)
		for {
			if !(aai >= 0) {
				break
			}

			Xprintf(cgtls, "%0*lx\x00", iqlibc.ppVaList(cgbp+8, iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(2)*iqlibc.ppUint32FromInt64(4)), *(*tnmp_limb_t)(iqunsafe.ppPointer(aap + ppuintptr(aai)*4))))
			if aai == 0 {
				ccv5 = "\x00"
			} else {
				ccv5 = " \x00"
			}
			Xprintf(cgtls, ccv5, 0)

			goto cg_4
		cg_4:
			;
			aai--
		}
	}
	Xputs(cgtls, "\x00")
}

var sitest ppint32

func sicheck_one(cgtls *iqlibc.ppTLS, aaqp tnmp_ptr, aarp tnmp_srcptr, aanp tnmp_srcptr, aann tnmp_size_t, aadp tnmp_srcptr, aadn tnmp_size_t, aafname ppuintptr, aaq_allowed_err tnmp_limb_t) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aa__gmp_i, aaqn, ccv10, ccv15, ccv16, ccv20, ccv25, ccv3 tnmp_size_t
	var aa__gmp_result, ccv11, ccv12, ccv17, ccv21, ccv22, ccv26, ccv27, ccv4, ccv5 ppint32
	var aa__gmp_x, aa__gmp_y, aai tnmp_limb_t
	var aamsg, aatvalue, ccv1 ppuintptr
	var aatp tnmp_ptr
	var ccv14, ccv19, ccv24, ccv29, ccv7, ccv8 ppbool
	var pp_ /* __tmp_marker at bp+0 */ ppuintptr
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__gmp_i, aa__gmp_result, aa__gmp_x, aa__gmp_y, aai, aamsg, aaqn, aatp, aatvalue, ccv1, ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv17, ccv19, ccv20, ccv21, ccv22, ccv24, ccv25, ccv26, ccv27, ccv29, ccv3, ccv4, ccv5, ccv7, ccv8
	cgtls.AllocaEntry()
	defer cgtls.AllocaExit()
	aaqn = aann - aadn + ppint32(1)
	*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) = ppuintptr(0)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(iqlibc.ppUint32FromInt32(aann+iqlibc.ppInt32FromInt32(1))*ppuint32(4) <= ppuint32(0x7f00)) != 0), ppint32(1)) != 0 {
		ccv1 = X__builtin_alloca(cgtls, ppuint32(iqlibc.ppUint32FromInt32(aann+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
	} else {
		ccv1 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, ppuint32(iqlibc.ppUint32FromInt32(aann+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
	}
	aatp = ccv1
	if aadn >= aaqn {
		Xrefmpn_mul(cgtls, aatp, aadp, aadn, aaqp, aaqn)
	} else {
		Xrefmpn_mul(cgtls, aatp, aaqp, aaqn, aadp, aadn)
	}

	aai = ppuint32(0)
	for {
		if ccv8 = aai < aaq_allowed_err; ccv8 {
			if ccv7 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(aann)*4)) > ppuint32(0); !ccv7 {
				aa__gmp_result = 0
				aa__gmp_i = aann
				for {
					aa__gmp_i--
					ccv3 = aa__gmp_i
					if !(ccv3 >= iqlibc.ppInt32FromInt32(0)) {
						break
					}
					aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(aa__gmp_i)*4))
					aa__gmp_y = *(*tnmp_limb_t)(iqunsafe.ppPointer(aanp + ppuintptr(aa__gmp_i)*4))
					if aa__gmp_x != aa__gmp_y {
						if aa__gmp_x > aa__gmp_y {
							ccv4 = ppint32(1)
						} else {
							ccv4 = -ppint32(1)
						}
						aa__gmp_result = ccv4
						break
					}
				}
				ccv5 = aa__gmp_result
				goto cg_6
			cg_6:
			}
		}
		if !(ccv8 && (ccv7 || ccv5 > 0)) {
			break
		}
		Xrefmpn_sub(cgtls, aatp, aatp, aann+ppint32(1), aadp, aadn)
		goto cg_2
	cg_2:
		;
		aai++
	}

	if ccv14 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(aann)*4)) > ppuint32(0); !ccv14 {
		aa__gmp_result = 0
		aa__gmp_i = aann
		for {
			aa__gmp_i--
			ccv10 = aa__gmp_i
			if !(ccv10 >= iqlibc.ppInt32FromInt32(0)) {
				break
			}
			aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(aa__gmp_i)*4))
			aa__gmp_y = *(*tnmp_limb_t)(iqunsafe.ppPointer(aanp + ppuintptr(aa__gmp_i)*4))
			if aa__gmp_x != aa__gmp_y {
				if aa__gmp_x > aa__gmp_y {
					ccv11 = ppint32(1)
				} else {
					ccv11 = -ppint32(1)
				}
				aa__gmp_result = ccv11
				break
			}
		}
		ccv12 = aa__gmp_result
		goto cg_13
	cg_13:
	}
	if !(ccv14 || ccv12 > 0) {
		goto cg_9
	}

	aamsg = "q too large\x00"
	aatvalue = "Q*D\x00"
	goto pperror
pperror:
	;
	Xprintf(cgtls, "\r*******************************************************************************\n\x00", 0)
	Xprintf(cgtls, "%s failed test %ld: %s\n\x00", iqlibc.ppVaList(cgbp+16, aafname, sitest, aamsg))
	Xprintf(cgtls, "N=    \x00", 0)
	sidumpy(cgtls, aanp, aann)
	Xprintf(cgtls, "D=    \x00", 0)
	sidumpy(cgtls, aadp, aadn)
	Xprintf(cgtls, "Q=    \x00", 0)
	sidumpy(cgtls, aaqp, aaqn)
	if aarp != 0 {
		Xprintf(cgtls, "R=    \x00", 0)
		sidumpy(cgtls, aarp, aadn)
	}
	Xprintf(cgtls, "%5s=\x00", iqlibc.ppVaList(cgbp+16, aatvalue))
	sidumpy(cgtls, aatp, aann+ppint32(1))
	Xprintf(cgtls, "nn = %ld, dn = %ld, qn = %ld\n\x00", iqlibc.ppVaList(cgbp+16, aann, aadn, aaqn))
	Xabort(cgtls)
cg_9:
	;

	Xrefmpn_sub_n(cgtls, aatp, aanp, aatp, aann)
	aatvalue = "N-Q*D\x00"

	if ccv19 = aann == aadn; !ccv19 {
		ccv15 = aann - aadn
		for cgcond := true; cgcond; cgcond = ccv15 != iqlibc.ppInt32FromInt32(0) {
			ccv15--
			ccv16 = ccv15
			if *(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(aadn)*4 + ppuintptr(ccv16)*4)) != iqlibc.ppUint32FromInt32(0) {
				ccv17 = 0
				goto cg_18
			}
		}
		ccv17 = ppint32(1)
		goto cg_18
	cg_18:
	}
	if ccv24 = !(ccv19 || ccv17 != 0); !ccv24 {
		aa__gmp_result = 0
		aa__gmp_i = aadn
		for {
			aa__gmp_i--
			ccv20 = aa__gmp_i
			if !(ccv20 >= iqlibc.ppInt32FromInt32(0)) {
				break
			}
			aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(aa__gmp_i)*4))
			aa__gmp_y = *(*tnmp_limb_t)(iqunsafe.ppPointer(aadp + ppuintptr(aa__gmp_i)*4))
			if aa__gmp_x != aa__gmp_y {
				if aa__gmp_x > aa__gmp_y {
					ccv21 = ppint32(1)
				} else {
					ccv21 = -ppint32(1)
				}
				aa__gmp_result = ccv21
				break
			}
		}
		ccv22 = aa__gmp_result
		goto cg_23
	cg_23:
	}
	if ccv24 || ccv22 >= 0 {

		aamsg = "q too small\x00"
		goto pperror
	}

	if ccv29 = aarp != 0; ccv29 {
		aa__gmp_result = 0
		aa__gmp_i = aadn
		for {
			aa__gmp_i--
			ccv25 = aa__gmp_i
			if !(ccv25 >= iqlibc.ppInt32FromInt32(0)) {
				break
			}
			aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aa__gmp_i)*4))
			aa__gmp_y = *(*tnmp_limb_t)(iqunsafe.ppPointer(aatp + ppuintptr(aa__gmp_i)*4))
			if aa__gmp_x != aa__gmp_y {
				if aa__gmp_x > aa__gmp_y {
					ccv26 = ppint32(1)
				} else {
					ccv26 = -ppint32(1)
				}
				aa__gmp_result = ccv26
				break
			}
		}
		ccv27 = aa__gmp_result
		goto cg_28
	cg_28:
	}
	if ccv29 && ccv27 != 0 {

		aamsg = "r incorrect\x00"
		goto pperror
	}

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) != ppuintptr(0)) != 0), 0) != 0 {
		X__gmp_tmp_reentrant_free(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp)))
	}
}

/* These are *bit* sizes. */

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aa__d0, aa__d1, aa__m, aa__q0, aa__q1, aa__r0, aa__r1, aa__u, aa__v, aa__x0, aa__x110, aa__x24, aa__x31 tnUWtype
	var aa__dst, aa__dst1, aa__dst10, aa__dst11, aa__dst12, aa__dst13, aa__dst14, aa__dst15, aa__dst16, aa__dst17, aa__dst18, aa__dst19, aa__dst2, aa__dst20, aa__dst21, aa__dst22, aa__dst23, aa__dst24, aa__dst3, aa__dst4, aa__dst5, aa__dst6, aa__dst7, aa__dst8, aa__dst9, aa__gmp_p, aadnp, aadup, aajunkp, aanp, aaqp, aarp, aascratch, ccv101, ccv105, ccv107, ccv111, ccv113, ccv117, ccv119, ccv12, ccv123, ccv125, ccv129, ccv131, ccv135, ccv137, ccv14, ccv141, ccv143, ccv147, ccv149, ccv152, ccv156, ccv158, ccv162, ccv164, ccv168, ccv170, ccv174, ccv176, ccv20, ccv28, ccv39, ccv41, ccv45, ccv47, ccv51, ccv53, ccv57, ccv59, ccv63, ccv65, ccv69, ccv71, ccv75, ccv77, ccv81, ccv83, ccv87, ccv89, ccv93, ccv95, ccv99 tnmp_ptr
	var aa__gmp_c, aa__gmp_c1, aa__gmp_l, aa__gmp_r, aa__gmp_r1, aa__gmp_x, aa__gmp_x1, aa__x, aa__x1, aa__x10, aa__x11, aa__x12, aa__x13, aa__x14, aa__x15, aa__x16, aa__x17, aa__x18, aa__x19, aa__x2, aa__x20, aa__x21, aa__x22, aa__x23, aa__x3, aa__x4, aa__x5, aa__x6, aa__x7, aa__x8, aa__x9, aa_dummy, aa_mask, aa_p, aa_t0, aa_t1, aa_v, aaqh, aaqran0, aaqran1, aaran, aarran0, aarran1, aat, ccv177, ccv23, ccv31 tnmp_limb_t
	var aa__gmp_i, aa__gmp_i1, aa__gmp_j, aa__gmp_j1, aa__gmp_j2, aa__gmp_j3, aa__gmp_n, aa__n, aa__n1, aa__n10, aa__n11, aa__n12, aa__n13, aa__n14, aa__n15, aa__n16, aa__n17, aa__n18, aa__n19, aa__n2, aa__n20, aa__n21, aa__n22, aa__n23, aa__n24, aa__n3, aa__n4, aa__n5, aa__n6, aa__n7, aa__n8, aa__n9, aaalloc, aaclearn, aadn, aai, aaitch, aamaxdn, aamaxnn, aann, ccv10, ccv103, ccv109, ccv115, ccv121, ccv127, ccv133, ccv139, ccv145, ccv150, ccv154, ccv160, ccv166, ccv172, ccv22, ccv30, ccv37, ccv43, ccv49, ccv55, ccv61, ccv67, ccv73, ccv79, ccv85, ccv91, ccv97 tnmp_size_t
	var aa__src, aa__src1, aa__src10, aa__src11, aa__src12, aa__src13, aa__src14, aa__src15, aa__src16, aa__src17, aa__src18, aa__src19, aa__src2, aa__src20, aa__src21, aa__src22, aa__src23, aa__src3, aa__src4, aa__src5, aa__src6, aa__src7, aa__src8, aa__src9, ccv100, ccv102, ccv106, ccv108, ccv112, ccv114, ccv118, ccv120, ccv124, ccv126, ccv13, ccv130, ccv132, ccv136, ccv138, ccv142, ccv144, ccv148, ccv153, ccv157, ccv159, ccv163, ccv165, ccv169, ccv171, ccv175, ccv21, ccv29, ccv36, ccv40, ccv42, ccv46, ccv48, ccv52, ccv54, ccv58, ccv60, ccv64, ccv66, ccv70, ccv72, ccv76, ccv78, ccv82, ccv84, ccv88, ccv9, ccv90, ccv94, ccv96 tnmp_srcptr
	var aa__uh, aa__ul, aa__vh, aa__vl tnUHWtype
	var aacount, aareps_nondefault, ccv1, ccv7 ppint32
	var aadbits, aamaxdbits, aamaxnbits, aanbits, ccv16, ccv18 ppuint32
	var aaenvval, ccv3, ccv4, ccv5 ppuintptr
	var aarands tngmp_randstate_ptr
	var aarepfactor ppfloat64
	var ccv15 tnmpz_srcptr
	var pp_ /* __tmp_marker at bp+76 */ ppuintptr
	var pp_ /* d at bp+12 */ tnmpz_t
	var pp_ /* dinv at bp+72 */ tngmp_pi1_t
	var pp_ /* end at bp+80 */ ppuintptr
	var pp_ /* junk at bp+60 */ tnmpz_t
	var pp_ /* n at bp+0 */ tnmpz_t
	var pp_ /* q at bp+24 */ tnmpz_t
	var pp_ /* qh at bp+84 */ tnmp_limb_t
	var pp_ /* r at bp+36 */ tnmpz_t
	var pp_ /* tz at bp+48 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__d0, aa__d1, aa__dst, aa__dst1, aa__dst10, aa__dst11, aa__dst12, aa__dst13, aa__dst14, aa__dst15, aa__dst16, aa__dst17, aa__dst18, aa__dst19, aa__dst2, aa__dst20, aa__dst21, aa__dst22, aa__dst23, aa__dst24, aa__dst3, aa__dst4, aa__dst5, aa__dst6, aa__dst7, aa__dst8, aa__dst9, aa__gmp_c, aa__gmp_c1, aa__gmp_i, aa__gmp_i1, aa__gmp_j, aa__gmp_j1, aa__gmp_j2, aa__gmp_j3, aa__gmp_l, aa__gmp_n, aa__gmp_p, aa__gmp_r, aa__gmp_r1, aa__gmp_x, aa__gmp_x1, aa__m, aa__n, aa__n1, aa__n10, aa__n11, aa__n12, aa__n13, aa__n14, aa__n15, aa__n16, aa__n17, aa__n18, aa__n19, aa__n2, aa__n20, aa__n21, aa__n22, aa__n23, aa__n24, aa__n3, aa__n4, aa__n5, aa__n6, aa__n7, aa__n8, aa__n9, aa__q0, aa__q1, aa__r0, aa__r1, aa__src, aa__src1, aa__src10, aa__src11, aa__src12, aa__src13, aa__src14, aa__src15, aa__src16, aa__src17, aa__src18, aa__src19, aa__src2, aa__src20, aa__src21, aa__src22, aa__src23, aa__src3, aa__src4, aa__src5, aa__src6, aa__src7, aa__src8, aa__src9, aa__u, aa__uh, aa__ul, aa__v, aa__vh, aa__vl, aa__x, aa__x0, aa__x1, aa__x10, aa__x11, aa__x110, aa__x12, aa__x13, aa__x14, aa__x15, aa__x16, aa__x17, aa__x18, aa__x19, aa__x2, aa__x20, aa__x21, aa__x22, aa__x23, aa__x24, aa__x3, aa__x31, aa__x4, aa__x5, aa__x6, aa__x7, aa__x8, aa__x9, aa_dummy, aa_mask, aa_p, aa_t0, aa_t1, aa_v, aaalloc, aaclearn, aacount, aadbits, aadn, aadnp, aadup, aaenvval, aai, aaitch, aajunkp, aamaxdbits, aamaxdn, aamaxnbits, aamaxnn, aanbits, aann, aanp, aaqh, aaqp, aaqran0, aaqran1, aaran, aarands, aarepfactor, aareps_nondefault, aarp, aarran0, aarran1, aascratch, aat, ccv1, ccv10, ccv100, ccv101, ccv102, ccv103, ccv105, ccv106, ccv107, ccv108, ccv109, ccv111, ccv112, ccv113, ccv114, ccv115, ccv117, ccv118, ccv119, ccv12, ccv120, ccv121, ccv123, ccv124, ccv125, ccv126, ccv127, ccv129, ccv13, ccv130, ccv131, ccv132, ccv133, ccv135, ccv136, ccv137, ccv138, ccv139, ccv14, ccv141, ccv142, ccv143, ccv144, ccv145, ccv147, ccv148, ccv149, ccv15, ccv150, ccv152, ccv153, ccv154, ccv156, ccv157, ccv158, ccv159, ccv16, ccv160, ccv162, ccv163, ccv164, ccv165, ccv166, ccv168, ccv169, ccv170, ccv171, ccv172, ccv174, ccv175, ccv176, ccv177, ccv18, ccv20, ccv21, ccv22, ccv23, ccv28, ccv29, ccv3, ccv30, ccv31, ccv36, ccv37, ccv39, ccv4, ccv40, ccv41, ccv42, ccv43, ccv45, ccv46, ccv47, ccv48, ccv49, ccv5, ccv51, ccv52, ccv53, ccv54, ccv55, ccv57, ccv58, ccv59, ccv60, ccv61, ccv63, ccv64, ccv65, ccv66, ccv67, ccv69, ccv7, ccv70, ccv71, ccv72, ccv73, ccv75, ccv76, ccv77, ccv78, ccv79, ccv81, ccv82, ccv83, ccv84, ccv85, ccv87, ccv88, ccv89, ccv9, ccv90, ccv91, ccv93, ccv94, ccv95, ccv96, ccv97, ccv99
	cgtls.AllocaEntry()
	defer cgtls.AllocaExit()
	aacount = ppint32(mvCOUNT)

	aareps_nondefault = 0
	if aaargc > ppint32(1) {
		aacount = Xstrtol(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*4)), cgbp+80, 0)
		if *(*ppuint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 80)))) != 0 || aacount <= 0 {
			Xfprintf(cgtls, Xstderr, "Invalid test count: %s.\n\x00", iqlibc.ppVaList(cgbp+96, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*4))))
			Xexit(cgtls, ppint32(1))
		}
		aaargv += 4
		aaargc--
		aareps_nondefault = ppint32(1)
	}
	aaenvval = Xgetenv(cgtls, "GMP_CHECK_REPFACTOR\x00")
	if aaenvval != iqlibc.ppUintptrFromInt32(0) {
		aarepfactor = Xstrtod(cgtls, aaenvval, cgbp+80)
		if *(*ppuint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 80)))) != 0 || aarepfactor <= iqlibc.ppFloat64FromInt32(0) {
			Xfprintf(cgtls, Xstderr, "Invalid repfactor: %f.\n\x00", iqlibc.ppVaList(cgbp+96, aarepfactor))
			Xexit(cgtls, ppint32(1))
		}
		aacount = ppint32(ppfloat64(aacount) * aarepfactor)
		if aacount > ppint32(iqlibc.ppInt32FromInt32(1)) {
			ccv1 = aacount
		} else {
			ccv1 = ppint32(iqlibc.ppInt32FromInt32(1))
		}
		aacount = ccv1
		aareps_nondefault = ppint32(1)
	}
	if aareps_nondefault != 0 {
		Xprintf(cgtls, "Running test with %ld repetitions (include this in bug reports)\n\x00", iqlibc.ppVaList(cgbp+96, aacount))
	}

	aamaxdbits = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1) << iqlibc.ppInt32FromInt32(mvSIZE_LOG))
	aamaxnbits = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1) << (iqlibc.ppInt32FromInt32(mvSIZE_LOG) + iqlibc.ppInt32FromInt32(1)))

	Xtests_start(cgtls)
	if !(X__gmp_rands_initialized != 0) {
		X__gmp_rands_initialized = ppuint8(1)
		X__gmp_randinit_mt_noseed(cgtls, ppuintptr(iqunsafe.ppPointer(&X__gmp_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	aarands = ppuintptr(iqunsafe.ppPointer(&X__gmp_rands))

	X__gmpz_init(cgtls, cgbp)
	X__gmpz_init(cgtls, cgbp+12)
	X__gmpz_init(cgtls, cgbp+24)
	X__gmpz_init(cgtls, cgbp+36)
	X__gmpz_init(cgtls, cgbp+48)
	X__gmpz_init(cgtls, cgbp+60)

	aamaxnn = iqlibc.ppInt32FromUint32(aamaxnbits/iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) + ppuint32(1))
	aamaxdn = iqlibc.ppInt32FromUint32(aamaxdbits/iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) + ppuint32(1))

	*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 76)) = ppuintptr(0)

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(iqlibc.ppUint32FromInt32(aamaxnn+iqlibc.ppInt32FromInt32(2))*ppuint32(4) <= ppuint32(0x7f00)) != 0), ppint32(1)) != 0 {
		ccv3 = X__builtin_alloca(cgtls, ppuint32(iqlibc.ppUint32FromInt32(aamaxnn+iqlibc.ppInt32FromInt32(2))*ppuint32(4)))
	} else {
		ccv3 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp+76, ppuint32(iqlibc.ppUint32FromInt32(aamaxnn+iqlibc.ppInt32FromInt32(2))*ppuint32(4)))
	}
	aaqp = ccv3 + ppuintptr(1)*4
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(iqlibc.ppUint32FromInt32(aamaxnn+iqlibc.ppInt32FromInt32(2))*ppuint32(4) <= ppuint32(0x7f00)) != 0), ppint32(1)) != 0 {
		ccv4 = X__builtin_alloca(cgtls, ppuint32(iqlibc.ppUint32FromInt32(aamaxnn+iqlibc.ppInt32FromInt32(2))*ppuint32(4)))
	} else {
		ccv4 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp+76, ppuint32(iqlibc.ppUint32FromInt32(aamaxnn+iqlibc.ppInt32FromInt32(2))*ppuint32(4)))
	}
	aarp = ccv4 + ppuintptr(1)*4
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(iqlibc.ppUint32FromInt32(aamaxdn)*ppuint32(4) <= ppuint32(0x7f00)) != 0), ppint32(1)) != 0 {
		ccv5 = X__builtin_alloca(cgtls, ppuint32(iqlibc.ppUint32FromInt32(aamaxdn)*ppuint32(4)))
	} else {
		ccv5 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp+76, ppuint32(iqlibc.ppUint32FromInt32(aamaxdn)*ppuint32(4)))
	}
	aadnp = ccv5

	aaalloc = ppint32(1)
	aascratch = (*(*func(*iqlibc.ppTLS, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_allocate_func})))(cgtls, ppuint32(iqlibc.ppUint32FromInt32(aaalloc)*ppuint32(4)))

	sitest = ppint32(-ppint32(300))
	for {
		if !(sitest < aacount) {
			break
		}

		aanbits = Xurandom(cgtls)%(aamaxnbits-iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))) + iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))

		if sitest < 0 {
			aadbits = iqlibc.ppUint32FromInt32(sitest+iqlibc.ppInt32FromInt32(300))%(aanbits-ppuint32(1)) + ppuint32(1)
		} else {
			aadbits = Xurandom(cgtls)%(aanbits-ppuint32(1))%aamaxdbits + ppuint32(1)
		}

		for {
			X__gmpz_rrandomb(cgtls, cgbp+12, aarands, aadbits)
			goto cg_8
		cg_8:
			;
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+12)).fd_mp_size < 0 {
				ccv7 = -ppint32(1)
			} else {
				ccv7 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+12)).fd_mp_size > 0)
			}
			if !(ccv7 == 0) {
				break
			}
		}
		aadn = ppint32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 12)).fd_mp_size)
		aadup = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 12)).fd_mp_d
		if aadn != 0 {
			aa__n = aadn - ppint32(1)
			aa__dst = aadnp
			aa__src = aadup
			ccv9 = aa__src
			aa__src += 4
			aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv9))
			if aa__n != 0 {
				for {
					ccv12 = aa__dst
					aa__dst += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv12)) = aa__x
					ccv13 = aa__src
					aa__src += 4
					aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv13))
					goto cg_11
				cg_11:
					;
					aa__n--
					ccv10 = aa__n
					if !(ccv10 != 0) {
						break
					}
				}
			}
			ccv14 = aa__dst
			aa__dst += 4
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv14)) = aa__x
		}

		*(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) |= iqlibc.ppUint32FromInt32(1) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) - iqlibc.ppInt32FromInt32(1))

		if sitest%ppint32(2) == 0 {

			X__gmpz_rrandomb(cgtls, cgbp, aarands, aanbits)
			aann = ppint32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aann >= aadn)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(196), "nn >= dn\x00")
			}
		} else {

			for cgcond := true; cgcond; cgcond = aann > aamaxnn || aann < aadn {

				X__gmpz_rrandomb(cgtls, cgbp+24, aarands, Xurandom(cgtls)%(aanbits-aadbits+ppuint32(1)))
				X__gmpz_rrandomb(cgtls, cgbp+36, aarands, Xurandom(cgtls)%ppuint32(X__gmpz_sizeinbase(cgtls, cgbp+12, ppint32(2))))
				X__gmpz_mul(cgtls, cgbp, cgbp+24, cgbp+12)
				X__gmpz_add(cgtls, cgbp, cgbp, cgbp+36)
				aann = ppint32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size)
			}
		}

		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aann <= aamaxnn)) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(211), "nn <= maxnn\x00")
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aadn <= aamaxdn)) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(212), "dn <= maxdn\x00")
		}

		X__gmpz_urandomb(cgtls, cgbp+60, aarands, aanbits)
		aajunkp = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 60)).fd_mp_d

		aanp = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_d

		X__gmpz_urandomb(cgtls, cgbp+48, aarands, ppuint32(32))
		ccv15 = cgbp + 48
		aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv15)).fd_mp_d
		aa__gmp_n = ppint32((*tn__mpz_struct)(iqunsafe.ppPointer(ccv15)).fd_mp_size)
		aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
		if aa__gmp_n != 0 {
			ccv18 = aa__gmp_l
		} else {
			ccv18 = ppuint32(0)
		}
		ccv16 = ccv18
		goto cg_17
	cg_17:
		aat = ccv16

		if aat%ppuint32(17) == ppuint32(0) {

			*(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) = ^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aadup + ppuintptr(aadn-ppint32(1))*4)) = ^iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)
		}

		switch iqlibc.ppInt32FromUint32(aat) % iqlibc.ppInt32FromInt32(16) {

		case 0:
			aaclearn = iqlibc.ppInt32FromUint32(Xurandom(cgtls) % iqlibc.ppUint32FromInt32(aann))
			aai = aaclearn
			for {
				if !(aai < aann) {
					break
				}
				*(*tnmp_limb_t)(iqunsafe.ppPointer(aanp + ppuintptr(aai)*4)) = ppuint32(0)
				goto cg_19
			cg_19:
				;
				aai++
			}
			break
			fallthrough
		case ppint32(1):
			ccv20 = aanp + ppuintptr(aann)*4 - ppuintptr(aadn)*4
			ccv21 = aadnp
			ccv22 = aadn
			ccv23 = Xurandom(cgtls)
			aa__gmp_x1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv21))
			aa__gmp_r1 = aa__gmp_x1 - ccv23
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv20)) = aa__gmp_r1
			if aa__gmp_x1 < ccv23 {
				aa__gmp_c1 = ppuint32(1)
				aa__gmp_i1 = iqlibc.ppInt32FromInt32(1)
				for {
					if !(aa__gmp_i1 < ccv22) {
						break
					}
					aa__gmp_x1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv21 + ppuintptr(aa__gmp_i1)*4))
					aa__gmp_r1 = aa__gmp_x1 - ppuint32(1)
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv20 + ppuintptr(aa__gmp_i1)*4)) = aa__gmp_r1
					aa__gmp_i1++
					if !(aa__gmp_x1 < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1))) {
						if ccv21 != ccv20 {
							aa__gmp_j2 = aa__gmp_i1
							for {
								if !(aa__gmp_j2 < ccv22) {
									break
								}
								*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv20 + ppuintptr(aa__gmp_j2)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv21 + ppuintptr(aa__gmp_j2)*4))
								goto cg_25
							cg_25:
								;
								aa__gmp_j2++
							}
						}
						aa__gmp_c1 = ppuint32(0)
						break
					}
					goto cg_24
				cg_24:
				}
			} else {
				if ccv21 != ccv20 {
					aa__gmp_j3 = ppint32(iqlibc.ppInt32FromInt32(1))
					for {
						if !(aa__gmp_j3 < ccv22) {
							break
						}
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv20 + ppuintptr(aa__gmp_j3)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv21 + ppuintptr(aa__gmp_j3)*4))
						goto cg_26
					cg_26:
						;
						aa__gmp_j3++
					}
				}
				aa__gmp_c1 = ppuint32(0)
			}
			pp_ = aa__gmp_c1
			goto cg_27
		cg_27:
			;
			break
			fallthrough
		case ppint32(2):
			ccv28 = aanp + ppuintptr(aann)*4 - ppuintptr(aadn)*4
			ccv29 = aadnp
			ccv30 = aadn
			ccv31 = Xurandom(cgtls)
			aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv29))
			aa__gmp_r = aa__gmp_x + ccv31
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv28)) = aa__gmp_r
			if aa__gmp_r < ccv31 {
				aa__gmp_c = ppuint32(1)
				aa__gmp_i = iqlibc.ppInt32FromInt32(1)
				for {
					if !(aa__gmp_i < ccv30) {
						break
					}
					aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv29 + ppuintptr(aa__gmp_i)*4))
					aa__gmp_r = aa__gmp_x + ppuint32(1)
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv28 + ppuintptr(aa__gmp_i)*4)) = aa__gmp_r
					aa__gmp_i++
					if !(aa__gmp_r < iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1))) {
						if ccv29 != ccv28 {
							aa__gmp_j = aa__gmp_i
							for {
								if !(aa__gmp_j < ccv30) {
									break
								}
								*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv28 + ppuintptr(aa__gmp_j)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv29 + ppuintptr(aa__gmp_j)*4))
								goto cg_33
							cg_33:
								;
								aa__gmp_j++
							}
						}
						aa__gmp_c = ppuint32(0)
						break
					}
					goto cg_32
				cg_32:
				}
			} else {
				if ccv29 != ccv28 {
					aa__gmp_j1 = ppint32(iqlibc.ppInt32FromInt32(1))
					for {
						if !(aa__gmp_j1 < ccv30) {
							break
						}
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv28 + ppuintptr(aa__gmp_j1)*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv29 + ppuintptr(aa__gmp_j1)*4))
						goto cg_34
					cg_34:
						;
						aa__gmp_j1++
					}
				}
				aa__gmp_c = ppuint32(0)
			}
			pp_ = aa__gmp_c
			goto cg_35
		cg_35:
			;
			break
		}

		if aadn >= ppint32(2) {
			aa__d1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))
			aa__d0 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1))
			aa__q1 = ^*(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) / aa__d1
			aa__r1 = ^*(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) - aa__q1*aa__d1
			aa__m = aa__q1 * aa__d0
			aa__r1 = aa__r1*(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))) | ^iqlibc.ppUint32FromInt32(0)>>(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))
			if aa__r1 < aa__m {
				aa__q1--
				aa__r1 += *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4))
				if aa__r1 >= *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) { /* i.e. we didn't get carry when adding to __r1 */
					if aa__r1 < aa__m {
						aa__q1--
						aa__r1 += *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4))
					}
				}
			}
			aa__r1 -= aa__m
			aa__q0 = aa__r1 / aa__d1
			aa__r0 = aa__r1 - aa__q0*aa__d1
			aa__m = aa__q0 * aa__d0
			aa__r0 = aa__r0*(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))) | ^iqlibc.ppUint32FromInt32(0)&(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))-iqlibc.ppUint32FromInt32(1))
			if aa__r0 < aa__m {
				aa__q0--
				aa__r0 += *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4))
				if aa__r0 >= *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) {
					if aa__r0 < aa__m {
						aa__q0--
						aa__r0 += *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4))
					}
				}
			}
			aa__r0 -= aa__m
			aa_v = aa__q1*(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))) | aa__q0
			aa_dummy = aa__r0
			aa_p = *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) * aa_v
			aa_p += *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(2))*4))
			if aa_p < *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(2))*4)) {
				aa_v--
				aa_mask = -iqlibc.ppBoolUint32(aa_p >= *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)))
				aa_p -= *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4))
				aa_v += aa_mask
				aa_p -= aa_mask & *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4))
			}
			aa__u = *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(2))*4))
			aa__v = aa_v
			aa__ul = ppuint32(aa__u & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)))
			aa__uh = ppuint32(aa__u >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)))
			aa__vl = ppuint32(aa__v & (iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) - iqlibc.ppUint32FromInt32(1)))
			aa__vh = ppuint32(aa__v >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)))
			aa__x0 = ppuint32(aa__ul) * ppuint32(aa__vl)
			aa__x110 = ppuint32(aa__ul) * ppuint32(aa__vh)
			aa__x24 = ppuint32(aa__uh) * ppuint32(aa__vl)
			aa__x31 = ppuint32(aa__uh) * ppuint32(aa__vh)
			aa__x110 += aa__x0 >> (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2)) /* this can't give carry */
			aa__x110 += aa__x24                                                                           /* but this indeed can */
			if aa__x110 < aa__x24 {                                                                       /* did we get it? */
				aa__x31 += iqlibc.ppUint32FromInt32(1) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) / iqlibc.ppInt32FromInt32(2))
			} /* yes, add it in the proper pos. */
			aa_t1 = aa__x31 + aa__x110>>(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))
			aa_t0 = aa__x110<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2)) + aa__x0&(iqlibc.ppUint32FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)/iqlibc.ppInt32FromInt32(2))-iqlibc.ppUint32FromInt32(1))
			aa_p += aa_t1
			if aa_p < aa_t1 {
				aa_v--
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(aa_p >= *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4))) != 0), 0) != 0 {
					if aa_p > *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)) || aa_t0 >= *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(2))*4)) {
						aa_v--
					}
				}
			}
			(*(*tngmp_pi1_t)(iqunsafe.ppPointer(cgbp + 72))).fdinv32 = aa_v
		}

		aarran0 = Xurandom(cgtls)
		aarran1 = Xurandom(cgtls)
		aaqran0 = Xurandom(cgtls)
		aaqran1 = Xurandom(cgtls)

		*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) = aaqran0
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn+ppint32(1))*4)) = aaqran1
		*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) = aarran0

		aaran = Xurandom(cgtls)

		if ppfloat64(aann-aadn)*ppfloat64(aadn) < ppfloat64(100000) {

			/* Test mpn_sbpi1_div_qr */
			if aadn > ppint32(2) {

				if aann != 0 {
					aa__n1 = aann - ppint32(1)
					aa__dst1 = aarp
					aa__src1 = aanp
					ccv36 = aa__src1
					aa__src1 += 4
					aa__x1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv36))
					if aa__n1 != 0 {
						for {
							ccv39 = aa__dst1
							aa__dst1 += 4
							*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv39)) = aa__x1
							ccv40 = aa__src1
							aa__src1 += 4
							aa__x1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv40))
							goto cg_38
						cg_38:
							;
							aa__n1--
							ccv37 = aa__n1
							if !(ccv37 != 0) {
								break
							}
						}
					}
					ccv41 = aa__dst1
					aa__dst1 += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv41)) = aa__x1
				}
				if aann > aadn {
					if aann-aadn != 0 {
						aa__n2 = aann - aadn - ppint32(1)
						aa__dst2 = aaqp
						aa__src2 = aajunkp
						ccv42 = aa__src2
						aa__src2 += 4
						aa__x2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv42))
						if aa__n2 != 0 {
							for {
								ccv45 = aa__dst2
								aa__dst2 += 4
								*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv45)) = aa__x2
								ccv46 = aa__src2
								aa__src2 += 4
								aa__x2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv46))
								goto cg_44
							cg_44:
								;
								aa__n2--
								ccv43 = aa__n2
								if !(ccv43 != 0) {
									break
								}
							}
						}
						ccv47 = aa__dst2
						aa__dst2 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv47)) = aa__x2
					}
				}
				*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_sbpi1_div_qr(cgtls, aaqp, aarp, aann, aadnp, aadn, (*(*tngmp_pi1_t)(iqunsafe.ppPointer(cgbp + 72))).fdinv32)
				sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadnp, aadn, "mpn_sbpi1_div_qr\x00", ppuint32(0))
			}

			/* Test mpn_sbpi1_divappr_q */
			if aadn > ppint32(2) {

				if aann != 0 {
					aa__n3 = aann - ppint32(1)
					aa__dst3 = aarp
					aa__src3 = aanp
					ccv48 = aa__src3
					aa__src3 += 4
					aa__x3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv48))
					if aa__n3 != 0 {
						for {
							ccv51 = aa__dst3
							aa__dst3 += 4
							*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv51)) = aa__x3
							ccv52 = aa__src3
							aa__src3 += 4
							aa__x3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv52))
							goto cg_50
						cg_50:
							;
							aa__n3--
							ccv49 = aa__n3
							if !(ccv49 != 0) {
								break
							}
						}
					}
					ccv53 = aa__dst3
					aa__dst3 += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv53)) = aa__x3
				}
				if aann > aadn {
					if aann-aadn != 0 {
						aa__n4 = aann - aadn - ppint32(1)
						aa__dst4 = aaqp
						aa__src4 = aajunkp
						ccv54 = aa__src4
						aa__src4 += 4
						aa__x4 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv54))
						if aa__n4 != 0 {
							for {
								ccv57 = aa__dst4
								aa__dst4 += 4
								*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv57)) = aa__x4
								ccv58 = aa__src4
								aa__src4 += 4
								aa__x4 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv58))
								goto cg_56
							cg_56:
								;
								aa__n4--
								ccv55 = aa__n4
								if !(ccv55 != 0) {
									break
								}
							}
						}
						ccv59 = aa__dst4
						aa__dst4 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv59)) = aa__x4
					}
				}
				*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_sbpi1_divappr_q(cgtls, aaqp, aarp, aann, aadnp, aadn, (*(*tngmp_pi1_t)(iqunsafe.ppPointer(cgbp + 72))).fdinv32)
				sicheck_one(cgtls, aaqp, iqlibc.ppUintptrFromInt32(0), aanp, aann, aadnp, aadn, "mpn_sbpi1_divappr_q\x00", ppuint32(1))
			}

			/* Test mpn_sbpi1_div_q */
			if aadn > ppint32(2) {

				if aann != 0 {
					aa__n5 = aann - ppint32(1)
					aa__dst5 = aarp
					aa__src5 = aanp
					ccv60 = aa__src5
					aa__src5 += 4
					aa__x5 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv60))
					if aa__n5 != 0 {
						for {
							ccv63 = aa__dst5
							aa__dst5 += 4
							*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv63)) = aa__x5
							ccv64 = aa__src5
							aa__src5 += 4
							aa__x5 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv64))
							goto cg_62
						cg_62:
							;
							aa__n5--
							ccv61 = aa__n5
							if !(ccv61 != 0) {
								break
							}
						}
					}
					ccv65 = aa__dst5
					aa__dst5 += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv65)) = aa__x5
				}
				if aann > aadn {
					if aann-aadn != 0 {
						aa__n6 = aann - aadn - ppint32(1)
						aa__dst6 = aaqp
						aa__src6 = aajunkp
						ccv66 = aa__src6
						aa__src6 += 4
						aa__x6 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv66))
						if aa__n6 != 0 {
							for {
								ccv69 = aa__dst6
								aa__dst6 += 4
								*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv69)) = aa__x6
								ccv70 = aa__src6
								aa__src6 += 4
								aa__x6 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv70))
								goto cg_68
							cg_68:
								;
								aa__n6--
								ccv67 = aa__n6
								if !(ccv67 != 0) {
									break
								}
							}
						}
						ccv71 = aa__dst6
						aa__dst6 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv71)) = aa__x6
					}
				}
				*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_sbpi1_div_q(cgtls, aaqp, aarp, aann, aadnp, aadn, (*(*tngmp_pi1_t)(iqunsafe.ppPointer(cgbp + 72))).fdinv32)
				sicheck_one(cgtls, aaqp, iqlibc.ppUintptrFromInt32(0), aanp, aann, aadnp, aadn, "mpn_sbpi1_div_q\x00", ppuint32(0))
			}

			/* Test mpn_sec_div_qr */
			aaitch = X__gmpn_sec_div_qr_itch(cgtls, aann, aadn)
			if aaitch+ppint32(1) > aaalloc {

				aascratch = (*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_reallocate_func})))(cgtls, aascratch, ppuint32(iqlibc.ppUint32FromInt32(aaalloc)*ppuint32(4)), ppuint32(iqlibc.ppUint32FromInt32(aaitch+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
				aaalloc = aaitch + ppint32(1)
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)) = aaran
			if aann != 0 {
				aa__n7 = aann - ppint32(1)
				aa__dst7 = aarp
				aa__src7 = aanp
				ccv72 = aa__src7
				aa__src7 += 4
				aa__x7 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv72))
				if aa__n7 != 0 {
					for {
						ccv75 = aa__dst7
						aa__dst7 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv75)) = aa__x7
						ccv76 = aa__src7
						aa__src7 += 4
						aa__x7 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv76))
						goto cg_74
					cg_74:
						;
						aa__n7--
						ccv73 = aa__n7
						if !(ccv73 != 0) {
							break
						}
					}
				}
				ccv77 = aa__dst7
				aa__dst7 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv77)) = aa__x7
			}
			if aann >= aadn {
				if aann-aadn+ppint32(1) != 0 {
					aa__n8 = aann - aadn + ppint32(1) - ppint32(1)
					aa__dst8 = aaqp
					aa__src8 = aajunkp
					ccv78 = aa__src8
					aa__src8 += 4
					aa__x8 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv78))
					if aa__n8 != 0 {
						for {
							ccv81 = aa__dst8
							aa__dst8 += 4
							*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv81)) = aa__x8
							ccv82 = aa__src8
							aa__src8 += 4
							aa__x8 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv82))
							goto cg_80
						cg_80:
							;
							aa__n8--
							ccv79 = aa__n8
							if !(ccv79 != 0) {
								break
							}
						}
					}
					ccv83 = aa__dst8
					aa__dst8 += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv83)) = aa__x8
				}
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_sec_div_qr(cgtls, aaqp, aarp, aann, aadup, aadn, aascratch)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaran == *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(301), "ran == scratch[itch]\x00")
			}
			sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadup, aadn, "mpn_sec_div_qr (unnorm)\x00", ppuint32(0))

			/* Test mpn_sec_div_r */
			aaitch = X__gmpn_sec_div_r_itch(cgtls, aann, aadn)
			if aaitch+ppint32(1) > aaalloc {

				aascratch = (*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_reallocate_func})))(cgtls, aascratch, ppuint32(iqlibc.ppUint32FromInt32(aaalloc)*ppuint32(4)), ppuint32(iqlibc.ppUint32FromInt32(aaitch+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
				aaalloc = aaitch + ppint32(1)
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)) = aaran
			if aann != 0 {
				aa__n9 = aann - ppint32(1)
				aa__dst9 = aarp
				aa__src9 = aanp
				ccv84 = aa__src9
				aa__src9 += 4
				aa__x9 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv84))
				if aa__n9 != 0 {
					for {
						ccv87 = aa__dst9
						aa__dst9 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv87)) = aa__x9
						ccv88 = aa__src9
						aa__src9 += 4
						aa__x9 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv88))
						goto cg_86
					cg_86:
						;
						aa__n9--
						ccv85 = aa__n9
						if !(ccv85 != 0) {
							break
						}
					}
				}
				ccv89 = aa__dst9
				aa__dst9 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv89)) = aa__x9
			}
			X__gmpn_sec_div_r(cgtls, aarp, aann, aadup, aadn, aascratch)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaran == *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(314), "ran == scratch[itch]\x00")
			}
			/* Note: Since check_one cannot cope with remainder-only functions, we
			   pass qp[] from the previous function, mpn_sec_div_qr.  */
			sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadup, aadn, "mpn_sec_div_r (unnorm)\x00", ppuint32(0))

			/* Normalised case, mpn_sec_div_qr */
			aaitch = X__gmpn_sec_div_qr_itch(cgtls, aann, aadn)
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)) = aaran

			if aann != 0 {
				aa__n10 = aann - ppint32(1)
				aa__dst10 = aarp
				aa__src10 = aanp
				ccv90 = aa__src10
				aa__src10 += 4
				aa__x10 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv90))
				if aa__n10 != 0 {
					for {
						ccv93 = aa__dst10
						aa__dst10 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv93)) = aa__x10
						ccv94 = aa__src10
						aa__src10 += 4
						aa__x10 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv94))
						goto cg_92
					cg_92:
						;
						aa__n10--
						ccv91 = aa__n10
						if !(ccv91 != 0) {
							break
						}
					}
				}
				ccv95 = aa__dst10
				aa__dst10 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv95)) = aa__x10
			}
			if aann >= aadn {
				if aann-aadn+ppint32(1) != 0 {
					aa__n11 = aann - aadn + ppint32(1) - ppint32(1)
					aa__dst11 = aaqp
					aa__src11 = aajunkp
					ccv96 = aa__src11
					aa__src11 += 4
					aa__x11 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv96))
					if aa__n11 != 0 {
						for {
							ccv99 = aa__dst11
							aa__dst11 += 4
							*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv99)) = aa__x11
							ccv100 = aa__src11
							aa__src11 += 4
							aa__x11 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv100))
							goto cg_98
						cg_98:
							;
							aa__n11--
							ccv97 = aa__n11
							if !(ccv97 != 0) {
								break
							}
						}
					}
					ccv101 = aa__dst11
					aa__dst11 += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv101)) = aa__x11
				}
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_sec_div_qr(cgtls, aaqp, aarp, aann, aadnp, aadn, aascratch)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaran == *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(327), "ran == scratch[itch]\x00")
			}
			sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadnp, aadn, "mpn_sec_div_qr (norm)\x00", ppuint32(0))

			/* Normalised case, mpn_sec_div_r */
			aaitch = X__gmpn_sec_div_r_itch(cgtls, aann, aadn)
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)) = aaran
			if aann != 0 {
				aa__n12 = aann - ppint32(1)
				aa__dst12 = aarp
				aa__src12 = aanp
				ccv102 = aa__src12
				aa__src12 += 4
				aa__x12 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv102))
				if aa__n12 != 0 {
					for {
						ccv105 = aa__dst12
						aa__dst12 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv105)) = aa__x12
						ccv106 = aa__src12
						aa__src12 += 4
						aa__x12 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv106))
						goto cg_104
					cg_104:
						;
						aa__n12--
						ccv103 = aa__n12
						if !(ccv103 != 0) {
							break
						}
					}
				}
				ccv107 = aa__dst12
				aa__dst12 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv107)) = aa__x12
			}
			X__gmpn_sec_div_r(cgtls, aarp, aann, aadnp, aadn, aascratch)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaran == *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(335), "ran == scratch[itch]\x00")
			}
			/* Note: Since check_one cannot cope with remainder-only functions, we
			   pass qp[] from the previous function, mpn_sec_div_qr.  */
			sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadnp, aadn, "mpn_sec_div_r (norm)\x00", ppuint32(0))
		}

		/* Test mpn_dcpi1_div_qr */
		if aadn >= ppint32(6) && aann-aadn >= ppint32(3) {

			if aann != 0 {
				aa__n13 = aann - ppint32(1)
				aa__dst13 = aarp
				aa__src13 = aanp
				ccv108 = aa__src13
				aa__src13 += 4
				aa__x13 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv108))
				if aa__n13 != 0 {
					for {
						ccv111 = aa__dst13
						aa__dst13 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv111)) = aa__x13
						ccv112 = aa__src13
						aa__src13 += 4
						aa__x13 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv112))
						goto cg_110
					cg_110:
						;
						aa__n13--
						ccv109 = aa__n13
						if !(ccv109 != 0) {
							break
						}
					}
				}
				ccv113 = aa__dst13
				aa__dst13 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv113)) = aa__x13
			}
			if aann > aadn {
				if aann-aadn != 0 {
					aa__n14 = aann - aadn - ppint32(1)
					aa__dst14 = aaqp
					aa__src14 = aajunkp
					ccv114 = aa__src14
					aa__src14 += 4
					aa__x14 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv114))
					if aa__n14 != 0 {
						for {
							ccv117 = aa__dst14
							aa__dst14 += 4
							*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv117)) = aa__x14
							ccv118 = aa__src14
							aa__src14 += 4
							aa__x14 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv118))
							goto cg_116
						cg_116:
							;
							aa__n14--
							ccv115 = aa__n14
							if !(ccv115 != 0) {
								break
							}
						}
					}
					ccv119 = aa__dst14
					aa__dst14 += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv119)) = aa__x14
				}
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_dcpi1_div_qr(cgtls, aaqp, aarp, aann, aadnp, aadn, cgbp+72)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(348), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn+ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(348), "qp[nn - dn + 1] == qran1\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aarran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(349), "rp[-1] == rran0\x00")
			}
			sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadnp, aadn, "mpn_dcpi1_div_qr\x00", ppuint32(0))
		}

		/* Test mpn_dcpi1_divappr_q */
		if aadn >= ppint32(6) && aann-aadn >= ppint32(3) {

			if aann != 0 {
				aa__n15 = aann - ppint32(1)
				aa__dst15 = aarp
				aa__src15 = aanp
				ccv120 = aa__src15
				aa__src15 += 4
				aa__x15 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv120))
				if aa__n15 != 0 {
					for {
						ccv123 = aa__dst15
						aa__dst15 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv123)) = aa__x15
						ccv124 = aa__src15
						aa__src15 += 4
						aa__x15 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv124))
						goto cg_122
					cg_122:
						;
						aa__n15--
						ccv121 = aa__n15
						if !(ccv121 != 0) {
							break
						}
					}
				}
				ccv125 = aa__dst15
				aa__dst15 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv125)) = aa__x15
			}
			if aann > aadn {
				if aann-aadn != 0 {
					aa__n16 = aann - aadn - ppint32(1)
					aa__dst16 = aaqp
					aa__src16 = aajunkp
					ccv126 = aa__src16
					aa__src16 += 4
					aa__x16 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv126))
					if aa__n16 != 0 {
						for {
							ccv129 = aa__dst16
							aa__dst16 += 4
							*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv129)) = aa__x16
							ccv130 = aa__src16
							aa__src16 += 4
							aa__x16 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv130))
							goto cg_128
						cg_128:
							;
							aa__n16--
							ccv127 = aa__n16
							if !(ccv127 != 0) {
								break
							}
						}
					}
					ccv131 = aa__dst16
					aa__dst16 += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv131)) = aa__x16
				}
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_dcpi1_divappr_q(cgtls, aaqp, aarp, aann, aadnp, aadn, cgbp+72)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(360), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn+ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(360), "qp[nn - dn + 1] == qran1\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aarran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(361), "rp[-1] == rran0\x00")
			}
			sicheck_one(cgtls, aaqp, iqlibc.ppUintptrFromInt32(0), aanp, aann, aadnp, aadn, "mpn_dcpi1_divappr_q\x00", ppuint32(1))
		}

		/* Test mpn_dcpi1_div_q */
		if aadn >= ppint32(6) && aann-aadn >= ppint32(3) {

			if aann != 0 {
				aa__n17 = aann - ppint32(1)
				aa__dst17 = aarp
				aa__src17 = aanp
				ccv132 = aa__src17
				aa__src17 += 4
				aa__x17 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv132))
				if aa__n17 != 0 {
					for {
						ccv135 = aa__dst17
						aa__dst17 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv135)) = aa__x17
						ccv136 = aa__src17
						aa__src17 += 4
						aa__x17 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv136))
						goto cg_134
					cg_134:
						;
						aa__n17--
						ccv133 = aa__n17
						if !(ccv133 != 0) {
							break
						}
					}
				}
				ccv137 = aa__dst17
				aa__dst17 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv137)) = aa__x17
			}
			if aann > aadn {
				if aann-aadn != 0 {
					aa__n18 = aann - aadn - ppint32(1)
					aa__dst18 = aaqp
					aa__src18 = aajunkp
					ccv138 = aa__src18
					aa__src18 += 4
					aa__x18 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv138))
					if aa__n18 != 0 {
						for {
							ccv141 = aa__dst18
							aa__dst18 += 4
							*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv141)) = aa__x18
							ccv142 = aa__src18
							aa__src18 += 4
							aa__x18 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv142))
							goto cg_140
						cg_140:
							;
							aa__n18--
							ccv139 = aa__n18
							if !(ccv139 != 0) {
								break
							}
						}
					}
					ccv143 = aa__dst18
					aa__dst18 += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv143)) = aa__x18
				}
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_dcpi1_div_q(cgtls, aaqp, aarp, aann, aadnp, aadn, cgbp+72)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(372), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn+ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(372), "qp[nn - dn + 1] == qran1\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aarran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(373), "rp[-1] == rran0\x00")
			}
			sicheck_one(cgtls, aaqp, iqlibc.ppUintptrFromInt32(0), aanp, aann, aadnp, aadn, "mpn_dcpi1_div_q\x00", ppuint32(0))
		}

		/* Test mpn_mu_div_qr */
		if aann-aadn > ppint32(2) && aadn >= ppint32(2) {

			aaitch = X__gmpn_mu_div_qr_itch(cgtls, aann, aadn, 0)
			if aaitch+ppint32(1) > aaalloc {

				aascratch = (*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_reallocate_func})))(cgtls, aascratch, ppuint32(iqlibc.ppUint32FromInt32(aaalloc)*ppuint32(4)), ppuint32(iqlibc.ppUint32FromInt32(aaitch+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
				aaalloc = aaitch + ppint32(1)
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)) = aaran
			if aann-aadn != 0 {
				aa__n19 = aann - aadn - ppint32(1)
				aa__dst19 = aaqp
				aa__src19 = aajunkp
				ccv144 = aa__src19
				aa__src19 += 4
				aa__x19 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv144))
				if aa__n19 != 0 {
					for {
						ccv147 = aa__dst19
						aa__dst19 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv147)) = aa__x19
						ccv148 = aa__src19
						aa__src19 += 4
						aa__x19 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv148))
						goto cg_146
					cg_146:
						;
						aa__n19--
						ccv145 = aa__n19
						if !(ccv145 != 0) {
							break
						}
					}
				}
				ccv149 = aa__dst19
				aa__dst19 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv149)) = aa__x19
			}
			if aadn != 0 {
				aa__dst20 = aarp
				aa__n20 = aadn
				for {
					ccv152 = aa__dst20
					aa__dst20 += 4
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv152)) = iqlibc.ppUint32FromInt32(0)
					goto cg_151
				cg_151:
					;
					aa__n20--
					ccv150 = aa__n20
					if !(ccv150 != 0) {
						break
					}
				}
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aadn)*4)) = aarran1
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_mu_div_qr(cgtls, aaqp, aarp, aanp, aann, aadnp, aadn, aascratch)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaran == *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(391), "ran == scratch[itch]\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(392), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn+ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(392), "qp[nn - dn + 1] == qran1\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aarran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(393), "rp[-1] == rran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp + ppuintptr(aadn)*4)) == aarran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(393), "rp[dn] == rran1\x00")
			}
			sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadnp, aadn, "mpn_mu_div_qr\x00", ppuint32(0))
		}

		/* Test mpn_mu_divappr_q */
		if aann-aadn > ppint32(2) && aadn >= ppint32(2) {

			aaitch = X__gmpn_mu_divappr_q_itch(cgtls, aann, aadn, 0)
			if aaitch+ppint32(1) > aaalloc {

				aascratch = (*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_reallocate_func})))(cgtls, aascratch, ppuint32(iqlibc.ppUint32FromInt32(aaalloc)*ppuint32(4)), ppuint32(iqlibc.ppUint32FromInt32(aaitch+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
				aaalloc = aaitch + ppint32(1)
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)) = aaran
			if aann-aadn != 0 {
				aa__n21 = aann - aadn - ppint32(1)
				aa__dst21 = aaqp
				aa__src20 = aajunkp
				ccv153 = aa__src20
				aa__src20 += 4
				aa__x20 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv153))
				if aa__n21 != 0 {
					for {
						ccv156 = aa__dst21
						aa__dst21 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv156)) = aa__x20
						ccv157 = aa__src20
						aa__src20 += 4
						aa__x20 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv157))
						goto cg_155
					cg_155:
						;
						aa__n21--
						ccv154 = aa__n21
						if !(ccv154 != 0) {
							break
						}
					}
				}
				ccv158 = aa__dst21
				aa__dst21 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv158)) = aa__x20
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_mu_divappr_q(cgtls, aaqp, aanp, aann, aadnp, aadn, aascratch)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaran == *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(409), "ran == scratch[itch]\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(410), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn+ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(410), "qp[nn - dn + 1] == qran1\x00")
			}
			sicheck_one(cgtls, aaqp, iqlibc.ppUintptrFromInt32(0), aanp, aann, aadnp, aadn, "mpn_mu_divappr_q\x00", ppuint32(4))
		}

		/* Test mpn_mu_div_q */
		if aann-aadn > ppint32(2) && aadn >= ppint32(2) {

			aaitch = X__gmpn_mu_div_q_itch(cgtls, aann, aadn, 0)
			if aaitch+ppint32(1) > aaalloc {

				aascratch = (*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_reallocate_func})))(cgtls, aascratch, ppuint32(iqlibc.ppUint32FromInt32(aaalloc)*ppuint32(4)), ppuint32(iqlibc.ppUint32FromInt32(aaitch+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
				aaalloc = aaitch + ppint32(1)
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)) = aaran
			if aann-aadn != 0 {
				aa__n22 = aann - aadn - ppint32(1)
				aa__dst22 = aaqp
				aa__src21 = aajunkp
				ccv159 = aa__src21
				aa__src21 += 4
				aa__x21 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv159))
				if aa__n22 != 0 {
					for {
						ccv162 = aa__dst22
						aa__dst22 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv162)) = aa__x21
						ccv163 = aa__src21
						aa__src21 += 4
						aa__x21 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv163))
						goto cg_161
					cg_161:
						;
						aa__n22--
						ccv160 = aa__n22
						if !(ccv160 != 0) {
							break
						}
					}
				}
				ccv164 = aa__dst22
				aa__dst22 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv164)) = aa__x21
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn)*4)) = X__gmpn_mu_div_q(cgtls, aaqp, aanp, aann, aadnp, aadn, aascratch)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaran == *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(426), "ran == scratch[itch]\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(427), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn+ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(427), "qp[nn - dn + 1] == qran1\x00")
			}
			sicheck_one(cgtls, aaqp, iqlibc.ppUintptrFromInt32(0), aanp, aann, aadnp, aadn, "mpn_mu_div_q\x00", ppuint32(0))
		}

		if ppint32(1) != 0 {

			aaitch = aann + ppint32(1)
			if aaitch+ppint32(1) > aaalloc {

				aascratch = (*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_reallocate_func})))(cgtls, aascratch, ppuint32(iqlibc.ppUint32FromInt32(aaalloc)*ppuint32(4)), ppuint32(iqlibc.ppUint32FromInt32(aaitch+iqlibc.ppInt32FromInt32(1))*ppuint32(4)))
				aaalloc = aaitch + ppint32(1)
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)) = aaran
			if aann-aadn+ppint32(1) != 0 {
				aa__n23 = aann - aadn + ppint32(1) - ppint32(1)
				aa__dst23 = aaqp
				aa__src22 = aajunkp
				ccv165 = aa__src22
				aa__src22 += 4
				aa__x22 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv165))
				if aa__n23 != 0 {
					for {
						ccv168 = aa__dst23
						aa__dst23 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv168)) = aa__x22
						ccv169 = aa__src22
						aa__src22 += 4
						aa__x22 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv169))
						goto cg_167
					cg_167:
						;
						aa__n23--
						ccv166 = aa__n23
						if !(ccv166 != 0) {
							break
						}
					}
				}
				ccv170 = aa__dst23
				aa__dst23 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv170)) = aa__x22
			}
			X__gmpn_div_q(cgtls, aaqp, aanp, aann, aadup, aadn, aascratch)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(aaran == *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*4)))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(442), "ran == scratch[itch]\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(443), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-aadn+ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(443), "qp[nn - dn + 1] == qran1\x00")
			}
			sicheck_one(cgtls, aaqp, iqlibc.ppUintptrFromInt32(0), aanp, aann, aadup, aadn, "mpn_div_q\x00", ppuint32(0))
		}

		if aadn >= ppint32(2) && aann >= ppint32(2) {

			/* mpn_divrem_2 */
			if aann != 0 {
				aa__n24 = aann - ppint32(1)
				aa__dst24 = aarp
				aa__src23 = aanp
				ccv171 = aa__src23
				aa__src23 += 4
				aa__x23 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv171))
				if aa__n24 != 0 {
					for {
						ccv174 = aa__dst24
						aa__dst24 += 4
						*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv174)) = aa__x23
						ccv175 = aa__src23
						aa__src23 += 4
						aa__x23 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv175))
						goto cg_173
					cg_173:
						;
						aa__n24--
						ccv172 = aa__n24
						if !(ccv172 != 0) {
							break
						}
					}
				}
				ccv176 = aa__dst24
				aa__dst24 += 4
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv176)) = aa__x23
			}
			ccv177 = aaqran1
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(1))*4)) = ccv177
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(2))*4)) = ccv177

			aaqh = X__gmpn_divrem_2(cgtls, aaqp, 0, aarp, aann, aadnp+ppuintptr(aadn)*4-ppuintptr(2)*4)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(2))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(456), "qp[nn - 2] == qran1\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(457), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(457), "qp[nn - 1] == qran1\x00")
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(2))*4)) = aaqh
			sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadnp+ppuintptr(aadn)*4-ppuintptr(2)*4, ppint32(2), "mpn_divrem_2\x00", ppuint32(0))

			/* Missing: divrem_2 with fraction limbs. */

			/* mpn_div_qr_2 */
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(2))*4)) = aaqran1

			aaqh = X__gmpn_div_qr_2(cgtls, aaqp, aarp, aanp, aann, aadup+ppuintptr(aadn)*4-ppuintptr(2)*4)
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(2))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(467), "qp[nn - 2] == qran1\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(468), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(468), "qp[nn - 1] == qran1\x00")
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(2))*4)) = aaqh
			sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadup+ppuintptr(aadn)*4-ppuintptr(2)*4, ppint32(2), "mpn_div_qr_2\x00", ppuint32(0))
		}
		if aadn >= ppint32(1) && aann >= ppint32(1) {

			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(1))*4)) = aaqran1
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aarp)) = X__gmpn_div_qr_1(cgtls, aaqp, cgbp+84, aanp, aann, *(*tnmp_limb_t)(iqunsafe.ppPointer(aadnp + ppuintptr(aadn-ppint32(1))*4)))
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*4)) == aaqran0)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(478), "qp[-1] == qran0\x00")
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(1))*4)) == aaqran1)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "t-div.c\x00", ppint32(478), "qp[nn - 1] == qran1\x00")
			}
			*(*tnmp_limb_t)(iqunsafe.ppPointer(aaqp + ppuintptr(aann-ppint32(1))*4)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 84))
			sicheck_one(cgtls, aaqp, aarp, aanp, aann, aadnp+ppuintptr(aadn)*4-ppuintptr(1)*4, ppint32(1), "mpn_div_qr_1\x00", ppuint32(0))
		}

		goto cg_6
	cg_6:
		;
		sitest++
	}

	(*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t))(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_free_func})))(cgtls, aascratch, ppuint32(iqlibc.ppUint32FromInt32(aaalloc)*ppuint32(4)))

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt32(iqlibc.ppBoolInt32(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 76)) != ppuintptr(0)) != 0), 0) != 0 {
		X__gmp_tmp_reentrant_free(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 76)))
	}

	X__gmpz_clear(cgtls, cgbp)
	X__gmpz_clear(cgtls, cgbp+12)
	X__gmpz_clear(cgtls, cgbp+24)
	X__gmpz_clear(cgtls, cgbp+36)
	X__gmpz_clear(cgtls, cgbp+48)
	X__gmpz_clear(cgtls, cgbp+60)

	Xtests_end(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_alloca(*iqlibc.ppTLS, ppuint32) ppuintptr

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

var ___gmp_allocate_func ppuintptr

func ___gmp_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

var ___gmp_free_func ppuintptr

func ___gmp_randinit_mt_noseed(*iqlibc.ppTLS, ppuintptr)

var ___gmp_rands [1]tn__gmp_randstate_struct

var ___gmp_rands_initialized ppuint8

var ___gmp_reallocate_func ppuintptr

func ___gmp_tmp_reentrant_alloc(*iqlibc.ppTLS, ppuintptr, ppuint32) ppuintptr

func ___gmp_tmp_reentrant_free(*iqlibc.ppTLS, ppuintptr)

func ___gmpn_dcpi1_div_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpn_dcpi1_div_qr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpn_dcpi1_divappr_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpn_div_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr)

func ___gmpn_div_qr_1(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32, ppuint32) ppuint32

func ___gmpn_div_qr_2(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpn_divrem_2(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpn_mu_div_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpn_mu_div_q_itch(*iqlibc.ppTLS, ppint32, ppint32, ppint32) ppint32

func ___gmpn_mu_div_qr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpn_mu_div_qr_itch(*iqlibc.ppTLS, ppint32, ppint32, ppint32) ppint32

func ___gmpn_mu_divappr_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpn_mu_divappr_q_itch(*iqlibc.ppTLS, ppint32, ppint32, ppint32) ppint32

func ___gmpn_sbpi1_div_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuint32) ppuint32

func ___gmpn_sbpi1_div_qr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuint32) ppuint32

func ___gmpn_sbpi1_divappr_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuint32) ppuint32

func ___gmpn_sec_div_qr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpn_sec_div_qr_itch(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___gmpn_sec_div_r(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr, ppint32, ppuintptr)

func ___gmpn_sec_div_r_itch(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___gmpz_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_rrandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32)

func ___gmpz_sizeinbase(*iqlibc.ppTLS, ppuintptr, ppint32) ppuint32

func ___gmpz_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32)

func _abort(*iqlibc.ppTLS)

func _exit(*iqlibc.ppTLS, ppint32)

func _fprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _puts(*iqlibc.ppTLS, ppuintptr) ppint32

func _refmpn_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32)

func _refmpn_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuintptr, ppint32) ppuint32

func _refmpn_sub_n(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppuint32

var _stderr ppuintptr

func _strtod(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppfloat64

func _strtol(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _tests_end(*iqlibc.ppTLS)

func _tests_start(*iqlibc.ppTLS)

func _urandom(*iqlibc.ppTLS) ppuint32

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
