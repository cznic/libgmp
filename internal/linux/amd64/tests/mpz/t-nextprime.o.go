// Code generated for linux/amd64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -ignore-unsupported-alignment -DHAVE_CONFIG_H -I. -I../.. -I../.. -I../../tests -DNDEBUG -mlong-double-64 -c -o t-nextprime.o.go t-nextprime.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvASSERT_FILE = "__FILE__"
const mvASSERT_LINE = "__LINE__"
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBINV_NEWTON_THRESHOLD = 300
const mvBMOD_1_TO_MOD_1_THRESHOLD = 10
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDC_BDIV_Q_THRESHOLD = 180
const mvDC_DIVAPPR_Q_THRESHOLD = 200
const mvDELAYTIMER_MAX = 0x7fffffff
const mvDIVEXACT_1_THRESHOLD = 0
const mvDIVEXACT_BY3_METHOD = 0
const mvDIVEXACT_JEB_THRESHOLD = 25
const mvDOPRNT_CONV_FIXED = 1
const mvDOPRNT_CONV_GENERAL = 3
const mvDOPRNT_CONV_SCIENTIFIC = 2
const mvDOPRNT_JUSTIFY_INTERNAL = 3
const mvDOPRNT_JUSTIFY_LEFT = 1
const mvDOPRNT_JUSTIFY_NONE = 0
const mvDOPRNT_JUSTIFY_RIGHT = 2
const mvDOPRNT_SHOWBASE_NO = 2
const mvDOPRNT_SHOWBASE_NONZERO = 3
const mvDOPRNT_SHOWBASE_YES = 1
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFAC_DSC_THRESHOLD = 400
const mvFAC_ODD_THRESHOLD = 35
const mvFFT_FIRST_K = 4
const mvFIB_TABLE_LIMIT = 93
const mvFIB_TABLE_LUCNUM_LIMIT = 92
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFOPEN_MAX = 1000
const mvGCDEXT_DC_THRESHOLD = 600
const mvGCD_DC_THRESHOLD = 1000
const mvGET_STR_DC_THRESHOLD = 18
const mvGET_STR_PRECOMPUTE_THRESHOLD = 35
const mvGMP_LIMB_BITS = 64
const mvGMP_LIMB_BYTES = "SIZEOF_MP_LIMB_T"
const mvGMP_MPARAM_H_SUGGEST = "./mpn/generic/gmp-mparam.h"
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_ALARM = 1
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_CONST = 1
const mvHAVE_ATTRIBUTE_MALLOC = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_ATTRIBUTE_NORETURN = 1
const mvHAVE_CLOCK = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_CONFIG_H = 1
const mvHAVE_DECL_FGETC = 1
const mvHAVE_DECL_FSCANF = 1
const mvHAVE_DECL_OPTARG = 1
const mvHAVE_DECL_SYS_ERRLIST = 0
const mvHAVE_DECL_SYS_NERR = 0
const mvHAVE_DECL_UNGETC = 1
const mvHAVE_DECL_VFPRINTF = 1
const mvHAVE_DLFCN_H = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FCNTL_H = 1
const mvHAVE_FLOAT_H = 1
const mvHAVE_GETPAGESIZE = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_HIDDEN_ALIAS = 1
const mvHAVE_HOST_CPU_FAMILY_x86_64 = 1
const mvHAVE_HOST_CPU_goldmont = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTPTR_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LANGINFO_H = 1
const mvHAVE_LIMB_LITTLE_ENDIAN = 1
const mvHAVE_LOCALECONV = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_DOUBLE = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_MEMORY_H = 1
const mvHAVE_MEMSET = 1
const mvHAVE_MMAP = 1
const mvHAVE_MPROTECT = 1
const mvHAVE_NL_LANGINFO = 1
const mvHAVE_NL_TYPES_H = 1
const mvHAVE_POPEN = 1
const mvHAVE_PTRDIFF_T = 1
const mvHAVE_QUAD_T = 1
const mvHAVE_RAISE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGALTSTACK = 1
const mvHAVE_SIGSTACK = 1
const mvHAVE_SPEED_CYCLECOUNTER = 2
const mvHAVE_STACK_T = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDLIB_H = 1
const mvHAVE_STRCHR = 1
const mvHAVE_STRERROR = 1
const mvHAVE_STRINGS_H = 1
const mvHAVE_STRING_H = 1
const mvHAVE_STRNLEN = 1
const mvHAVE_STRTOL = 1
const mvHAVE_STRTOUL = 1
const mvHAVE_SYSCONF = 1
const mvHAVE_SYS_MMAN_H = 1
const mvHAVE_SYS_PARAM_H = 1
const mvHAVE_SYS_RESOURCE_H = 1
const mvHAVE_SYS_STAT_H = 1
const mvHAVE_SYS_SYSINFO_H = 1
const mvHAVE_SYS_TIMES_H = 1
const mvHAVE_SYS_TIME_H = 1
const mvHAVE_SYS_TYPES_H = 1
const mvHAVE_TIMES = 1
const mvHAVE_UINT_LEAST32_T = 1
const mvHAVE_UNISTD_H = 1
const mvHAVE_VSNPRINTF = 1
const mvHGCD_APPR_THRESHOLD = 400
const mvHGCD_REDUCE_THRESHOLD = 1000
const mvHGCD_THRESHOLD = 400
const mvHOST_NAME_MAX = 255
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT64_MAX"
const mvINTPTR_MIN = "INT64_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_HIGHBIT = "INT_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvINV_APPR_THRESHOLD = "INV_NEWTON_THRESHOLD"
const mvINV_NEWTON_THRESHOLD = 200
const mvIOV_MAX = 1024
const mvLIMBS_PER_ULONG = 1
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_HIGHBIT = "LONG_MIN"
const mvLONG_MAX = "__LONG_MAX"
const mvLSYM_PREFIX = ".L"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMATRIX22_STRASSEN_THRESHOLD = 30
const mvMB_LEN_MAX = 4
const mvMOD_BKNP1_ONLY3 = 0
const mvMPN_FFT_TABLE_SIZE = 16
const mvMPN_TOOM22_MUL_MINSIZE = 6
const mvMPN_TOOM2_SQR_MINSIZE = 4
const mvMPN_TOOM32_MUL_MINSIZE = 10
const mvMPN_TOOM33_MUL_MINSIZE = 17
const mvMPN_TOOM3_SQR_MINSIZE = 17
const mvMPN_TOOM42_MULMID_MINSIZE = 4
const mvMPN_TOOM42_MUL_MINSIZE = 10
const mvMPN_TOOM43_MUL_MINSIZE = 25
const mvMPN_TOOM44_MUL_MINSIZE = 30
const mvMPN_TOOM4_SQR_MINSIZE = 30
const mvMPN_TOOM53_MUL_MINSIZE = 17
const mvMPN_TOOM54_MUL_MINSIZE = 31
const mvMPN_TOOM63_MUL_MINSIZE = 49
const mvMPN_TOOM6H_MUL_MINSIZE = 46
const mvMPN_TOOM6_SQR_MINSIZE = 46
const mvMPN_TOOM8H_MUL_MINSIZE = 86
const mvMPN_TOOM8_SQR_MINSIZE = 86
const mvMP_BASES_BIG_BASE_CTZ_10 = 19
const mvMP_BASES_CHARS_PER_LIMB_10 = 19
const mvMP_BASES_NORMALIZATION_STEPS_10 = 0
const mvMP_EXP_T_MAX = "MP_SIZE_T_MAX"
const mvMP_EXP_T_MIN = "MP_SIZE_T_MIN"
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMULLO_BASECASE_THRESHOLD = 0
const mvMULLO_BASECASE_THRESHOLD_LIMIT = "MULLO_BASECASE_THRESHOLD"
const mvMULMID_TOOM42_THRESHOLD = "MUL_TOOM22_THRESHOLD"
const mvMULMOD_BNM1_THRESHOLD = 16
const mvMUL_TOOM22_THRESHOLD = 30
const mvMUL_TOOM22_THRESHOLD_LIMIT = "MUL_TOOM22_THRESHOLD"
const mvMUL_TOOM32_TO_TOOM43_THRESHOLD = 100
const mvMUL_TOOM32_TO_TOOM53_THRESHOLD = 110
const mvMUL_TOOM33_THRESHOLD = 100
const mvMUL_TOOM33_THRESHOLD_LIMIT = "MUL_TOOM33_THRESHOLD"
const mvMUL_TOOM42_TO_TOOM53_THRESHOLD = 100
const mvMUL_TOOM42_TO_TOOM63_THRESHOLD = 110
const mvMUL_TOOM43_TO_TOOM54_THRESHOLD = 150
const mvMUL_TOOM44_THRESHOLD = 300
const mvMUL_TOOM6H_THRESHOLD = 350
const mvMUL_TOOM8H_THRESHOLD = 450
const mvMUPI_DIV_QR_THRESHOLD = 200
const mvMU_BDIV_QR_THRESHOLD = 2000
const mvMU_BDIV_Q_THRESHOLD = 2000
const mvMU_DIVAPPR_Q_THRESHOLD = 2000
const mvMU_DIV_QR_THRESHOLD = 2000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvODD_CENTRAL_BINOMIAL_OFFSET = 13
const mvODD_CENTRAL_BINOMIAL_TABLE_LIMIT = 35
const mvODD_DOUBLEFACTORIAL_TABLE_LIMIT = 33
const mvODD_FACTORIAL_EXTTABLE_LIMIT = 67
const mvODD_FACTORIAL_TABLE_LIMIT = 25
const mvPACKAGE = "gmp"
const mvPACKAGE_BUGREPORT = "gmp-bugs@gmplib.org (see https://gmplib.org/manual/Reporting-Bugs.html)"
const mvPACKAGE_NAME = "GNU MP"
const mvPACKAGE_STRING = "GNU MP 6.3.0"
const mvPACKAGE_TARNAME = "gmp"
const mvPACKAGE_URL = "http://www.gnu.org/software/gmp/"
const mvPACKAGE_VERSION = "6.3.0"
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPP_FIRST_OMITTED = 59
const mvPREINV_MOD_1_TO_MOD_1_THRESHOLD = 10
const mvPRIMESIEVE_HIGHEST_PRIME = 5351
const mvPRIMESIEVE_NUMBEROF_TABLE = 28
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT64_MAX"
const mvPTRDIFF_MIN = "INT64_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvREDC_1_TO_REDC_N_THRESHOLD = 100
const mvRETSIGTYPE = "void"
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSET_STR_DC_THRESHOLD = 750
const mvSET_STR_PRECOMPUTE_THRESHOLD = 2000
const mvSHRT_HIGHBIT = "SHRT_MIN"
const mvSHRT_MAX = 0x7fff
const mvSIEVESIZE = 512
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZEOF_MP_LIMB_T = 8
const mvSIZEOF_UNSIGNED = 4
const mvSIZEOF_UNSIGNED_LONG = 8
const mvSIZEOF_UNSIGNED_SHORT = 2
const mvSIZEOF_VOID_P = 8
const mvSIZE_MAX = "UINT64_MAX"
const mvSQRLO_BASECASE_THRESHOLD = 0
const mvSQRLO_BASECASE_THRESHOLD_LIMIT = "SQRLO_BASECASE_THRESHOLD"
const mvSQRLO_DC_THRESHOLD = "MULLO_DC_THRESHOLD"
const mvSQRLO_DC_THRESHOLD_LIMIT = "SQRLO_DC_THRESHOLD"
const mvSQRLO_SQR_THRESHOLD = "MULLO_MUL_N_THRESHOLD"
const mvSQRMOD_BNM1_THRESHOLD = 16
const mvSQR_BASECASE_THRESHOLD = 0
const mvSQR_TOOM2_THRESHOLD = 50
const mvSQR_TOOM3_THRESHOLD = 120
const mvSQR_TOOM3_THRESHOLD_LIMIT = "SQR_TOOM3_THRESHOLD"
const mvSQR_TOOM4_THRESHOLD = 400
const mvSQR_TOOM6_THRESHOLD = "MUL_TOOM6H_THRESHOLD"
const mvSQR_TOOM8_THRESHOLD = "MUL_TOOM8H_THRESHOLD"
const mvSSIZE_MAX = "LONG_MAX"
const mvSTDC_HEADERS = 1
const mvSYMLOOP_MAX = 40
const mvTABLE_LIMIT_2N_MINUS_POPC_2N = 81
const mvTARGET_REGISTER_STARVED = 0
const mvTIME_WITH_SYS_TIME = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTUNE_SQR_TOOM2_MAX = "SQR_TOOM2_MAX_GENERIC"
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT64_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSE_LEADING_REGPARM = 0
const mvUSE_PREINV_DIVREM_1 = 1
const mvUSHRT_MAX = 65535
const mvVERSION = "6.3.0"
const mvWANT_FFT = 1
const mvWANT_TMP_ALLOCA = 1
const mvWANT_TMP_DEBUG = 0
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_LIMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_IEEE_FLOATS = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 2
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__FXSR__ = 1
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG -mlong-double-64"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 0x7fffffffffffffff
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MMX_WITH_SSE__ = 1
const mv__MMX__ = 1
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "l"
const mv__PRIPTR = "l"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 16
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__SSE2_MATH__ = 1
const mv__SSE2__ = 1
const mv__SSE_MATH__ = 1
const mv__SSE__ = 1
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__amd64 = 1
const mv__amd64__ = 1
const mv__code_model_small__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__k8 = 1
const mv__k8__ = 1
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv__x86_64 = 1
const mv__x86_64__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_newalloc = "_mpz_realloc"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvbinvert_limb_table = "__gmp_binvert_limb_table"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_init_primesieve = "__gmp_init_primesieve"
const mvgmp_nextprime = "__gmp_nextprime"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_primesieve = "__gmp_primesieve"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvjacobi_table = "__gmp_jacobi_table"
const mvlinux = 1
const mvmodlimb_invert = "binvert_limb"
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpn_fft_mul = "mpn_nussbaumer_mul"
const mvmpn_get_d = "__gmpn_get_d"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_gcd = "__gmpz_divexact_gcd"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_inp_str_nowhite = "__gmpz_inp_str_nowhite"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucas_mod = "__gmpz_lucas_mod"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_n_pow_ui = "__gmpz_n_pow_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_oddfac_1 = "__gmpz_oddfac_1"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_prodlimbs = "__gmpz_prodlimbs"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_stronglucas = "__gmpz_stronglucas"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvrestrict = "__restrict"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

type tnuintptr_t = ppuint64

type tnintptr_t = ppint64

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tngmp_uint_least32_t = ppuint32

type tngmp_intptr_t = ppint64

type tngmp_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tngmp_pi2_t = struct {
	fdinv21 tnmp_limb_t
	fdinv32 tnmp_limb_t
	fdinv53 tnmp_limb_t
}

type tutmp_align_t = struct {
	fdd [0]ppfloat64
	fdp [0]ppuintptr
	fdl tnmp_limb_t
}

type tstmp_reentrant_t = struct {
	fdnext ppuintptr
	fdsize tnsize_t
}

type tngmp_randfnptr_t = struct {
	fdrandseed_fn  ppuintptr
	fdrandget_fn   ppuintptr
	fdrandclear_fn ppuintptr
	fdrandiset_fn  ppuintptr
}

type tetoom6_flags = ppint32

const ectoom6_all_pos = 0
const ectoom6_vm1_neg = 1
const ectoom6_vm2_neg = 2

type tetoom7_flags = ppint32

const ectoom7_w1_neg = 1
const ectoom7_w3_neg = 2

type tngmp_primesieve_t = struct {
	fdd       ppuint64
	fds0      ppuint64
	fdsqrt_s0 ppuint64
	fds       [513]ppuint8
}

type tsfft_table_nk = struct {
	fd__ccgo0 uint32
}

type tsbases = struct {
	fdchars_per_limb    ppint32
	fdlogb2             tnmp_limb_t
	fdlog2b             tnmp_limb_t
	fdbig_base          tnmp_limb_t
	fdbig_base_inverted tnmp_limb_t
}

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tuieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type tnmp_double_limb_t = struct {
	fdd0 tnmp_limb_t
	fdd1 tnmp_limb_t
}

type tshgcd_matrix1 = struct {
	fdu [2][2]tnmp_limb_t
}

type tshgcd_matrix = struct {
	fdalloc tnmp_size_t
	fdn     tnmp_size_t
	fdp     [2][2]tnmp_ptr
}

type tsgcdext_ctx = struct {
	fdgp    tnmp_ptr
	fdgn    tnmp_size_t
	fdup    tnmp_ptr
	fdusize ppuintptr
	fdun    tnmp_size_t
	fdu0    tnmp_ptr
	fdu1    tnmp_ptr
	fdtp    tnmp_ptr
}

type tspowers = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tnpowers_t = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tsdoprnt_params_t = struct {
	fdbase         ppint32
	fdconv         ppint32
	fdexpfmt       ppuintptr
	fdexptimes4    ppint32
	fdfill         ppint8
	fdjustify      ppint32
	fdprec         ppint32
	fdshowbase     ppint32
	fdshowpoint    ppint32
	fdshowtrailing ppint32
	fdsign         ppint8
	fdwidth        ppint32
}

type tngmp_doscan_scan_t = ppuintptr

type tngmp_doscan_step_t = ppuintptr

type tngmp_doscan_get_t = ppuintptr

type tngmp_doscan_unget_t = ppuintptr

type tsgmp_doscan_funs_t = struct {
	fdscan  tngmp_doscan_scan_t
	fdstep  tngmp_doscan_step_t
	fdget   tngmp_doscan_get_t
	fdunget tngmp_doscan_unget_t
}

type tn__jmp_buf = [8]ppuint64

type tnjmp_buf = [1]ts__jmp_buf_tag

type ts__jmp_buf_tag = struct {
	fd__jb tn__jmp_buf
	fd__fl ppuint64
	fd__ss [16]ppuint64
}

type tnsigjmp_buf = [1]ts__jmp_buf_tag

/* Establish ostringstream and istringstream.  Do this here so as to hide
   the conditionals, rather than putting stuff in each test program.

   Oldish versions of g++, like 2.95.2, don't have <sstream>, only
   <strstream>.  Fake up ostringstream and istringstream classes, but not a
   full implementation, just enough for our purposes.  */

func Xrefmpz_nextprime(cgtls *iqlibc.ppTLS, aap tnmpz_ptr, aat tnmpz_srcptr) {

	X__gmpz_add_ui(cgtls, aap, aat, ppuint64(1))
	for !(X__gmpz_probab_prime_p(cgtls, aap, ppint32(10)) != 0) {
		X__gmpz_add_ui(cgtls, aap, aap, ppuint64(1))
	}
}

func Xrefmpz_prevprime(cgtls *iqlibc.ppTLS, aap tnmpz_ptr, aat tnmpz_srcptr) {

	if X__gmpz_cmp_ui(cgtls, aat, ppuint64(2)) <= 0 {
		return
	}

	X__gmpz_sub_ui(cgtls, aap, aat, ppuint64(1))
	for !(X__gmpz_probab_prime_p(cgtls, aap, ppint32(10)) != 0) {
		X__gmpz_sub_ui(cgtls, aap, aap, ppuint64(1))
	}
}

func Xtest_largegap(cgtls *iqlibc.ppTLS, aalow ppuintptr, aagap ppint32) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var pp_ /* nxt at bp+16 */ tnmpz_t
	var pp_ /* t at bp+0 */ tnmpz_t
	X__gmpz_init(cgtls, cgbp)
	X__gmpz_init(cgtls, cgbp+16)

	X__gmpz_nextprime(cgtls, cgbp+16, aalow)
	X__gmpz_sub(cgtls, cgbp, cgbp+16, aalow)

	if X__gmpz_cmp_ui(cgtls, cgbp, iqlibc.ppUint64FromInt32(aagap)) != 0 {

		X__gmp_printf(cgtls, "nextprime gap %Zd => %Zd != %d\n\x00", iqlibc.ppVaList(cgbp+40, aalow, cgbp+16, aagap))
		Xabort(cgtls)
	}

	X__gmpz_prevprime(cgtls, cgbp, cgbp+16)
	if X__gmpz_cmp(cgtls, cgbp, aalow) != 0 {

		X__gmp_printf(cgtls, "prevprime gap %Zd => %Zd != %d\n\x00", iqlibc.ppVaList(cgbp+40, cgbp+16, cgbp, aagap))
		Xabort(cgtls)
	}

	X__gmpz_clear(cgtls, cgbp)
	X__gmpz_clear(cgtls, cgbp+16)
}

func Xtest_largegaps(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var pp_ /* n at bp+0 */ tnmpz_t

	X__gmpz_init(cgtls, cgbp)

	// largest gap with start < 2^32.
	X__gmpz_set_str(cgtls, cgbp, "3842610773\x00", ppint32(10))
	Xtest_largegap(cgtls, cgbp, ppint32(336))

	// largest gap with start < 2^64.
	X__gmpz_set_str(cgtls, cgbp, "18361375334787046697\x00", ppint32(10))
	Xtest_largegap(cgtls, cgbp, ppint32(1550))

	// test high merit primegap in the P30 digit range.
	X__gmpz_set_str(cgtls, cgbp, "3001549619028223830552751967\x00", ppint32(10))
	Xtest_largegap(cgtls, cgbp, ppint32(2184))

	// test high merit primegap in the P100 range.
	X__gmpz_primorial_ui(cgtls, cgbp, ppuint64(257))
	X__gmpz_divexact_ui(cgtls, cgbp, cgbp, ppuint64(5610))
	X__gmpz_mul_ui(cgtls, cgbp, cgbp, ppuint64(4280516017))
	X__gmpz_sub_ui(cgtls, cgbp, cgbp, ppuint64(2560))
	Xtest_largegap(cgtls, cgbp, ppint32(9006))

	// test high merit primegap in the P200 range.
	X__gmpz_primorial_ui(cgtls, cgbp, ppuint64(409))
	X__gmpz_divexact_ui(cgtls, cgbp, cgbp, ppuint64(30))
	X__gmpz_mul_ui(cgtls, cgbp, cgbp, ppuint64(3483347771))
	X__gmpz_sub_ui(cgtls, cgbp, cgbp, ppuint64(7016))
	Xtest_largegap(cgtls, cgbp, ppint32(15900))

	X__gmpz_clear(cgtls, cgbp)
}

func Xtest_bitboundaries(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var pp_ /* n at bp+0 */ tnmpz_t
	X__gmpz_init(cgtls, cgbp)

	X__gmpz_set_str(cgtls, cgbp, "0xfff1\x00", 0)
	Xtest_largegap(cgtls, cgbp, ppint32(16))

	X__gmpz_set_str(cgtls, cgbp, "0xfffffffb\x00", 0)
	Xtest_largegap(cgtls, cgbp, ppint32(20))

	X__gmpz_set_str(cgtls, cgbp, "0xffffffffffc5\x00", 0)
	Xtest_largegap(cgtls, cgbp, ppint32(80))

	X__gmpz_set_str(cgtls, cgbp, "0xffffffffffffffc5\x00", 0)
	Xtest_largegap(cgtls, cgbp, ppint32(72))

	X__gmpz_set_str(cgtls, cgbp, "0xffffffffffffffffffbf\x00", 0)
	Xtest_largegap(cgtls, cgbp, ppint32(78))

	X__gmpz_set_str(cgtls, cgbp, "0xffffffffffffffffffffffef\x00", 0)
	Xtest_largegap(cgtls, cgbp, ppint32(78))

	X__gmpz_set_str(cgtls, cgbp, "0xffffffffffffffffffffffffffb5\x00", 0)
	Xtest_largegap(cgtls, cgbp, ppint32(100))

	X__gmpz_set_str(cgtls, cgbp, "0xffffffffffffffffffffffffffffff61\x00", 0)
	Xtest_largegap(cgtls, cgbp, ppint32(210))

	X__gmpz_set_str(cgtls, cgbp, "0xffffffffffffffffffffffffffffffffffffffffffffff13\x00", 0)
	Xtest_largegap(cgtls, cgbp, ppint32(370))

	X__gmpz_clear(cgtls, cgbp)
}

func Xrun(cgtls *iqlibc.ppTLS, aastart ppuintptr, aareps ppint32, aaend ppuintptr, aadiffs ppuintptr) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aa__gmp_l tnmp_limb_t
	var aa__gmp_n tnmp_size_t
	var aa__gmp_p tnmp_ptr
	var aai ppint32
	var ccv2 tnmpz_srcptr
	var ccv3, ccv5 ppuint64
	var ccv6, ccv7 ppbool
	var pp_ /* x at bp+0 */ tnmpz_t
	var pp_ /* y at bp+16 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__gmp_l, aa__gmp_n, aa__gmp_p, aai, ccv2, ccv3, ccv5, ccv6, ccv7

	X__gmpz_init_set_str(cgtls, cgbp, aastart, 0)
	X__gmpz_init(cgtls, cgbp+16)

	aai = 0
	for {
		if !(aai < aareps) {
			break
		}

		X__gmpz_nextprime(cgtls, cgbp+16, cgbp)
		X__gmpz_sub(cgtls, cgbp, cgbp+16, cgbp)

		if ccv7 = aadiffs != iqlibc.ppUintptrFromInt32(0); ccv7 {
			if ccv6 = !(X__gmpz_fits_sshort_p(cgtls, cgbp) != 0); !ccv6 {
				ccv2 = cgbp
				aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv2)).fd_mp_d
				aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv2)).fd_mp_size)
				aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
				if aa__gmp_n != 0 {
					ccv5 = aa__gmp_l
				} else {
					ccv5 = ppuint64(0)
				}
				ccv3 = ccv5
				goto cg_4
			cg_4:
			}
		}
		if ccv7 && (ccv6 || ppint32(*(*ppint16)(iqunsafe.ppPointer(aadiffs + ppuintptr(aai)*2))) != ppint32(iqlibc.ppInt16FromUint64(ccv3))) {

			X__gmp_printf(cgtls, "diff list discrepancy\n\x00", 0)
			Xabort(cgtls)
		}
		X__gmpz_swap(cgtls, cgbp, cgbp+16)

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpz_set_str(cgtls, cgbp+16, aaend, 0)

	if X__gmpz_cmp(cgtls, cgbp, cgbp+16) != 0 {

		X__gmp_printf(cgtls, "got  %Zd\n\x00", iqlibc.ppVaList(cgbp+40, cgbp))
		X__gmp_printf(cgtls, "want %Zd\n\x00", iqlibc.ppVaList(cgbp+40, cgbp+16))
		Xabort(cgtls)
	}

	X__gmpz_clear(cgtls, cgbp+16)
	X__gmpz_clear(cgtls, cgbp)
}

func Xrun_p(cgtls *iqlibc.ppTLS, aastart ppuintptr, aareps ppint32, aaend ppuintptr, aadiffs ppuintptr) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aa__gmp_l tnmp_limb_t
	var aa__gmp_n tnmp_size_t
	var aa__gmp_p tnmp_ptr
	var aai ppint32
	var ccv11, ccv3, ccv5, ccv9 ppuint64
	var ccv2, ccv8 tnmpz_srcptr
	var ccv6, ccv7 ppbool
	var pp_ /* x at bp+0 */ tnmpz_t
	var pp_ /* y at bp+16 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__gmp_l, aa__gmp_n, aa__gmp_p, aai, ccv11, ccv2, ccv3, ccv5, ccv6, ccv7, ccv8, ccv9

	X__gmpz_init_set_str(cgtls, cgbp, aaend, 0)
	X__gmpz_init(cgtls, cgbp+16)

	// Last rep doesn't share same data with nextprime
	aai = 0
	for {
		if !(aai < aareps-ppint32(1)) {
			break
		}

		X__gmpz_prevprime(cgtls, cgbp+16, cgbp)
		X__gmpz_sub(cgtls, cgbp, cgbp, cgbp+16)

		if ccv7 = aadiffs != iqlibc.ppUintptrFromInt32(0); ccv7 {
			if ccv6 = !(X__gmpz_fits_sshort_p(cgtls, cgbp) != 0); !ccv6 {
				ccv2 = cgbp
				aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv2)).fd_mp_d
				aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv2)).fd_mp_size)
				aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
				if aa__gmp_n != 0 {
					ccv5 = aa__gmp_l
				} else {
					ccv5 = ppuint64(0)
				}
				ccv3 = ccv5
				goto cg_4
			cg_4:
			}
		}
		if ccv7 && (ccv6 || ppint32(*(*ppint16)(iqunsafe.ppPointer(aadiffs + ppuintptr(aareps-aai-ppint32(1))*2))) != ppint32(iqlibc.ppInt16FromUint64(ccv3))) {

			ccv8 = cgbp
			aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv8)).fd_mp_d
			aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv8)).fd_mp_size)
			aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
			if aa__gmp_n != 0 {
				ccv11 = aa__gmp_l
			} else {
				ccv11 = ppuint64(0)
			}
			ccv9 = ccv11
			goto cg_10
		cg_10:
			X__gmp_printf(cgtls, "diff list discrepancy %Zd, %d vs %d\n\x00", iqlibc.ppVaList(cgbp+40, cgbp+16, ppint32(*(*ppint16)(iqunsafe.ppPointer(aadiffs + ppuintptr(aai)*2))), ccv9))
			Xabort(cgtls)
		}
		X__gmpz_swap(cgtls, cgbp, cgbp+16)

		goto cg_1
	cg_1:
		;
		aai++
	}

	// starts aren't always prime, so check that result is less than or equal
	X__gmpz_prevprime(cgtls, cgbp, cgbp)

	X__gmpz_set_str(cgtls, cgbp+16, aastart, 0)
	if X__gmpz_cmp(cgtls, cgbp, cgbp+16) > 0 {

		X__gmp_printf(cgtls, "got  %Zd\n\x00", iqlibc.ppVaList(cgbp+40, cgbp))
		X__gmp_printf(cgtls, "want %Zd\n\x00", iqlibc.ppVaList(cgbp+40, cgbp+16))
		Xabort(cgtls)
	}

	X__gmpz_clear(cgtls, cgbp+16)
	X__gmpz_clear(cgtls, cgbp)
}

func Xtest_ref(cgtls *iqlibc.ppTLS, aarands tngmp_randstate_ptr, aareps ppint32, aafunc ppuintptr, aaref_func ppuintptr) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aa__gmp_l tnmp_limb_t
	var aa__gmp_n tnmp_size_t
	var aa__gmp_p tnmp_ptr
	var aai ppint32
	var aasize_range, ccv3, ccv5, ccv7, ccv9 ppuint64
	var ccv2, ccv6 tnmpz_srcptr
	var pp_ /* bs at bp+0 */ tnmpz_t
	var pp_ /* ref_p at bp+48 */ tnmpz_t
	var pp_ /* test_p at bp+32 */ tnmpz_t
	var pp_ /* x at bp+16 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__gmp_l, aa__gmp_n, aa__gmp_p, aai, aasize_range, ccv2, ccv3, ccv5, ccv6, ccv7, ccv9

	X__gmpz_init(cgtls, cgbp)
	X__gmpz_init(cgtls, cgbp+16)
	X__gmpz_init(cgtls, cgbp+32)
	X__gmpz_init(cgtls, cgbp+48)

	aai = 0
	for {
		if !(aai < aareps) {
			break
		}

		X__gmpz_urandomb(cgtls, cgbp, aarands, ppuint64(32))
		ccv2 = cgbp
		aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv2)).fd_mp_d
		aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv2)).fd_mp_size)
		aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
		if aa__gmp_n != 0 {
			ccv5 = aa__gmp_l
		} else {
			ccv5 = ppuint64(0)
		}
		ccv3 = ccv5
		goto cg_4
	cg_4:
		aasize_range = ccv3%ppuint64(8) + ppuint64(2) /* 0..1024 bit operands */

		X__gmpz_urandomb(cgtls, cgbp, aarands, aasize_range)
		ccv6 = cgbp
		aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv6)).fd_mp_d
		aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv6)).fd_mp_size)
		aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
		if aa__gmp_n != 0 {
			ccv9 = aa__gmp_l
		} else {
			ccv9 = ppuint64(0)
		}
		ccv7 = ccv9
		goto cg_8
	cg_8:
		X__gmpz_rrandomb(cgtls, cgbp+16, aarands, ccv7)

		(*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, cgbp+32, cgbp+16)
		(*(*func(*iqlibc.ppTLS, ppuintptr, ppuintptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aaref_func})))(cgtls, cgbp+48, cgbp+16)
		if X__gmpz_cmp(cgtls, cgbp+32, cgbp+48) != 0 {

			X__gmp_printf(cgtls, "start %Zd\n\x00", iqlibc.ppVaList(cgbp+72, cgbp+16))
			X__gmp_printf(cgtls, "got   %Zd\n\x00", iqlibc.ppVaList(cgbp+72, cgbp+32))
			X__gmp_printf(cgtls, "want  %Zd\n\x00", iqlibc.ppVaList(cgbp+72, cgbp+48))
			Xabort(cgtls)
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpz_clear(cgtls, cgbp)
	X__gmpz_clear(cgtls, cgbp+16)
	X__gmpz_clear(cgtls, cgbp+32)
	X__gmpz_clear(cgtls, cgbp+48)
}

func Xtest_nextprime(cgtls *iqlibc.ppTLS, aarands tngmp_randstate_ptr, aareps ppint32) {

	Xrun(cgtls, "2\x00", ppint32(1000), "0x1ef7\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff1)))

	Xrun(cgtls, "3\x00", iqlibc.ppInt32FromInt32(1000)-iqlibc.ppInt32FromInt32(1), "0x1ef7\x00", iqlibc.ppUintptrFromInt32(0))

	Xrun(cgtls, "0x8a43866f5776ccd5b02186e90d28946aeb0ed914\x00", ppint32(50), "0x8a43866f5776ccd5b02186e90d28946aeb0eeec5\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff3)))

	Xrun(cgtls, "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C\x00", ppint32(50), "0x100000000000000000000000000000000010ab\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff4)))

	Xrun(cgtls, "0x1c2c26be55317530311facb648ea06b359b969715db83292ab8cf898d8b1b\x00", ppint32(50), "0x1c2c26be55317530311facb648ea06b359b969715db83292ab8cf898da957\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff5)))

	Xrun(cgtls, "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\x00", ppint32(50), "0x10000000000000000000000000000155B\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff6)))

	Xtest_ref(cgtls, aarands, aareps, pp__ccgo_fp(X__gmpz_nextprime), pp__ccgo_fp(Xrefmpz_nextprime))
}

func Xtest_prevprime(cgtls *iqlibc.ppTLS, aarands tngmp_randstate_ptr, aareps ppint32) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aai ppint64
	var aaretval, aatemp ppint32
	var pp_ /* n at bp+0 */ tnmpz_t
	var pp_ /* prvp at bp+16 */ tnmpz_t
	pp_, pp_, pp_ = aai, aaretval, aatemp

	X__gmpz_init(cgtls, cgbp)
	X__gmpz_init(cgtls, cgbp+16)

	/* Test mpz_prevprime(n <= 2) returns 0, leaves rop unchanged. */

	aatemp = ppint32(123)
	X__gmpz_set_ui(cgtls, cgbp+16, iqlibc.ppUint64FromInt32(aatemp))
	aai = 0
	for {
		if !(aai <= ppint64(2)) {
			break
		}

		X__gmpz_set_si(cgtls, cgbp, aai)
		aaretval = X__gmpz_prevprime(cgtls, cgbp+16, cgbp)
		if aaretval != 0 || X__gmpz_cmp_ui(cgtls, cgbp+16, iqlibc.ppUint64FromInt32(aatemp)) != 0 {

			X__gmp_printf(cgtls, "mpz_prevprime(%Zd) return (%d) rop (%Zd)\n\x00", iqlibc.ppVaList(cgbp+40, cgbp, aaretval, cgbp+16))
			Xabort(cgtls)
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmpz_clear(cgtls, cgbp)
	X__gmpz_clear(cgtls, cgbp+16)

	Xrun_p(cgtls, "2\x00", ppint32(1000), "0x1ef7\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff1)))

	Xrun_p(cgtls, "3\x00", iqlibc.ppInt32FromInt32(1000)-iqlibc.ppInt32FromInt32(1), "0x1ef7\x00", iqlibc.ppUintptrFromInt32(0))

	Xrun_p(cgtls, "0x8a43866f5776ccd5b02186e90d28946aeb0ed914\x00", ppint32(50), "0x8a43866f5776ccd5b02186e90d28946aeb0eeec5\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff3)))

	Xrun_p(cgtls, "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C\x00", ppint32(50), "0x100000000000000000000000000000000010ab\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff4)))

	Xrun_p(cgtls, "0x1c2c26be55317530311facb648ea06b359b969715db83292ab8cf898d8b1b\x00", ppint32(50), "0x1c2c26be55317530311facb648ea06b359b969715db83292ab8cf898da957\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff5)))

	Xrun_p(cgtls, "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\x00", ppint32(50), "0x10000000000000000000000000000155B\x00", ppuintptr(iqunsafe.ppPointer(&Xdiff6)))

	// Cast away int return from mpz_prevprime for test ref.
	Xtest_ref(cgtls, aarands, aareps, pp__ccgo_fp(X__gmpz_prevprime), pp__ccgo_fp(Xrefmpz_prevprime))
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaenvval ppuintptr
	var aarands tngmp_randstate_ptr
	var aarepfactor ppfloat64
	var aareps, aareps_nondefault, ccv2 ppint32
	var pp_ /* end at bp+0 */ ppuintptr
	pp_, pp_, pp_, pp_, pp_, pp_ = aaenvval, aarands, aarepfactor, aareps, aareps_nondefault, ccv2
	aareps = ppint32(20)

	Xtests_start(cgtls)

	if !(X__gmp_rands_initialized != 0) {
		X__gmp_rands_initialized = ppint8(1)
		X__gmp_randinit_mt_noseed(cgtls, ppuintptr(iqunsafe.ppPointer(&X__gmp_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	aarands = ppuintptr(iqunsafe.ppPointer(&X__gmp_rands))
	aareps_nondefault = 0
	if aaargc > ppint32(1) {
		aareps = ppint32(Xstrtol(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8)), cgbp, 0))
		if *(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp)))) != 0 || aareps <= 0 {
			Xfprintf(cgtls, Xstderr, "Invalid test count: %s.\n\x00", iqlibc.ppVaList(cgbp+16, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8))))
			Xexit(cgtls, ppint32(1))
		}
		aaargv += 8
		aaargc--
		aareps_nondefault = ppint32(1)
	}
	aaenvval = Xgetenv(cgtls, "GMP_CHECK_REPFACTOR\x00")
	if aaenvval != iqlibc.ppUintptrFromInt32(0) {
		aarepfactor = Xstrtod(cgtls, aaenvval, cgbp)
		if *(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp)))) != 0 || aarepfactor <= iqlibc.ppFloat64FromInt32(0) {
			Xfprintf(cgtls, Xstderr, "Invalid repfactor: %f.\n\x00", iqlibc.ppVaList(cgbp+16, aarepfactor))
			Xexit(cgtls, ppint32(1))
		}
		aareps = ppint32(ppfloat64(aareps) * aarepfactor)
		if aareps > ppint32(1) {
			ccv2 = aareps
		} else {
			ccv2 = ppint32(1)
		}
		aareps = ccv2
		aareps_nondefault = ppint32(1)
	}
	if aareps_nondefault != 0 {
		Xprintf(cgtls, "Running test with %ld repetitions (include this in bug reports)\n\x00", iqlibc.ppVaList(cgbp+16, ppint64(aareps)))
	}

	Xtest_nextprime(cgtls, aarands, aareps)
	Xtest_prevprime(cgtls, aarands, aareps)

	Xtest_largegaps(cgtls)
	Xtest_bitboundaries(cgtls)

	Xtests_end(cgtls)
	return 0
}

var Xdiff1 = [1000]ppint16{
	0:   ppint16(1),
	1:   ppint16(2),
	2:   ppint16(2),
	3:   ppint16(4),
	4:   ppint16(2),
	5:   ppint16(4),
	6:   ppint16(2),
	7:   ppint16(4),
	8:   ppint16(6),
	9:   ppint16(2),
	10:  ppint16(6),
	11:  ppint16(4),
	12:  ppint16(2),
	13:  ppint16(4),
	14:  ppint16(6),
	15:  ppint16(6),
	16:  ppint16(2),
	17:  ppint16(6),
	18:  ppint16(4),
	19:  ppint16(2),
	20:  ppint16(6),
	21:  ppint16(4),
	22:  ppint16(6),
	23:  ppint16(8),
	24:  ppint16(4),
	25:  ppint16(2),
	26:  ppint16(4),
	27:  ppint16(2),
	28:  ppint16(4),
	29:  ppint16(14),
	30:  ppint16(4),
	31:  ppint16(6),
	32:  ppint16(2),
	33:  ppint16(10),
	34:  ppint16(2),
	35:  ppint16(6),
	36:  ppint16(6),
	37:  ppint16(4),
	38:  ppint16(6),
	39:  ppint16(6),
	40:  ppint16(2),
	41:  ppint16(10),
	42:  ppint16(2),
	43:  ppint16(4),
	44:  ppint16(2),
	45:  ppint16(12),
	46:  ppint16(12),
	47:  ppint16(4),
	48:  ppint16(2),
	49:  ppint16(4),
	50:  ppint16(6),
	51:  ppint16(2),
	52:  ppint16(10),
	53:  ppint16(6),
	54:  ppint16(6),
	55:  ppint16(6),
	56:  ppint16(2),
	57:  ppint16(6),
	58:  ppint16(4),
	59:  ppint16(2),
	60:  ppint16(10),
	61:  ppint16(14),
	62:  ppint16(4),
	63:  ppint16(2),
	64:  ppint16(4),
	65:  ppint16(14),
	66:  ppint16(6),
	67:  ppint16(10),
	68:  ppint16(2),
	69:  ppint16(4),
	70:  ppint16(6),
	71:  ppint16(8),
	72:  ppint16(6),
	73:  ppint16(6),
	74:  ppint16(4),
	75:  ppint16(6),
	76:  ppint16(8),
	77:  ppint16(4),
	78:  ppint16(8),
	79:  ppint16(10),
	80:  ppint16(2),
	81:  ppint16(10),
	82:  ppint16(2),
	83:  ppint16(6),
	84:  ppint16(4),
	85:  ppint16(6),
	86:  ppint16(8),
	87:  ppint16(4),
	88:  ppint16(2),
	89:  ppint16(4),
	90:  ppint16(12),
	91:  ppint16(8),
	92:  ppint16(4),
	93:  ppint16(8),
	94:  ppint16(4),
	95:  ppint16(6),
	96:  ppint16(12),
	97:  ppint16(2),
	98:  ppint16(18),
	99:  ppint16(6),
	100: ppint16(10),
	101: ppint16(6),
	102: ppint16(6),
	103: ppint16(2),
	104: ppint16(6),
	105: ppint16(10),
	106: ppint16(6),
	107: ppint16(6),
	108: ppint16(2),
	109: ppint16(6),
	110: ppint16(6),
	111: ppint16(4),
	112: ppint16(2),
	113: ppint16(12),
	114: ppint16(10),
	115: ppint16(2),
	116: ppint16(4),
	117: ppint16(6),
	118: ppint16(6),
	119: ppint16(2),
	120: ppint16(12),
	121: ppint16(4),
	122: ppint16(6),
	123: ppint16(8),
	124: ppint16(10),
	125: ppint16(8),
	126: ppint16(10),
	127: ppint16(8),
	128: ppint16(6),
	129: ppint16(6),
	130: ppint16(4),
	131: ppint16(8),
	132: ppint16(6),
	133: ppint16(4),
	134: ppint16(8),
	135: ppint16(4),
	136: ppint16(14),
	137: ppint16(10),
	138: ppint16(12),
	139: ppint16(2),
	140: ppint16(10),
	141: ppint16(2),
	142: ppint16(4),
	143: ppint16(2),
	144: ppint16(10),
	145: ppint16(14),
	146: ppint16(4),
	147: ppint16(2),
	148: ppint16(4),
	149: ppint16(14),
	150: ppint16(4),
	151: ppint16(2),
	152: ppint16(4),
	153: ppint16(20),
	154: ppint16(4),
	155: ppint16(8),
	156: ppint16(10),
	157: ppint16(8),
	158: ppint16(4),
	159: ppint16(6),
	160: ppint16(6),
	161: ppint16(14),
	162: ppint16(4),
	163: ppint16(6),
	164: ppint16(6),
	165: ppint16(8),
	166: ppint16(6),
	167: ppint16(12),
	168: ppint16(4),
	169: ppint16(6),
	170: ppint16(2),
	171: ppint16(10),
	172: ppint16(2),
	173: ppint16(6),
	174: ppint16(10),
	175: ppint16(2),
	176: ppint16(10),
	177: ppint16(2),
	178: ppint16(6),
	179: ppint16(18),
	180: ppint16(4),
	181: ppint16(2),
	182: ppint16(4),
	183: ppint16(6),
	184: ppint16(6),
	185: ppint16(8),
	186: ppint16(6),
	187: ppint16(6),
	188: ppint16(22),
	189: ppint16(2),
	190: ppint16(10),
	191: ppint16(8),
	192: ppint16(10),
	193: ppint16(6),
	194: ppint16(6),
	195: ppint16(8),
	196: ppint16(12),
	197: ppint16(4),
	198: ppint16(6),
	199: ppint16(6),
	200: ppint16(2),
	201: ppint16(6),
	202: ppint16(12),
	203: ppint16(10),
	204: ppint16(18),
	205: ppint16(2),
	206: ppint16(4),
	207: ppint16(6),
	208: ppint16(2),
	209: ppint16(6),
	210: ppint16(4),
	211: ppint16(2),
	212: ppint16(4),
	213: ppint16(12),
	214: ppint16(2),
	215: ppint16(6),
	216: ppint16(34),
	217: ppint16(6),
	218: ppint16(6),
	219: ppint16(8),
	220: ppint16(18),
	221: ppint16(10),
	222: ppint16(14),
	223: ppint16(4),
	224: ppint16(2),
	225: ppint16(4),
	226: ppint16(6),
	227: ppint16(8),
	228: ppint16(4),
	229: ppint16(2),
	230: ppint16(6),
	231: ppint16(12),
	232: ppint16(10),
	233: ppint16(2),
	234: ppint16(4),
	235: ppint16(2),
	236: ppint16(4),
	237: ppint16(6),
	238: ppint16(12),
	239: ppint16(12),
	240: ppint16(8),
	241: ppint16(12),
	242: ppint16(6),
	243: ppint16(4),
	244: ppint16(6),
	245: ppint16(8),
	246: ppint16(4),
	247: ppint16(8),
	248: ppint16(4),
	249: ppint16(14),
	250: ppint16(4),
	251: ppint16(6),
	252: ppint16(2),
	253: ppint16(4),
	254: ppint16(6),
	255: ppint16(2),
	256: ppint16(6),
	257: ppint16(10),
	258: ppint16(20),
	259: ppint16(6),
	260: ppint16(4),
	261: ppint16(2),
	262: ppint16(24),
	263: ppint16(4),
	264: ppint16(2),
	265: ppint16(10),
	266: ppint16(12),
	267: ppint16(2),
	268: ppint16(10),
	269: ppint16(8),
	270: ppint16(6),
	271: ppint16(6),
	272: ppint16(6),
	273: ppint16(18),
	274: ppint16(6),
	275: ppint16(4),
	276: ppint16(2),
	277: ppint16(12),
	278: ppint16(10),
	279: ppint16(12),
	280: ppint16(8),
	281: ppint16(16),
	282: ppint16(14),
	283: ppint16(6),
	284: ppint16(4),
	285: ppint16(2),
	286: ppint16(4),
	287: ppint16(2),
	288: ppint16(10),
	289: ppint16(12),
	290: ppint16(6),
	291: ppint16(6),
	292: ppint16(18),
	293: ppint16(2),
	294: ppint16(16),
	295: ppint16(2),
	296: ppint16(22),
	297: ppint16(6),
	298: ppint16(8),
	299: ppint16(6),
	300: ppint16(4),
	301: ppint16(2),
	302: ppint16(4),
	303: ppint16(8),
	304: ppint16(6),
	305: ppint16(10),
	306: ppint16(2),
	307: ppint16(10),
	308: ppint16(14),
	309: ppint16(10),
	310: ppint16(6),
	311: ppint16(12),
	312: ppint16(2),
	313: ppint16(4),
	314: ppint16(2),
	315: ppint16(10),
	316: ppint16(12),
	317: ppint16(2),
	318: ppint16(16),
	319: ppint16(2),
	320: ppint16(6),
	321: ppint16(4),
	322: ppint16(2),
	323: ppint16(10),
	324: ppint16(8),
	325: ppint16(18),
	326: ppint16(24),
	327: ppint16(4),
	328: ppint16(6),
	329: ppint16(8),
	330: ppint16(16),
	331: ppint16(2),
	332: ppint16(4),
	333: ppint16(8),
	334: ppint16(16),
	335: ppint16(2),
	336: ppint16(4),
	337: ppint16(8),
	338: ppint16(6),
	339: ppint16(6),
	340: ppint16(4),
	341: ppint16(12),
	342: ppint16(2),
	343: ppint16(22),
	344: ppint16(6),
	345: ppint16(2),
	346: ppint16(6),
	347: ppint16(4),
	348: ppint16(6),
	349: ppint16(14),
	350: ppint16(6),
	351: ppint16(4),
	352: ppint16(2),
	353: ppint16(6),
	354: ppint16(4),
	355: ppint16(6),
	356: ppint16(12),
	357: ppint16(6),
	358: ppint16(6),
	359: ppint16(14),
	360: ppint16(4),
	361: ppint16(6),
	362: ppint16(12),
	363: ppint16(8),
	364: ppint16(6),
	365: ppint16(4),
	366: ppint16(26),
	367: ppint16(18),
	368: ppint16(10),
	369: ppint16(8),
	370: ppint16(4),
	371: ppint16(6),
	372: ppint16(2),
	373: ppint16(6),
	374: ppint16(22),
	375: ppint16(12),
	376: ppint16(2),
	377: ppint16(16),
	378: ppint16(8),
	379: ppint16(4),
	380: ppint16(12),
	381: ppint16(14),
	382: ppint16(10),
	383: ppint16(2),
	384: ppint16(4),
	385: ppint16(8),
	386: ppint16(6),
	387: ppint16(6),
	388: ppint16(4),
	389: ppint16(2),
	390: ppint16(4),
	391: ppint16(6),
	392: ppint16(8),
	393: ppint16(4),
	394: ppint16(2),
	395: ppint16(6),
	396: ppint16(10),
	397: ppint16(2),
	398: ppint16(10),
	399: ppint16(8),
	400: ppint16(4),
	401: ppint16(14),
	402: ppint16(10),
	403: ppint16(12),
	404: ppint16(2),
	405: ppint16(6),
	406: ppint16(4),
	407: ppint16(2),
	408: ppint16(16),
	409: ppint16(14),
	410: ppint16(4),
	411: ppint16(6),
	412: ppint16(8),
	413: ppint16(6),
	414: ppint16(4),
	415: ppint16(18),
	416: ppint16(8),
	417: ppint16(10),
	418: ppint16(6),
	419: ppint16(6),
	420: ppint16(8),
	421: ppint16(10),
	422: ppint16(12),
	423: ppint16(14),
	424: ppint16(4),
	425: ppint16(6),
	426: ppint16(6),
	427: ppint16(2),
	428: ppint16(28),
	429: ppint16(2),
	430: ppint16(10),
	431: ppint16(8),
	432: ppint16(4),
	433: ppint16(14),
	434: ppint16(4),
	435: ppint16(8),
	436: ppint16(12),
	437: ppint16(6),
	438: ppint16(12),
	439: ppint16(4),
	440: ppint16(6),
	441: ppint16(20),
	442: ppint16(10),
	443: ppint16(2),
	444: ppint16(16),
	445: ppint16(26),
	446: ppint16(4),
	447: ppint16(2),
	448: ppint16(12),
	449: ppint16(6),
	450: ppint16(4),
	451: ppint16(12),
	452: ppint16(6),
	453: ppint16(8),
	454: ppint16(4),
	455: ppint16(8),
	456: ppint16(22),
	457: ppint16(2),
	458: ppint16(4),
	459: ppint16(2),
	460: ppint16(12),
	461: ppint16(28),
	462: ppint16(2),
	463: ppint16(6),
	464: ppint16(6),
	465: ppint16(6),
	466: ppint16(4),
	467: ppint16(6),
	468: ppint16(2),
	469: ppint16(12),
	470: ppint16(4),
	471: ppint16(12),
	472: ppint16(2),
	473: ppint16(10),
	474: ppint16(2),
	475: ppint16(16),
	476: ppint16(2),
	477: ppint16(16),
	478: ppint16(6),
	479: ppint16(20),
	480: ppint16(16),
	481: ppint16(8),
	482: ppint16(4),
	483: ppint16(2),
	484: ppint16(4),
	485: ppint16(2),
	486: ppint16(22),
	487: ppint16(8),
	488: ppint16(12),
	489: ppint16(6),
	490: ppint16(10),
	491: ppint16(2),
	492: ppint16(4),
	493: ppint16(6),
	494: ppint16(2),
	495: ppint16(6),
	496: ppint16(10),
	497: ppint16(2),
	498: ppint16(12),
	499: ppint16(10),
	500: ppint16(2),
	501: ppint16(10),
	502: ppint16(14),
	503: ppint16(6),
	504: ppint16(4),
	505: ppint16(6),
	506: ppint16(8),
	507: ppint16(6),
	508: ppint16(6),
	509: ppint16(16),
	510: ppint16(12),
	511: ppint16(2),
	512: ppint16(4),
	513: ppint16(14),
	514: ppint16(6),
	515: ppint16(4),
	516: ppint16(8),
	517: ppint16(10),
	518: ppint16(8),
	519: ppint16(6),
	520: ppint16(6),
	521: ppint16(22),
	522: ppint16(6),
	523: ppint16(2),
	524: ppint16(10),
	525: ppint16(14),
	526: ppint16(4),
	527: ppint16(6),
	528: ppint16(18),
	529: ppint16(2),
	530: ppint16(10),
	531: ppint16(14),
	532: ppint16(4),
	533: ppint16(2),
	534: ppint16(10),
	535: ppint16(14),
	536: ppint16(4),
	537: ppint16(8),
	538: ppint16(18),
	539: ppint16(4),
	540: ppint16(6),
	541: ppint16(2),
	542: ppint16(4),
	543: ppint16(6),
	544: ppint16(2),
	545: ppint16(12),
	546: ppint16(4),
	547: ppint16(20),
	548: ppint16(22),
	549: ppint16(12),
	550: ppint16(2),
	551: ppint16(4),
	552: ppint16(6),
	553: ppint16(6),
	554: ppint16(2),
	555: ppint16(6),
	556: ppint16(22),
	557: ppint16(2),
	558: ppint16(6),
	559: ppint16(16),
	560: ppint16(6),
	561: ppint16(12),
	562: ppint16(2),
	563: ppint16(6),
	564: ppint16(12),
	565: ppint16(16),
	566: ppint16(2),
	567: ppint16(4),
	568: ppint16(6),
	569: ppint16(14),
	570: ppint16(4),
	571: ppint16(2),
	572: ppint16(18),
	573: ppint16(24),
	574: ppint16(10),
	575: ppint16(6),
	576: ppint16(2),
	577: ppint16(10),
	578: ppint16(2),
	579: ppint16(10),
	580: ppint16(2),
	581: ppint16(10),
	582: ppint16(6),
	583: ppint16(2),
	584: ppint16(10),
	585: ppint16(2),
	586: ppint16(10),
	587: ppint16(6),
	588: ppint16(8),
	589: ppint16(30),
	590: ppint16(10),
	591: ppint16(2),
	592: ppint16(10),
	593: ppint16(8),
	594: ppint16(6),
	595: ppint16(10),
	596: ppint16(18),
	597: ppint16(6),
	598: ppint16(12),
	599: ppint16(12),
	600: ppint16(2),
	601: ppint16(18),
	602: ppint16(6),
	603: ppint16(4),
	604: ppint16(6),
	605: ppint16(6),
	606: ppint16(18),
	607: ppint16(2),
	608: ppint16(10),
	609: ppint16(14),
	610: ppint16(6),
	611: ppint16(4),
	612: ppint16(2),
	613: ppint16(4),
	614: ppint16(24),
	615: ppint16(2),
	616: ppint16(12),
	617: ppint16(6),
	618: ppint16(16),
	619: ppint16(8),
	620: ppint16(6),
	621: ppint16(6),
	622: ppint16(18),
	623: ppint16(16),
	624: ppint16(2),
	625: ppint16(4),
	626: ppint16(6),
	627: ppint16(2),
	628: ppint16(6),
	629: ppint16(6),
	630: ppint16(10),
	631: ppint16(6),
	632: ppint16(12),
	633: ppint16(12),
	634: ppint16(18),
	635: ppint16(2),
	636: ppint16(6),
	637: ppint16(4),
	638: ppint16(18),
	639: ppint16(8),
	640: ppint16(24),
	641: ppint16(4),
	642: ppint16(2),
	643: ppint16(4),
	644: ppint16(6),
	645: ppint16(2),
	646: ppint16(12),
	647: ppint16(4),
	648: ppint16(14),
	649: ppint16(30),
	650: ppint16(10),
	651: ppint16(6),
	652: ppint16(12),
	653: ppint16(14),
	654: ppint16(6),
	655: ppint16(10),
	656: ppint16(12),
	657: ppint16(2),
	658: ppint16(4),
	659: ppint16(6),
	660: ppint16(8),
	661: ppint16(6),
	662: ppint16(10),
	663: ppint16(2),
	664: ppint16(4),
	665: ppint16(14),
	666: ppint16(6),
	667: ppint16(6),
	668: ppint16(4),
	669: ppint16(6),
	670: ppint16(2),
	671: ppint16(10),
	672: ppint16(2),
	673: ppint16(16),
	674: ppint16(12),
	675: ppint16(8),
	676: ppint16(18),
	677: ppint16(4),
	678: ppint16(6),
	679: ppint16(12),
	680: ppint16(2),
	681: ppint16(6),
	682: ppint16(6),
	683: ppint16(6),
	684: ppint16(28),
	685: ppint16(6),
	686: ppint16(14),
	687: ppint16(4),
	688: ppint16(8),
	689: ppint16(10),
	690: ppint16(8),
	691: ppint16(12),
	692: ppint16(18),
	693: ppint16(4),
	694: ppint16(2),
	695: ppint16(4),
	696: ppint16(24),
	697: ppint16(12),
	698: ppint16(6),
	699: ppint16(2),
	700: ppint16(16),
	701: ppint16(6),
	702: ppint16(6),
	703: ppint16(14),
	704: ppint16(10),
	705: ppint16(14),
	706: ppint16(4),
	707: ppint16(30),
	708: ppint16(6),
	709: ppint16(6),
	710: ppint16(6),
	711: ppint16(8),
	712: ppint16(6),
	713: ppint16(4),
	714: ppint16(2),
	715: ppint16(12),
	716: ppint16(6),
	717: ppint16(4),
	718: ppint16(2),
	719: ppint16(6),
	720: ppint16(22),
	721: ppint16(6),
	722: ppint16(2),
	723: ppint16(4),
	724: ppint16(18),
	725: ppint16(2),
	726: ppint16(4),
	727: ppint16(12),
	728: ppint16(2),
	729: ppint16(6),
	730: ppint16(4),
	731: ppint16(26),
	732: ppint16(6),
	733: ppint16(6),
	734: ppint16(4),
	735: ppint16(8),
	736: ppint16(10),
	737: ppint16(32),
	738: ppint16(16),
	739: ppint16(2),
	740: ppint16(6),
	741: ppint16(4),
	742: ppint16(2),
	743: ppint16(4),
	744: ppint16(2),
	745: ppint16(10),
	746: ppint16(14),
	747: ppint16(6),
	748: ppint16(4),
	749: ppint16(8),
	750: ppint16(10),
	751: ppint16(6),
	752: ppint16(20),
	753: ppint16(4),
	754: ppint16(2),
	755: ppint16(6),
	756: ppint16(30),
	757: ppint16(4),
	758: ppint16(8),
	759: ppint16(10),
	760: ppint16(6),
	761: ppint16(6),
	762: ppint16(8),
	763: ppint16(6),
	764: ppint16(12),
	765: ppint16(4),
	766: ppint16(6),
	767: ppint16(2),
	768: ppint16(6),
	769: ppint16(4),
	770: ppint16(6),
	771: ppint16(2),
	772: ppint16(10),
	773: ppint16(2),
	774: ppint16(16),
	775: ppint16(6),
	776: ppint16(20),
	777: ppint16(4),
	778: ppint16(12),
	779: ppint16(14),
	780: ppint16(28),
	781: ppint16(6),
	782: ppint16(20),
	783: ppint16(4),
	784: ppint16(18),
	785: ppint16(8),
	786: ppint16(6),
	787: ppint16(4),
	788: ppint16(6),
	789: ppint16(14),
	790: ppint16(6),
	791: ppint16(6),
	792: ppint16(10),
	793: ppint16(2),
	794: ppint16(10),
	795: ppint16(12),
	796: ppint16(8),
	797: ppint16(10),
	798: ppint16(2),
	799: ppint16(10),
	800: ppint16(8),
	801: ppint16(12),
	802: ppint16(10),
	803: ppint16(24),
	804: ppint16(2),
	805: ppint16(4),
	806: ppint16(8),
	807: ppint16(6),
	808: ppint16(4),
	809: ppint16(8),
	810: ppint16(18),
	811: ppint16(10),
	812: ppint16(6),
	813: ppint16(6),
	814: ppint16(2),
	815: ppint16(6),
	816: ppint16(10),
	817: ppint16(12),
	818: ppint16(2),
	819: ppint16(10),
	820: ppint16(6),
	821: ppint16(6),
	822: ppint16(6),
	823: ppint16(8),
	824: ppint16(6),
	825: ppint16(10),
	826: ppint16(6),
	827: ppint16(2),
	828: ppint16(6),
	829: ppint16(6),
	830: ppint16(6),
	831: ppint16(10),
	832: ppint16(8),
	833: ppint16(24),
	834: ppint16(6),
	835: ppint16(22),
	836: ppint16(2),
	837: ppint16(18),
	838: ppint16(4),
	839: ppint16(8),
	840: ppint16(10),
	841: ppint16(30),
	842: ppint16(8),
	843: ppint16(18),
	844: ppint16(4),
	845: ppint16(2),
	846: ppint16(10),
	847: ppint16(6),
	848: ppint16(2),
	849: ppint16(6),
	850: ppint16(4),
	851: ppint16(18),
	852: ppint16(8),
	853: ppint16(12),
	854: ppint16(18),
	855: ppint16(16),
	856: ppint16(6),
	857: ppint16(2),
	858: ppint16(12),
	859: ppint16(6),
	860: ppint16(10),
	861: ppint16(2),
	862: ppint16(10),
	863: ppint16(2),
	864: ppint16(6),
	865: ppint16(10),
	866: ppint16(14),
	867: ppint16(4),
	868: ppint16(24),
	869: ppint16(2),
	870: ppint16(16),
	871: ppint16(2),
	872: ppint16(10),
	873: ppint16(2),
	874: ppint16(10),
	875: ppint16(20),
	876: ppint16(4),
	877: ppint16(2),
	878: ppint16(4),
	879: ppint16(8),
	880: ppint16(16),
	881: ppint16(6),
	882: ppint16(6),
	883: ppint16(2),
	884: ppint16(12),
	885: ppint16(16),
	886: ppint16(8),
	887: ppint16(4),
	888: ppint16(6),
	889: ppint16(30),
	890: ppint16(2),
	891: ppint16(10),
	892: ppint16(2),
	893: ppint16(6),
	894: ppint16(4),
	895: ppint16(6),
	896: ppint16(6),
	897: ppint16(8),
	898: ppint16(6),
	899: ppint16(4),
	900: ppint16(12),
	901: ppint16(6),
	902: ppint16(8),
	903: ppint16(12),
	904: ppint16(4),
	905: ppint16(14),
	906: ppint16(12),
	907: ppint16(10),
	908: ppint16(24),
	909: ppint16(6),
	910: ppint16(12),
	911: ppint16(6),
	912: ppint16(2),
	913: ppint16(22),
	914: ppint16(8),
	915: ppint16(18),
	916: ppint16(10),
	917: ppint16(6),
	918: ppint16(14),
	919: ppint16(4),
	920: ppint16(2),
	921: ppint16(6),
	922: ppint16(10),
	923: ppint16(8),
	924: ppint16(6),
	925: ppint16(4),
	926: ppint16(6),
	927: ppint16(30),
	928: ppint16(14),
	929: ppint16(10),
	930: ppint16(2),
	931: ppint16(12),
	932: ppint16(10),
	933: ppint16(2),
	934: ppint16(16),
	935: ppint16(2),
	936: ppint16(18),
	937: ppint16(24),
	938: ppint16(18),
	939: ppint16(6),
	940: ppint16(16),
	941: ppint16(18),
	942: ppint16(6),
	943: ppint16(2),
	944: ppint16(18),
	945: ppint16(4),
	946: ppint16(6),
	947: ppint16(2),
	948: ppint16(10),
	949: ppint16(8),
	950: ppint16(10),
	951: ppint16(6),
	952: ppint16(6),
	953: ppint16(8),
	954: ppint16(4),
	955: ppint16(6),
	956: ppint16(2),
	957: ppint16(10),
	958: ppint16(2),
	959: ppint16(12),
	960: ppint16(4),
	961: ppint16(6),
	962: ppint16(6),
	963: ppint16(2),
	964: ppint16(12),
	965: ppint16(4),
	966: ppint16(14),
	967: ppint16(18),
	968: ppint16(4),
	969: ppint16(6),
	970: ppint16(20),
	971: ppint16(4),
	972: ppint16(8),
	973: ppint16(6),
	974: ppint16(4),
	975: ppint16(8),
	976: ppint16(4),
	977: ppint16(14),
	978: ppint16(6),
	979: ppint16(4),
	980: ppint16(14),
	981: ppint16(12),
	982: ppint16(4),
	983: ppint16(2),
	984: ppint16(30),
	985: ppint16(4),
	986: ppint16(24),
	987: ppint16(6),
	988: ppint16(6),
	989: ppint16(12),
	990: ppint16(12),
	991: ppint16(14),
	992: ppint16(6),
	993: ppint16(4),
	994: ppint16(2),
	995: ppint16(4),
	996: ppint16(18),
	997: ppint16(6),
	998: ppint16(12),
	999: ppint16(8),
}

var Xdiff3 = [50]ppint16{
	0:  ppint16(33),
	1:  ppint16(32),
	2:  ppint16(136),
	3:  ppint16(116),
	4:  ppint16(24),
	5:  ppint16(22),
	6:  ppint16(104),
	7:  ppint16(114),
	8:  ppint16(76),
	9:  ppint16(278),
	10: ppint16(238),
	11: ppint16(162),
	12: ppint16(36),
	13: ppint16(44),
	14: ppint16(388),
	15: ppint16(134),
	16: ppint16(130),
	17: ppint16(26),
	18: ppint16(312),
	19: ppint16(42),
	20: ppint16(138),
	21: ppint16(28),
	22: ppint16(24),
	23: ppint16(80),
	24: ppint16(138),
	25: ppint16(108),
	26: ppint16(270),
	27: ppint16(12),
	28: ppint16(330),
	29: ppint16(130),
	30: ppint16(98),
	31: ppint16(102),
	32: ppint16(162),
	33: ppint16(34),
	34: ppint16(36),
	35: ppint16(170),
	36: ppint16(90),
	37: ppint16(34),
	38: ppint16(14),
	39: ppint16(6),
	40: ppint16(24),
	41: ppint16(66),
	42: ppint16(154),
	43: ppint16(218),
	44: ppint16(70),
	45: ppint16(132),
	46: ppint16(188),
	47: ppint16(88),
	48: ppint16(80),
	49: ppint16(82),
}

var Xdiff4 = [50]ppint16{
	0:  ppint16(239),
	1:  ppint16(92),
	2:  ppint16(64),
	3:  ppint16(6),
	4:  ppint16(104),
	5:  ppint16(24),
	6:  ppint16(46),
	7:  ppint16(258),
	8:  ppint16(68),
	9:  ppint16(18),
	10: ppint16(54),
	11: ppint16(100),
	12: ppint16(68),
	13: ppint16(154),
	14: ppint16(26),
	15: ppint16(4),
	16: ppint16(38),
	17: ppint16(142),
	18: ppint16(168),
	19: ppint16(42),
	20: ppint16(18),
	21: ppint16(26),
	22: ppint16(286),
	23: ppint16(104),
	24: ppint16(136),
	25: ppint16(116),
	26: ppint16(40),
	27: ppint16(2),
	28: ppint16(28),
	29: ppint16(110),
	30: ppint16(52),
	31: ppint16(78),
	32: ppint16(104),
	33: ppint16(24),
	34: ppint16(54),
	35: ppint16(96),
	36: ppint16(4),
	37: ppint16(626),
	38: ppint16(196),
	39: ppint16(24),
	40: ppint16(56),
	41: ppint16(36),
	42: ppint16(52),
	43: ppint16(102),
	44: ppint16(48),
	45: ppint16(156),
	46: ppint16(26),
	47: ppint16(18),
	48: ppint16(42),
	49: ppint16(40),
}

var Xdiff5 = [50]ppint16{
	0:  ppint16(268),
	1:  ppint16(120),
	2:  ppint16(320),
	3:  ppint16(184),
	4:  ppint16(396),
	5:  ppint16(2),
	6:  ppint16(94),
	7:  ppint16(108),
	8:  ppint16(20),
	9:  ppint16(318),
	10: ppint16(274),
	11: ppint16(14),
	12: ppint16(64),
	13: ppint16(122),
	14: ppint16(220),
	15: ppint16(108),
	16: ppint16(18),
	17: ppint16(174),
	18: ppint16(6),
	19: ppint16(24),
	20: ppint16(348),
	21: ppint16(32),
	22: ppint16(64),
	23: ppint16(116),
	24: ppint16(268),
	25: ppint16(162),
	26: ppint16(20),
	27: ppint16(156),
	28: ppint16(28),
	29: ppint16(110),
	30: ppint16(52),
	31: ppint16(428),
	32: ppint16(196),
	33: ppint16(14),
	34: ppint16(262),
	35: ppint16(30),
	36: ppint16(194),
	37: ppint16(120),
	38: ppint16(300),
	39: ppint16(66),
	40: ppint16(268),
	41: ppint16(12),
	42: ppint16(428),
	43: ppint16(370),
	44: ppint16(212),
	45: ppint16(198),
	46: ppint16(192),
	47: ppint16(130),
	48: ppint16(30),
	49: ppint16(80),
}

var Xdiff6 = [50]ppint16{
	0:  ppint16(179),
	1:  ppint16(30),
	2:  ppint16(84),
	3:  ppint16(108),
	4:  ppint16(112),
	5:  ppint16(36),
	6:  ppint16(42),
	7:  ppint16(110),
	8:  ppint16(52),
	9:  ppint16(132),
	10: ppint16(60),
	11: ppint16(30),
	12: ppint16(326),
	13: ppint16(114),
	14: ppint16(496),
	15: ppint16(92),
	16: ppint16(100),
	17: ppint16(272),
	18: ppint16(36),
	19: ppint16(54),
	20: ppint16(90),
	21: ppint16(4),
	22: ppint16(2),
	23: ppint16(24),
	24: ppint16(40),
	25: ppint16(398),
	26: ppint16(150),
	27: ppint16(72),
	28: ppint16(60),
	29: ppint16(16),
	30: ppint16(8),
	31: ppint16(4),
	32: ppint16(80),
	33: ppint16(16),
	34: ppint16(2),
	35: ppint16(342),
	36: ppint16(112),
	37: ppint16(14),
	38: ppint16(136),
	39: ppint16(236),
	40: ppint16(40),
	41: ppint16(18),
	42: ppint16(50),
	43: ppint16(192),
	44: ppint16(198),
	45: ppint16(204),
	46: ppint16(40),
	47: ppint16(266),
	48: ppint16(42),
	49: ppint16(274),
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___gmp_printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmp_randinit_mt_noseed(*iqlibc.ppTLS, ppuintptr)

var ___gmp_rands [1]tn__gmp_randstate_struct

var ___gmp_rands_initialized ppint8

func ___gmpz_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_cmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmpz_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint64) ppint32

func ___gmpz_divexact_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_fits_sshort_p(*iqlibc.ppTLS, ppuintptr) ppint32

func ___gmpz_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_init_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func ___gmpz_mul_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_nextprime(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpz_prevprime(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmpz_primorial_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

func ___gmpz_probab_prime_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func ___gmpz_rrandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_set_si(*iqlibc.ppTLS, ppuintptr, ppint64)

func ___gmpz_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func ___gmpz_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

func ___gmpz_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_sub_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_swap(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpz_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func _abort(*iqlibc.ppTLS)

func _exit(*iqlibc.ppTLS, ppint32)

func _fprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

var _stderr ppuintptr

func _strtod(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppfloat64

func _strtol(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint64

func _tests_end(*iqlibc.ppTLS)

func _tests_start(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
