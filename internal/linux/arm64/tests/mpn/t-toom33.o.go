// Code generated for linux/arm64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -ignore-unsupported-alignment -DHAVE_CONFIG_H -I. -I../.. -I../.. -I../../tests -DNDEBUG -c -o t-toom33.o.go t-toom33.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvASSERT_FILE = "__FILE__"
const mvASSERT_LINE = "__LINE__"
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBINV_NEWTON_THRESHOLD = 300
const mvBMOD_1_TO_MOD_1_THRESHOLD = 10
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvCOUNT = 1000
const mvDC_BDIV_Q_THRESHOLD = 180
const mvDC_DIVAPPR_Q_THRESHOLD = 200
const mvDELAYTIMER_MAX = 0x7fffffff
const mvDIVEXACT_1_THRESHOLD = 0
const mvDIVEXACT_BY3_METHOD = 0
const mvDIVEXACT_JEB_THRESHOLD = 25
const mvDOPRNT_CONV_FIXED = 1
const mvDOPRNT_CONV_GENERAL = 3
const mvDOPRNT_CONV_SCIENTIFIC = 2
const mvDOPRNT_JUSTIFY_INTERNAL = 3
const mvDOPRNT_JUSTIFY_LEFT = 1
const mvDOPRNT_JUSTIFY_NONE = 0
const mvDOPRNT_JUSTIFY_RIGHT = 2
const mvDOPRNT_SHOWBASE_NO = 2
const mvDOPRNT_SHOWBASE_NONZERO = 3
const mvDOPRNT_SHOWBASE_YES = 1
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFAC_DSC_THRESHOLD = 400
const mvFAC_ODD_THRESHOLD = 35
const mvFFT_FIRST_K = 4
const mvFIB_TABLE_LIMIT = 93
const mvFIB_TABLE_LUCNUM_LIMIT = 92
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFOPEN_MAX = 1000
const mvGCDEXT_DC_THRESHOLD = 600
const mvGCD_DC_THRESHOLD = 1000
const mvGET_STR_DC_THRESHOLD = 18
const mvGET_STR_PRECOMPUTE_THRESHOLD = 35
const mvGMP_LIMB_BITS = 64
const mvGMP_LIMB_BYTES = "SIZEOF_MP_LIMB_T"
const mvGMP_MPARAM_H_SUGGEST = "./mpn/generic/gmp-mparam.h"
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_ALARM = 1
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_CONST = 1
const mvHAVE_ATTRIBUTE_MALLOC = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_ATTRIBUTE_NORETURN = 1
const mvHAVE_CLOCK = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_CONFIG_H = 1
const mvHAVE_DECL_FGETC = 1
const mvHAVE_DECL_FSCANF = 1
const mvHAVE_DECL_OPTARG = 1
const mvHAVE_DECL_SYS_ERRLIST = 0
const mvHAVE_DECL_SYS_NERR = 0
const mvHAVE_DECL_UNGETC = 1
const mvHAVE_DECL_VFPRINTF = 1
const mvHAVE_DLFCN_H = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FCNTL_H = 1
const mvHAVE_FLOAT_H = 1
const mvHAVE_GETPAGESIZE = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_HIDDEN_ALIAS = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTPTR_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LANGINFO_H = 1
const mvHAVE_LIMB_LITTLE_ENDIAN = 1
const mvHAVE_LOCALECONV = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_DOUBLE = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_MEMORY_H = 1
const mvHAVE_MEMSET = 1
const mvHAVE_MMAP = 1
const mvHAVE_MPROTECT = 1
const mvHAVE_NL_LANGINFO = 1
const mvHAVE_NL_TYPES_H = 1
const mvHAVE_POPEN = 1
const mvHAVE_PTRDIFF_T = 1
const mvHAVE_QUAD_T = 1
const mvHAVE_RAISE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGALTSTACK = 1
const mvHAVE_SIGSTACK = 1
const mvHAVE_STACK_T = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDLIB_H = 1
const mvHAVE_STRCHR = 1
const mvHAVE_STRERROR = 1
const mvHAVE_STRINGS_H = 1
const mvHAVE_STRING_H = 1
const mvHAVE_STRNLEN = 1
const mvHAVE_STRTOL = 1
const mvHAVE_STRTOUL = 1
const mvHAVE_SYSCONF = 1
const mvHAVE_SYS_MMAN_H = 1
const mvHAVE_SYS_PARAM_H = 1
const mvHAVE_SYS_RESOURCE_H = 1
const mvHAVE_SYS_STAT_H = 1
const mvHAVE_SYS_SYSINFO_H = 1
const mvHAVE_SYS_TIMES_H = 1
const mvHAVE_SYS_TIME_H = 1
const mvHAVE_SYS_TYPES_H = 1
const mvHAVE_TIMES = 1
const mvHAVE_UINT_LEAST32_T = 1
const mvHAVE_UNISTD_H = 1
const mvHAVE_VSNPRINTF = 1
const mvHGCD_APPR_THRESHOLD = 400
const mvHGCD_REDUCE_THRESHOLD = 1000
const mvHGCD_THRESHOLD = 400
const mvHOST_NAME_MAX = 255
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT64_MAX"
const mvINTPTR_MIN = "INT64_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_HIGHBIT = "INT_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvINV_APPR_THRESHOLD = "INV_NEWTON_THRESHOLD"
const mvINV_NEWTON_THRESHOLD = 200
const mvIOV_MAX = 1024
const mvLIMBS_PER_ULONG = 1
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_HIGHBIT = "LONG_MIN"
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMATRIX22_STRASSEN_THRESHOLD = 30
const mvMB_LEN_MAX = 4
const mvMIN_AN = "MUL_TOOM33_THRESHOLD"
const mvMOD_BKNP1_ONLY3 = 0
const mvMPN_FFT_TABLE_SIZE = 16
const mvMPN_TOOM22_MUL_MINSIZE = 6
const mvMPN_TOOM2_SQR_MINSIZE = 4
const mvMPN_TOOM32_MUL_MINSIZE = 10
const mvMPN_TOOM33_MUL_MINSIZE = 17
const mvMPN_TOOM3_SQR_MINSIZE = 17
const mvMPN_TOOM42_MULMID_MINSIZE = 4
const mvMPN_TOOM42_MUL_MINSIZE = 10
const mvMPN_TOOM43_MUL_MINSIZE = 25
const mvMPN_TOOM44_MUL_MINSIZE = 30
const mvMPN_TOOM4_SQR_MINSIZE = 30
const mvMPN_TOOM53_MUL_MINSIZE = 17
const mvMPN_TOOM54_MUL_MINSIZE = 31
const mvMPN_TOOM63_MUL_MINSIZE = 49
const mvMPN_TOOM6H_MUL_MINSIZE = 46
const mvMPN_TOOM6_SQR_MINSIZE = 46
const mvMPN_TOOM8H_MUL_MINSIZE = 86
const mvMPN_TOOM8_SQR_MINSIZE = 86
const mvMP_BASES_BIG_BASE_CTZ_10 = 19
const mvMP_BASES_CHARS_PER_LIMB_10 = 19
const mvMP_BASES_NORMALIZATION_STEPS_10 = 0
const mvMP_EXP_T_MAX = "MP_SIZE_T_MAX"
const mvMP_EXP_T_MIN = "MP_SIZE_T_MIN"
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMULLO_BASECASE_THRESHOLD = 0
const mvMULLO_BASECASE_THRESHOLD_LIMIT = "MULLO_BASECASE_THRESHOLD"
const mvMULMID_TOOM42_THRESHOLD = "MUL_TOOM22_THRESHOLD"
const mvMULMOD_BNM1_THRESHOLD = 16
const mvMUL_TOOM22_THRESHOLD = 30
const mvMUL_TOOM22_THRESHOLD_LIMIT = "MUL_TOOM22_THRESHOLD"
const mvMUL_TOOM32_TO_TOOM43_THRESHOLD = 100
const mvMUL_TOOM32_TO_TOOM53_THRESHOLD = 110
const mvMUL_TOOM33_THRESHOLD = 100
const mvMUL_TOOM33_THRESHOLD_LIMIT = "MUL_TOOM33_THRESHOLD"
const mvMUL_TOOM42_TO_TOOM53_THRESHOLD = 100
const mvMUL_TOOM42_TO_TOOM63_THRESHOLD = 110
const mvMUL_TOOM43_TO_TOOM54_THRESHOLD = 150
const mvMUL_TOOM44_THRESHOLD = 300
const mvMUL_TOOM6H_THRESHOLD = 350
const mvMUL_TOOM8H_THRESHOLD = 450
const mvMUPI_DIV_QR_THRESHOLD = 200
const mvMU_BDIV_QR_THRESHOLD = 2000
const mvMU_BDIV_Q_THRESHOLD = 2000
const mvMU_DIVAPPR_Q_THRESHOLD = 2000
const mvMU_DIV_QR_THRESHOLD = 2000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvODD_CENTRAL_BINOMIAL_OFFSET = 13
const mvODD_CENTRAL_BINOMIAL_TABLE_LIMIT = 35
const mvODD_DOUBLEFACTORIAL_TABLE_LIMIT = 33
const mvODD_FACTORIAL_EXTTABLE_LIMIT = 67
const mvODD_FACTORIAL_TABLE_LIMIT = 25
const mvPACKAGE = "gmp"
const mvPACKAGE_BUGREPORT = "gmp-bugs@gmplib.org (see https://gmplib.org/manual/Reporting-Bugs.html)"
const mvPACKAGE_NAME = "GNU MP"
const mvPACKAGE_STRING = "GNU MP 6.3.0"
const mvPACKAGE_TARNAME = "gmp"
const mvPACKAGE_URL = "http://www.gnu.org/software/gmp/"
const mvPACKAGE_VERSION = "6.3.0"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPP_FIRST_OMITTED = 59
const mvPREINV_MOD_1_TO_MOD_1_THRESHOLD = 10
const mvPRIMESIEVE_HIGHEST_PRIME = 5351
const mvPRIMESIEVE_NUMBEROF_TABLE = 28
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT64_MAX"
const mvPTRDIFF_MIN = "INT64_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvREDC_1_TO_REDC_N_THRESHOLD = 100
const mvRETSIGTYPE = "void"
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSET_STR_DC_THRESHOLD = 750
const mvSET_STR_PRECOMPUTE_THRESHOLD = 2000
const mvSHRT_HIGHBIT = "SHRT_MIN"
const mvSHRT_MAX = 0x7fff
const mvSIEVESIZE = 512
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZEOF_MP_LIMB_T = 8
const mvSIZEOF_UNSIGNED = 4
const mvSIZEOF_UNSIGNED_LONG = 8
const mvSIZEOF_UNSIGNED_SHORT = 2
const mvSIZEOF_VOID_P = 8
const mvSIZE_LOG = 10
const mvSIZE_MAX = "UINT64_MAX"
const mvSQRLO_BASECASE_THRESHOLD = 0
const mvSQRLO_BASECASE_THRESHOLD_LIMIT = "SQRLO_BASECASE_THRESHOLD"
const mvSQRLO_DC_THRESHOLD = "MULLO_DC_THRESHOLD"
const mvSQRLO_DC_THRESHOLD_LIMIT = "SQRLO_DC_THRESHOLD"
const mvSQRLO_SQR_THRESHOLD = "MULLO_MUL_N_THRESHOLD"
const mvSQRMOD_BNM1_THRESHOLD = 16
const mvSQR_BASECASE_THRESHOLD = 0
const mvSQR_TOOM2_THRESHOLD = 50
const mvSQR_TOOM3_THRESHOLD = 120
const mvSQR_TOOM3_THRESHOLD_LIMIT = "SQR_TOOM3_THRESHOLD"
const mvSQR_TOOM4_THRESHOLD = 400
const mvSQR_TOOM6_THRESHOLD = "MUL_TOOM6H_THRESHOLD"
const mvSQR_TOOM8_THRESHOLD = "MUL_TOOM8H_THRESHOLD"
const mvSSIZE_MAX = "LONG_MAX"
const mvSTDC_HEADERS = 1
const mvSYMLOOP_MAX = 40
const mvTABLE_LIMIT_2N_MINUS_POPC_2N = 81
const mvTARGET_REGISTER_STARVED = 0
const mvTIME_WITH_SYS_TIME = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTUNE_SQR_TOOM2_MAX = "SQR_TOOM2_MAX_GENERIC"
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT64_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSE_LEADING_REGPARM = 0
const mvUSE_PREINV_DIVREM_1 = 1
const mvUSHRT_MAX = 65535
const mvVERSION = "6.3.0"
const mvWANT_FFT = 1
const mvWANT_TMP_ALLOCA = 1
const mvWANT_TMP_DEBUG = 0
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_LIMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_IEEE_FLOATS = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__AARCH64EL__ = 1
const mv__AARCH64_CMODEL_SMALL__ = 1
const mv__ARM_64BIT_STATE = 1
const mv__ARM_ALIGN_MAX_PWR = 28
const mv__ARM_ALIGN_MAX_STACK_PWR = 16
const mv__ARM_ARCH = 8
const mv__ARM_ARCH_8A = 1
const mv__ARM_ARCH_ISA_A64 = 1
const mv__ARM_ARCH_PROFILE = 65
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_FMA = 1
const mv__ARM_FEATURE_IDIV = 1
const mv__ARM_FEATURE_NUMERIC_MAXMIN = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 14
const mv__ARM_FP16_ARGS = 1
const mv__ARM_FP16_FORMAT_IEEE = 1
const mv__ARM_NEON = 1
const mv__ARM_PCS_AAPCS64 = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 36
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 2
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_C99__ = 0
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 256
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 36
const mv__LDBL_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__LDBL_DIG__ = 33
const mv__LDBL_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 113
const mv__LDBL_MAX_10_EXP__ = 4932
const mv__LDBL_MAX_EXP__ = 16384
const mv__LDBL_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LDBL_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__LDBL_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 0x7fffffffffffffff
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "l"
const mv__PRIPTR = "l"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__aarch64__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_newalloc = "_mpz_realloc"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvbinvert_limb_table = "__gmp_binvert_limb_table"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_init_primesieve = "__gmp_init_primesieve"
const mvgmp_nextprime = "__gmp_nextprime"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_primesieve = "__gmp_primesieve"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvjacobi_table = "__gmp_jacobi_table"
const mvlinux = 1
const mvmodlimb_invert = "binvert_limb"
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpn_fft_mul = "mpn_nussbaumer_mul"
const mvmpn_get_d = "__gmpn_get_d"
const mvmpn_toomMN_mul = "mpn_toom33_mul"
const mvmpn_toomMN_mul_itch = "mpn_toom33_mul_itch"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_gcd = "__gmpz_divexact_gcd"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_inp_str_nowhite = "__gmpz_inp_str_nowhite"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucas_mod = "__gmpz_lucas_mod"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_n_pow_ui = "__gmpz_n_pow_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_oddfac_1 = "__gmpz_oddfac_1"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_prodlimbs = "__gmpz_prodlimbs"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_stronglucas = "__gmpz_stronglucas"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvrestrict = "__restrict"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint64

type tnwchar_t = ppuint32

type tnsize_t = ppuint64

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

type tnuintptr_t = ppuint64

type tnintptr_t = ppint64

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tngmp_uint_least32_t = ppuint32

type tngmp_intptr_t = ppint64

type tngmp_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tngmp_pi2_t = struct {
	fdinv21 tnmp_limb_t
	fdinv32 tnmp_limb_t
	fdinv53 tnmp_limb_t
}

type tutmp_align_t = struct {
	fdd [0]ppfloat64
	fdp [0]ppuintptr
	fdl tnmp_limb_t
}

type tstmp_reentrant_t = struct {
	fdnext ppuintptr
	fdsize tnsize_t
}

type tngmp_randfnptr_t = struct {
	fdrandseed_fn  ppuintptr
	fdrandget_fn   ppuintptr
	fdrandclear_fn ppuintptr
	fdrandiset_fn  ppuintptr
}

type tetoom6_flags = ppint32

const ectoom6_all_pos = 0
const ectoom6_vm1_neg = 1
const ectoom6_vm2_neg = 2

type tetoom7_flags = ppint32

const ectoom7_w1_neg = 1
const ectoom7_w3_neg = 2

type tngmp_primesieve_t = struct {
	fdd       ppuint64
	fds0      ppuint64
	fdsqrt_s0 ppuint64
	fds       [513]ppuint8
}

type tsfft_table_nk = struct {
	fd__ccgo0 uint32
}

type tsbases = struct {
	fdchars_per_limb    ppint32
	fdlogb2             tnmp_limb_t
	fdlog2b             tnmp_limb_t
	fdbig_base          tnmp_limb_t
	fdbig_base_inverted tnmp_limb_t
}

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tuieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type tnmp_double_limb_t = struct {
	fdd0 tnmp_limb_t
	fdd1 tnmp_limb_t
}

type tshgcd_matrix1 = struct {
	fdu [2][2]tnmp_limb_t
}

type tshgcd_matrix = struct {
	fdalloc tnmp_size_t
	fdn     tnmp_size_t
	fdp     [2][2]tnmp_ptr
}

type tsgcdext_ctx = struct {
	fdgp    tnmp_ptr
	fdgn    tnmp_size_t
	fdup    tnmp_ptr
	fdusize ppuintptr
	fdun    tnmp_size_t
	fdu0    tnmp_ptr
	fdu1    tnmp_ptr
	fdtp    tnmp_ptr
}

type tspowers = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tnpowers_t = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tsdoprnt_params_t = struct {
	fdbase         ppint32
	fdconv         ppint32
	fdexpfmt       ppuintptr
	fdexptimes4    ppint32
	fdfill         ppuint8
	fdjustify      ppint32
	fdprec         ppint32
	fdshowbase     ppint32
	fdshowpoint    ppint32
	fdshowtrailing ppint32
	fdsign         ppuint8
	fdwidth        ppint32
}

type tngmp_doscan_scan_t = ppuintptr

type tngmp_doscan_step_t = ppuintptr

type tngmp_doscan_get_t = ppuintptr

type tngmp_doscan_unget_t = ppuintptr

type tsgmp_doscan_funs_t = struct {
	fdscan  tngmp_doscan_scan_t
	fdstep  tngmp_doscan_step_t
	fdget   tngmp_doscan_get_t
	fdunget tngmp_doscan_unget_t
}

type tn__jmp_buf = [22]ppuint64

type tnjmp_buf = [1]ts__jmp_buf_tag

type ts__jmp_buf_tag = struct {
	fd__jb tn__jmp_buf
	fd__fl ppuint64
	fd__ss [16]ppuint64
}

type tnsigjmp_buf = [1]ts__jmp_buf_tag

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)
	var aa__gmp_i, aaan, aabn, aaitch, ccv10 tnmp_size_t
	var aa__gmp_result, aacount, aareps_nondefault, aatest, ccv1, ccv11, ccv12 ppint32
	var aa__gmp_x, aa__gmp_y tnmp_limb_t
	var aaap, aabp, aapp, aarefp, aascratch tnmp_ptr
	var aaenvval, ccv3, ccv4, ccv5, ccv6, ccv7 ppuintptr
	var aarands tngmp_randstate_ptr
	var aarepfactor ppfloat64
	var aasize_min, aasize_range ppuint32
	var ccv14 ppbool
	var pp_ /* __tmp_marker at bp+0 */ ppuintptr
	var pp_ /* end at bp+8 */ ppuintptr
	var pp_ /* p_after at bp+24 */ tnmp_limb_t
	var pp_ /* p_before at bp+16 */ tnmp_limb_t
	var pp_ /* s_after at bp+40 */ tnmp_limb_t
	var pp_ /* s_before at bp+32 */ tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__gmp_i, aa__gmp_result, aa__gmp_x, aa__gmp_y, aaan, aaap, aabn, aabp, aacount, aaenvval, aaitch, aapp, aarands, aarefp, aarepfactor, aareps_nondefault, aascratch, aasize_min, aasize_range, aatest, ccv1, ccv10, ccv11, ccv12, ccv14, ccv3, ccv4, ccv5, ccv6, ccv7
	cgtls.AllocaEntry()
	defer cgtls.AllocaExit()
	aacount = ppint32(mvCOUNT)
	*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) = ppuintptr(0)
	aareps_nondefault = 0
	if aaargc > ppint32(1) {
		aacount = ppint32(Xstrtol(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8)), cgbp+8, 0))
		if *(*ppuint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 8)))) != 0 || aacount <= 0 {
			Xfprintf(cgtls, Xstderr, "Invalid test count: %s.\n\x00", iqlibc.ppVaList(cgbp+56, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8))))
			Xexit(cgtls, ppint32(1))
		}
		aaargv += 8
		aaargc--
		aareps_nondefault = ppint32(1)
	}
	aaenvval = Xgetenv(cgtls, "GMP_CHECK_REPFACTOR\x00")
	if aaenvval != iqlibc.ppUintptrFromInt32(0) {
		aarepfactor = Xstrtod(cgtls, aaenvval, cgbp+8)
		if *(*ppuint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 8)))) != 0 || aarepfactor <= iqlibc.ppFloat64FromInt32(0) {
			Xfprintf(cgtls, Xstderr, "Invalid repfactor: %f.\n\x00", iqlibc.ppVaList(cgbp+56, aarepfactor))
			Xexit(cgtls, ppint32(1))
		}
		aacount = ppint32(ppfloat64(aacount) * aarepfactor)
		if aacount > ppint32(1) {
			ccv1 = aacount
		} else {
			ccv1 = ppint32(1)
		}
		aacount = ccv1
		aareps_nondefault = ppint32(1)
	}
	if aareps_nondefault != 0 {
		Xprintf(cgtls, "Running test with %ld repetitions (include this in bug reports)\n\x00", iqlibc.ppVaList(cgbp+56, ppint64(aacount)))
	}
	Xtests_start(cgtls)
	if !(X__gmp_rands_initialized != 0) {
		X__gmp_rands_initialized = ppuint8(1)
		X__gmp_randinit_mt_noseed(cgtls, ppuintptr(iqunsafe.ppPointer(&X__gmp_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	aarands = ppuintptr(iqunsafe.ppPointer(&X__gmp_rands))
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))*iqlibc.ppUint64FromInt64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv3 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))*iqlibc.ppUint64FromInt64(8))
	} else {
		ccv3 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))*iqlibc.ppUint64FromInt64(8))
	}
	aaap = ccv3
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))*iqlibc.ppUint64FromInt64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv4 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))*iqlibc.ppUint64FromInt64(8))
	} else {
		ccv4 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))*iqlibc.ppUint64FromInt64(8))
	}
	aabp = ccv4
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))*iqlibc.ppUint64FromInt64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv5 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))*iqlibc.ppUint64FromInt64(8))
	} else {
		ccv5 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))*iqlibc.ppUint64FromInt64(8))
	}
	aarefp = ccv5
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt64FromInt32(2))*iqlibc.ppUint64FromInt64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv6 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt64FromInt32(2))*iqlibc.ppUint64FromInt64(8))
	} else {
		ccv6 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt64FromInt32(2))*iqlibc.ppUint64FromInt64(8))
	}
	aapp = ppuintptr(1)*8 + ccv6
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt32(3)*(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))+ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(2))*iqlibc.ppUint64FromInt64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv7 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt32(3)*(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))+ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(2))*iqlibc.ppUint64FromInt64(8))
	} else {
		ccv7 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt32(3)*(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))+ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(2))*iqlibc.ppUint64FromInt64(8))
	}
	aascratch = ppuintptr(1)*8 + ccv7
	aatest = 0
	for {
		if !(aatest < aacount) {
			break
		}
		aasize_min = ppuint32(1)
		for {
			if !(ppint64(1)<<aasize_min < ppint64(mvMUL_TOOM33_THRESHOLD)) {
				break
			}
			goto cg_9
		cg_9:
			;
			aasize_min++
		}
		aasize_range = ppuint32(ppuint64(aasize_min) + X__gmp_urandomm_ui(cgtls, aarands, ppuint64(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvSIZE_LOG)+iqlibc.ppInt32FromInt32(1))-aasize_min)))
		aaan = iqlibc.ppInt64FromUint64(ppuint64(mvMUL_TOOM33_THRESHOLD) + X__gmp_urandomm_ui(cgtls, aarands, iqlibc.ppUint64FromInt64(ppint64(1)<<aasize_range+ppint64(1)-ppint64(mvMUL_TOOM33_THRESHOLD))))
		aabn = iqlibc.ppInt64FromUint64(ppuint64(1) + ppuint64(2)*(iqlibc.ppUint64FromInt64(aaan+iqlibc.ppInt64FromInt32(2))/iqlibc.ppUint64FromInt32(3)) + X__gmp_urandomm_ui(cgtls, aarands, iqlibc.ppUint64FromInt64(aaan+ppint64(1))-(ppuint64(1)+ppuint64(2)*(iqlibc.ppUint64FromInt64(aaan+iqlibc.ppInt64FromInt32(2))/iqlibc.ppUint64FromInt32(3)))))
		X__gmpn_random2(cgtls, aaap, aaan)
		X__gmpn_random2(cgtls, aabp, aabn)
		X__gmpn_random2(cgtls, aapp-ppuintptr(1)*8, aaan+aabn+ppint64(2))
		*(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 16)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aapp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*8))
		*(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 24)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aapp + ppuintptr(aaan+aabn)*8))
		aaitch = iqlibc.ppInt64FromInt32(3)*aaan + ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aaitch <= iqlibc.ppInt64FromInt32(3)*(iqlibc.ppInt64FromInt64(1)<<iqlibc.ppInt32FromInt32(mvSIZE_LOG))+ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "toom-shared.h\x00", ppint32(103), "itch <= (3 * ((1L << 10)) + (64 - 0))\x00")
		}
		X__gmpn_random2(cgtls, aascratch-ppuintptr(1)*8, aaitch+ppint64(2))
		*(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 32)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(-iqlibc.ppInt32FromInt32(1))*8))
		*(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 40)) = *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*8))
		X__gmpn_toom33_mul(cgtls, aapp, aaap, aaan, aabp, aabn, aascratch)
		Xrefmpn_mul(cgtls, aarefp, aaap, aaan, aabp, aabn)
		if ccv14 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aapp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 16)) || *(*tnmp_limb_t)(iqunsafe.ppPointer(aapp + ppuintptr(aaan+aabn)*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 24)) || *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(-iqlibc.ppInt32FromInt32(1))*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 32)) || *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 40)); !ccv14 {
			aa__gmp_result = 0
			aa__gmp_i = aaan + aabn
			for {
				aa__gmp_i--
				ccv10 = aa__gmp_i
				if !(ccv10 >= iqlibc.ppInt64FromInt32(0)) {
					break
				}
				aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(aarefp + ppuintptr(aa__gmp_i)*8))
				aa__gmp_y = *(*tnmp_limb_t)(iqunsafe.ppPointer(aapp + ppuintptr(aa__gmp_i)*8))
				if aa__gmp_x != aa__gmp_y {
					if aa__gmp_x > aa__gmp_y {
						ccv11 = ppint32(1)
					} else {
						ccv11 = -ppint32(1)
					}
					aa__gmp_result = ccv11
					break
				}
			}
			ccv12 = aa__gmp_result
			goto cg_13
		cg_13:
		}
		if ccv14 || ccv12 != 0 {
			Xprintf(cgtls, "ERROR in test %d, an = %d, bn = %d\n\x00", iqlibc.ppVaList(cgbp+56, aatest, ppint32(aaan), ppint32(aabn)))
			if *(*tnmp_limb_t)(iqunsafe.ppPointer(aapp + ppuintptr(-iqlibc.ppInt32FromInt32(1))*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 16)) {
				Xprintf(cgtls, "before pp:\x00", 0)
				X__gmpn_dump(cgtls, aapp-ppuintptr(1)*8, ppint64(1))
				Xprintf(cgtls, "keep:   \x00", 0)
				X__gmpn_dump(cgtls, cgbp+16, ppint64(1))
			}
			if *(*tnmp_limb_t)(iqunsafe.ppPointer(aapp + ppuintptr(aaan+aabn)*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 24)) {
				Xprintf(cgtls, "after pp:\x00", 0)
				X__gmpn_dump(cgtls, aapp+ppuintptr(aaan)*8+ppuintptr(aabn)*8, ppint64(1))
				Xprintf(cgtls, "keep:   \x00", 0)
				X__gmpn_dump(cgtls, cgbp+24, ppint64(1))
			}
			if *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(-iqlibc.ppInt32FromInt32(1))*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 32)) {
				Xprintf(cgtls, "before scratch:\x00", 0)
				X__gmpn_dump(cgtls, aascratch-ppuintptr(1)*8, ppint64(1))
				Xprintf(cgtls, "keep:   \x00", 0)
				X__gmpn_dump(cgtls, cgbp+32, ppint64(1))
			}
			if *(*tnmp_limb_t)(iqunsafe.ppPointer(aascratch + ppuintptr(aaitch)*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 40)) {
				Xprintf(cgtls, "after scratch:\x00", 0)
				X__gmpn_dump(cgtls, aascratch+ppuintptr(aaitch)*8, ppint64(1))
				Xprintf(cgtls, "keep:   \x00", 0)
				X__gmpn_dump(cgtls, cgbp+40, ppint64(1))
			}
			X__gmpn_dump(cgtls, aaap, aaan)
			X__gmpn_dump(cgtls, aabp, aabn)
			X__gmpn_dump(cgtls, aapp, aaan+aabn)
			X__gmpn_dump(cgtls, aarefp, aaan+aabn)
			Xabort(cgtls)
		}
		goto cg_8
	cg_8:
		;
		aatest++
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) != ppuintptr(0)) != 0), 0) != 0 {
		X__gmp_tmp_reentrant_free(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp)))
	}
	Xtests_end(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_alloca(*iqlibc.ppTLS, ppuint64) ppuintptr

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___gmp_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func ___gmp_randinit_mt_noseed(*iqlibc.ppTLS, ppuintptr)

var ___gmp_rands [1]tn__gmp_randstate_struct

var ___gmp_rands_initialized ppuint8

func ___gmp_tmp_reentrant_alloc(*iqlibc.ppTLS, ppuintptr, ppuint64) ppuintptr

func ___gmp_tmp_reentrant_free(*iqlibc.ppTLS, ppuintptr)

func ___gmp_urandomm_ui(*iqlibc.ppTLS, ppuintptr, ppuint64) ppuint64

func ___gmpn_dump(*iqlibc.ppTLS, ppuintptr, ppint64)

func ___gmpn_random2(*iqlibc.ppTLS, ppuintptr, ppint64)

func ___gmpn_toom33_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppuintptr, ppint64, ppuintptr)

func _abort(*iqlibc.ppTLS)

func _exit(*iqlibc.ppTLS, ppint32)

func _fprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _refmpn_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppuintptr, ppint64)

var _stderr ppuintptr

func _strtod(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppfloat64

func _strtol(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint64

func _tests_end(*iqlibc.ppTLS)

func _tests_start(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
