// Code generated for linux/s390x by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -DHAVE_CONFIG_H -I. -I.. -I.. -DNDEBUG -mlong-double-64 -c misc.c -o misc.o.go', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvASSERT_FILE = "__FILE__"
const mvASSERT_LINE = "__LINE__"
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBINV_NEWTON_THRESHOLD = 300
const mvBMOD_1_TO_MOD_1_THRESHOLD = 10
const mvBUFSIZ = 1024
const mvBUS_ADRALN = 1
const mvBUS_ADRERR = 2
const mvBUS_MCEERR_AO = 5
const mvBUS_MCEERR_AR = 4
const mvBUS_OBJERR = 3
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCLD_CONTINUED = 6
const mvCLD_DUMPED = 3
const mvCLD_EXITED = 1
const mvCLD_KILLED = 2
const mvCLD_STOPPED = 5
const mvCLD_TRAPPED = 4
const mvCLOCKS_PER_SEC = 1000000
const mvCLOCK_BOOTTIME = 7
const mvCLOCK_BOOTTIME_ALARM = 9
const mvCLOCK_MONOTONIC = 1
const mvCLOCK_MONOTONIC_COARSE = 6
const mvCLOCK_MONOTONIC_RAW = 4
const mvCLOCK_PROCESS_CPUTIME_ID = 2
const mvCLOCK_REALTIME = 0
const mvCLOCK_REALTIME_ALARM = 8
const mvCLOCK_REALTIME_COARSE = 5
const mvCLOCK_SGI_CYCLE = 10
const mvCLOCK_TAI = 11
const mvCLOCK_THREAD_CPUTIME_ID = 3
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDC_BDIV_Q_THRESHOLD = 180
const mvDC_DIVAPPR_Q_THRESHOLD = 200
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvDIVEXACT_1_THRESHOLD = 0
const mvDIVEXACT_BY3_METHOD = 0
const mvDIVEXACT_JEB_THRESHOLD = 25
const mvDOPRNT_CONV_FIXED = 1
const mvDOPRNT_CONV_GENERAL = 3
const mvDOPRNT_CONV_SCIENTIFIC = 2
const mvDOPRNT_JUSTIFY_INTERNAL = 3
const mvDOPRNT_JUSTIFY_LEFT = 1
const mvDOPRNT_JUSTIFY_NONE = 0
const mvDOPRNT_JUSTIFY_RIGHT = 2
const mvDOPRNT_SHOWBASE_NO = 2
const mvDOPRNT_SHOWBASE_NONZERO = 3
const mvDOPRNT_SHOWBASE_YES = 1
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFAC_DSC_THRESHOLD = 400
const mvFAC_ODD_THRESHOLD = 35
const mvFD_SETSIZE = 1024
const mvFFT_FIRST_K = 4
const mvFIB_TABLE_LIMIT = 93
const mvFIB_TABLE_LUCNUM_LIMIT = 92
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvFPE_FLTDIV = 3
const mvFPE_FLTINV = 7
const mvFPE_FLTOVF = 4
const mvFPE_FLTRES = 6
const mvFPE_FLTSUB = 8
const mvFPE_FLTUND = 5
const mvFPE_INTDIV = 1
const mvFPE_INTOVF = 2
const mvGCDEXT_DC_THRESHOLD = 600
const mvGCD_DC_THRESHOLD = 1000
const mvGET_STR_DC_THRESHOLD = 18
const mvGET_STR_PRECOMPUTE_THRESHOLD = 35
const mvGMP_LIMB_BITS = 64
const mvGMP_LIMB_BYTES = "SIZEOF_MP_LIMB_T"
const mvGMP_MPARAM_H_SUGGEST = "./mpn/generic/gmp-mparam.h"
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_ALARM = 1
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_CONST = 1
const mvHAVE_ATTRIBUTE_MALLOC = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_ATTRIBUTE_NORETURN = 1
const mvHAVE_CLOCK = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_CONFIG_H = 1
const mvHAVE_DECL_FGETC = 1
const mvHAVE_DECL_FSCANF = 1
const mvHAVE_DECL_OPTARG = 1
const mvHAVE_DECL_SYS_ERRLIST = 1
const mvHAVE_DECL_SYS_NERR = 1
const mvHAVE_DECL_UNGETC = 1
const mvHAVE_DECL_VFPRINTF = 1
const mvHAVE_DLFCN_H = 1
const mvHAVE_DOUBLE_IEEE_BIG_ENDIAN = 1
const mvHAVE_FCNTL_H = 1
const mvHAVE_FLOAT_H = 1
const mvHAVE_GETPAGESIZE = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_HIDDEN_ALIAS = 1
const mvHAVE_HOST_CPU_s390_z15 = 1
const mvHAVE_HOST_CPU_s390_zarch = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTPTR_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LANGINFO_H = 1
const mvHAVE_LIMB_BIG_ENDIAN = 1
const mvHAVE_LOCALECONV = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_DOUBLE = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_MEMORY_H = 1
const mvHAVE_MEMSET = 1
const mvHAVE_MMAP = 1
const mvHAVE_MPROTECT = 1
const mvHAVE_NL_LANGINFO = 1
const mvHAVE_NL_TYPES_H = 1
const mvHAVE_POPEN = 1
const mvHAVE_PTRDIFF_T = 1
const mvHAVE_QUAD_T = 1
const mvHAVE_RAISE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGALTSTACK = 1
const mvHAVE_SIGSTACK = 1
const mvHAVE_STACK_T = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDLIB_H = 1
const mvHAVE_STRCHR = 1
const mvHAVE_STRERROR = 1
const mvHAVE_STRINGS_H = 1
const mvHAVE_STRING_H = 1
const mvHAVE_STRNLEN = 1
const mvHAVE_STRTOL = 1
const mvHAVE_STRTOUL = 1
const mvHAVE_SYSCONF = 1
const mvHAVE_SYSCTL = 1
const mvHAVE_SYS_MMAN_H = 1
const mvHAVE_SYS_PARAM_H = 1
const mvHAVE_SYS_RESOURCE_H = 1
const mvHAVE_SYS_STAT_H = 1
const mvHAVE_SYS_SYSCTL_H = 1
const mvHAVE_SYS_SYSINFO_H = 1
const mvHAVE_SYS_TIMES_H = 1
const mvHAVE_SYS_TIME_H = 1
const mvHAVE_SYS_TYPES_H = 1
const mvHAVE_TIMES = 1
const mvHAVE_UINT_LEAST32_T = 1
const mvHAVE_UNISTD_H = 1
const mvHAVE_VSNPRINTF = 1
const mvHGCD_APPR_THRESHOLD = 400
const mvHGCD_REDUCE_THRESHOLD = 1000
const mvHGCD_THRESHOLD = 400
const mvHOST_NAME_MAX = 255
const mvILL_BADSTK = 8
const mvILL_COPROC = 7
const mvILL_ILLADR = 3
const mvILL_ILLOPC = 1
const mvILL_ILLOPN = 2
const mvILL_ILLTRP = 4
const mvILL_PRVOPC = 5
const mvILL_PRVREG = 6
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT64_MAX"
const mvINTPTR_MIN = "INT64_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_HIGHBIT = "INT_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvINV_APPR_THRESHOLD = "INV_NEWTON_THRESHOLD"
const mvINV_NEWTON_THRESHOLD = 200
const mvIOV_MAX = 1024
const mvITIMER_PROF = 2
const mvITIMER_REAL = 0
const mvITIMER_VIRTUAL = 1
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLIMBS_PER_ULONG = 1
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_HIGHBIT = "LONG_MIN"
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMATRIX22_STRASSEN_THRESHOLD = 30
const mvMB_LEN_MAX = 4
const mvMINSIGSTKSZ = 4096
const mvMOD_BKNP1_ONLY3 = 0
const mvMPN_FFT_TABLE_SIZE = 16
const mvMPN_TOOM22_MUL_MINSIZE = 6
const mvMPN_TOOM2_SQR_MINSIZE = 4
const mvMPN_TOOM32_MUL_MINSIZE = 10
const mvMPN_TOOM33_MUL_MINSIZE = 17
const mvMPN_TOOM3_SQR_MINSIZE = 17
const mvMPN_TOOM42_MULMID_MINSIZE = 4
const mvMPN_TOOM42_MUL_MINSIZE = 10
const mvMPN_TOOM43_MUL_MINSIZE = 25
const mvMPN_TOOM44_MUL_MINSIZE = 30
const mvMPN_TOOM4_SQR_MINSIZE = 30
const mvMPN_TOOM53_MUL_MINSIZE = 17
const mvMPN_TOOM54_MUL_MINSIZE = 31
const mvMPN_TOOM63_MUL_MINSIZE = 49
const mvMPN_TOOM6H_MUL_MINSIZE = 46
const mvMPN_TOOM6_SQR_MINSIZE = 46
const mvMPN_TOOM8H_MUL_MINSIZE = 86
const mvMPN_TOOM8_SQR_MINSIZE = 86
const mvMP_BASES_BIG_BASE_CTZ_10 = 19
const mvMP_BASES_CHARS_PER_LIMB_10 = 19
const mvMP_BASES_NORMALIZATION_STEPS_10 = 0
const mvMP_EXP_T_MAX = "MP_SIZE_T_MAX"
const mvMP_EXP_T_MIN = "MP_SIZE_T_MIN"
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMULLO_BASECASE_THRESHOLD = 0
const mvMULLO_BASECASE_THRESHOLD_LIMIT = "MULLO_BASECASE_THRESHOLD"
const mvMULMID_TOOM42_THRESHOLD = "MUL_TOOM22_THRESHOLD"
const mvMULMOD_BNM1_THRESHOLD = 16
const mvMUL_TOOM22_THRESHOLD = 30
const mvMUL_TOOM22_THRESHOLD_LIMIT = "MUL_TOOM22_THRESHOLD"
const mvMUL_TOOM32_TO_TOOM43_THRESHOLD = 100
const mvMUL_TOOM32_TO_TOOM53_THRESHOLD = 110
const mvMUL_TOOM33_THRESHOLD = 100
const mvMUL_TOOM33_THRESHOLD_LIMIT = "MUL_TOOM33_THRESHOLD"
const mvMUL_TOOM42_TO_TOOM53_THRESHOLD = 100
const mvMUL_TOOM42_TO_TOOM63_THRESHOLD = 110
const mvMUL_TOOM43_TO_TOOM54_THRESHOLD = 150
const mvMUL_TOOM44_THRESHOLD = 300
const mvMUL_TOOM6H_THRESHOLD = 350
const mvMUL_TOOM8H_THRESHOLD = 450
const mvMUPI_DIV_QR_THRESHOLD = 200
const mvMU_BDIV_QR_THRESHOLD = 2000
const mvMU_BDIV_Q_THRESHOLD = 2000
const mvMU_DIVAPPR_Q_THRESHOLD = 2000
const mvMU_DIV_QR_THRESHOLD = 2000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNSIG = "_NSIG"
const mvNZERO = 20
const mvODD_CENTRAL_BINOMIAL_OFFSET = 13
const mvODD_CENTRAL_BINOMIAL_TABLE_LIMIT = 35
const mvODD_DOUBLEFACTORIAL_TABLE_LIMIT = 33
const mvODD_FACTORIAL_EXTTABLE_LIMIT = 67
const mvODD_FACTORIAL_TABLE_LIMIT = 25
const mvPACKAGE = "gmp"
const mvPACKAGE_BUGREPORT = "gmp-bugs@gmplib.org (see https://gmplib.org/manual/Reporting-Bugs.html)"
const mvPACKAGE_NAME = "GNU MP"
const mvPACKAGE_STRING = "GNU MP 6.3.0"
const mvPACKAGE_TARNAME = "gmp"
const mvPACKAGE_URL = "http://www.gnu.org/software/gmp/"
const mvPACKAGE_VERSION = "6.3.0"
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPOLL_ERR = 4
const mvPOLL_HUP = 6
const mvPOLL_IN = 1
const mvPOLL_MSG = 3
const mvPOLL_OUT = 2
const mvPOLL_PRI = 5
const mvPP_FIRST_OMITTED = 59
const mvPREINV_MOD_1_TO_MOD_1_THRESHOLD = 10
const mvPRIMESIEVE_HIGHEST_PRIME = 5351
const mvPRIMESIEVE_NUMBEROF_TABLE = 28
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT64_MAX"
const mvPTRDIFF_MIN = "INT64_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvREDC_1_TO_REDC_N_THRESHOLD = 100
const mvRETSIGTYPE = "void"
const mvRE_DUP_MAX = 255
const mvSA_EXPOSE_TAGBITS = 0x00000800
const mvSA_NOCLDSTOP = 1
const mvSA_NOCLDWAIT = 2
const mvSA_NODEFER = 0x40000000
const mvSA_NOMASK = "SA_NODEFER"
const mvSA_ONESHOT = "SA_RESETHAND"
const mvSA_ONSTACK = 0x08000000
const mvSA_RESETHAND = 0x80000000
const mvSA_RESTART = 0x10000000
const mvSA_RESTORER = 0x04000000
const mvSA_SIGINFO = 4
const mvSA_UNSUPPORTED = 0x00000400
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEGV_ACCERR = 2
const mvSEGV_BNDERR = 3
const mvSEGV_MAPERR = 1
const mvSEGV_MTEAERR = 8
const mvSEGV_MTESERR = 9
const mvSEGV_PKUERR = 4
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSET_STR_DC_THRESHOLD = 750
const mvSET_STR_PRECOMPUTE_THRESHOLD = 2000
const mvSHRT_HIGHBIT = "SHRT_MIN"
const mvSHRT_MAX = 0x7fff
const mvSIEVESIZE = 512
const mvSIGABRT = 6
const mvSIGALRM = 14
const mvSIGBUS = 7
const mvSIGCHLD = 17
const mvSIGCONT = 18
const mvSIGEV_NONE = 1
const mvSIGEV_SIGNAL = 0
const mvSIGEV_THREAD = 2
const mvSIGEV_THREAD_ID = 4
const mvSIGFPE = 8
const mvSIGHUP = 1
const mvSIGILL = 4
const mvSIGINT = 2
const mvSIGIO = 29
const mvSIGIOT = "SIGABRT"
const mvSIGKILL = 9
const mvSIGPIPE = 13
const mvSIGPOLL = "SIGIO"
const mvSIGPROF = 27
const mvSIGPWR = 30
const mvSIGQUIT = 3
const mvSIGSEGV = 11
const mvSIGSTKFLT = 16
const mvSIGSTKSZ = 10240
const mvSIGSTOP = 19
const mvSIGSYS = 31
const mvSIGTERM = 15
const mvSIGTRAP = 5
const mvSIGTSTP = 20
const mvSIGTTIN = 21
const mvSIGTTOU = 22
const mvSIGUNUSED = "SIGSYS"
const mvSIGURG = 23
const mvSIGUSR1 = 10
const mvSIGUSR2 = 12
const mvSIGVTALRM = 26
const mvSIGWINCH = 28
const mvSIGXCPU = 24
const mvSIGXFSZ = 25
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIG_BLOCK = 0
const mvSIG_SETMASK = 2
const mvSIG_UNBLOCK = 1
const mvSIZEOF_MP_LIMB_T = 8
const mvSIZEOF_UNSIGNED = 4
const mvSIZEOF_UNSIGNED_LONG = 8
const mvSIZEOF_UNSIGNED_SHORT = 2
const mvSIZEOF_VOID_P = 8
const mvSIZE_MAX = "UINT64_MAX"
const mvSI_KERNEL = 128
const mvSI_USER = 0
const mvSQRLO_BASECASE_THRESHOLD = 0
const mvSQRLO_BASECASE_THRESHOLD_LIMIT = "SQRLO_BASECASE_THRESHOLD"
const mvSQRLO_DC_THRESHOLD = "MULLO_DC_THRESHOLD"
const mvSQRLO_DC_THRESHOLD_LIMIT = "SQRLO_DC_THRESHOLD"
const mvSQRLO_SQR_THRESHOLD = "MULLO_MUL_N_THRESHOLD"
const mvSQRMOD_BNM1_THRESHOLD = 16
const mvSQR_BASECASE_THRESHOLD = 0
const mvSQR_TOOM2_THRESHOLD = 50
const mvSQR_TOOM3_THRESHOLD = 120
const mvSQR_TOOM3_THRESHOLD_LIMIT = "SQR_TOOM3_THRESHOLD"
const mvSQR_TOOM4_THRESHOLD = 400
const mvSQR_TOOM6_THRESHOLD = "MUL_TOOM6H_THRESHOLD"
const mvSQR_TOOM8_THRESHOLD = "MUL_TOOM8H_THRESHOLD"
const mvSSIZE_MAX = "LONG_MAX"
const mvSS_DISABLE = 2
const mvSS_FLAG_BITS = "SS_AUTODISARM"
const mvSS_ONSTACK = 1
const mvSTDC_HEADERS = 1
const mvSYMLOOP_MAX = 40
const mvSYS_SECCOMP = 1
const mvSYS_USER_DISPATCH = 2
const mvTABLE_LIMIT_2N_MINUS_POPC_2N = 81
const mvTARGET_REGISTER_STARVED = 0
const mvTIMER_ABSTIME = 1
const mvTIME_UTC = 1
const mvTIME_WITH_SYS_TIME = 1
const mvTMP_MAX = 10000
const mvTRAP_BRANCH = 3
const mvTRAP_BRKPT = 1
const mvTRAP_HWBKPT = 4
const mvTRAP_TRACE = 2
const mvTRAP_UNK = 5
const mvTTY_NAME_MAX = 32
const mvTUNE_SQR_TOOM2_MAX = "SQR_TOOM2_MAX_GENERIC"
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT64_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSE_LEADING_REGPARM = 0
const mvUSE_PREINV_DIVREM_1 = 1
const mvUSHRT_MAX = 65535
const mvVERSION = "6.3.0"
const mvWANT_FFT = 1
const mvWANT_TMP_ALLOCA = 1
const mvWANT_TMP_DEBUG = 0
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_LIMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_IEEE_FLOATS = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_NSIG = 65
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ARCH__ = 9
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 8
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 4321
const mv__BYTE_ORDER__ = "__ORDER_BIG_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_BIG_ENDIAN__"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.2204460492503131e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.7976931348623157e+308
const mv__FLT32X_MIN__ = 2.2250738585072014e-308
const mv__FLT32X_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.1920928955078125e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.4028234663852886e+38
const mv__FLT32_MIN__ = 1.1754943508222875e-38
const mv__FLT32_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.2204460492503131e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.7976931348623157e+308
const mv__FLT64_MIN__ = 2.2250738585072014e-308
const mv__FLT64_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.1920928955078125e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.4028234663852886e+38
const mv__FLT_MIN__ = 1.1754943508222875e-38
const mv__FLT_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FP_FAST_FMAL = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG -mlong-double-64"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 1
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC__ = 10
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1014
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.2204460492503131e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.7976931348623157e+308
const mv__LDBL_MIN__ = 2.2250738585072014e-308
const mv__LDBL_NORM_MAX__ = 1.7976931348623157e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 0x7fffffffffffffff
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "l"
const mv__PRIPTR = "l"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "10.2.1 20210110"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__s390__ = 1
const mv__s390x__ = 1
const mv__tm_gmtoff = "tm_gmtoff"
const mv__tm_zone = "tm_zone"
const mv__ucontext = "ucontext"
const mv__unix = 1
const mv__unix__ = 1
const mv__zarch__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_newalloc = "_mpz_realloc"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvbinvert_limb_table = "__gmp_binvert_limb_table"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_init_primesieve = "__gmp_init_primesieve"
const mvgmp_nextprime = "__gmp_nextprime"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_primesieve = "__gmp_primesieve"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvjacobi_table = "__gmp_jacobi_table"
const mvlinux = 1
const mvmodlimb_invert = "binvert_limb"
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpn_fft_mul = "mpn_nussbaumer_mul"
const mvmpn_get_d = "__gmpn_get_d"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_gcd = "__gmpz_divexact_gcd"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_inp_str_nowhite = "__gmpz_inp_str_nowhite"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucas_mod = "__gmpz_lucas_mod"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_n_pow_ui = "__gmpz_n_pow_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_oddfac_1 = "__gmpz_oddfac_1"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_prodlimbs = "__gmpz_prodlimbs"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_stronglucas = "__gmpz_stronglucas"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvrestrict = "__restrict"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnlocale_t = ppuintptr

type tnsize_t = ppuint64

type tntime_t = ppint64

type tnclock_t = ppint64

type tstimespec = struct {
	fdtv_sec  tntime_t
	fdtv_nsec ppint64
}

type tnpid_t = ppint32

type tnuid_t = ppuint32

type tnpthread_t = ppuintptr

type tnsigset_t = struct {
	fd__bits [16]ppuint64
}

type ts__sigset_t = tnsigset_t

type tnpthread_attr_t = struct {
	fd__u struct {
		fd__vi [0][14]ppint32
		fd__s  [0][7]ppuint64
		fd__i  [14]ppint32
	}
}

type tnstack_t = struct {
	fdss_sp    ppuintptr
	fdss_flags ppint32
	fdss_size  tnsize_t
}

type tssigaltstack = tnstack_t

type tngreg_t = ppuint64

type tngregset_t = [27]ppuint64

type tn__psw_t = struct {
	fdmask ppuint64
	fdaddr ppuint64
}

type tnfpreg_t = struct {
	fdf [0]ppfloat32
	fdd ppfloat64
}

type tnfpregset_t = struct {
	fdfpc  ppuint32
	fdfprs [16]tnfpreg_t
}

type tnmcontext_t = struct {
	fdpsw    tn__psw_t
	fdgregs  [16]ppuint64
	fdaregs  [16]ppuint32
	fdfpregs tnfpregset_t
}

type tssigcontext = struct {
	fdoldmask [1]ppuint64
	fdsregs   ppuintptr
}

type tnucontext_t = struct {
	fduc_flags    ppuint64
	fduc_link     ppuintptr
	fduc_stack    tnstack_t
	fduc_mcontext tnmcontext_t
	fduc_sigmask  tnsigset_t
}

type tsucontext = tnucontext_t

type tusigval = struct {
	fdsival_ptr   [0]ppuintptr
	fdsival_int   ppint32
	fd__ccgo_pad2 [4]byte
}

type tnsiginfo_t = struct {
	fdsi_signo    ppint32
	fdsi_errno    ppint32
	fdsi_code     ppint32
	fd__si_fields struct {
		fd__si_common [0]struct {
			fd__first struct {
				fd__timer [0]struct {
					fdsi_timerid ppint32
					fdsi_overrun ppint32
				}
				fd__piduid struct {
					fdsi_pid tnpid_t
					fdsi_uid tnuid_t
				}
			}
			fd__second struct {
				fd__sigchld [0]struct {
					fdsi_status ppint32
					fdsi_utime  tnclock_t
					fdsi_stime  tnclock_t
				}
				fdsi_value    tusigval
				fd__ccgo_pad2 [16]byte
			}
		}
		fd__sigfault [0]struct {
			fdsi_addr     ppuintptr
			fdsi_addr_lsb ppint16
			fd__first     struct {
				fdsi_pkey    [0]ppuint32
				fd__addr_bnd struct {
					fdsi_lower ppuintptr
					fdsi_upper ppuintptr
				}
			}
		}
		fd__sigpoll [0]struct {
			fdsi_band ppint64
			fdsi_fd   ppint32
		}
		fd__sigsys [0]struct {
			fdsi_call_addr ppuintptr
			fdsi_syscall   ppint32
			fdsi_arch      ppuint32
		}
		fd__pad [112]ppuint8
	}
}

type tssigaction = struct {
	fd__sa_handler struct {
		fdsa_sigaction [0]ppuintptr
		fdsa_handler   ppuintptr
	}
	fdsa_mask     tnsigset_t
	fdsa_flags    ppint32
	fdsa_restorer ppuintptr
}

type tssigevent = struct {
	fdsigev_value  tusigval
	fdsigev_signo  ppint32
	fdsigev_notify ppint32
	fd__sev_fields struct {
		fdsigev_notify_thread_id [0]tnpid_t
		fd__sev_thread           [0]struct {
			fdsigev_notify_function   ppuintptr
			fdsigev_notify_attributes ppuintptr
		}
		fd__pad [48]ppuint8
	}
}

type tnsig_t = ppuintptr

type tnsighandler_t = ppuintptr

type tnsig_atomic_t = ppint32

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnsuseconds_t = ppint64

type tstimeval = struct {
	fdtv_sec  tntime_t
	fdtv_usec tnsuseconds_t
}

type tnfd_mask = ppuint64

type tnfd_set = struct {
	fdfds_bits [16]ppuint64
}

type tsitimerval = struct {
	fdit_interval tstimeval
	fdit_value    tstimeval
}

type tstimezone = struct {
	fdtz_minuteswest ppint32
	fdtz_dsttime     ppint32
}

type tntimer_t = ppuintptr

type tnclockid_t = ppint32

type tstm = struct {
	fdtm_sec    ppint32
	fdtm_min    ppint32
	fdtm_hour   ppint32
	fdtm_mday   ppint32
	fdtm_mon    ppint32
	fdtm_year   ppint32
	fdtm_wday   ppint32
	fdtm_yday   ppint32
	fdtm_isdst  ppint32
	fdtm_gmtoff ppint64
	fdtm_zone   ppuintptr
}

type tsitimerspec = struct {
	fdit_interval tstimespec
	fdit_value    tstimespec
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

type tnuintptr_t = ppuint64

type tnintptr_t = ppint64

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tngmp_uint_least32_t = ppuint32

type tngmp_intptr_t = ppint64

type tngmp_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tngmp_pi2_t = struct {
	fdinv21 tnmp_limb_t
	fdinv32 tnmp_limb_t
	fdinv53 tnmp_limb_t
}

type tutmp_align_t = struct {
	fdd [0]ppfloat64
	fdp [0]ppuintptr
	fdl tnmp_limb_t
}

type tstmp_reentrant_t = struct {
	fdnext ppuintptr
	fdsize tnsize_t
}

type tngmp_randfnptr_t = struct {
	fdrandseed_fn  ppuintptr
	fdrandget_fn   ppuintptr
	fdrandclear_fn ppuintptr
	fdrandiset_fn  ppuintptr
}

type tetoom6_flags = ppint32

const ectoom6_all_pos = 0
const ectoom6_vm1_neg = 1
const ectoom6_vm2_neg = 2

type tetoom7_flags = ppint32

const ectoom7_w1_neg = 1
const ectoom7_w3_neg = 2

type tngmp_primesieve_t = struct {
	fdd       ppuint64
	fds0      ppuint64
	fdsqrt_s0 ppuint64
	fds       [513]ppuint8
}

type tsfft_table_nk = struct {
	fd__ccgo0 uint32
}

type tsbases = struct {
	fdchars_per_limb    ppint32
	fdlogb2             tnmp_limb_t
	fdlog2b             tnmp_limb_t
	fdbig_base          tnmp_limb_t
	fdbig_base_inverted tnmp_limb_t
}

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tuieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type tnmp_double_limb_t = struct {
	fdd0 tnmp_limb_t
	fdd1 tnmp_limb_t
}

type tshgcd_matrix1 = struct {
	fdu [2][2]tnmp_limb_t
}

type tshgcd_matrix = struct {
	fdalloc tnmp_size_t
	fdn     tnmp_size_t
	fdp     [2][2]tnmp_ptr
}

type tsgcdext_ctx = struct {
	fdgp    tnmp_ptr
	fdgn    tnmp_size_t
	fdup    tnmp_ptr
	fdusize ppuintptr
	fdun    tnmp_size_t
	fdu0    tnmp_ptr
	fdu1    tnmp_ptr
	fdtp    tnmp_ptr
}

type tspowers = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tnpowers_t = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tsdoprnt_params_t = struct {
	fdbase         ppint32
	fdconv         ppint32
	fdexpfmt       ppuintptr
	fdexptimes4    ppint32
	fdfill         ppuint8
	fdjustify      ppint32
	fdprec         ppint32
	fdshowbase     ppint32
	fdshowpoint    ppint32
	fdshowtrailing ppint32
	fdsign         ppuint8
	fdwidth        ppint32
}

type tngmp_doscan_scan_t = ppuintptr

type tngmp_doscan_step_t = ppuintptr

type tngmp_doscan_get_t = ppuintptr

type tngmp_doscan_unget_t = ppuintptr

type tsgmp_doscan_funs_t = struct {
	fdscan  tngmp_doscan_scan_t
	fdstep  tngmp_doscan_step_t
	fdget   tngmp_doscan_get_t
	fdunget tngmp_doscan_unget_t
}

type tn__jmp_buf = [18]ppuint64

type tnjmp_buf = [1]ts__jmp_buf_tag

type ts__jmp_buf_tag = struct {
	fd__jb tn__jmp_buf
	fd__fl ppuint64
	fd__ss [16]ppuint64
}

type tnsigjmp_buf = [1]ts__jmp_buf_tag

/* Establish ostringstream and istringstream.  Do this here so as to hide
   the conditionals, rather than putting stuff in each test program.

   Oldish versions of g++, like 2.95.2, don't have <sstream>, only
   <strstream>.  Fake up ostringstream and istringstream classes, but not a
   full implementation, just enough for our purposes.  */

// C documentation
//
//	/* The various tests setups and final checks, collected up together. */
func Xtests_start(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var pp_ /* version at bp+0 */ [10]ppuint8
	X__builtin_snprintf(cgtls, cgbp, ppuint64(10), "%u.%u.%u\x00", iqlibc.ppVaList(cgbp+24, ppint32(mv__GNU_MP_VERSION), ppint32(mv__GNU_MP_VERSION_MINOR), mv__GNU_MP_VERSION_PATCHLEVEL))

	if Xstrcmp(cgtls, X__gmp_version, cgbp) != 0 {

		Xfprintf(cgtls, Xstderr, "tests are not linked to the newly compiled library\n\x00", 0)
		Xfprintf(cgtls, Xstderr, "  local version is: %s\n\x00", iqlibc.ppVaList(cgbp+24, cgbp))
		Xfprintf(cgtls, Xstderr, "  linked version is: %s\n\x00", iqlibc.ppVaList(cgbp+24, X__gmp_version))
		Xabort(cgtls)
	}

	/* don't buffer, so output is not lost if a test causes a segv etc */
	Xsetbuf(cgtls, Xstdout, iqlibc.ppUintptrFromInt32(0))
	Xsetbuf(cgtls, Xstderr, iqlibc.ppUintptrFromInt32(0))

	Xtests_memory_start(cgtls)
	Xtests_rand_start(cgtls)
}

func Xtests_end(cgtls *iqlibc.ppTLS) {

	Xtests_rand_end(cgtls)
	Xtests_memory_end(cgtls)
}

func siseed_from_tod(cgtls *iqlibc.ppTLS, aarands tngmp_randstate_ptr) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaseed ppuint64
	var pp_ /* tv at bp+0 */ tstimeval
	pp_ = aaseed
	Xgettimeofday(cgtls, cgbp, iqlibc.ppUintptrFromInt32(0))
	aaseed = iqlibc.ppUint64FromInt64((*(*tstimeval)(iqunsafe.ppPointer(cgbp))).fdtv_sec) ^ iqlibc.ppUint64FromInt64((*(*tstimeval)(iqunsafe.ppPointer(cgbp))).fdtv_usec)<<ppint32(12)

	aaseed &= ppuint64(0xffffffff)
	X__gmp_randseed_ui(cgtls, aarands, aaseed)
	Xprintf(cgtls, "Seed GMP_CHECK_RANDOMIZE=%lu (include this in bug reports)\n\x00", iqlibc.ppVaList(cgbp+24, aaseed))
}

func siseed_from_urandom(cgtls *iqlibc.ppTLS, aarands tngmp_randstate_ptr, aafs ppuintptr) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var pp_ /* buf at bp+18 */ [6]ppuint8
	var pp_ /* seed at bp+0 */ tnmpz_t
	Xfread(cgtls, cgbp+18, ppuint64(1), ppuint64(6), aafs)
	X__gmpz_init(cgtls, cgbp)
	X__gmpz_import(cgtls, cgbp, ppuint64(6), ppint32(1), ppuint64(1), 0, ppuint64(0), cgbp+18)
	X__gmp_randseed(cgtls, aarands, cgbp)
	X__gmp_printf(cgtls, "Seed GMP_CHECK_RANDOMIZE=%Zd (include this in bug reports)\n\x00", iqlibc.ppVaList(cgbp+32, cgbp))
	X__gmpz_clear(cgtls, cgbp)
}

func Xtests_rand_start(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aafs, aaseed_string ppuintptr
	var aarands tngmp_randstate_ptr
	var pp_ /* seed at bp+0 */ tnmpz_t
	pp_, pp_, pp_ = aafs, aarands, aaseed_string

	if X__gmp_rands_initialized != 0 {

		Xprintf(cgtls, "Please let tests_start() initialize the global __gmp_rands.\n\x00", 0)
		Xprintf(cgtls, "ie. ensure that function is called before the first use of RANDS.\n\x00", 0)
		Xabort(cgtls)
	}

	X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&X__gmp_rands)))
	X__gmp_rands_initialized = ppuint8(1)
	aarands = ppuintptr(iqunsafe.ppPointer(&X__gmp_rands))

	aaseed_string = Xgetenv(cgtls, "GMP_CHECK_RANDOMIZE\x00")
	if aaseed_string != iqlibc.ppUintptrFromInt32(0) {

		if Xstrcmp(cgtls, aaseed_string, "0\x00") != 0 && Xstrcmp(cgtls, aaseed_string, "1\x00") != 0 {

			X__gmpz_init_set_str(cgtls, cgbp, aaseed_string, 0)
			X__gmp_printf(cgtls, "Re-seeding with GMP_CHECK_RANDOMIZE=%Zd\n\x00", iqlibc.ppVaList(cgbp+24, cgbp))
			X__gmp_randseed(cgtls, aarands, cgbp)
			X__gmpz_clear(cgtls, cgbp)
		} else {

			aafs = Xfopen(cgtls, "/dev/urandom\x00", "r\x00")
			if aafs != iqlibc.ppUintptrFromInt32(0) {

				siseed_from_urandom(cgtls, aarands, aafs)
				Xfclose(cgtls, aafs)
			} else {
				siseed_from_tod(cgtls, aarands)
			}
		}
		Xfflush(cgtls, Xstdout)
	}
}

func Xtests_rand_end(cgtls *iqlibc.ppTLS) {

	if X__gmp_rands_initialized != 0 {
		X__gmp_rands_initialized = ppuint8(0)
		X__gmp_randclear(cgtls, ppuintptr(iqunsafe.ppPointer(&X__gmp_rands)))
	}
}

// C documentation
//
//	/* Only used if CPU calling conventions checking is available. */
var Xcalling_conventions_function ppuintptr

// C documentation
//
//	/* Return p advanced to the next multiple of "align" bytes.  "align" must be
//	   a power of 2.  Care is taken not to assume sizeof(int)==sizeof(pointer).
//	   Using "unsigned long" avoids a warning on hpux.  */
func Xalign_pointer(cgtls *iqlibc.ppTLS, aap ppuintptr, aaalign tnsize_t) (cgr ppuintptr) {

	var aad tngmp_intptr_t
	var ccv1 ppuint64
	pp_, pp_ = aad, ccv1
	aad = iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(ppint64(aap)) & (aaalign - ppuint64(1)))
	if aad != 0 {
		ccv1 = aaalign - iqlibc.ppUint64FromInt64(aad)
	} else {
		ccv1 = ppuint64(0)
	}
	aad = iqlibc.ppInt64FromUint64(ccv1)
	return aap + ppuintptr(aad)
}

// C documentation
//
//	/* Note that memory allocated with this function can never be freed, because
//	   the start address of the block allocated is lost. */
func X__gmp_allocate_func_aligned(cgtls *iqlibc.ppTLS, aabytes tnsize_t, aaalign tnsize_t) (cgr ppuintptr) {

	return Xalign_pointer(cgtls, (*(*func(*iqlibc.ppTLS, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_allocate_func})))(cgtls, aabytes+aaalign-ppuint64(1)), aaalign)
}

func X__gmp_allocate_or_reallocate(cgtls *iqlibc.ppTLS, aaptr ppuintptr, aaoldsize tnsize_t, aanewsize tnsize_t) (cgr ppuintptr) {

	if aaptr == iqlibc.ppUintptrFromInt32(0) {
		return (*(*func(*iqlibc.ppTLS, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_allocate_func})))(cgtls, aanewsize)
	} else {
		return (*(*func(*iqlibc.ppTLS, ppuintptr, tnsize_t, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_reallocate_func})))(cgtls, aaptr, aaoldsize, aanewsize)
	}
	return cgr
}

func X__gmp_allocate_strdup(cgtls *iqlibc.ppTLS, aas ppuintptr) (cgr ppuintptr) {

	var aalen tnsize_t
	var aat ppuintptr
	pp_, pp_ = aalen, aat
	aalen = Xstrlen(cgtls, aas)
	aat = (*(*func(*iqlibc.ppTLS, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_allocate_func})))(cgtls, aalen+ppuint64(1))
	Xmemcpy(cgtls, aat, aas, aalen+ppuint64(1))
	return aat
}

func Xstrtoupper(cgtls *iqlibc.ppTLS, aas_orig ppuintptr) (cgr ppuintptr) {

	var aas ppuintptr
	pp_ = aas
	aas = aas_orig
	for {
		if !(iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aas))) != ppint32('\000')) {
			break
		}
		if iqlibc.ppBoolInt32(ppuint32(*(*ppuint8)(iqunsafe.ppPointer(aas)))-ppuint32('a') < ppuint32(26)) != 0 {
			*(*ppuint8)(iqunsafe.ppPointer(aas)) = iqlibc.ppUint8FromInt32(Xtoupper(cgtls, iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aas)))))
		}
		goto cg_1
	cg_1:
		;
		aas++
	}
	return aas_orig
}

func Xmpz_set_n(cgtls *iqlibc.ppTLS, aaz tnmpz_ptr, aap tnmp_srcptr, aasize tnmp_size_t) {

	var aa__dst, ccv5, ccv7 tnmp_ptr
	var aa__n, ccv3 tnmp_size_t
	var aa__src, ccv2, ccv6 tnmp_srcptr
	var aa__x tnmp_limb_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__dst, aa__n, aa__src, aa__x, ccv2, ccv3, ccv5, ccv6, ccv7

	for aasize > 0 {
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aap + ppuintptr(aasize-ppint64(1))*8)) != ppuint64(0) {
			break
		}
		aasize--
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(aasize > ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_alloc)) != 0), 0) != 0 {
		X__gmpz_realloc(cgtls, aaz, aasize)
	} else {
		pp_ = (*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_d
	}
	if aasize != 0 {
		aa__n = aasize - ppint64(1)
		aa__dst = (*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_d
		aa__src = aap
		ccv2 = aa__src
		aa__src += 8
		aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv2))
		if aa__n != 0 {
			for {
				ccv5 = aa__dst
				aa__dst += 8
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv5)) = aa__x
				ccv6 = aa__src
				aa__src += 8
				aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv6))
				goto cg_4
			cg_4:
				;
				aa__n--
				ccv3 = aa__n
				if !(ccv3 != 0) {
					break
				}
			}
		}
		ccv7 = aa__dst
		aa__dst += 8
		*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv7)) = aa__x
	}
	(*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_size = ppint32(aasize)
}

func Xmpz_init_set_n(cgtls *iqlibc.ppTLS, aaz tnmpz_ptr, aap tnmp_srcptr, aasize tnmp_size_t) {

	var aa__dst, ccv5, ccv7 tnmp_ptr
	var aa__n, ccv3 tnmp_size_t
	var aa__src, ccv2, ccv6 tnmp_srcptr
	var aa__x tnmp_limb_t
	var ccv1 ppint64
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__dst, aa__n, aa__src, aa__x, ccv1, ccv2, ccv3, ccv5, ccv6, ccv7

	for aasize > 0 {
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aap + ppuintptr(aasize-ppint64(1))*8)) != ppuint64(0) {
			break
		}
		aasize--
	}
	if aasize > ppint64(iqlibc.ppInt32FromInt32(1)) {
		ccv1 = aasize
	} else {
		ccv1 = ppint64(iqlibc.ppInt32FromInt32(1))
	}
	(*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_alloc = ppint32(ccv1)
	(*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_d = (*(*func(*iqlibc.ppTLS, tnsize_t) ppuintptr)(iqunsafe.ppPointer(&struct{ ppuintptr }{X__gmp_allocate_func})))(cgtls, iqlibc.ppUint64FromInt32((*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_alloc)*ppuint64(8))
	(*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_size = ppint32(aasize)
	if aasize != 0 {
		aa__n = aasize - ppint64(1)
		aa__dst = (*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_d
		aa__src = aap
		ccv2 = aa__src
		aa__src += 8
		aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv2))
		if aa__n != 0 {
			for {
				ccv5 = aa__dst
				aa__dst += 8
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv5)) = aa__x
				ccv6 = aa__src
				aa__src += 8
				aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv6))
				goto cg_4
			cg_4:
				;
				aa__n--
				ccv3 = aa__n
				if !(ccv3 != 0) {
					break
				}
			}
		}
		ccv7 = aa__dst
		aa__dst += 8
		*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv7)) = aa__x
	}
}

// C documentation
//
//	/* Find least significant limb position where p1,size and p2,size differ.  */
func Xmpn_diff_lowest(cgtls *iqlibc.ppTLS, aap1 tnmp_srcptr, aap2 tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_size_t) {

	var aai tnmp_size_t
	pp_ = aai

	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aap1 + ppuintptr(aai)*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(aap2 + ppuintptr(aai)*8)) {
			return aai
		}
		goto cg_1
	cg_1:
		;
		aai++
	}

	/* no differences */
	return ppint64(-ppint32(1))
}

// C documentation
//
//	/* Find most significant limb position where p1,size and p2,size differ.  */
func Xmpn_diff_highest(cgtls *iqlibc.ppTLS, aap1 tnmp_srcptr, aap2 tnmp_srcptr, aasize tnmp_size_t) (cgr tnmp_size_t) {

	var aai tnmp_size_t
	pp_ = aai

	aai = aasize - ppint64(1)
	for {
		if !(aai >= 0) {
			break
		}
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aap1 + ppuintptr(aai)*8)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(aap2 + ppuintptr(aai)*8)) {
			return aai
		}
		goto cg_1
	cg_1:
		;
		aai--
	}

	/* no differences */
	return ppint64(-ppint32(1))
}

// C documentation
//
//	/* Find least significant byte position where p1,size and p2,size differ.  */
func Xbyte_diff_lowest(cgtls *iqlibc.ppTLS, aap1 ppuintptr, aap2 ppuintptr, aasize tnmp_size_t) (cgr tnmp_size_t) {

	var aai tnmp_size_t
	pp_ = aai

	aai = 0
	for {
		if !(aai < aasize) {
			break
		}
		if iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aap1 + ppuintptr(aai)))) != iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aap2 + ppuintptr(aai)))) {
			return aai
		}
		goto cg_1
	cg_1:
		;
		aai++
	}

	/* no differences */
	return ppint64(-ppint32(1))
}

// C documentation
//
//	/* Find most significant byte position where p1,size and p2,size differ.  */
func Xbyte_diff_highest(cgtls *iqlibc.ppTLS, aap1 ppuintptr, aap2 ppuintptr, aasize tnmp_size_t) (cgr tnmp_size_t) {

	var aai tnmp_size_t
	pp_ = aai

	aai = aasize - ppint64(1)
	for {
		if !(aai >= 0) {
			break
		}
		if iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aap1 + ppuintptr(aai)))) != iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aap2 + ppuintptr(aai)))) {
			return aai
		}
		goto cg_1
	cg_1:
		;
		aai--
	}

	/* no differences */
	return ppint64(-ppint32(1))
}

func Xmpz_set_str_or_abort(cgtls *iqlibc.ppTLS, aaz tnmpz_ptr, aastr ppuintptr, aabase ppint32) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	if X__gmpz_set_str(cgtls, aaz, aastr, aabase) != 0 {

		Xfprintf(cgtls, Xstderr, "ERROR: mpz_set_str failed\n\x00", 0)
		Xfprintf(cgtls, Xstderr, "   str  = \"%s\"\n\x00", iqlibc.ppVaList(cgbp+8, aastr))
		Xfprintf(cgtls, Xstderr, "   base = %d\n\x00", iqlibc.ppVaList(cgbp+8, aabase))
		Xabort(cgtls)
	}
}

func Xmpq_set_str_or_abort(cgtls *iqlibc.ppTLS, aaq tnmpq_ptr, aastr ppuintptr, aabase ppint32) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	if X__gmpq_set_str(cgtls, aaq, aastr, aabase) != 0 {

		Xfprintf(cgtls, Xstderr, "ERROR: mpq_set_str failed\n\x00", 0)
		Xfprintf(cgtls, Xstderr, "   str  = \"%s\"\n\x00", iqlibc.ppVaList(cgbp+8, aastr))
		Xfprintf(cgtls, Xstderr, "   base = %d\n\x00", iqlibc.ppVaList(cgbp+8, aabase))
		Xabort(cgtls)
	}
}

func Xmpf_set_str_or_abort(cgtls *iqlibc.ppTLS, aaf tnmpf_ptr, aastr ppuintptr, aabase ppint32) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	if X__gmpf_set_str(cgtls, aaf, aastr, aabase) != 0 {

		Xfprintf(cgtls, Xstderr, "ERROR mpf_set_str failed\n\x00", 0)
		Xfprintf(cgtls, Xstderr, "   str  = \"%s\"\n\x00", iqlibc.ppVaList(cgbp+8, aastr))
		Xfprintf(cgtls, Xstderr, "   base = %d\n\x00", iqlibc.ppVaList(cgbp+8, aabase))
		Xabort(cgtls)
	}
}

// C documentation
//
//	/* Whether the absolute value of z is a power of 2. */
func Xmpz_pow2abs_p(cgtls *iqlibc.ppTLS, aaz tnmpz_srcptr) (cgr ppint32) {

	var aai, aasize tnmp_size_t
	var aaptr tnmp_srcptr
	var ccv1 ppint64
	pp_, pp_, pp_, pp_ = aai, aaptr, aasize, ccv1

	aasize = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_size)
	if aasize == 0 {
		return 0
	} /* zero is not a power of 2 */
	if aasize >= 0 {
		ccv1 = aasize
	} else {
		ccv1 = -aasize
	}
	aasize = ccv1

	aaptr = (*tn__mpz_struct)(iqunsafe.ppPointer(aaz)).fd_mp_d
	aai = 0
	for {
		if !(aai < aasize-ppint64(1)) {
			break
		}
		if *(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aai)*8)) != ppuint64(0) {
			return 0
		}
		goto cg_2
	cg_2:
		;
		aai++
	} /* non-zero low limb means not a power of 2 */

	return iqlibc.ppBoolInt32(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aai)*8))&(*(*tnmp_limb_t)(iqunsafe.ppPointer(aaptr + ppuintptr(aai)*8))-ppuint64(1)) == ppuint64(0)) /* high limb power of 2 */
}

/* Exponentially distributed between 0 and 2^nbits-1, meaning the number of
   bits in the result is uniformly distributed between 0 and nbits-1.

   FIXME: This is not a proper exponential distribution, since the
   probability function will have a stepped shape due to using a uniform
   distribution after choosing how many bits.  */

func Xmpz_erandomb(cgtls *iqlibc.ppTLS, aarop tnmpz_ptr, aarstate ppuintptr, aanbits ppuint64) {

	X__gmpz_urandomb(cgtls, aarop, aarstate, X__gmp_urandomm_ui(cgtls, aarstate, aanbits))
}

func Xmpz_erandomb_nonzero(cgtls *iqlibc.ppTLS, aarop tnmpz_ptr, aarstate ppuintptr, aanbits ppuint64) {

	var ccv1 ppint32
	pp_ = ccv1
	Xmpz_erandomb(cgtls, aarop, aarstate, aanbits)
	if (*tn__mpz_struct)(iqunsafe.ppPointer(aarop)).fd_mp_size < 0 {
		ccv1 = -ppint32(1)
	} else {
		ccv1 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(aarop)).fd_mp_size > 0)
	}
	if ccv1 == 0 {
		X__gmpz_set_ui(cgtls, aarop, ppuint64(1))
	}
}

func Xmpz_errandomb(cgtls *iqlibc.ppTLS, aarop tnmpz_ptr, aarstate ppuintptr, aanbits ppuint64) {

	X__gmpz_rrandomb(cgtls, aarop, aarstate, X__gmp_urandomm_ui(cgtls, aarstate, aanbits))
}

func Xmpz_errandomb_nonzero(cgtls *iqlibc.ppTLS, aarop tnmpz_ptr, aarstate ppuintptr, aanbits ppuint64) {

	var ccv1 ppint32
	pp_ = ccv1
	Xmpz_errandomb(cgtls, aarop, aarstate, aanbits)
	if (*tn__mpz_struct)(iqunsafe.ppPointer(aarop)).fd_mp_size < 0 {
		ccv1 = -ppint32(1)
	} else {
		ccv1 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(aarop)).fd_mp_size > 0)
	}
	if ccv1 == 0 {
		X__gmpz_set_ui(cgtls, aarop, ppuint64(1))
	}
}

func Xmpz_negrandom(cgtls *iqlibc.ppTLS, aarop tnmpz_ptr, aarstate ppuintptr) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aa__rstate tngmp_randstate_ptr
	var ccv1 tnmpz_ptr
	var ccv2 tnmpz_srcptr
	var pp_ /* n at bp+0 */ tnmp_limb_t
	pp_, pp_, pp_ = aa__rstate, ccv1, ccv2
	aa__rstate = aarstate
	(*(*func(*iqlibc.ppTLS, tngmp_randstate_ptr, tnmp_ptr, ppuint64))(iqunsafe.ppPointer(&struct{ ppuintptr }{(*tngmp_randfnptr_t)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(aa__rstate + 24)))).fdrandget_fn})))(cgtls, aa__rstate, cgbp, ppuint64(1))
	if *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp)) != ppuint64(0) {
		ccv1 = aarop
		ccv2 = aarop
		if ccv1 != ccv2 {
			X__gmpz_set(cgtls, ccv1, ccv2)
		}
		(*tn__mpz_struct)(iqunsafe.ppPointer(ccv1)).fd_mp_size = -(*tn__mpz_struct)(iqunsafe.ppPointer(ccv1)).fd_mp_size
	}
}

func Xmpz_clobber(cgtls *iqlibc.ppTLS, aarop tnmpz_ptr) {

	var aa__dst, ccv3 tnmp_ptr
	var aa__n, ccv1 tnmp_size_t
	pp_, pp_, pp_, pp_ = aa__dst, aa__n, ccv1, ccv3
	if (*tn__mpz_struct)(iqunsafe.ppPointer(aarop)).fd_mp_alloc != 0 {
		aa__dst = (*tn__mpz_struct)(iqunsafe.ppPointer(aarop)).fd_mp_d
		aa__n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(aarop)).fd_mp_alloc)
		for {
			ccv3 = aa__dst
			aa__dst += 8
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv3)) = iqlibc.ppUint64FromInt64(0)
			goto cg_2
		cg_2:
			;
			aa__n--
			ccv1 = aa__n
			if !(ccv1 != 0) {
				break
			}
		}
	}
	*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(aarop)).fd_mp_d)) = ppuint64(0xDEADBEEF)
	(*tn__mpz_struct)(iqunsafe.ppPointer(aarop)).fd_mp_size = ppint32(0xDEFACE)
}

func Xurandom(cgtls *iqlibc.ppTLS) (cgr tnmp_limb_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aa__rstate tngmp_randstate_ptr
	var pp_ /* n at bp+0 */ tnmp_limb_t
	pp_ = aa__rstate
	if !(X__gmp_rands_initialized != 0) {
		X__gmp_rands_initialized = ppuint8(1)
		X__gmp_randinit_mt_noseed(cgtls, ppuintptr(iqunsafe.ppPointer(&X__gmp_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	aa__rstate = ppuintptr(iqunsafe.ppPointer(&X__gmp_rands))
	(*(*func(*iqlibc.ppTLS, tngmp_randstate_ptr, tnmp_ptr, ppuint64))(iqunsafe.ppPointer(&struct{ ppuintptr }{(*tngmp_randfnptr_t)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(aa__rstate + 24)))).fdrandget_fn})))(cgtls, aa__rstate, cgbp, ppuint64(mvGMP_LIMB_BITS))
	return *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp))
}

// C documentation
//
//	/* Call (*func)() with various random number generators. */
func Xcall_rand_algs(cgtls *iqlibc.ppTLS, aafunc ppuintptr) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var pp_ /* a at bp+32 */ tnmpz_t
	var pp_ /* rstate at bp+0 */ tngmp_randstate_t

	X__gmpz_init(cgtls, cgbp+32)

	X__gmp_randinit_default(cgtls, cgbp)
	(*(*func(*iqlibc.ppTLS, ppuintptr, tngmp_randstate_ptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, "gmp_randinit_default\x00", cgbp)
	X__gmp_randclear(cgtls, cgbp)

	X__gmp_randinit_mt(cgtls, cgbp)
	(*(*func(*iqlibc.ppTLS, ppuintptr, tngmp_randstate_ptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, "gmp_randinit_mt\x00", cgbp)
	X__gmp_randclear(cgtls, cgbp)

	X__gmp_randinit_lc_2exp_size(cgtls, cgbp, ppuint64(8))
	(*(*func(*iqlibc.ppTLS, ppuintptr, tngmp_randstate_ptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, "gmp_randinit_lc_2exp_size 8\x00", cgbp)
	X__gmp_randclear(cgtls, cgbp)

	X__gmp_randinit_lc_2exp_size(cgtls, cgbp, ppuint64(16))
	(*(*func(*iqlibc.ppTLS, ppuintptr, tngmp_randstate_ptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, "gmp_randinit_lc_2exp_size 16\x00", cgbp)
	X__gmp_randclear(cgtls, cgbp)

	X__gmp_randinit_lc_2exp_size(cgtls, cgbp, ppuint64(128))
	(*(*func(*iqlibc.ppTLS, ppuintptr, tngmp_randstate_ptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, "gmp_randinit_lc_2exp_size 128\x00", cgbp)
	X__gmp_randclear(cgtls, cgbp)

	/* degenerate always zeros */
	X__gmpz_set_ui(cgtls, cgbp+32, ppuint64(0))
	X__gmp_randinit_lc_2exp(cgtls, cgbp, cgbp+32, ppuint64(0), ppuint64(8))
	(*(*func(*iqlibc.ppTLS, ppuintptr, tngmp_randstate_ptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, "gmp_randinit_lc_2exp a=0 c=0 m=8\x00", cgbp)
	X__gmp_randclear(cgtls, cgbp)

	/* degenerate always FFs */
	X__gmpz_set_ui(cgtls, cgbp+32, ppuint64(0))
	X__gmp_randinit_lc_2exp(cgtls, cgbp, cgbp+32, ppuint64(0xFF), ppuint64(8))
	(*(*func(*iqlibc.ppTLS, ppuintptr, tngmp_randstate_ptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, "gmp_randinit_lc_2exp a=0 c=0xFF m=8\x00", cgbp)
	X__gmp_randclear(cgtls, cgbp)

	X__gmpz_clear(cgtls, cgbp+32)
}

// C documentation
//
//	/* Return +infinity if available, or 0 if not.
//	   We don't want to use libm, so INFINITY or other system values are not
//	   used here.  */
func Xtests_infinity_d(cgtls *iqlibc.ppTLS) (cgr ppfloat64) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var pp_ /* x at bp+0 */ tuieee_double_extract
	iqlibc.SetBitFieldPtr32Uint32(cgbp+0, iqlibc.ppUint32FromInt32(2047), 1, 0xffe)
	iqlibc.SetBitFieldPtr32Uint32(cgbp+4, iqlibc.ppUint32FromInt32(0), 0, 0xffffffff)
	iqlibc.SetBitFieldPtr32Uint32(cgbp+0, iqlibc.ppUint32FromInt32(0), 12, 0xfffff000)
	iqlibc.SetBitFieldPtr32Uint32(cgbp+0, iqlibc.ppUint32FromInt32(0), 0, 0x1)
	return *(*ppfloat64)(iqunsafe.ppPointer(cgbp))
}

// C documentation
//
//	/* Return non-zero if d is an infinity (either positive or negative).
//	   Don't want libm, so don't use isinf() or other system tests.  */
func Xtests_isinf(cgtls *iqlibc.ppTLS, aad ppfloat64) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var pp_ /* x at bp+0 */ tuieee_double_extract
	*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) = aad
	return iqlibc.ppBoolInt32(ppint32(tngmp_uint_least32_t(*(*uint32)(iqunsafe.ppPointer(cgbp + 0))&0xffe>>1)) == ppint32(2047) && tngmp_uint_least32_t(tngmp_uint_least32_t(*(*uint32)(iqunsafe.ppPointer(cgbp + 4))&0xffffffff>>0)) == ppuint32(0) && ppint32(tngmp_uint_least32_t(*(*uint32)(iqunsafe.ppPointer(cgbp + 0))&0xfffff000>>12)) == 0)
}

// C documentation
//
//	/* Set the hardware floating point rounding mode.  Same mode values as mpfr,
//	   namely 0=nearest, 1=tozero, 2=up, 3=down.  Return 1 if successful, 0 if
//	   not.  */
func Xtests_hardware_setround(cgtls *iqlibc.ppTLS, aamode ppint32) (cgr ppint32) {

	return 0
}

// C documentation
//
//	/* Return the hardware floating point rounding mode, or -1 if unknown. */
func Xtests_hardware_getround(cgtls *iqlibc.ppTLS) (cgr ppint32) {

	return -ppint32(1)
}

/* tests_dbl_mant_bits() determines by experiment the number of bits in the
   mantissa of a "double".  If it's not possible to find a value (perhaps
   due to the compiler optimizing too aggressively), then return 0.

   This code is used rather than DBL_MANT_DIG from <float.h> since ancient
   systems like SunOS don't have that file, and since one GNU/Linux ARM
   system was seen where the float emulation seemed to have only 32 working
   bits, not the 53 float.h claimed.  */

func Xtests_dbl_mant_bits(cgtls *iqlibc.ppTLS) (cgr ppint32) {

	var aad, aax, aay ppfloat64
	pp_, pp_, pp_ = aad, aax, aay
	var snn = -ppint32(1)

	if snn != -ppint32(1) {
		return snn
	}

	snn = ppint32(1)
	aax = ppfloat64(2)
	for {

		/* see if 2^(n+1)+1 can be formed without rounding, if so then
		   continue, if not then "n" is the answer */
		aay = aax + ppfloat64(1)
		aad = aay - aax
		if aad != ppfloat64(1) {

			break
		}

		aax *= iqlibc.ppFloat64FromInt32(2)
		snn++

		if snn > ppint32(1000) {

			Xprintf(cgtls, "Oops, tests_dbl_mant_bits can't determine mantissa size\n\x00", 0)
			snn = 0
			break
		}

		goto cg_1
	cg_1:
	}
	return snn
}

/* See tests_setjmp_sigfpe in tests.h. */

var Xtests_sigfpe_target tnjmp_buf

func Xtests_sigfpe_handler(cgtls *iqlibc.ppTLS, aasig ppint32) {

	pptls.ppLongjmp(ppuintptr(iqunsafe.ppPointer(&Xtests_sigfpe_target)), iqlibc.ppInt32FromInt32(1))
}

func Xtests_sigfpe_done(cgtls *iqlibc.ppTLS) {

	Xsignal(cgtls, ppint32(mvSIGFPE), iqlibc.ppUintptrFromInt32(0))
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

var ___gmp_allocate_func ppuintptr

func ___gmp_printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmp_randclear(*iqlibc.ppTLS, ppuintptr)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

func ___gmp_randinit_lc_2exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppuint64)

func ___gmp_randinit_lc_2exp_size(*iqlibc.ppTLS, ppuintptr, ppuint64) ppint32

func ___gmp_randinit_mt(*iqlibc.ppTLS, ppuintptr)

func ___gmp_randinit_mt_noseed(*iqlibc.ppTLS, ppuintptr)

var ___gmp_rands [1]tn__gmp_randstate_struct

var ___gmp_rands_initialized ppuint8

func ___gmp_randseed(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmp_randseed_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

var ___gmp_reallocate_func ppuintptr

func ___gmp_urandomm_ui(*iqlibc.ppTLS, ppuintptr, ppuint64) ppuint64

var ___gmp_version ppuintptr

func ___gmpf_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func ___gmpq_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func ___gmpz_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_import(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint32, ppuint64, ppint32, ppuint64, ppuintptr)

func ___gmpz_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_init_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func ___gmpz_realloc(*iqlibc.ppTLS, ppuintptr, ppint64) ppuintptr

func ___gmpz_rrandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_set(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpz_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func ___gmpz_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

func ___gmpz_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func _abort(*iqlibc.ppTLS)

func _fclose(*iqlibc.ppTLS, ppuintptr) ppint32

func _fflush(*iqlibc.ppTLS, ppuintptr) ppint32

func _fopen(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _fprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _fread(*iqlibc.ppTLS, ppuintptr, ppuint64, ppuint64, ppuintptr) ppuint64

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _gettimeofday(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _memcpy(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppuintptr

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _setbuf(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _signal(*iqlibc.ppTLS, ppint32, ppuintptr) ppuintptr

func _snprintf(*iqlibc.ppTLS, ppuintptr, ppuint64, ppuintptr, ppuintptr) ppint32

var _stderr ppuintptr

var _stdout ppuintptr

func _strcmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _strlen(*iqlibc.ppTLS, ppuintptr) ppuint64

func _tests_memory_end(*iqlibc.ppTLS)

func _tests_memory_start(*iqlibc.ppTLS)

var _tests_sigfpe_target [1]ts__jmp_buf_tag

func _toupper(*iqlibc.ppTLS, ppint32) ppint32

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
