// Code generated for linux/s390x by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -DHAVE_CONFIG_H -I. -I.. -I.. -DNDEBUG -mlong-double-64 -c refmpf.c -o refmpf.o.go', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvASSERT_FILE = "__FILE__"
const mvASSERT_LINE = "__LINE__"
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBINV_NEWTON_THRESHOLD = 300
const mvBMOD_1_TO_MOD_1_THRESHOLD = 10
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDC_BDIV_Q_THRESHOLD = 180
const mvDC_DIVAPPR_Q_THRESHOLD = 200
const mvDELAYTIMER_MAX = 0x7fffffff
const mvDIVEXACT_1_THRESHOLD = 0
const mvDIVEXACT_BY3_METHOD = 0
const mvDIVEXACT_JEB_THRESHOLD = 25
const mvDOPRNT_CONV_FIXED = 1
const mvDOPRNT_CONV_GENERAL = 3
const mvDOPRNT_CONV_SCIENTIFIC = 2
const mvDOPRNT_JUSTIFY_INTERNAL = 3
const mvDOPRNT_JUSTIFY_LEFT = 1
const mvDOPRNT_JUSTIFY_NONE = 0
const mvDOPRNT_JUSTIFY_RIGHT = 2
const mvDOPRNT_SHOWBASE_NO = 2
const mvDOPRNT_SHOWBASE_NONZERO = 3
const mvDOPRNT_SHOWBASE_YES = 1
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFAC_DSC_THRESHOLD = 400
const mvFAC_ODD_THRESHOLD = 35
const mvFFT_FIRST_K = 4
const mvFIB_TABLE_LIMIT = 93
const mvFIB_TABLE_LUCNUM_LIMIT = 92
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFOPEN_MAX = 1000
const mvGCDEXT_DC_THRESHOLD = 600
const mvGCD_DC_THRESHOLD = 1000
const mvGET_STR_DC_THRESHOLD = 18
const mvGET_STR_PRECOMPUTE_THRESHOLD = 35
const mvGMP_LIMB_BITS = 64
const mvGMP_LIMB_BYTES = "SIZEOF_MP_LIMB_T"
const mvGMP_MPARAM_H_SUGGEST = "./mpn/generic/gmp-mparam.h"
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_ALARM = 1
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_CONST = 1
const mvHAVE_ATTRIBUTE_MALLOC = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_ATTRIBUTE_NORETURN = 1
const mvHAVE_CLOCK = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_CONFIG_H = 1
const mvHAVE_DECL_FGETC = 1
const mvHAVE_DECL_FSCANF = 1
const mvHAVE_DECL_OPTARG = 1
const mvHAVE_DECL_SYS_ERRLIST = 1
const mvHAVE_DECL_SYS_NERR = 1
const mvHAVE_DECL_UNGETC = 1
const mvHAVE_DECL_VFPRINTF = 1
const mvHAVE_DLFCN_H = 1
const mvHAVE_DOUBLE_IEEE_BIG_ENDIAN = 1
const mvHAVE_FCNTL_H = 1
const mvHAVE_FLOAT_H = 1
const mvHAVE_GETPAGESIZE = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_HIDDEN_ALIAS = 1
const mvHAVE_HOST_CPU_s390_z15 = 1
const mvHAVE_HOST_CPU_s390_zarch = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTPTR_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LANGINFO_H = 1
const mvHAVE_LIMB_BIG_ENDIAN = 1
const mvHAVE_LOCALECONV = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_DOUBLE = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_MEMORY_H = 1
const mvHAVE_MEMSET = 1
const mvHAVE_MMAP = 1
const mvHAVE_MPROTECT = 1
const mvHAVE_NL_LANGINFO = 1
const mvHAVE_NL_TYPES_H = 1
const mvHAVE_POPEN = 1
const mvHAVE_PTRDIFF_T = 1
const mvHAVE_QUAD_T = 1
const mvHAVE_RAISE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGALTSTACK = 1
const mvHAVE_SIGSTACK = 1
const mvHAVE_STACK_T = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDLIB_H = 1
const mvHAVE_STRCHR = 1
const mvHAVE_STRERROR = 1
const mvHAVE_STRINGS_H = 1
const mvHAVE_STRING_H = 1
const mvHAVE_STRNLEN = 1
const mvHAVE_STRTOL = 1
const mvHAVE_STRTOUL = 1
const mvHAVE_SYSCONF = 1
const mvHAVE_SYSCTL = 1
const mvHAVE_SYS_MMAN_H = 1
const mvHAVE_SYS_PARAM_H = 1
const mvHAVE_SYS_RESOURCE_H = 1
const mvHAVE_SYS_STAT_H = 1
const mvHAVE_SYS_SYSCTL_H = 1
const mvHAVE_SYS_SYSINFO_H = 1
const mvHAVE_SYS_TIMES_H = 1
const mvHAVE_SYS_TIME_H = 1
const mvHAVE_SYS_TYPES_H = 1
const mvHAVE_TIMES = 1
const mvHAVE_UINT_LEAST32_T = 1
const mvHAVE_UNISTD_H = 1
const mvHAVE_VSNPRINTF = 1
const mvHGCD_APPR_THRESHOLD = 400
const mvHGCD_REDUCE_THRESHOLD = 1000
const mvHGCD_THRESHOLD = 400
const mvHOST_NAME_MAX = 255
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT64_MAX"
const mvINTPTR_MIN = "INT64_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_HIGHBIT = "INT_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvINV_APPR_THRESHOLD = "INV_NEWTON_THRESHOLD"
const mvINV_NEWTON_THRESHOLD = 200
const mvIOV_MAX = 1024
const mvLIMBS_PER_ULONG = 1
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_HIGHBIT = "LONG_MIN"
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMATRIX22_STRASSEN_THRESHOLD = 30
const mvMB_LEN_MAX = 4
const mvMOD_BKNP1_ONLY3 = 0
const mvMPN_FFT_TABLE_SIZE = 16
const mvMPN_TOOM22_MUL_MINSIZE = 6
const mvMPN_TOOM2_SQR_MINSIZE = 4
const mvMPN_TOOM32_MUL_MINSIZE = 10
const mvMPN_TOOM33_MUL_MINSIZE = 17
const mvMPN_TOOM3_SQR_MINSIZE = 17
const mvMPN_TOOM42_MULMID_MINSIZE = 4
const mvMPN_TOOM42_MUL_MINSIZE = 10
const mvMPN_TOOM43_MUL_MINSIZE = 25
const mvMPN_TOOM44_MUL_MINSIZE = 30
const mvMPN_TOOM4_SQR_MINSIZE = 30
const mvMPN_TOOM53_MUL_MINSIZE = 17
const mvMPN_TOOM54_MUL_MINSIZE = 31
const mvMPN_TOOM63_MUL_MINSIZE = 49
const mvMPN_TOOM6H_MUL_MINSIZE = 46
const mvMPN_TOOM6_SQR_MINSIZE = 46
const mvMPN_TOOM8H_MUL_MINSIZE = 86
const mvMPN_TOOM8_SQR_MINSIZE = 86
const mvMP_BASES_BIG_BASE_CTZ_10 = 19
const mvMP_BASES_CHARS_PER_LIMB_10 = 19
const mvMP_BASES_NORMALIZATION_STEPS_10 = 0
const mvMP_EXP_T_MAX = "MP_SIZE_T_MAX"
const mvMP_EXP_T_MIN = "MP_SIZE_T_MIN"
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMULLO_BASECASE_THRESHOLD = 0
const mvMULLO_BASECASE_THRESHOLD_LIMIT = "MULLO_BASECASE_THRESHOLD"
const mvMULMID_TOOM42_THRESHOLD = "MUL_TOOM22_THRESHOLD"
const mvMULMOD_BNM1_THRESHOLD = 16
const mvMUL_TOOM22_THRESHOLD = 30
const mvMUL_TOOM22_THRESHOLD_LIMIT = "MUL_TOOM22_THRESHOLD"
const mvMUL_TOOM32_TO_TOOM43_THRESHOLD = 100
const mvMUL_TOOM32_TO_TOOM53_THRESHOLD = 110
const mvMUL_TOOM33_THRESHOLD = 100
const mvMUL_TOOM33_THRESHOLD_LIMIT = "MUL_TOOM33_THRESHOLD"
const mvMUL_TOOM42_TO_TOOM53_THRESHOLD = 100
const mvMUL_TOOM42_TO_TOOM63_THRESHOLD = 110
const mvMUL_TOOM43_TO_TOOM54_THRESHOLD = 150
const mvMUL_TOOM44_THRESHOLD = 300
const mvMUL_TOOM6H_THRESHOLD = 350
const mvMUL_TOOM8H_THRESHOLD = 450
const mvMUPI_DIV_QR_THRESHOLD = 200
const mvMU_BDIV_QR_THRESHOLD = 2000
const mvMU_BDIV_Q_THRESHOLD = 2000
const mvMU_DIVAPPR_Q_THRESHOLD = 2000
const mvMU_DIV_QR_THRESHOLD = 2000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvODD_CENTRAL_BINOMIAL_OFFSET = 13
const mvODD_CENTRAL_BINOMIAL_TABLE_LIMIT = 35
const mvODD_DOUBLEFACTORIAL_TABLE_LIMIT = 33
const mvODD_FACTORIAL_EXTTABLE_LIMIT = 67
const mvODD_FACTORIAL_TABLE_LIMIT = 25
const mvPACKAGE = "gmp"
const mvPACKAGE_BUGREPORT = "gmp-bugs@gmplib.org (see https://gmplib.org/manual/Reporting-Bugs.html)"
const mvPACKAGE_NAME = "GNU MP"
const mvPACKAGE_STRING = "GNU MP 6.3.0"
const mvPACKAGE_TARNAME = "gmp"
const mvPACKAGE_URL = "http://www.gnu.org/software/gmp/"
const mvPACKAGE_VERSION = "6.3.0"
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPP_FIRST_OMITTED = 59
const mvPREINV_MOD_1_TO_MOD_1_THRESHOLD = 10
const mvPRIMESIEVE_HIGHEST_PRIME = 5351
const mvPRIMESIEVE_NUMBEROF_TABLE = 28
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT64_MAX"
const mvPTRDIFF_MIN = "INT64_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvREDC_1_TO_REDC_N_THRESHOLD = 100
const mvRETSIGTYPE = "void"
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSET_STR_DC_THRESHOLD = 750
const mvSET_STR_PRECOMPUTE_THRESHOLD = 2000
const mvSHRT_HIGHBIT = "SHRT_MIN"
const mvSHRT_MAX = 0x7fff
const mvSIEVESIZE = 512
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZEOF_MP_LIMB_T = 8
const mvSIZEOF_UNSIGNED = 4
const mvSIZEOF_UNSIGNED_LONG = 8
const mvSIZEOF_UNSIGNED_SHORT = 2
const mvSIZEOF_VOID_P = 8
const mvSIZE_MAX = "UINT64_MAX"
const mvSQRLO_BASECASE_THRESHOLD = 0
const mvSQRLO_BASECASE_THRESHOLD_LIMIT = "SQRLO_BASECASE_THRESHOLD"
const mvSQRLO_DC_THRESHOLD = "MULLO_DC_THRESHOLD"
const mvSQRLO_DC_THRESHOLD_LIMIT = "SQRLO_DC_THRESHOLD"
const mvSQRLO_SQR_THRESHOLD = "MULLO_MUL_N_THRESHOLD"
const mvSQRMOD_BNM1_THRESHOLD = 16
const mvSQR_BASECASE_THRESHOLD = 0
const mvSQR_TOOM2_THRESHOLD = 50
const mvSQR_TOOM3_THRESHOLD = 120
const mvSQR_TOOM3_THRESHOLD_LIMIT = "SQR_TOOM3_THRESHOLD"
const mvSQR_TOOM4_THRESHOLD = 400
const mvSQR_TOOM6_THRESHOLD = "MUL_TOOM6H_THRESHOLD"
const mvSQR_TOOM8_THRESHOLD = "MUL_TOOM8H_THRESHOLD"
const mvSSIZE_MAX = "LONG_MAX"
const mvSTDC_HEADERS = 1
const mvSYMLOOP_MAX = 40
const mvTABLE_LIMIT_2N_MINUS_POPC_2N = 81
const mvTARGET_REGISTER_STARVED = 0
const mvTIME_WITH_SYS_TIME = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTUNE_SQR_TOOM2_MAX = "SQR_TOOM2_MAX_GENERIC"
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT64_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSE_LEADING_REGPARM = 0
const mvUSE_PREINV_DIVREM_1 = 1
const mvUSHRT_MAX = 65535
const mvVERSION = "6.3.0"
const mvWANT_FFT = 1
const mvWANT_TMP_ALLOCA = 1
const mvWANT_TMP_DEBUG = 0
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_LIMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_IEEE_FLOATS = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ARCH__ = 9
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 8
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 4321
const mv__BYTE_ORDER__ = "__ORDER_BIG_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_BIG_ENDIAN__"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.2204460492503131e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.7976931348623157e+308
const mv__FLT32X_MIN__ = 2.2250738585072014e-308
const mv__FLT32X_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.1920928955078125e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.4028234663852886e+38
const mv__FLT32_MIN__ = 1.1754943508222875e-38
const mv__FLT32_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.2204460492503131e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.7976931348623157e+308
const mv__FLT64_MIN__ = 2.2250738585072014e-308
const mv__FLT64_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.1920928955078125e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.4028234663852886e+38
const mv__FLT_MIN__ = 1.1754943508222875e-38
const mv__FLT_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FP_FAST_FMAL = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG -mlong-double-64"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 1
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC__ = 10
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1014
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.2204460492503131e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.7976931348623157e+308
const mv__LDBL_MIN__ = 2.2250738585072014e-308
const mv__LDBL_NORM_MAX__ = 1.7976931348623157e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 0x7fffffffffffffff
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "l"
const mv__PRIPTR = "l"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "10.2.1 20210110"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__s390__ = 1
const mv__s390x__ = 1
const mv__unix = 1
const mv__unix__ = 1
const mv__zarch__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_newalloc = "_mpz_realloc"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvbinvert_limb_table = "__gmp_binvert_limb_table"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_init_primesieve = "__gmp_init_primesieve"
const mvgmp_nextprime = "__gmp_nextprime"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_primesieve = "__gmp_primesieve"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvjacobi_table = "__gmp_jacobi_table"
const mvlinux = 1
const mvmodlimb_invert = "binvert_limb"
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpn_fft_mul = "mpn_nussbaumer_mul"
const mvmpn_get_d = "__gmpn_get_d"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_gcd = "__gmpz_divexact_gcd"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_inp_str_nowhite = "__gmpz_inp_str_nowhite"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucas_mod = "__gmpz_lucas_mod"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_n_pow_ui = "__gmpz_n_pow_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_oddfac_1 = "__gmpz_oddfac_1"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_prodlimbs = "__gmpz_prodlimbs"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_stronglucas = "__gmpz_stronglucas"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvrestrict = "__restrict"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

type tnuintptr_t = ppuint64

type tnintptr_t = ppint64

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tngmp_uint_least32_t = ppuint32

type tngmp_intptr_t = ppint64

type tngmp_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tngmp_pi2_t = struct {
	fdinv21 tnmp_limb_t
	fdinv32 tnmp_limb_t
	fdinv53 tnmp_limb_t
}

type tutmp_align_t = struct {
	fdd [0]ppfloat64
	fdp [0]ppuintptr
	fdl tnmp_limb_t
}

type tstmp_reentrant_t = struct {
	fdnext ppuintptr
	fdsize tnsize_t
}

type tngmp_randfnptr_t = struct {
	fdrandseed_fn  ppuintptr
	fdrandget_fn   ppuintptr
	fdrandclear_fn ppuintptr
	fdrandiset_fn  ppuintptr
}

type tetoom6_flags = ppint32

const ectoom6_all_pos = 0
const ectoom6_vm1_neg = 1
const ectoom6_vm2_neg = 2

type tetoom7_flags = ppint32

const ectoom7_w1_neg = 1
const ectoom7_w3_neg = 2

type tngmp_primesieve_t = struct {
	fdd       ppuint64
	fds0      ppuint64
	fdsqrt_s0 ppuint64
	fds       [513]ppuint8
}

type tsfft_table_nk = struct {
	fd__ccgo0 uint32
}

type tsbases = struct {
	fdchars_per_limb    ppint32
	fdlogb2             tnmp_limb_t
	fdlog2b             tnmp_limb_t
	fdbig_base          tnmp_limb_t
	fdbig_base_inverted tnmp_limb_t
}

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tuieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type tnmp_double_limb_t = struct {
	fdd0 tnmp_limb_t
	fdd1 tnmp_limb_t
}

type tshgcd_matrix1 = struct {
	fdu [2][2]tnmp_limb_t
}

type tshgcd_matrix = struct {
	fdalloc tnmp_size_t
	fdn     tnmp_size_t
	fdp     [2][2]tnmp_ptr
}

type tsgcdext_ctx = struct {
	fdgp    tnmp_ptr
	fdgn    tnmp_size_t
	fdup    tnmp_ptr
	fdusize ppuintptr
	fdun    tnmp_size_t
	fdu0    tnmp_ptr
	fdu1    tnmp_ptr
	fdtp    tnmp_ptr
}

type tspowers = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tnpowers_t = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tsdoprnt_params_t = struct {
	fdbase         ppint32
	fdconv         ppint32
	fdexpfmt       ppuintptr
	fdexptimes4    ppint32
	fdfill         ppuint8
	fdjustify      ppint32
	fdprec         ppint32
	fdshowbase     ppint32
	fdshowpoint    ppint32
	fdshowtrailing ppint32
	fdsign         ppuint8
	fdwidth        ppint32
}

type tngmp_doscan_scan_t = ppuintptr

type tngmp_doscan_step_t = ppuintptr

type tngmp_doscan_get_t = ppuintptr

type tngmp_doscan_unget_t = ppuintptr

type tsgmp_doscan_funs_t = struct {
	fdscan  tngmp_doscan_scan_t
	fdstep  tngmp_doscan_step_t
	fdget   tngmp_doscan_get_t
	fdunget tngmp_doscan_unget_t
}

type tn__jmp_buf = [18]ppuint64

type tnjmp_buf = [1]ts__jmp_buf_tag

type ts__jmp_buf_tag = struct {
	fd__jb tn__jmp_buf
	fd__fl ppuint64
	fd__ss [16]ppuint64
}

type tnsigjmp_buf = [1]ts__jmp_buf_tag

/* Establish ostringstream and istringstream.  Do this here so as to hide
   the conditionals, rather than putting stuff in each test program.

   Oldish versions of g++, like 2.95.2, don't have <sstream>, only
   <strstream>.  Fake up ostringstream and istringstream classes, but not a
   full implementation, just enough for our purposes.  */

func Xrefmpf_add(cgtls *iqlibc.ppTLS, aaw tnmpf_ptr, aau tnmpf_srcptr, aav tnmpf_srcptr) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aa__dst, aa__dst1, aa__dst2, aa__dst3, aa__dst4, aa__dst5, aa__dst6, aaut, aavt, aawt, ccv14, ccv16, ccv28, ccv31, ccv38, ccv40, ccv47, ccv49, ccv53, ccv55, ccv6, ccv8 tnmp_ptr
	var aa__n, aa__n1, aa__n2, aa__n3, aa__n4, aa__n5, aa__n6, aahi, aalo, aasize, ccv12, ccv26, ccv29, ccv36, ccv4, ccv45, ccv51 tnmp_size_t
	var aa__src, aa__src1, aa__src2, aa__src3, aa__src4, ccv11, ccv15, ccv3, ccv35, ccv39, ccv44, ccv48, ccv50, ccv54, ccv7 tnmp_srcptr
	var aa__x, aa__x1, aa__x2, aa__x3, aa__x4, aacy tnmp_limb_t
	var aaexp tnmp_exp_t
	var aaneg, aaoff, ccv1, ccv19, ccv20, ccv21, ccv22, ccv32, ccv33, ccv34, ccv41, ccv42, ccv43, ccv9 ppint32
	var ccv10, ccv2, ccv23, ccv24, ccv25 ppuintptr
	var ccv17, ccv18, ccv56 ppint64
	var pp_ /* __tmp_marker at bp+0 */ ppuintptr
	var pp_ /* tmp at bp+8 */ tnmpf_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__dst, aa__dst1, aa__dst2, aa__dst3, aa__dst4, aa__dst5, aa__dst6, aa__n, aa__n1, aa__n2, aa__n3, aa__n4, aa__n5, aa__n6, aa__src, aa__src1, aa__src2, aa__src3, aa__src4, aa__x, aa__x1, aa__x2, aa__x3, aa__x4, aacy, aaexp, aahi, aalo, aaneg, aaoff, aasize, aaut, aavt, aawt, ccv1, ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv2, ccv20, ccv21, ccv22, ccv23, ccv24, ccv25, ccv26, ccv28, ccv29, ccv3, ccv31, ccv32, ccv33, ccv34, ccv35, ccv36, ccv38, ccv39, ccv4, ccv40, ccv41, ccv42, ccv43, ccv44, ccv45, ccv47, ccv48, ccv49, ccv50, ccv51, ccv53, ccv54, ccv55, ccv56, ccv6, ccv7, ccv8, ccv9
	cgtls.AllocaEntry()
	defer cgtls.AllocaExit()

	*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) = ppuintptr(0)

	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size == 0 {

		if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
			ccv1 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		} else {
			ccv1 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		}
		aasize = ppint64(ccv1)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
			ccv2 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
		} else {
			ccv2 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
		}
		aawt = ccv2
		if aasize != 0 {
			aa__n = aasize - ppint64(1)
			aa__dst = aawt
			aa__src = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_d
			ccv3 = aa__src
			aa__src += 8
			aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv3))
			if aa__n != 0 {
				for {
					ccv6 = aa__dst
					aa__dst += 8
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv6)) = aa__x
					ccv7 = aa__src
					aa__src += 8
					aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv7))
					goto cg_5
				cg_5:
					;
					aa__n--
					ccv4 = aa__n
					if !(ccv4 != 0) {
						break
					}
				}
			}
			ccv8 = aa__dst
			aa__dst += 8
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv8)) = aa__x
		}
		aaexp = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp
		aaneg = iqlibc.ppBoolInt32((*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size < 0)
		goto ppdone
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size == 0 {

		if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
			ccv9 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		} else {
			ccv9 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		}
		aasize = ppint64(ccv9)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
			ccv10 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
		} else {
			ccv10 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
		}
		aawt = ccv10
		if aasize != 0 {
			aa__n1 = aasize - ppint64(1)
			aa__dst1 = aawt
			aa__src1 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_d
			ccv11 = aa__src1
			aa__src1 += 8
			aa__x1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv11))
			if aa__n1 != 0 {
				for {
					ccv14 = aa__dst1
					aa__dst1 += 8
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv14)) = aa__x1
					ccv15 = aa__src1
					aa__src1 += 8
					aa__x1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv15))
					goto cg_13
				cg_13:
					;
					aa__n1--
					ccv12 = aa__n1
					if !(ccv12 != 0) {
						break
					}
				}
			}
			ccv16 = aa__dst1
			aa__dst1 += 8
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv16)) = aa__x1
		}
		aaexp = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp
		aaneg = iqlibc.ppBoolInt32((*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size < 0)
		goto ppdone
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size^(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size < 0 {

		(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 8)).fd_mp_size = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 8)).fd_mp_exp = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp
		(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 8)).fd_mp_d = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_d
		Xrefmpf_sub(cgtls, aaw, aau, cgbp+8)
		return
	}
	aaneg = iqlibc.ppBoolInt32((*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size < 0)

	/* Compute the significance of the hi and lo end of the result.  */
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp > (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp {
		ccv17 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp
	} else {
		ccv17 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp
	}
	aahi = ccv17
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
		ccv19 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	} else {
		ccv19 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
		ccv20 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	} else {
		ccv20 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp-ppint64(ccv19) < (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp-ppint64(ccv20) {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
			ccv21 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		} else {
			ccv21 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		}
		ccv18 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp - ppint64(ccv21)
	} else {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
			ccv22 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		} else {
			ccv22 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		}
		ccv18 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp - ppint64(ccv22)
	}
	aalo = ccv18
	aasize = aahi - aalo
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv23 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	} else {
		ccv23 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	}
	aaut = ccv23
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv24 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	} else {
		ccv24 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	}
	aavt = ccv24
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv25 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	} else {
		ccv25 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	}
	aawt = ccv25
	if aasize != 0 {
		aa__dst2 = aaut
		aa__n2 = aasize
		for {
			ccv28 = aa__dst2
			aa__dst2 += 8
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv28)) = iqlibc.ppUint64FromInt64(0)
			goto cg_27
		cg_27:
			;
			aa__n2--
			ccv26 = aa__n2
			if !(ccv26 != 0) {
				break
			}
		}
	}
	if aasize != 0 {
		aa__dst3 = aavt
		aa__n3 = aasize
		for {
			ccv31 = aa__dst3
			aa__dst3 += 8
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv31)) = iqlibc.ppUint64FromInt64(0)
			goto cg_30
		cg_30:
			;
			aa__n3--
			ccv29 = aa__n3
			if !(ccv29 != 0) {
				break
			}
		}
	}

	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
		ccv32 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	} else {
		ccv32 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	}
	aaoff = ppint32(aasize + ((*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp - aahi) - ppint64(ccv32))
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
		ccv33 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	} else {
		ccv33 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	}
	if ccv33 != 0 {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
			ccv34 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		} else {
			ccv34 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		}
		aa__n4 = ppint64(ccv34 - ppint32(1))
		aa__dst4 = aaut + ppuintptr(aaoff)*8
		aa__src2 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_d
		ccv35 = aa__src2
		aa__src2 += 8
		aa__x2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv35))
		if aa__n4 != 0 {
			for {
				ccv38 = aa__dst4
				aa__dst4 += 8
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv38)) = aa__x2
				ccv39 = aa__src2
				aa__src2 += 8
				aa__x2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv39))
				goto cg_37
			cg_37:
				;
				aa__n4--
				ccv36 = aa__n4
				if !(ccv36 != 0) {
					break
				}
			}
		}
		ccv40 = aa__dst4
		aa__dst4 += 8
		*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv40)) = aa__x2
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
		ccv41 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	} else {
		ccv41 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	}
	aaoff = ppint32(aasize + ((*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp - aahi) - ppint64(ccv41))
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
		ccv42 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	} else {
		ccv42 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	}
	if ccv42 != 0 {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
			ccv43 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		} else {
			ccv43 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		}
		aa__n5 = ppint64(ccv43 - ppint32(1))
		aa__dst5 = aavt + ppuintptr(aaoff)*8
		aa__src3 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_d
		ccv44 = aa__src3
		aa__src3 += 8
		aa__x3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv44))
		if aa__n5 != 0 {
			for {
				ccv47 = aa__dst5
				aa__dst5 += 8
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv47)) = aa__x3
				ccv48 = aa__src3
				aa__src3 += 8
				aa__x3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv48))
				goto cg_46
			cg_46:
				;
				aa__n5--
				ccv45 = aa__n5
				if !(ccv45 != 0) {
					break
				}
			}
		}
		ccv49 = aa__dst5
		aa__dst5 += 8
		*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv49)) = aa__x3
	}

	aacy = X__gmpn_add_n(cgtls, aawt, aaut, aavt, aasize)
	*(*tnmp_limb_t)(iqunsafe.ppPointer(aawt + ppuintptr(aasize)*8)) = aacy

	aasize = tnmp_size_t(ppuint64(aasize) + aacy)
	aaexp = iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(aahi) + aacy)

	goto ppdone
ppdone:
	;
	if aasize > ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_prec) {

		aawt += ppuintptr(aasize-ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_prec)) * 8
		aasize = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_prec)
	}
	if aasize != 0 {
		aa__n6 = aasize - ppint64(1)
		aa__dst6 = (*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_d
		aa__src4 = aawt
		ccv50 = aa__src4
		aa__src4 += 8
		aa__x4 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv50))
		if aa__n6 != 0 {
			for {
				ccv53 = aa__dst6
				aa__dst6 += 8
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv53)) = aa__x4
				ccv54 = aa__src4
				aa__src4 += 8
				aa__x4 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv54))
				goto cg_52
			cg_52:
				;
				aa__n6--
				ccv51 = aa__n6
				if !(ccv51 != 0) {
					break
				}
			}
		}
		ccv55 = aa__dst6
		aa__dst6 += 8
		*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv55)) = aa__x4
	}
	if aaneg == 0 {
		ccv56 = aasize
	} else {
		ccv56 = -aasize
	}
	(*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_size = ppint32(ccv56)
	(*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_exp = aaexp
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) != ppuintptr(0)) != 0), 0) != 0 {
		X__gmp_tmp_reentrant_free(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp)))
	}
}

/* Add 1 "unit in last place" (ie. in the least significant limb) to f.
   f cannot be zero, since that has no well-defined "last place".

   This routine is designed for use in cases where we pay close attention to
   the size of the data value and are using that (and the exponent) to
   indicate the accurate part of a result, or similar.  For this reason, if
   there's a carry out we don't store 1 and adjust the exponent, we just
   leave 100..00.  We don't even adjust if there's a carry out of prec+1
   limbs, but instead give up in that case (which we intend shouldn't arise
   in normal circumstances).  */

func Xrefmpf_add_ulp(cgtls *iqlibc.ppTLS, aaf tnmpf_ptr) {

	var aaabs_fsize, aafsize tnmp_size_t
	var aac tnmp_limb_t
	var aafp tnmp_ptr
	var ccv1 ppint32
	var ccv2 ppint64
	pp_, pp_, pp_, pp_, pp_, pp_ = aaabs_fsize, aac, aafp, aafsize, ccv1, ccv2
	aafp = (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_d
	aafsize = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size)
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size >= 0 {
		ccv1 = (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size
	} else {
		ccv1 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size
	}
	aaabs_fsize = ppint64(ccv1)

	if aafsize == 0 {

		Xprintf(cgtls, "Oops, refmpf_add_ulp called with f==0\n\x00", 0)
		Xabort(cgtls)
	}

	aac = Xrefmpn_add_1(cgtls, aafp, aafp, aaabs_fsize, iqlibc.ppUint64FromInt64(1))
	if aac != ppuint64(0) {

		if aaabs_fsize >= ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_prec+ppint32(1)) {

			Xprintf(cgtls, "Oops, refmpf_add_ulp carried out of prec+1 limbs\n\x00", 0)
			Xabort(cgtls)
		}

		*(*tnmp_limb_t)(iqunsafe.ppPointer(aafp + ppuintptr(aaabs_fsize)*8)) = aac
		aaabs_fsize++
		if aafsize > 0 {
			ccv2 = aaabs_fsize
		} else {
			ccv2 = -aaabs_fsize
		}
		(*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size = ppint32(ccv2)
		(*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_exp++
	}
}

// C documentation
//
//	/* Fill f with size limbs of the given value, setup as an integer. */
func Xrefmpf_fill(cgtls *iqlibc.ppTLS, aaf tnmpf_ptr, aasize tnmp_size_t, aavalue tnmp_limb_t) {

	var ccv1 ppint64
	pp_ = ccv1

	if ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_prec+iqlibc.ppInt32FromInt32(1)) < aasize {
		ccv1 = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_prec + iqlibc.ppInt32FromInt32(1))
	} else {
		ccv1 = aasize
	}
	aasize = ccv1
	(*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size = ppint32(aasize)
	(*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_exp = aasize
	Xrefmpn_fill(cgtls, (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_d, aasize, aavalue)
}

// C documentation
//
//	/* Strip high zero limbs from the f data, adjusting exponent accordingly. */
func Xrefmpf_normalize(cgtls *iqlibc.ppTLS, aaf tnmpf_ptr) {

	var ccv1, ccv3 ppint32
	var ccv2 ppbool
	pp_, pp_, pp_ = ccv1, ccv2, ccv3
	for {
		if ccv2 = (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size != 0; ccv2 {
			if (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size >= 0 {
				ccv1 = (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size
			} else {
				ccv1 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size
			}
		}
		if !(ccv2 && *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_d + ppuintptr(ccv1-ppint32(1))*8)) == ppuint64(0)) {
			break
		}

		if (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size >= 0 {
			ccv3 = (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size - ppint32(1)
		} else {
			ccv3 = (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size + ppint32(1)
		}
		(*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size = ccv3
		(*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_exp--

	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_size == 0 {
		(*tn__mpf_struct)(iqunsafe.ppPointer(aaf)).fd_mp_exp = 0
	}
}

/* refmpf_set_overlap sets up dst as a copy of src, but with PREC(dst)
   unchanged, in preparation for an overlap test.

   The full value of src is copied, and the space at PTR(dst) is extended as
   necessary.  The way PREC(dst) is unchanged is as per an mpf_set_prec_raw.
   The return value is the new PTR(dst) space precision, in bits, ready for
   a restoring mpf_set_prec_raw before mpf_clear.  */

func Xrefmpf_set_overlap(cgtls *iqlibc.ppTLS, aadst tnmpf_ptr, aasrc tnmpf_srcptr) (cgr ppuint64) {

	var aadprec, aassize tnmp_size_t
	var aaret ppuint64
	var ccv1 ppint32
	var ccv2 ppint64
	pp_, pp_, pp_, pp_, pp_ = aadprec, aaret, aassize, ccv1, ccv2
	aadprec = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aadst)).fd_mp_prec)
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aasrc)).fd_mp_size >= 0 {
		ccv1 = (*tn__mpf_struct)(iqunsafe.ppPointer(aasrc)).fd_mp_size
	} else {
		ccv1 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aasrc)).fd_mp_size
	}
	aassize = ppint64(ccv1)

	if aadprec > aassize {
		ccv2 = aadprec
	} else {
		ccv2 = aassize
	}
	Xrefmpf_set_prec_limbs(cgtls, aadst, iqlibc.ppUint64FromInt64(ccv2))
	X__gmpf_set(cgtls, aadst, aasrc)

	aaret = X__gmpf_get_prec(cgtls, aadst)
	(*tn__mpf_struct)(iqunsafe.ppPointer(aadst)).fd_mp_prec = ppint32(aadprec)
	return aaret
}

// C documentation
//
//	/* Like mpf_set_prec, but taking a precision in limbs.
//	   PREC(f) ends up as the given "prec" value.  */
func Xrefmpf_set_prec_limbs(cgtls *iqlibc.ppTLS, aaf tnmpf_ptr, aaprec ppuint64) {

	X__gmpf_set_prec(cgtls, aaf, aaprec*iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
}

func Xrefmpf_sub(cgtls *iqlibc.ppTLS, aaw tnmpf_ptr, aau tnmpf_srcptr, aav tnmpf_srcptr) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aa__dst, aa__dst1, aa__dst2, aa__dst3, aa__dst4, aa__dst5, aa__dst6, aaut, aavt, aawt, ccv14, ccv16, ccv28, ccv31, ccv38, ccv40, ccv47, ccv49, ccv57, ccv59, ccv6, ccv8 tnmp_ptr
	var aa__gmp_i, aa__n, aa__n1, aa__n2, aa__n3, aa__n4, aa__n5, aa__n6, aahi, aalo, aasize, ccv12, ccv26, ccv29, ccv36, ccv4, ccv45, ccv50, ccv55 tnmp_size_t
	var aa__gmp_result, aaneg, aaoff, ccv1, ccv19, ccv20, ccv21, ccv22, ccv32, ccv33, ccv34, ccv41, ccv42, ccv43, ccv51, ccv52, ccv9 ppint32
	var aa__gmp_x, aa__gmp_y, aa__x, aa__x1, aa__x2, aa__x3, aa__x4 tnmp_limb_t
	var aa__src, aa__src1, aa__src2, aa__src3, aa__src4, ccv11, ccv15, ccv3, ccv35, ccv39, ccv44, ccv48, ccv54, ccv58, ccv7 tnmp_srcptr
	var aaexp tnmp_exp_t
	var ccv10, ccv2, ccv23, ccv24, ccv25 ppuintptr
	var ccv17, ccv18, ccv60 ppint64
	var pp_ /* __tmp_marker at bp+0 */ ppuintptr
	var pp_ /* tmp at bp+8 */ tnmpf_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__dst, aa__dst1, aa__dst2, aa__dst3, aa__dst4, aa__dst5, aa__dst6, aa__gmp_i, aa__gmp_result, aa__gmp_x, aa__gmp_y, aa__n, aa__n1, aa__n2, aa__n3, aa__n4, aa__n5, aa__n6, aa__src, aa__src1, aa__src2, aa__src3, aa__src4, aa__x, aa__x1, aa__x2, aa__x3, aa__x4, aaexp, aahi, aalo, aaneg, aaoff, aasize, aaut, aavt, aawt, ccv1, ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv2, ccv20, ccv21, ccv22, ccv23, ccv24, ccv25, ccv26, ccv28, ccv29, ccv3, ccv31, ccv32, ccv33, ccv34, ccv35, ccv36, ccv38, ccv39, ccv4, ccv40, ccv41, ccv42, ccv43, ccv44, ccv45, ccv47, ccv48, ccv49, ccv50, ccv51, ccv52, ccv54, ccv55, ccv57, ccv58, ccv59, ccv6, ccv60, ccv7, ccv8, ccv9
	cgtls.AllocaEntry()
	defer cgtls.AllocaExit()

	*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) = ppuintptr(0)

	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size == 0 {

		if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
			ccv1 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		} else {
			ccv1 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		}
		aasize = ppint64(ccv1)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
			ccv2 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
		} else {
			ccv2 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
		}
		aawt = ccv2
		if aasize != 0 {
			aa__n = aasize - ppint64(1)
			aa__dst = aawt
			aa__src = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_d
			ccv3 = aa__src
			aa__src += 8
			aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv3))
			if aa__n != 0 {
				for {
					ccv6 = aa__dst
					aa__dst += 8
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv6)) = aa__x
					ccv7 = aa__src
					aa__src += 8
					aa__x = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv7))
					goto cg_5
				cg_5:
					;
					aa__n--
					ccv4 = aa__n
					if !(ccv4 != 0) {
						break
					}
				}
			}
			ccv8 = aa__dst
			aa__dst += 8
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv8)) = aa__x
		}
		aaexp = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp
		aaneg = iqlibc.ppBoolInt32((*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size > 0)
		goto ppdone
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size == 0 {

		if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
			ccv9 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		} else {
			ccv9 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		}
		aasize = ppint64(ccv9)
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
			ccv10 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
		} else {
			ccv10 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
		}
		aawt = ccv10
		if aasize != 0 {
			aa__n1 = aasize - ppint64(1)
			aa__dst1 = aawt
			aa__src1 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_d
			ccv11 = aa__src1
			aa__src1 += 8
			aa__x1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv11))
			if aa__n1 != 0 {
				for {
					ccv14 = aa__dst1
					aa__dst1 += 8
					*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv14)) = aa__x1
					ccv15 = aa__src1
					aa__src1 += 8
					aa__x1 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv15))
					goto cg_13
				cg_13:
					;
					aa__n1--
					ccv12 = aa__n1
					if !(ccv12 != 0) {
						break
					}
				}
			}
			ccv16 = aa__dst1
			aa__dst1 += 8
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv16)) = aa__x1
		}
		aaexp = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp
		aaneg = iqlibc.ppBoolInt32((*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size < 0)
		goto ppdone
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size^(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size < 0 {

		(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 8)).fd_mp_size = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 8)).fd_mp_exp = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp
		(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp + 8)).fd_mp_d = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_d
		Xrefmpf_add(cgtls, aaw, aau, cgbp+8)
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size < 0 {
			X__gmpf_neg(cgtls, aaw, aaw)
		}
		return
	}
	aaneg = iqlibc.ppBoolInt32((*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size < 0)

	/* Compute the significance of the hi and lo end of the result.  */
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp > (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp {
		ccv17 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp
	} else {
		ccv17 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp
	}
	aahi = ccv17
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
		ccv19 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	} else {
		ccv19 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
		ccv20 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	} else {
		ccv20 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp-ppint64(ccv19) < (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp-ppint64(ccv20) {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
			ccv21 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		} else {
			ccv21 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		}
		ccv18 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp - ppint64(ccv21)
	} else {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
			ccv22 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		} else {
			ccv22 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		}
		ccv18 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp - ppint64(ccv22)
	}
	aalo = ccv18
	aasize = aahi - aalo
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv23 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	} else {
		ccv23 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	}
	aaut = ccv23
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv24 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	} else {
		ccv24 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	}
	aavt = ccv24
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8) <= ppuint64(0x7f00)) != 0), ppint64(1)) != 0 {
		ccv25 = X__builtin_alloca(cgtls, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	} else {
		ccv25 = X__gmp_tmp_reentrant_alloc(cgtls, cgbp, iqlibc.ppUint64FromInt64(aasize+iqlibc.ppInt64FromInt32(1))*ppuint64(8))
	}
	aawt = ccv25
	if aasize != 0 {
		aa__dst2 = aaut
		aa__n2 = aasize
		for {
			ccv28 = aa__dst2
			aa__dst2 += 8
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv28)) = iqlibc.ppUint64FromInt64(0)
			goto cg_27
		cg_27:
			;
			aa__n2--
			ccv26 = aa__n2
			if !(ccv26 != 0) {
				break
			}
		}
	}
	if aasize != 0 {
		aa__dst3 = aavt
		aa__n3 = aasize
		for {
			ccv31 = aa__dst3
			aa__dst3 += 8
			*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv31)) = iqlibc.ppUint64FromInt64(0)
			goto cg_30
		cg_30:
			;
			aa__n3--
			ccv29 = aa__n3
			if !(ccv29 != 0) {
				break
			}
		}
	}

	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
		ccv32 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	} else {
		ccv32 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	}
	aaoff = ppint32(aasize + ((*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_exp - aahi) - ppint64(ccv32))
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
		ccv33 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	} else {
		ccv33 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
	}
	if ccv33 != 0 {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size >= 0 {
			ccv34 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		} else {
			ccv34 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_size
		}
		aa__n4 = ppint64(ccv34 - ppint32(1))
		aa__dst4 = aaut + ppuintptr(aaoff)*8
		aa__src2 = (*tn__mpf_struct)(iqunsafe.ppPointer(aau)).fd_mp_d
		ccv35 = aa__src2
		aa__src2 += 8
		aa__x2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv35))
		if aa__n4 != 0 {
			for {
				ccv38 = aa__dst4
				aa__dst4 += 8
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv38)) = aa__x2
				ccv39 = aa__src2
				aa__src2 += 8
				aa__x2 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv39))
				goto cg_37
			cg_37:
				;
				aa__n4--
				ccv36 = aa__n4
				if !(ccv36 != 0) {
					break
				}
			}
		}
		ccv40 = aa__dst4
		aa__dst4 += 8
		*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv40)) = aa__x2
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
		ccv41 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	} else {
		ccv41 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	}
	aaoff = ppint32(aasize + ((*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_exp - aahi) - ppint64(ccv41))
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
		ccv42 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	} else {
		ccv42 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
	}
	if ccv42 != 0 {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size >= 0 {
			ccv43 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		} else {
			ccv43 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_size
		}
		aa__n5 = ppint64(ccv43 - ppint32(1))
		aa__dst5 = aavt + ppuintptr(aaoff)*8
		aa__src3 = (*tn__mpf_struct)(iqunsafe.ppPointer(aav)).fd_mp_d
		ccv44 = aa__src3
		aa__src3 += 8
		aa__x3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv44))
		if aa__n5 != 0 {
			for {
				ccv47 = aa__dst5
				aa__dst5 += 8
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv47)) = aa__x3
				ccv48 = aa__src3
				aa__src3 += 8
				aa__x3 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv48))
				goto cg_46
			cg_46:
				;
				aa__n5--
				ccv45 = aa__n5
				if !(ccv45 != 0) {
					break
				}
			}
		}
		ccv49 = aa__dst5
		aa__dst5 += 8
		*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv49)) = aa__x3
	}

	aa__gmp_result = 0
	aa__gmp_i = aasize
	for {
		aa__gmp_i--
		ccv50 = aa__gmp_i
		if !(ccv50 >= iqlibc.ppInt64FromInt32(0)) {
			break
		}
		aa__gmp_x = *(*tnmp_limb_t)(iqunsafe.ppPointer(aaut + ppuintptr(aa__gmp_i)*8))
		aa__gmp_y = *(*tnmp_limb_t)(iqunsafe.ppPointer(aavt + ppuintptr(aa__gmp_i)*8))
		if aa__gmp_x != aa__gmp_y {
			if aa__gmp_x > aa__gmp_y {
				ccv51 = ppint32(1)
			} else {
				ccv51 = -ppint32(1)
			}
			aa__gmp_result = ccv51
			break
		}
	}
	ccv52 = aa__gmp_result
	goto cg_53
cg_53:
	if ccv52 >= 0 {
		X__gmpn_sub_n(cgtls, aawt, aaut, aavt, aasize)
	} else {

		X__gmpn_sub_n(cgtls, aawt, aavt, aaut, aasize)

		aaneg ^= ppint32(1)
	}
	aaexp = aahi
	for aasize != 0 && *(*tnmp_limb_t)(iqunsafe.ppPointer(aawt + ppuintptr(aasize-ppint64(1))*8)) == ppuint64(0) {

		aasize--
		aaexp--
	}

	goto ppdone
ppdone:
	;
	if aasize > ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_prec) {

		aawt += ppuintptr(aasize-ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_prec)) * 8
		aasize = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_prec)
	}
	if aasize != 0 {
		aa__n6 = aasize - ppint64(1)
		aa__dst6 = (*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_d
		aa__src4 = aawt
		ccv54 = aa__src4
		aa__src4 += 8
		aa__x4 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv54))
		if aa__n6 != 0 {
			for {
				ccv57 = aa__dst6
				aa__dst6 += 8
				*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv57)) = aa__x4
				ccv58 = aa__src4
				aa__src4 += 8
				aa__x4 = *(*tnmp_limb_t)(iqunsafe.ppPointer(ccv58))
				goto cg_56
			cg_56:
				;
				aa__n6--
				ccv55 = aa__n6
				if !(ccv55 != 0) {
					break
				}
			}
		}
		ccv59 = aa__dst6
		aa__dst6 += 8
		*(*tnmp_limb_t)(iqunsafe.ppPointer(ccv59)) = aa__x4
	}
	if aaneg == 0 {
		ccv60 = aasize
	} else {
		ccv60 = -aasize
	}
	(*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_size = ppint32(ccv60)
	(*tn__mpf_struct)(iqunsafe.ppPointer(aaw)).fd_mp_exp = aaexp
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) != ppuintptr(0)) != 0), 0) != 0 {
		X__gmp_tmp_reentrant_free(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp)))
	}
}

/* Validate got by comparing to want.  Return 1 if good, 0 if bad.

   The data in got is compared to that in want, up to either PREC(got) limbs
   or the size of got, whichever is bigger.  Clearly we always demand
   PREC(got) of accuracy, but we go further and say that if got is bigger
   then any extra must be correct too.

   want needs to have enough data to allow this comparison.  The size in
   want doesn't have to be that big though, if it's smaller then further low
   limbs are taken to be zero.

   This validation approach is designed to allow some flexibility in exactly
   how much data is generated by an mpf function, ie. either prec or prec+1
   limbs.  We don't try to make a reference function that emulates that same
   size decision, instead the idea is for a validation function to generate
   at least as much data as the real function, then compare.  */

func Xrefmpf_validate(cgtls *iqlibc.ppTLS, aaname ppuintptr, aagot tnmpf_srcptr, aawant tnmpf_srcptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aabad, ccv1, ccv10, ccv12, ccv2 ppint32
	var aacmpsize, aagsize, aai, aawsize tnmp_size_t
	var aaglimb, aawlimb tnmp_limb_t
	var aagp, aawp tnmp_srcptr
	var ccv3, ccv4, ccv5 ppint64
	var ccv7, ccv8 ppuint64
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aabad, aacmpsize, aaglimb, aagp, aagsize, aai, aawlimb, aawp, aawsize, ccv1, ccv10, ccv12, ccv2, ccv3, ccv4, ccv5, ccv7, ccv8
	aabad = 0

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_prec) >= ppint64((iqlibc.ppInt32FromInt32(53)+iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-iqlibc.ppInt32FromInt32(1))/(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpf.c\x00", ppint32(309), "((got)->_mp_prec) >= ((mp_size_t) ((((53) > (53) ? (53) : (53)) + 2 * (64 - 0) - 1) / (64 - 0)))\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size >= 0 {
		ccv1 = (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size
	} else {
		ccv1 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size
	}
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv1 <= (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_prec+iqlibc.ppInt32FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpf.c\x00", ppint32(309), "((((got)->_mp_size)) >= 0 ? (((got)->_mp_size)) : -(((got)->_mp_size))) <= ((got)->_mp_prec)+1\x00")
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size == 0 {
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_exp == iqlibc.ppInt64FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpf.c\x00", ppint32(309), "((got)->_mp_exp) == 0\x00")
		}
	}
	if (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size != 0 {
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size >= 0 {
			ccv2 = (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size
		} else {
			ccv2 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_d + ppuintptr(ccv2-ppint32(1))*8)) != iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "refmpf.c\x00", ppint32(309), "((got)->_mp_d)[((((got)->_mp_size)) >= 0 ? (((got)->_mp_size)) : -(((got)->_mp_size))) - 1] != 0\x00")
		}
	}

	if (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_exp != (*tn__mpf_struct)(iqunsafe.ppPointer(aawant)).fd_mp_exp {

		Xprintf(cgtls, "%s: wrong exponent\n\x00", iqlibc.ppVaList(cgbp+8, aaname))
		aabad = ppint32(1)
	}

	aagsize = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size)
	aawsize = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aawant)).fd_mp_size)
	if aagsize < 0 && aawsize > 0 || aagsize > 0 && aawsize < 0 {

		Xprintf(cgtls, "%s: wrong sign\n\x00", iqlibc.ppVaList(cgbp+8, aaname))
		aabad = ppint32(1)
	}

	if aagsize >= 0 {
		ccv3 = aagsize
	} else {
		ccv3 = -aagsize
	}
	aagsize = ccv3
	if aawsize >= 0 {
		ccv4 = aawsize
	} else {
		ccv4 = -aawsize
	}
	aawsize = ccv4

	/* most significant limb of respective data */
	aagp = (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_d + ppuintptr(aagsize)*8 - ppuintptr(1)*8
	aawp = (*tn__mpf_struct)(iqunsafe.ppPointer(aawant)).fd_mp_d + ppuintptr(aawsize)*8 - ppuintptr(1)*8

	/* compare limb data */
	if ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_prec) > aagsize {
		ccv5 = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_prec)
	} else {
		ccv5 = aagsize
	}
	aacmpsize = ccv5
	aai = 0
	for {
		if !(aai < aacmpsize) {
			break
		}

		if aai < aagsize {
			ccv7 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aagp + ppuintptr(-aai)*8))
		} else {
			ccv7 = ppuint64(0)
		}
		aaglimb = ccv7
		if aai < aawsize {
			ccv8 = *(*tnmp_limb_t)(iqunsafe.ppPointer(aawp + ppuintptr(-aai)*8))
		} else {
			ccv8 = ppuint64(0)
		}
		aawlimb = ccv8

		if aaglimb != aawlimb {

			Xprintf(cgtls, "%s: wrong data starting at index %ld from top\n\x00", iqlibc.ppVaList(cgbp+8, aaname, aai))
			aabad = ppint32(1)
			break
		}

		goto cg_6
	cg_6:
		;
		aai++
	}

	if aabad != 0 {

		Xprintf(cgtls, "  prec       %d\n\x00", iqlibc.ppVaList(cgbp+8, (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_prec))
		Xprintf(cgtls, "  exp got    %ld\n\x00", iqlibc.ppVaList(cgbp+8, (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_exp))
		Xprintf(cgtls, "  exp want   %ld\n\x00", iqlibc.ppVaList(cgbp+8, (*tn__mpf_struct)(iqunsafe.ppPointer(aawant)).fd_mp_exp))
		Xprintf(cgtls, "  size got   %d\n\x00", iqlibc.ppVaList(cgbp+8, (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size))
		Xprintf(cgtls, "  size want  %d\n\x00", iqlibc.ppVaList(cgbp+8, (*tn__mpf_struct)(iqunsafe.ppPointer(aawant)).fd_mp_size))
		Xprintf(cgtls, "  limbs (high to low)\n\x00", 0)
		Xprintf(cgtls, "   got  \x00", 0)
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size >= 0 {
			ccv10 = (*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size
		} else {
			ccv10 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_size
		}
		aai = ppint64(ccv10 - ppint32(1))
		for {
			if !(aai >= 0) {
				break
			}

			X__gmp_printf(cgtls, "%MX\x00", iqlibc.ppVaList(cgbp+8, *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_d + ppuintptr(aai)*8))))
			if aai != 0 {
				Xprintf(cgtls, ",\x00", 0)
			}

			goto cg_9
		cg_9:
			;
			aai--
		}
		Xprintf(cgtls, "\n\x00", 0)
		Xprintf(cgtls, "   want \x00", 0)
		if (*tn__mpf_struct)(iqunsafe.ppPointer(aawant)).fd_mp_size >= 0 {
			ccv12 = (*tn__mpf_struct)(iqunsafe.ppPointer(aawant)).fd_mp_size
		} else {
			ccv12 = -(*tn__mpf_struct)(iqunsafe.ppPointer(aawant)).fd_mp_size
		}
		aai = ppint64(ccv12 - ppint32(1))
		for {
			if !(aai >= 0) {
				break
			}

			X__gmp_printf(cgtls, "%MX\x00", iqlibc.ppVaList(cgbp+8, *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpf_struct)(iqunsafe.ppPointer(aawant)).fd_mp_d + ppuintptr(aai)*8))))
			if aai != 0 {
				Xprintf(cgtls, ",\x00", 0)
			}

			goto cg_11
		cg_11:
			;
			aai--
		}
		Xprintf(cgtls, "\n\x00", 0)
		return 0
	}

	return ppint32(1)
}

func Xrefmpf_validate_division(cgtls *iqlibc.ppTLS, aaname ppuintptr, aagot tnmpf_srcptr, aan tnmpf_srcptr, aad tnmpf_srcptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aadp, aanp tnmp_srcptr
	var aadsize, aansize, aaprec, aaqsize, aasign, aatsize tnmp_size_t
	var aaqp, aarp, aatp tnmp_ptr
	var aaret ppint32
	var ccv1, ccv2, ccv3 ppint64
	var pp_ /* want at bp+0 */ tnmpf_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aadp, aadsize, aanp, aansize, aaprec, aaqp, aaqsize, aaret, aarp, aasign, aatp, aatsize, ccv1, ccv2, ccv3

	aansize = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aan)).fd_mp_size)
	aadsize = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aad)).fd_mp_size)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aadsize != iqlibc.ppInt64FromInt32(0))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpf.c\x00", ppint32(391), "dsize != 0\x00")
	}

	aasign = aansize ^ aadsize
	if aansize >= 0 {
		ccv1 = aansize
	} else {
		ccv1 = -aansize
	}
	aansize = ccv1
	if aadsize >= 0 {
		ccv2 = aadsize
	} else {
		ccv2 = -aadsize
	}
	aadsize = ccv2

	aanp = (*tn__mpf_struct)(iqunsafe.ppPointer(aan)).fd_mp_d
	aadp = (*tn__mpf_struct)(iqunsafe.ppPointer(aad)).fd_mp_d
	aaprec = ppint64((*tn__mpf_struct)(iqunsafe.ppPointer(aagot)).fd_mp_prec)

	(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_exp = (*tn__mpf_struct)(iqunsafe.ppPointer(aan)).fd_mp_exp - (*tn__mpf_struct)(iqunsafe.ppPointer(aad)).fd_mp_exp + ppint64(1)

	aaqsize = aaprec + ppint64(2)            /* at least prec+1 limbs, after high zero */
	aatsize = aaqsize + aadsize - ppint64(1) /* dividend size to give desired qsize */

	/* dividend n, extended or truncated */
	aatp = Xrefmpn_malloc_limbs(cgtls, aatsize)
	Xrefmpn_copy_extend(cgtls, aatp, aatsize, aanp, aansize)

	aaqp = Xrefmpn_malloc_limbs(cgtls, aaqsize)
	aarp = Xrefmpn_malloc_limbs(cgtls, aadsize) /* remainder, unused */

	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aaqsize == aatsize-aadsize+iqlibc.ppInt64FromInt32(1))) != 0), 0) != 0 {
		X__gmp_assert_fail(cgtls, "refmpf.c\x00", ppint32(413), "qsize == tsize - dsize + 1\x00")
	}
	Xrefmpn_tdiv_qr(cgtls, aaqp, aarp, iqlibc.ppInt64FromInt32(0), aatp, aatsize, aadp, aadsize)

	(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_d = aaqp
	if aasign >= 0 {
		ccv3 = aaqsize
	} else {
		ccv3 = -aaqsize
	}
	(*tn__mpf_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size = ppint32(ccv3)
	Xrefmpf_normalize(cgtls, cgbp)

	aaret = Xrefmpf_validate(cgtls, aaname, aagot, cgbp)

	Xfree(cgtls, aatp)
	Xfree(cgtls, aaqp)
	Xfree(cgtls, aarp)

	return aaret
}

func ___builtin_alloca(*iqlibc.ppTLS, ppuint64) ppuintptr

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___gmp_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func ___gmp_printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmp_tmp_reentrant_alloc(*iqlibc.ppTLS, ppuintptr, ppuint64) ppuintptr

func ___gmp_tmp_reentrant_free(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_get_prec(*iqlibc.ppTLS, ppuintptr) ppuint64

func ___gmpf_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpf_set(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpf_set_prec(*iqlibc.ppTLS, ppuintptr, ppuint64)

func ___gmpn_add_n(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint64) ppuint64

func ___gmpn_sub_n(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint64) ppuint64

func _abort(*iqlibc.ppTLS)

func _free(*iqlibc.ppTLS, ppuintptr)

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _refmpn_add_1(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppuint64) ppuint64

func _refmpn_copy_extend(*iqlibc.ppTLS, ppuintptr, ppint64, ppuintptr, ppint64)

func _refmpn_fill(*iqlibc.ppTLS, ppuintptr, ppint64, ppuint64)

func _refmpn_malloc_limbs(*iqlibc.ppTLS, ppint64) ppuintptr

func _refmpn_tdiv_qr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppuintptr, ppint64, ppuintptr, ppint64)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
