// Code generated for linux/riscv64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -ignore-unsupported-alignment -DHAVE_CONFIG_H -I. -I../.. -I../.. -I../../tests -DNDEBUG -c -o t-rand.o.go t-rand.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE = 16
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDELAYTIMER_MAX = 0x7fffffff
const mvENTS = 10
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_CONFIG_H = 1
const mvHOST_NAME_MAX = 255
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMB_LEN_MAX = 4
const mvMQ_PRIO_MAX = 32768
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEED = 1
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 1
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 36
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 1
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 1
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 1
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 1
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 1
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 1
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 1
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 14
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1019
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 36
const mv__LDBL_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__LDBL_DIG__ = 33
const mv__LDBL_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 1
const mv__LDBL_MANT_DIG__ = 113
const mv__LDBL_MAX_10_EXP__ = 4932
const mv__LDBL_MAX_EXP__ = 16384
const mv__LDBL_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LDBL_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__LDBL_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "14.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__riscv = 1
const mv__riscv_a = 2001000
const mv__riscv_arch_test = 1
const mv__riscv_atomic = 1
const mv__riscv_c = 2000000
const mv__riscv_cmodel_medany = 1
const mv__riscv_compressed = 1
const mv__riscv_d = 2002000
const mv__riscv_div = 1
const mv__riscv_f = 2002000
const mv__riscv_fdiv = 1
const mv__riscv_flen = 64
const mv__riscv_float_abi_double = 1
const mv__riscv_fsqrt = 1
const mv__riscv_i = 2001000
const mv__riscv_m = 2000000
const mv__riscv_misaligned_slow = 1
const mv__riscv_mul = 1
const mv__riscv_muldiv = 1
const mv__riscv_xlen = 64
const mv__riscv_zicsr = 2000000
const mv__riscv_zifencei = 2000000
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnwchar_t = ppint32

type tnsize_t = ppuint64

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

/* Define CC and CFLAGS which were used to build this version of GMP */

/* Major version number is the value of __GNU_MP__ too, above. */

// C documentation
//
//	/* These were generated by this very program.  Do not edit!  */
//	/* Integers.  */
var Xz1 = [10]ppuintptr{
	0: "0\x00",
	1: "1\x00",
	2: "1\x00",
	3: "1\x00",
	4: "1\x00",
	5: "0\x00",
	6: "1\x00",
	7: "1\x00",
	8: "1\x00",
	9: "1\x00",
}
var Xz2 = [10]ppuintptr{
	0: "0\x00",
	1: "3\x00",
	2: "1\x00",
	3: "3\x00",
	4: "3\x00",
	5: "0\x00",
	6: "3\x00",
	7: "3\x00",
	8: "3\x00",
	9: "1\x00",
}
var Xz3 = [10]ppuintptr{
	0: "4\x00",
	1: "3\x00",
	2: "1\x00",
	3: "7\x00",
	4: "3\x00",
	5: "0\x00",
	6: "3\x00",
	7: "3\x00",
	8: "3\x00",
	9: "1\x00",
}
var Xz4 = [10]ppuintptr{
	0: "c\x00",
	1: "3\x00",
	2: "1\x00",
	3: "f\x00",
	4: "b\x00",
	5: "8\x00",
	6: "3\x00",
	7: "3\x00",
	8: "3\x00",
	9: "1\x00",
}
var Xz5 = [10]ppuintptr{
	0: "1c\x00",
	1: "13\x00",
	2: "11\x00",
	3: "1f\x00",
	4: "b\x00",
	5: "18\x00",
	6: "3\x00",
	7: "13\x00",
	8: "3\x00",
	9: "1\x00",
}

var Xz10 = [10]ppuintptr{
	0: "29c\x00",
	1: "213\x00",
	2: "f1\x00",
	3: "17f\x00",
	4: "12b\x00",
	5: "178\x00",
	6: "383\x00",
	7: "d3\x00",
	8: "3a3\x00",
	9: "281\x00",
}

var Xz15 = [10]ppuintptr{
	0: "29c\x00",
	1: "1a13\x00",
	2: "74f1\x00",
	3: "257f\x00",
	4: "592b\x00",
	5: "4978\x00",
	6: "4783\x00",
	7: "7cd3\x00",
	8: "5ba3\x00",
	9: "4681\x00",
}
var Xz16 = [10]ppuintptr{
	0: "29c\x00",
	1: "9a13\x00",
	2: "74f1\x00",
	3: "a57f\x00",
	4: "d92b\x00",
	5: "4978\x00",
	6: "c783\x00",
	7: "fcd3\x00",
	8: "5ba3\x00",
	9: "c681\x00",
}
var Xz17 = [10]ppuintptr{
	0: "51e\x00",
	1: "f17a\x00",
	2: "54ff\x00",
	3: "1a335\x00",
	4: "cf65\x00",
	5: "5d6f\x00",
	6: "583f\x00",
	7: "618f\x00",
	8: "1bc6\x00",
	9: "98ff\x00",
}

var Xz31 = [10]ppuintptr{
	0: "3aecd515\x00",
	1: "13ae8ec6\x00",
	2: "518c8090\x00",
	3: "81ca077\x00",
	4: "70b7134\x00",
	5: "7ee78d71\x00",
	6: "323a7636\x00",
	7: "2122cb1a\x00",
	8: "19811941\x00",
	9: "41fd605\x00",
}
var Xz32 = [10]ppuintptr{
	0: "baecd515\x00",
	1: "13ae8ec6\x00",
	2: "518c8090\x00",
	3: "881ca077\x00",
	4: "870b7134\x00",
	5: "7ee78d71\x00",
	6: "323a7636\x00",
	7: "a122cb1a\x00",
	8: "99811941\x00",
	9: "841fd605\x00",
}
var Xz33 = [10]ppuintptr{
	0: "1faf4cca\x00",
	1: "15d6ef83b\x00",
	2: "9095fe72\x00",
	3: "1b6a3dff6\x00",
	4: "b17cbddd\x00",
	5: "16e5209d4\x00",
	6: "6f65b12c\x00",
	7: "493bbbc6\x00",
	8: "abf2a5d5\x00",
	9: "6d491a3c\x00",
}

var Xz63 = [10]ppuintptr{
	0: "48a74f367fa7b5c8\x00",
	1: "3ba9e9dc1b263076\x00",
	2: "1e0ac84e7678e0fb\x00",
	3: "11416581728b3e35\x00",
	4: "36ab610523f0f1f7\x00",
	5: "3e540e8e95c0eb4b\x00",
	6: "439ae16057dbc9d3\x00",
	7: "734fb260db243950\x00",
	8: "7d3a317effc289bf\x00",
	9: "1d80301fb3d1a0d1\x00",
}
var Xz64 = [10]ppuintptr{
	0: "48a74f367fa7b5c8\x00",
	1: "bba9e9dc1b263076\x00",
	2: "9e0ac84e7678e0fb\x00",
	3: "11416581728b3e35\x00",
	4: "b6ab610523f0f1f7\x00",
	5: "be540e8e95c0eb4b\x00",
	6: "439ae16057dbc9d3\x00",
	7: "f34fb260db243950\x00",
	8: "fd3a317effc289bf\x00",
	9: "1d80301fb3d1a0d1\x00",
}
var Xz65 = [10]ppuintptr{
	0: "1ff77710d846d49f0\x00",
	1: "1b1411701d709ee10\x00",
	2: "31ffa81a208b6af4\x00",
	3: "446638d431d3c681\x00",
	4: "df5c569d5baa8b55\x00",
	5: "197d99ea9bf28e5a0\x00",
	6: "191ade09edd94cfae\x00",
	7: "194acefa6dde5e18d\x00",
	8: "1afc1167c56272d92\x00",
	9: "d092994da72f206f\x00",
}

var Xz127 = [10]ppuintptr{
	0: "2f66ba932aaf58a071fd8f0742a99a0c\x00",
	1: "73cfa3c664c9c1753507ca60ec6b8425\x00",
	2: "53ea074ca131dec12cd68b8aa8e20278\x00",
	3: "3cf5ac8c343532f8a53cc0eb47581f73\x00",
	4: "50c11d5869e208aa1b9aa317b8c2d0a9\x00",
	5: "b23163c892876472b1ef19642eace09\x00",
	6: "489f4c03d41f87509c8d6c90ce674f95\x00",
	7: "2ab8748c96aa6762ea1932b44c9d7164\x00",
	8: "98cb5591fc05ad31afbbc1d67b90edd\x00",
	9: "77848bb991fd0be331adcf1457fbc672\x00",
}
var Xz128 = [10]ppuintptr{
	0: "af66ba932aaf58a071fd8f0742a99a0c\x00",
	1: "73cfa3c664c9c1753507ca60ec6b8425\x00",
	2: "53ea074ca131dec12cd68b8aa8e20278\x00",
	3: "3cf5ac8c343532f8a53cc0eb47581f73\x00",
	4: "50c11d5869e208aa1b9aa317b8c2d0a9\x00",
	5: "8b23163c892876472b1ef19642eace09\x00",
	6: "489f4c03d41f87509c8d6c90ce674f95\x00",
	7: "aab8748c96aa6762ea1932b44c9d7164\x00",
	8: "98cb5591fc05ad31afbbc1d67b90edd\x00",
	9: "f7848bb991fd0be331adcf1457fbc672\x00",
}

// C documentation
//
//	/* Floats.  */
var Xf1 = [10]ppuintptr{
	0: "0.@0\x00",
	1: "0.8@0\x00",
	2: "0.8@0\x00",
	3: "0.8@0\x00",
	4: "0.8@0\x00",
	5: "0.@0\x00",
	6: "0.8@0\x00",
	7: "0.8@0\x00",
	8: "0.8@0\x00",
	9: "0.8@0\x00",
}
var Xf2 = [10]ppuintptr{
	0: "0.@0\x00",
	1: "0.c@0\x00",
	2: "0.4@0\x00",
	3: "0.c@0\x00",
	4: "0.c@0\x00",
	5: "0.@0\x00",
	6: "0.c@0\x00",
	7: "0.c@0\x00",
	8: "0.c@0\x00",
	9: "0.4@0\x00",
}
var Xf3 = [10]ppuintptr{
	0: "0.8@0\x00",
	1: "0.6@0\x00",
	2: "0.2@0\x00",
	3: "0.e@0\x00",
	4: "0.6@0\x00",
	5: "0.@0\x00",
	6: "0.6@0\x00",
	7: "0.6@0\x00",
	8: "0.6@0\x00",
	9: "0.2@0\x00",
}
var Xf4 = [10]ppuintptr{
	0: "0.c@0\x00",
	1: "0.3@0\x00",
	2: "0.1@0\x00",
	3: "0.f@0\x00",
	4: "0.b@0\x00",
	5: "0.8@0\x00",
	6: "0.3@0\x00",
	7: "0.3@0\x00",
	8: "0.3@0\x00",
	9: "0.1@0\x00",
}
var Xf5 = [10]ppuintptr{
	0: "0.e@0\x00",
	1: "0.98@0\x00",
	2: "0.88@0\x00",
	3: "0.f8@0\x00",
	4: "0.58@0\x00",
	5: "0.c@0\x00",
	6: "0.18@0\x00",
	7: "0.98@0\x00",
	8: "0.18@0\x00",
	9: "0.8@-1\x00",
}

var Xf10 = [10]ppuintptr{
	0: "0.a7@0\x00",
	1: "0.84c@0\x00",
	2: "0.3c4@0\x00",
	3: "0.5fc@0\x00",
	4: "0.4ac@0\x00",
	5: "0.5e@0\x00",
	6: "0.e0c@0\x00",
	7: "0.34c@0\x00",
	8: "0.e8c@0\x00",
	9: "0.a04@0\x00",
}

var Xf15 = [10]ppuintptr{
	0: "0.538@-1\x00",
	1: "0.3426@0\x00",
	2: "0.e9e2@0\x00",
	3: "0.4afe@0\x00",
	4: "0.b256@0\x00",
	5: "0.92f@0\x00",
	6: "0.8f06@0\x00",
	7: "0.f9a6@0\x00",
	8: "0.b746@0\x00",
	9: "0.8d02@0\x00",
}
var Xf16 = [10]ppuintptr{
	0: "0.29c@-1\x00",
	1: "0.9a13@0\x00",
	2: "0.74f1@0\x00",
	3: "0.a57f@0\x00",
	4: "0.d92b@0\x00",
	5: "0.4978@0\x00",
	6: "0.c783@0\x00",
	7: "0.fcd3@0\x00",
	8: "0.5ba3@0\x00",
	9: "0.c681@0\x00",
}
var Xf17 = [10]ppuintptr{
	0: "0.28f@-1\x00",
	1: "0.78bd@0\x00",
	2: "0.2a7f8@0\x00",
	3: "0.d19a8@0\x00",
	4: "0.67b28@0\x00",
	5: "0.2eb78@0\x00",
	6: "0.2c1f8@0\x00",
	7: "0.30c78@0\x00",
	8: "0.de3@-1\x00",
	9: "0.4c7f8@0\x00",
}

var Xf31 = [10]ppuintptr{
	0: "0.75d9aa2a@0\x00",
	1: "0.275d1d8c@0\x00",
	2: "0.a319012@0\x00",
	3: "0.103940ee@0\x00",
	4: "0.e16e268@-1\x00",
	5: "0.fdcf1ae2@0\x00",
	6: "0.6474ec6c@0\x00",
	7: "0.42459634@0\x00",
	8: "0.33023282@0\x00",
	9: "0.83fac0a@-1\x00",
}
var Xf32 = [10]ppuintptr{
	0: "0.baecd515@0\x00",
	1: "0.13ae8ec6@0\x00",
	2: "0.518c809@0\x00",
	3: "0.881ca077@0\x00",
	4: "0.870b7134@0\x00",
	5: "0.7ee78d71@0\x00",
	6: "0.323a7636@0\x00",
	7: "0.a122cb1a@0\x00",
	8: "0.99811941@0\x00",
	9: "0.841fd605@0\x00",
}
var Xf33 = [10]ppuintptr{
	0: "0.fd7a665@-1\x00",
	1: "0.aeb77c1d8@0\x00",
	2: "0.484aff39@0\x00",
	3: "0.db51effb@0\x00",
	4: "0.58be5eee8@0\x00",
	5: "0.b72904ea@0\x00",
	6: "0.37b2d896@0\x00",
	7: "0.249ddde3@0\x00",
	8: "0.55f952ea8@0\x00",
	9: "0.36a48d1e@0\x00",
}

var Xf63 = [10]ppuintptr{
	0: "0.914e9e6cff4f6b9@0\x00",
	1: "0.7753d3b8364c60ec@0\x00",
	2: "0.3c15909cecf1c1f6@0\x00",
	3: "0.2282cb02e5167c6a@0\x00",
	4: "0.6d56c20a47e1e3ee@0\x00",
	5: "0.7ca81d1d2b81d696@0\x00",
	6: "0.8735c2c0afb793a6@0\x00",
	7: "0.e69f64c1b64872a@0\x00",
	8: "0.fa7462fdff85137e@0\x00",
	9: "0.3b00603f67a341a2@0\x00",
}
var Xf64 = [10]ppuintptr{
	0: "0.48a74f367fa7b5c8@0\x00",
	1: "0.bba9e9dc1b263076@0\x00",
	2: "0.9e0ac84e7678e0fb@0\x00",
	3: "0.11416581728b3e35@0\x00",
	4: "0.b6ab610523f0f1f7@0\x00",
	5: "0.be540e8e95c0eb4b@0\x00",
	6: "0.439ae16057dbc9d3@0\x00",
	7: "0.f34fb260db24395@0\x00",
	8: "0.fd3a317effc289bf@0\x00",
	9: "0.1d80301fb3d1a0d1@0\x00",
}
var Xf65 = [10]ppuintptr{
	0: "0.ffbbb886c236a4f8@0\x00",
	1: "0.d8a08b80eb84f708@0\x00",
	2: "0.18ffd40d1045b57a@0\x00",
	3: "0.22331c6a18e9e3408@0\x00",
	4: "0.6fae2b4eadd545aa8@0\x00",
	5: "0.cbeccf54df9472d@0\x00",
	6: "0.c8d6f04f6eca67d7@0\x00",
	7: "0.ca5677d36ef2f0c68@0\x00",
	8: "0.d7e08b3e2b1396c9@0\x00",
	9: "0.68494ca6d39790378@0\x00",
}

var Xf127 = [10]ppuintptr{
	0: "0.5ecd7526555eb140e3fb1e0e85533418@0\x00",
	1: "0.e79f478cc99382ea6a0f94c1d8d7084a@0\x00",
	2: "0.a7d40e994263bd8259ad171551c404f@0\x00",
	3: "0.79eb5918686a65f14a7981d68eb03ee6@0\x00",
	4: "0.a1823ab0d3c411543735462f7185a152@0\x00",
	5: "0.16462c791250ec8e563de32c85d59c12@0\x00",
	6: "0.913e9807a83f0ea1391ad9219cce9f2a@0\x00",
	7: "0.5570e9192d54cec5d4326568993ae2c8@0\x00",
	8: "0.13196ab23f80b5a635f7783acf721dba@0\x00",
	9: "0.ef09177323fa17c6635b9e28aff78ce4@0\x00",
}
var Xf128 = [10]ppuintptr{
	0: "0.af66ba932aaf58a071fd8f0742a99a0c@0\x00",
	1: "0.73cfa3c664c9c1753507ca60ec6b8425@0\x00",
	2: "0.53ea074ca131dec12cd68b8aa8e20278@0\x00",
	3: "0.3cf5ac8c343532f8a53cc0eb47581f73@0\x00",
	4: "0.50c11d5869e208aa1b9aa317b8c2d0a9@0\x00",
	5: "0.8b23163c892876472b1ef19642eace09@0\x00",
	6: "0.489f4c03d41f87509c8d6c90ce674f95@0\x00",
	7: "0.aab8748c96aa6762ea1932b44c9d7164@0\x00",
	8: "0.98cb5591fc05ad31afbbc1d67b90edd@-1\x00",
	9: "0.f7848bb991fd0be331adcf1457fbc672@0\x00",
}

type tsrt = struct {
	fds     ppuintptr
	fdnbits ppint32
}

var sizarr = [18]tsrt{
	0: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz1)),
		fdnbits: ppint32(1),
	},
	1: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz2)),
		fdnbits: ppint32(2),
	},
	2: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz3)),
		fdnbits: ppint32(3),
	},
	3: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz4)),
		fdnbits: ppint32(4),
	},
	4: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz5)),
		fdnbits: ppint32(5),
	},
	5: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz10)),
		fdnbits: ppint32(10),
	},
	6: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz15)),
		fdnbits: ppint32(15),
	},
	7: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz16)),
		fdnbits: ppint32(16),
	},
	8: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz17)),
		fdnbits: ppint32(17),
	},
	9: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz31)),
		fdnbits: ppint32(31),
	},
	10: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz32)),
		fdnbits: ppint32(32),
	},
	11: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz33)),
		fdnbits: ppint32(33),
	},
	12: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz63)),
		fdnbits: ppint32(63),
	},
	13: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz64)),
		fdnbits: ppint32(64),
	},
	14: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz65)),
		fdnbits: ppint32(65),
	},
	15: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz127)),
		fdnbits: ppint32(127),
	},
	16: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xz128)),
		fdnbits: ppint32(128),
	},
	17: {},
}

var sifarr = [18]tsrt{
	0: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf1)),
		fdnbits: ppint32(1),
	},
	1: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf2)),
		fdnbits: ppint32(2),
	},
	2: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf3)),
		fdnbits: ppint32(3),
	},
	3: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf4)),
		fdnbits: ppint32(4),
	},
	4: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf5)),
		fdnbits: ppint32(5),
	},
	5: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf10)),
		fdnbits: ppint32(10),
	},
	6: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf15)),
		fdnbits: ppint32(15),
	},
	7: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf16)),
		fdnbits: ppint32(16),
	},
	8: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf17)),
		fdnbits: ppint32(17),
	},
	9: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf31)),
		fdnbits: ppint32(31),
	},
	10: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf32)),
		fdnbits: ppint32(32),
	},
	11: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf33)),
		fdnbits: ppint32(33),
	},
	12: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf63)),
		fdnbits: ppint32(63),
	},
	13: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf64)),
		fdnbits: ppint32(64),
	},
	14: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf65)),
		fdnbits: ppint32(65),
	},
	15: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf127)),
		fdnbits: ppint32(127),
	},
	16: {
		fds:     ppuintptr(iqunsafe.ppPointer(&Xf128)),
		fdnbits: ppint32(128),
	},
	17: {},
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(144)
	defer cgtls.ppFree(144)

	var aaa ppuintptr
	var aafunc, aai, aanbits, aaverify_mode_flag ppint32
	var pp_ /* f at bp+64 */ tnmpf_t
	var pp_ /* rf at bp+88 */ tnmpf_t
	var pp_ /* rstate at bp+0 */ tngmp_randstate_t
	var pp_ /* rz at bp+48 */ tnmpz_t
	var pp_ /* z at bp+32 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_ = aaa, aafunc, aai, aanbits, aaverify_mode_flag
	var snusage = [84]ppuint8{'u', 's', 'a', 'g', 'e', ':', ' ', 't', '-', 'r', 'a', 'n', 'd', ' ', '[', 'f', 'u', 'n', 'c', 't', 'i', 'o', 'n', ' ', 'n', 'b', 'i', 't', 's', ']', 10, ' ', ' ', 'f', 'u', 'n', 'c', 't', 'i', 'o', 'n', ' ', 'i', 's', ' ', 'o', 'n', 'e', ' ', 'o', 'f', ' ', 'z', ',', ' ', 'f', 10, ' ', ' ', 'n', 'b', 'i', 't', 's', ' ', 'i', 's', ' ', 'n', 'u', 'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ', 'b', 'i', 't', 's', 10}
	aafunc = 0
	aanbits = ppint32(1)
	aaverify_mode_flag = ppint32(1)

	if aaargc > ppint32(1) {

		if aaargc < ppint32(3) {

			Xfputs(cgtls, ppuintptr(iqunsafe.ppPointer(&snusage)), Xstderr)
			Xexit(cgtls, ppint32(1))
		}
		aaverify_mode_flag = 0
		if iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8))))) == ppint32('z') {
			aafunc = 0
		}
		if iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8))))) == ppint32('f') {
			aafunc = 1
		}
		aanbits = Xatoi(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 2*8)))
	}

	X__gmpz_init(cgtls, cgbp+48)

	if aaverify_mode_flag != 0 {

		/* Test z.  */
		X__gmpz_init(cgtls, cgbp+32)
		aaa = ppuintptr(iqunsafe.ppPointer(&sizarr))
		for {
			if !((*tsrt)(iqunsafe.ppPointer(aaa)).fds != iqlibc.ppUintptrFromInt32(0)) {
				break
			}

			X__gmp_randinit(cgtls, cgbp, ppint32(ecGMP_RAND_ALG_LC), iqlibc.ppVaList(cgbp+120, (*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits))
			if X__gmp_errno != ppint32(ecGMP_ERROR_NONE) {
				Xexit(cgtls, ppint32(1))
			}
			X__gmp_randseed_ui(cgtls, cgbp, ppuint64(mvSEED))

			aai = 0
			for {
				if !(aai < ppint32(mvENTS)) {
					break
				}

				X__gmpz_urandomb(cgtls, cgbp+48, cgbp, iqlibc.ppUint64FromInt32((*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits))
				X__gmpz_set_str(cgtls, cgbp+32, *(*ppuintptr)(iqunsafe.ppPointer((*tsrt)(iqunsafe.ppPointer(aaa)).fds + ppuintptr(aai)*8)), ppint32(mvBASE))
				if X__gmpz_cmp(cgtls, cgbp+32, cgbp+48) != 0 {

					Xprintf(cgtls, "z%d: \x00", iqlibc.ppVaList(cgbp+120, (*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits))
					X__gmpz_out_str(cgtls, Xstdout, ppint32(mvBASE), cgbp+48)
					Xprintf(cgtls, " should be \x00", 0)
					X__gmpz_out_str(cgtls, Xstdout, ppint32(mvBASE), cgbp+32)
					Xputs(cgtls, "\x00")
					Xexit(cgtls, ppint32(1))
				}

				goto cg_2
			cg_2:
				;
				aai++
			}
			X__gmp_randclear(cgtls, cgbp)

			goto cg_1
		cg_1:
			;
			aaa += 16
		}
		X__gmpz_clear(cgtls, cgbp+32)

		/* Test f.  */
		aaa = ppuintptr(iqunsafe.ppPointer(&sifarr))
		for {
			if !((*tsrt)(iqunsafe.ppPointer(aaa)).fds != iqlibc.ppUintptrFromInt32(0)) {
				break
			}

			X__gmp_randinit(cgtls, cgbp, ppint32(ecGMP_RAND_ALG_LC), iqlibc.ppVaList(cgbp+120, (*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits))
			if X__gmp_errno != ppint32(ecGMP_ERROR_NONE) {
				Xexit(cgtls, ppint32(1))
			}
			X__gmp_randseed_ui(cgtls, cgbp, ppuint64(mvSEED))

			X__gmpf_init2(cgtls, cgbp+64, iqlibc.ppUint64FromInt32((*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits))
			X__gmpf_init2(cgtls, cgbp+88, iqlibc.ppUint64FromInt32((*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits))
			aai = 0
			for {
				if !(aai < ppint32(mvENTS)) {
					break
				}

				X__gmpf_urandomb(cgtls, cgbp+88, cgbp, iqlibc.ppUint64FromInt32((*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits))
				X__gmpf_set_str(cgtls, cgbp+64, *(*ppuintptr)(iqunsafe.ppPointer((*tsrt)(iqunsafe.ppPointer(aaa)).fds + ppuintptr(aai)*8)), ppint32(mvBASE))
				if X__gmpf_cmp(cgtls, cgbp+64, cgbp+88) != 0 {

					Xprintf(cgtls, "f%d: \x00", iqlibc.ppVaList(cgbp+120, (*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits))
					X__gmpf_out_str(cgtls, Xstdout, ppint32(mvBASE), iqlibc.ppUint64FromInt32((*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits), cgbp+88)
					Xprintf(cgtls, " should be \x00", 0)
					X__gmpf_out_str(cgtls, Xstdout, ppint32(mvBASE), iqlibc.ppUint64FromInt32((*tsrt)(iqunsafe.ppPointer(aaa)).fdnbits), cgbp+64)
					Xputs(cgtls, "\x00")
					Xexit(cgtls, ppint32(1))
				}

				goto cg_4
			cg_4:
				;
				aai++
			}
			X__gmp_randclear(cgtls, cgbp)
			X__gmpf_clear(cgtls, cgbp+64)
			X__gmpf_clear(cgtls, cgbp+88)

			goto cg_3
		cg_3:
			;
			aaa += 16
		}

	} else { /* Print mode.  */

		X__gmp_randinit(cgtls, cgbp, ppint32(ecGMP_RAND_ALG_LC), iqlibc.ppVaList(cgbp+120, aanbits))
		if X__gmp_errno != ppint32(ecGMP_ERROR_NONE) {
			Xexit(cgtls, ppint32(1))
		}
		X__gmp_randseed_ui(cgtls, cgbp, ppuint64(mvSEED))

		switch aafunc {

		case 0:
			Xprintf(cgtls, "char *z%d[ENTS] = {\x00", iqlibc.ppVaList(cgbp+120, aanbits))
			aai = 0
			for {
				if !(aai < ppint32(mvENTS)) {
					break
				}

				X__gmpz_urandomb(cgtls, cgbp+48, cgbp, iqlibc.ppUint64FromInt32(aanbits))
				Xprintf(cgtls, "\"\x00", 0)
				X__gmpz_out_str(cgtls, Xstdout, ppint32(mvBASE), cgbp+48)
				Xprintf(cgtls, "\"\x00", 0)
				if aai != iqlibc.ppInt32FromInt32(mvENTS)-iqlibc.ppInt32FromInt32(1) {
					Xprintf(cgtls, ", \x00", 0)
				}

				goto cg_5
			cg_5:
				;
				aai++
			}
			Xprintf(cgtls, "};\n\x00", 0)
			Xprintf(cgtls, "  {z%d, %d},\n\x00", iqlibc.ppVaList(cgbp+120, aanbits, aanbits))
			break

			fallthrough
		case 1:
			Xprintf(cgtls, "char *f%d[ENTS] = {\x00", iqlibc.ppVaList(cgbp+120, aanbits))
			X__gmpf_init2(cgtls, cgbp+88, iqlibc.ppUint64FromInt32(aanbits))
			aai = 0
			for {
				if !(aai < ppint32(mvENTS)) {
					break
				}

				X__gmpf_urandomb(cgtls, cgbp+88, cgbp, iqlibc.ppUint64FromInt32(aanbits))
				Xprintf(cgtls, "\"\x00", 0)
				X__gmpf_out_str(cgtls, Xstdout, ppint32(mvBASE), iqlibc.ppUint64FromInt32(aanbits), cgbp+88)
				Xprintf(cgtls, "\"\x00", 0)
				if aai != iqlibc.ppInt32FromInt32(mvENTS)-iqlibc.ppInt32FromInt32(1) {
					Xprintf(cgtls, ", \x00", 0)
				}

				goto cg_6
			cg_6:
				;
				aai++
			}
			Xprintf(cgtls, "};\n\x00", 0)
			Xprintf(cgtls, "  {f%d, %d},\n\x00", iqlibc.ppVaList(cgbp+120, aanbits, aanbits))
			X__gmpf_clear(cgtls, cgbp+88)
			break

			fallthrough
		default:
			Xexit(cgtls, ppint32(1))
		}

		X__gmp_randclear(cgtls, cgbp)
	}

	X__gmpz_clear(cgtls, cgbp+48)

	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

var ___gmp_errno ppint32

func ___gmp_randclear(*iqlibc.ppTLS, ppuintptr)

func ___gmp_randinit(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func ___gmp_randseed_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

func ___gmpf_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_cmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmpf_init2(*iqlibc.ppTLS, ppuintptr, ppuint64)

func ___gmpf_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr) ppuint64

func ___gmpf_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func ___gmpf_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_cmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmpz_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr) ppuint64

func ___gmpz_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func ___gmpz_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func _atoi(*iqlibc.ppTLS, ppuintptr) ppint32

func _exit(*iqlibc.ppTLS, ppint32)

func _fputs(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _puts(*iqlibc.ppTLS, ppuintptr) ppint32

var _stderr ppuintptr

var _stdout ppuintptr

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
