// Code generated for linux/riscv64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -D__attribute_pure__= -extended-errors -ignore-unsupported-alignment -DHAVE_CONFIG_H -I. -I../.. -I../.. -I../../tests -DNDEBUG -c -o reuse.o.go reuse.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvASSERT_FILE = "__FILE__"
const mvASSERT_LINE = "__LINE__"
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBINV_NEWTON_THRESHOLD = 300
const mvBMOD_1_TO_MOD_1_THRESHOLD = 10
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDC_BDIV_Q_THRESHOLD = 180
const mvDC_DIVAPPR_Q_THRESHOLD = 200
const mvDELAYTIMER_MAX = 0x7fffffff
const mvDIVEXACT_1_THRESHOLD = 0
const mvDIVEXACT_BY3_METHOD = 0
const mvDIVEXACT_JEB_THRESHOLD = 25
const mvDOPRNT_CONV_FIXED = 1
const mvDOPRNT_CONV_GENERAL = 3
const mvDOPRNT_CONV_SCIENTIFIC = 2
const mvDOPRNT_JUSTIFY_INTERNAL = 3
const mvDOPRNT_JUSTIFY_LEFT = 1
const mvDOPRNT_JUSTIFY_NONE = 0
const mvDOPRNT_JUSTIFY_RIGHT = 2
const mvDOPRNT_SHOWBASE_NO = 2
const mvDOPRNT_SHOWBASE_NONZERO = 3
const mvDOPRNT_SHOWBASE_YES = 1
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFAC_DSC_THRESHOLD = 400
const mvFAC_ODD_THRESHOLD = 35
const mvFFT_FIRST_K = 4
const mvFIB_TABLE_LIMIT = 93
const mvFIB_TABLE_LUCNUM_LIMIT = 92
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFOPEN_MAX = 1000
const mvF_LOCK = 1
const mvF_OK = 0
const mvF_TEST = 3
const mvF_TLOCK = 2
const mvF_ULOCK = 0
const mvGCDEXT_DC_THRESHOLD = 600
const mvGCD_DC_THRESHOLD = 1000
const mvGET_STR_DC_THRESHOLD = 18
const mvGET_STR_PRECOMPUTE_THRESHOLD = 35
const mvGMP_LIMB_BITS = 64
const mvGMP_LIMB_BYTES = "SIZEOF_MP_LIMB_T"
const mvGMP_MPARAM_H_SUGGEST = "./mpn/generic/gmp-mparam.h"
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvHAVE_ALARM = 1
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_CONST = 1
const mvHAVE_ATTRIBUTE_MALLOC = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_ATTRIBUTE_NORETURN = 1
const mvHAVE_CLOCK = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_CONFIG_H = 1
const mvHAVE_DECL_FGETC = 1
const mvHAVE_DECL_FSCANF = 1
const mvHAVE_DECL_OPTARG = 1
const mvHAVE_DECL_SYS_ERRLIST = 0
const mvHAVE_DECL_SYS_NERR = 0
const mvHAVE_DECL_UNGETC = 1
const mvHAVE_DECL_VFPRINTF = 1
const mvHAVE_DLFCN_H = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FCNTL_H = 1
const mvHAVE_FLOAT_H = 1
const mvHAVE_GETPAGESIZE = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_HIDDEN_ALIAS = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTPTR_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LANGINFO_H = 1
const mvHAVE_LIMB_LITTLE_ENDIAN = 1
const mvHAVE_LOCALECONV = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_DOUBLE = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_MEMORY_H = 1
const mvHAVE_MEMSET = 1
const mvHAVE_MMAP = 1
const mvHAVE_MPROTECT = 1
const mvHAVE_NL_LANGINFO = 1
const mvHAVE_NL_TYPES_H = 1
const mvHAVE_POPEN = 1
const mvHAVE_PTRDIFF_T = 1
const mvHAVE_QUAD_T = 1
const mvHAVE_RAISE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGALTSTACK = 1
const mvHAVE_SIGSTACK = 1
const mvHAVE_STACK_T = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDLIB_H = 1
const mvHAVE_STRCHR = 1
const mvHAVE_STRERROR = 1
const mvHAVE_STRINGS_H = 1
const mvHAVE_STRING_H = 1
const mvHAVE_STRNLEN = 1
const mvHAVE_STRTOL = 1
const mvHAVE_STRTOUL = 1
const mvHAVE_SYSCONF = 1
const mvHAVE_SYS_MMAN_H = 1
const mvHAVE_SYS_PARAM_H = 1
const mvHAVE_SYS_RESOURCE_H = 1
const mvHAVE_SYS_STAT_H = 1
const mvHAVE_SYS_SYSINFO_H = 1
const mvHAVE_SYS_TIMES_H = 1
const mvHAVE_SYS_TIME_H = 1
const mvHAVE_SYS_TYPES_H = 1
const mvHAVE_TIMES = 1
const mvHAVE_UINT_LEAST32_T = 1
const mvHAVE_UNISTD_H = 1
const mvHAVE_VSNPRINTF = 1
const mvHGCD_APPR_THRESHOLD = 400
const mvHGCD_REDUCE_THRESHOLD = 1000
const mvHGCD_THRESHOLD = 400
const mvHOST_NAME_MAX = 255
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT64_MAX"
const mvINTPTR_MIN = "INT64_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_HIGHBIT = "INT_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvINV_APPR_THRESHOLD = "INV_NEWTON_THRESHOLD"
const mvINV_NEWTON_THRESHOLD = 200
const mvIOV_MAX = 1024
const mvLIMBS_PER_ULONG = 1
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_HIGHBIT = "LONG_MIN"
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_INCR = 1
const mvL_SET = 0
const mvL_XTND = 2
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMATRIX22_STRASSEN_THRESHOLD = 30
const mvMB_LEN_MAX = 4
const mvMOD_BKNP1_ONLY3 = 0
const mvMPN_FFT_TABLE_SIZE = 16
const mvMPN_TOOM22_MUL_MINSIZE = 6
const mvMPN_TOOM2_SQR_MINSIZE = 4
const mvMPN_TOOM32_MUL_MINSIZE = 10
const mvMPN_TOOM33_MUL_MINSIZE = 17
const mvMPN_TOOM3_SQR_MINSIZE = 17
const mvMPN_TOOM42_MULMID_MINSIZE = 4
const mvMPN_TOOM42_MUL_MINSIZE = 10
const mvMPN_TOOM43_MUL_MINSIZE = 25
const mvMPN_TOOM44_MUL_MINSIZE = 30
const mvMPN_TOOM4_SQR_MINSIZE = 30
const mvMPN_TOOM53_MUL_MINSIZE = 17
const mvMPN_TOOM54_MUL_MINSIZE = 31
const mvMPN_TOOM63_MUL_MINSIZE = 49
const mvMPN_TOOM6H_MUL_MINSIZE = 46
const mvMPN_TOOM6_SQR_MINSIZE = 46
const mvMPN_TOOM8H_MUL_MINSIZE = 86
const mvMPN_TOOM8_SQR_MINSIZE = 86
const mvMP_BASES_BIG_BASE_CTZ_10 = 19
const mvMP_BASES_CHARS_PER_LIMB_10 = 19
const mvMP_BASES_NORMALIZATION_STEPS_10 = 0
const mvMP_EXP_T_MAX = "MP_SIZE_T_MAX"
const mvMP_EXP_T_MIN = "MP_SIZE_T_MIN"
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMULLO_BASECASE_THRESHOLD = 0
const mvMULLO_BASECASE_THRESHOLD_LIMIT = "MULLO_BASECASE_THRESHOLD"
const mvMULMID_TOOM42_THRESHOLD = "MUL_TOOM22_THRESHOLD"
const mvMULMOD_BNM1_THRESHOLD = 16
const mvMUL_TOOM22_THRESHOLD = 30
const mvMUL_TOOM22_THRESHOLD_LIMIT = "MUL_TOOM22_THRESHOLD"
const mvMUL_TOOM32_TO_TOOM43_THRESHOLD = 100
const mvMUL_TOOM32_TO_TOOM53_THRESHOLD = 110
const mvMUL_TOOM33_THRESHOLD = 100
const mvMUL_TOOM33_THRESHOLD_LIMIT = "MUL_TOOM33_THRESHOLD"
const mvMUL_TOOM42_TO_TOOM53_THRESHOLD = 100
const mvMUL_TOOM42_TO_TOOM63_THRESHOLD = 110
const mvMUL_TOOM43_TO_TOOM54_THRESHOLD = 150
const mvMUL_TOOM44_THRESHOLD = 300
const mvMUL_TOOM6H_THRESHOLD = 350
const mvMUL_TOOM8H_THRESHOLD = 450
const mvMUPI_DIV_QR_THRESHOLD = 200
const mvMU_BDIV_QR_THRESHOLD = 2000
const mvMU_BDIV_Q_THRESHOLD = 2000
const mvMU_DIVAPPR_Q_THRESHOLD = 2000
const mvMU_DIV_QR_THRESHOLD = 2000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvODD_CENTRAL_BINOMIAL_OFFSET = 13
const mvODD_CENTRAL_BINOMIAL_TABLE_LIMIT = 35
const mvODD_DOUBLEFACTORIAL_TABLE_LIMIT = 33
const mvODD_FACTORIAL_EXTTABLE_LIMIT = 67
const mvODD_FACTORIAL_TABLE_LIMIT = 25
const mvPACKAGE = "gmp"
const mvPACKAGE_BUGREPORT = "gmp-bugs@gmplib.org (see https://gmplib.org/manual/Reporting-Bugs.html)"
const mvPACKAGE_NAME = "GNU MP"
const mvPACKAGE_STRING = "GNU MP 6.3.0"
const mvPACKAGE_TARNAME = "gmp"
const mvPACKAGE_URL = "http://www.gnu.org/software/gmp/"
const mvPACKAGE_VERSION = "6.3.0"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPOSIX_CLOSE_RESTART = 0
const mvPP_FIRST_OMITTED = 59
const mvPREINV_MOD_1_TO_MOD_1_THRESHOLD = 10
const mvPRIMESIEVE_HIGHEST_PRIME = 5351
const mvPRIMESIEVE_NUMBEROF_TABLE = 28
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT64_MAX"
const mvPTRDIFF_MIN = "INT64_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvREDC_1_TO_REDC_N_THRESHOLD = 100
const mvRETSIGTYPE = "void"
const mvRE_DUP_MAX = 255
const mvR_OK = 4
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEEK_DATA = 3
const mvSEEK_HOLE = 4
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSET_STR_DC_THRESHOLD = 750
const mvSET_STR_PRECOMPUTE_THRESHOLD = 2000
const mvSHRT_HIGHBIT = "SHRT_MIN"
const mvSHRT_MAX = 0x7fff
const mvSIEVESIZE = 512
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZEOF_MP_LIMB_T = 8
const mvSIZEOF_UNSIGNED = 4
const mvSIZEOF_UNSIGNED_LONG = 8
const mvSIZEOF_UNSIGNED_SHORT = 2
const mvSIZEOF_VOID_P = 8
const mvSIZE_MAX = "UINT64_MAX"
const mvSQRLO_BASECASE_THRESHOLD = 0
const mvSQRLO_BASECASE_THRESHOLD_LIMIT = "SQRLO_BASECASE_THRESHOLD"
const mvSQRLO_DC_THRESHOLD = "MULLO_DC_THRESHOLD"
const mvSQRLO_DC_THRESHOLD_LIMIT = "SQRLO_DC_THRESHOLD"
const mvSQRLO_SQR_THRESHOLD = "MULLO_MUL_N_THRESHOLD"
const mvSQRMOD_BNM1_THRESHOLD = 16
const mvSQR_BASECASE_THRESHOLD = 0
const mvSQR_TOOM2_THRESHOLD = 50
const mvSQR_TOOM3_THRESHOLD = 120
const mvSQR_TOOM3_THRESHOLD_LIMIT = "SQR_TOOM3_THRESHOLD"
const mvSQR_TOOM4_THRESHOLD = 400
const mvSQR_TOOM6_THRESHOLD = "MUL_TOOM6H_THRESHOLD"
const mvSQR_TOOM8_THRESHOLD = "MUL_TOOM8H_THRESHOLD"
const mvSSIZE_MAX = "LONG_MAX"
const mvSTDC_HEADERS = 1
const mvSTDERR_FILENO = 2
const mvSTDIN_FILENO = 0
const mvSTDOUT_FILENO = 1
const mvSYMLOOP_MAX = 40
const mvTABLE_LIMIT_2N_MINUS_POPC_2N = 81
const mvTARGET_REGISTER_STARVED = 0
const mvTIME_WITH_SYS_TIME = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTUNE_SQR_TOOM2_MAX = "SQR_TOOM2_MAX_GENERIC"
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT64_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSE_LEADING_REGPARM = 0
const mvUSE_PREINV_DIVREM_1 = 1
const mvUSHRT_MAX = 65535
const mvVERSION = "6.3.0"
const mvWANT_FFT = 1
const mvWANT_TMP_ALLOCA = 1
const mvWANT_TMP_DEBUG = 0
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_OK = 2
const mvW_TYPE_SIZE = "GMP_LIMB_BITS"
const mvX_OK = 1
const mv_CS_GNU_LIBC_VERSION = 2
const mv_CS_GNU_LIBPTHREAD_VERSION = 3
const mv_CS_PATH = 0
const mv_CS_POSIX_V5_WIDTH_RESTRICTED_ENVS = 4
const mv_CS_POSIX_V6_ILP32_OFF32_CFLAGS = 1116
const mv_CS_POSIX_V6_ILP32_OFF32_LDFLAGS = 1117
const mv_CS_POSIX_V6_ILP32_OFF32_LIBS = 1118
const mv_CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = 1119
const mv_CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = 1120
const mv_CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = 1121
const mv_CS_POSIX_V6_ILP32_OFFBIG_LIBS = 1122
const mv_CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = 1123
const mv_CS_POSIX_V6_LP64_OFF64_CFLAGS = 1124
const mv_CS_POSIX_V6_LP64_OFF64_LDFLAGS = 1125
const mv_CS_POSIX_V6_LP64_OFF64_LIBS = 1126
const mv_CS_POSIX_V6_LP64_OFF64_LINTFLAGS = 1127
const mv_CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = 1128
const mv_CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = 1129
const mv_CS_POSIX_V6_LPBIG_OFFBIG_LIBS = 1130
const mv_CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = 1131
const mv_CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = 1
const mv_CS_POSIX_V7_ILP32_OFF32_CFLAGS = 1132
const mv_CS_POSIX_V7_ILP32_OFF32_LDFLAGS = 1133
const mv_CS_POSIX_V7_ILP32_OFF32_LIBS = 1134
const mv_CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = 1135
const mv_CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = 1136
const mv_CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = 1137
const mv_CS_POSIX_V7_ILP32_OFFBIG_LIBS = 1138
const mv_CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = 1139
const mv_CS_POSIX_V7_LP64_OFF64_CFLAGS = 1140
const mv_CS_POSIX_V7_LP64_OFF64_LDFLAGS = 1141
const mv_CS_POSIX_V7_LP64_OFF64_LIBS = 1142
const mv_CS_POSIX_V7_LP64_OFF64_LINTFLAGS = 1143
const mv_CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = 1144
const mv_CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = 1145
const mv_CS_POSIX_V7_LPBIG_OFFBIG_LIBS = 1146
const mv_CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = 1147
const mv_CS_POSIX_V7_THREADS_CFLAGS = 1150
const mv_CS_POSIX_V7_THREADS_LDFLAGS = 1151
const mv_CS_POSIX_V7_WIDTH_RESTRICTED_ENVS = 5
const mv_CS_V6_ENV = 1148
const mv_CS_V7_ENV = 1149
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_IEEE_FLOATS = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_PC_2_SYMLINKS = 20
const mv_PC_ALLOC_SIZE_MIN = 18
const mv_PC_ASYNC_IO = 10
const mv_PC_CHOWN_RESTRICTED = 6
const mv_PC_FILESIZEBITS = 13
const mv_PC_LINK_MAX = 0
const mv_PC_MAX_CANON = 1
const mv_PC_MAX_INPUT = 2
const mv_PC_NAME_MAX = 3
const mv_PC_NO_TRUNC = 7
const mv_PC_PATH_MAX = 4
const mv_PC_PIPE_BUF = 5
const mv_PC_PRIO_IO = 11
const mv_PC_REC_INCR_XFER_SIZE = 14
const mv_PC_REC_MAX_XFER_SIZE = 15
const mv_PC_REC_MIN_XFER_SIZE = 16
const mv_PC_REC_XFER_ALIGN = 17
const mv_PC_SOCK_MAXBUF = 12
const mv_PC_SYMLINK_MAX = 19
const mv_PC_SYNC_IO = 9
const mv_PC_VDISABLE = 8
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_C_BIND = "_POSIX_VERSION"
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX2_VERSION = "_POSIX_VERSION"
const mv_POSIX_ADVISORY_INFO = "_POSIX_VERSION"
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_ASYNCHRONOUS_IO = "_POSIX_VERSION"
const mv_POSIX_BARRIERS = "_POSIX_VERSION"
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CHOWN_RESTRICTED = 1
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_CLOCK_SELECTION = "_POSIX_VERSION"
const mv_POSIX_CPUTIME = "_POSIX_VERSION"
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_FSYNC = "_POSIX_VERSION"
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_IPV6 = "_POSIX_VERSION"
const mv_POSIX_JOB_CONTROL = 1
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAPPED_FILES = "_POSIX_VERSION"
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MEMLOCK = "_POSIX_VERSION"
const mv_POSIX_MEMLOCK_RANGE = "_POSIX_VERSION"
const mv_POSIX_MEMORY_PROTECTION = "_POSIX_VERSION"
const mv_POSIX_MESSAGE_PASSING = "_POSIX_VERSION"
const mv_POSIX_MONOTONIC_CLOCK = "_POSIX_VERSION"
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_NO_TRUNC = 1
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RAW_SOCKETS = "_POSIX_VERSION"
const mv_POSIX_READER_WRITER_LOCKS = "_POSIX_VERSION"
const mv_POSIX_REALTIME_SIGNALS = "_POSIX_VERSION"
const mv_POSIX_REGEXP = 1
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SAVED_IDS = 1
const mv_POSIX_SEMAPHORES = "_POSIX_VERSION"
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SHARED_MEMORY_OBJECTS = "_POSIX_VERSION"
const mv_POSIX_SHELL = 1
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SPAWN = "_POSIX_VERSION"
const mv_POSIX_SPIN_LOCKS = "_POSIX_VERSION"
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREADS = "_POSIX_VERSION"
const mv_POSIX_THREAD_ATTR_STACKADDR = "_POSIX_VERSION"
const mv_POSIX_THREAD_ATTR_STACKSIZE = "_POSIX_VERSION"
const mv_POSIX_THREAD_CPUTIME = "_POSIX_VERSION"
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_PRIORITY_SCHEDULING = "_POSIX_VERSION"
const mv_POSIX_THREAD_PROCESS_SHARED = "_POSIX_VERSION"
const mv_POSIX_THREAD_SAFE_FUNCTIONS = "_POSIX_VERSION"
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMEOUTS = "_POSIX_VERSION"
const mv_POSIX_TIMERS = "_POSIX_VERSION"
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_POSIX_V6_LP64_OFF64 = 1
const mv_POSIX_V7_LP64_OFF64 = 1
const mv_POSIX_VDISABLE = 0
const mv_POSIX_VERSION = 200809
const mv_SC_2_CHAR_TERM = 95
const mv_SC_2_C_BIND = 47
const mv_SC_2_C_DEV = 48
const mv_SC_2_FORT_DEV = 49
const mv_SC_2_FORT_RUN = 50
const mv_SC_2_LOCALEDEF = 52
const mv_SC_2_PBS = 168
const mv_SC_2_PBS_ACCOUNTING = 169
const mv_SC_2_PBS_CHECKPOINT = 175
const mv_SC_2_PBS_LOCATE = 170
const mv_SC_2_PBS_MESSAGE = 171
const mv_SC_2_PBS_TRACK = 172
const mv_SC_2_SW_DEV = 51
const mv_SC_2_UPE = 97
const mv_SC_2_VERSION = 46
const mv_SC_ADVISORY_INFO = 132
const mv_SC_AIO_LISTIO_MAX = 23
const mv_SC_AIO_MAX = 24
const mv_SC_AIO_PRIO_DELTA_MAX = 25
const mv_SC_ARG_MAX = 0
const mv_SC_ASYNCHRONOUS_IO = 12
const mv_SC_ATEXIT_MAX = 87
const mv_SC_AVPHYS_PAGES = 86
const mv_SC_BARRIERS = 133
const mv_SC_BC_BASE_MAX = 36
const mv_SC_BC_DIM_MAX = 37
const mv_SC_BC_SCALE_MAX = 38
const mv_SC_BC_STRING_MAX = 39
const mv_SC_CHILD_MAX = 1
const mv_SC_CLK_TCK = 2
const mv_SC_CLOCK_SELECTION = 137
const mv_SC_COLL_WEIGHTS_MAX = 40
const mv_SC_CPUTIME = 138
const mv_SC_DELAYTIMER_MAX = 26
const mv_SC_EXPR_NEST_MAX = 42
const mv_SC_FSYNC = 15
const mv_SC_GETGR_R_SIZE_MAX = 69
const mv_SC_GETPW_R_SIZE_MAX = 70
const mv_SC_HOST_NAME_MAX = 180
const mv_SC_IOV_MAX = 60
const mv_SC_IPV6 = 235
const mv_SC_JOB_CONTROL = 7
const mv_SC_LINE_MAX = 43
const mv_SC_LOGIN_NAME_MAX = 71
const mv_SC_MAPPED_FILES = 16
const mv_SC_MEMLOCK = 17
const mv_SC_MEMLOCK_RANGE = 18
const mv_SC_MEMORY_PROTECTION = 19
const mv_SC_MESSAGE_PASSING = 20
const mv_SC_MINSIGSTKSZ = 249
const mv_SC_MONOTONIC_CLOCK = 149
const mv_SC_MQ_OPEN_MAX = 27
const mv_SC_MQ_PRIO_MAX = 28
const mv_SC_NGROUPS_MAX = 3
const mv_SC_NPROCESSORS_CONF = 83
const mv_SC_NPROCESSORS_ONLN = 84
const mv_SC_NZERO = 109
const mv_SC_OPEN_MAX = 4
const mv_SC_PAGESIZE = 30
const mv_SC_PAGE_SIZE = 30
const mv_SC_PASS_MAX = 88
const mv_SC_PHYS_PAGES = 85
const mv_SC_PRIORITIZED_IO = 13
const mv_SC_PRIORITY_SCHEDULING = 10
const mv_SC_RAW_SOCKETS = 236
const mv_SC_READER_WRITER_LOCKS = 153
const mv_SC_REALTIME_SIGNALS = 9
const mv_SC_REGEXP = 155
const mv_SC_RE_DUP_MAX = 44
const mv_SC_RTSIG_MAX = 31
const mv_SC_SAVED_IDS = 8
const mv_SC_SEMAPHORES = 21
const mv_SC_SEM_NSEMS_MAX = 32
const mv_SC_SEM_VALUE_MAX = 33
const mv_SC_SHARED_MEMORY_OBJECTS = 22
const mv_SC_SHELL = 157
const mv_SC_SIGQUEUE_MAX = 34
const mv_SC_SIGSTKSZ = 250
const mv_SC_SPAWN = 159
const mv_SC_SPIN_LOCKS = 154
const mv_SC_SPORADIC_SERVER = 160
const mv_SC_SS_REPL_MAX = 241
const mv_SC_STREAMS = 174
const mv_SC_STREAM_MAX = 5
const mv_SC_SYMLOOP_MAX = 173
const mv_SC_SYNCHRONIZED_IO = 14
const mv_SC_THREADS = 67
const mv_SC_THREAD_ATTR_STACKADDR = 77
const mv_SC_THREAD_ATTR_STACKSIZE = 78
const mv_SC_THREAD_CPUTIME = 139
const mv_SC_THREAD_DESTRUCTOR_ITERATIONS = 73
const mv_SC_THREAD_KEYS_MAX = 74
const mv_SC_THREAD_PRIORITY_SCHEDULING = 79
const mv_SC_THREAD_PRIO_INHERIT = 80
const mv_SC_THREAD_PRIO_PROTECT = 81
const mv_SC_THREAD_PROCESS_SHARED = 82
const mv_SC_THREAD_ROBUST_PRIO_INHERIT = 247
const mv_SC_THREAD_ROBUST_PRIO_PROTECT = 248
const mv_SC_THREAD_SAFE_FUNCTIONS = 68
const mv_SC_THREAD_SPORADIC_SERVER = 161
const mv_SC_THREAD_STACK_MIN = 75
const mv_SC_THREAD_THREADS_MAX = 76
const mv_SC_TIMEOUTS = 164
const mv_SC_TIMERS = 11
const mv_SC_TIMER_MAX = 35
const mv_SC_TRACE = 181
const mv_SC_TRACE_EVENT_FILTER = 182
const mv_SC_TRACE_EVENT_NAME_MAX = 242
const mv_SC_TRACE_INHERIT = 183
const mv_SC_TRACE_LOG = 184
const mv_SC_TRACE_NAME_MAX = 243
const mv_SC_TRACE_SYS_MAX = 244
const mv_SC_TRACE_USER_EVENT_MAX = 245
const mv_SC_TTY_NAME_MAX = 72
const mv_SC_TYPED_MEMORY_OBJECTS = 165
const mv_SC_TZNAME_MAX = 6
const mv_SC_UIO_MAXIOV = 60
const mv_SC_V6_ILP32_OFF32 = 176
const mv_SC_V6_ILP32_OFFBIG = 177
const mv_SC_V6_LP64_OFF64 = 178
const mv_SC_V6_LPBIG_OFFBIG = 179
const mv_SC_V7_ILP32_OFF32 = 237
const mv_SC_V7_ILP32_OFFBIG = 238
const mv_SC_V7_LP64_OFF64 = 239
const mv_SC_V7_LPBIG_OFFBIG = 240
const mv_SC_VERSION = 29
const mv_SC_XBS5_ILP32_OFF32 = 125
const mv_SC_XBS5_ILP32_OFFBIG = 126
const mv_SC_XBS5_LP64_OFF64 = 127
const mv_SC_XBS5_LPBIG_OFFBIG = 128
const mv_SC_XOPEN_CRYPT = 92
const mv_SC_XOPEN_ENH_I18N = 93
const mv_SC_XOPEN_LEGACY = 129
const mv_SC_XOPEN_REALTIME = 130
const mv_SC_XOPEN_REALTIME_THREADS = 131
const mv_SC_XOPEN_SHM = 94
const mv_SC_XOPEN_STREAMS = 246
const mv_SC_XOPEN_UNIX = 91
const mv_SC_XOPEN_VERSION = 89
const mv_SC_XOPEN_XCU_VERSION = 90
const mv_SC_XOPEN_XPG2 = 98
const mv_SC_XOPEN_XPG3 = 99
const mv_SC_XOPEN_XPG4 = 100
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_ENH_I18N = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv_XOPEN_UNIX = 1
const mv_XOPEN_VERSION = 700
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 1
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 36
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 1
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 1
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 1
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 1
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 1
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 1
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 1
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "gcc"
const mv__GMP_CFLAGS = "-DNDEBUG"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 14
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 3
const mv__GNU_MP_VERSION_PATCHLEVEL = 0
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1019
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 36
const mv__LDBL_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__LDBL_DIG__ = 33
const mv__LDBL_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 1
const mv__LDBL_MANT_DIG__ = 113
const mv__LDBL_MAX_10_EXP__ = 4932
const mv__LDBL_MAX_EXP__ = 16384
const mv__LDBL_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LDBL_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__LDBL_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 0x7fffffffffffffff
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "l"
const mv__PRIPTR = "l"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "14.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__riscv = 1
const mv__riscv_a = 2001000
const mv__riscv_arch_test = 1
const mv__riscv_atomic = 1
const mv__riscv_c = 2000000
const mv__riscv_cmodel_medany = 1
const mv__riscv_compressed = 1
const mv__riscv_d = 2002000
const mv__riscv_div = 1
const mv__riscv_f = 2002000
const mv__riscv_fdiv = 1
const mv__riscv_flen = 64
const mv__riscv_float_abi_double = 1
const mv__riscv_fsqrt = 1
const mv__riscv_i = 2001000
const mv__riscv_m = 2000000
const mv__riscv_misaligned_slow = 1
const mv__riscv_mul = 1
const mv__riscv_muldiv = 1
const mv__riscv_xlen = 64
const mv__riscv_zicsr = 2000000
const mv__riscv_zifencei = 2000000
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_newalloc = "_mpz_realloc"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvbinvert_limb_table = "__gmp_binvert_limb_table"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_init_primesieve = "__gmp_init_primesieve"
const mvgmp_nextprime = "__gmp_nextprime"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_primesieve = "__gmp_primesieve"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvjacobi_table = "__gmp_jacobi_table"
const mvlinux = 1
const mvmodlimb_invert = "binvert_limb"
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpn_fft_mul = "mpn_nussbaumer_mul"
const mvmpn_get_d = "__gmpn_get_d"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clear = "__gmpz_clear"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_gcd = "__gmpz_divexact_gcd"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init = "__gmpz_init"
const mvmpz_init2 = "__gmpz_init2"
const mvmpz_init_set = "__gmpz_init_set"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_init_set_ui = "__gmpz_init_set_ui"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_inp_str_nowhite = "__gmpz_inp_str_nowhite"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucas_mod = "__gmpz_lucas_mod"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_n_pow_ui = "__gmpz_n_pow_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_oddfac_1 = "__gmpz_oddfac_1"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_prevprime = "__gmpz_prevprime"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_prodlimbs = "__gmpz_prodlimbs"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_stronglucas = "__gmpz_stronglucas"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvrestrict = "__restrict"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlocale_t = ppuintptr

type tnintptr_t = ppint64

type tnpid_t = ppint32

type tnuid_t = ppuint32

type tngid_t = ppuint32

type tnuseconds_t = ppuint32

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

type tngmp_randstate_ptr = ppuintptr

type tngmp_randstate_srcptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8
const ecGMP_ERROR_MPZ_OVERFLOW = 16

type tnuintptr_t = ppuint64

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tngmp_uint_least32_t = ppuint32

type tngmp_intptr_t = ppint64

type tngmp_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tngmp_pi2_t = struct {
	fdinv21 tnmp_limb_t
	fdinv32 tnmp_limb_t
	fdinv53 tnmp_limb_t
}

type tutmp_align_t = struct {
	fdd [0]ppfloat64
	fdp [0]ppuintptr
	fdl tnmp_limb_t
}

type tstmp_reentrant_t = struct {
	fdnext ppuintptr
	fdsize tnsize_t
}

type tngmp_randfnptr_t = struct {
	fdrandseed_fn  ppuintptr
	fdrandget_fn   ppuintptr
	fdrandclear_fn ppuintptr
	fdrandiset_fn  ppuintptr
}

type tetoom6_flags = ppint32

const ectoom6_all_pos = 0
const ectoom6_vm1_neg = 1
const ectoom6_vm2_neg = 2

type tetoom7_flags = ppint32

const ectoom7_w1_neg = 1
const ectoom7_w3_neg = 2

type tngmp_primesieve_t = struct {
	fdd       ppuint64
	fds0      ppuint64
	fdsqrt_s0 ppuint64
	fds       [513]ppuint8
}

type tsfft_table_nk = struct {
	fd__ccgo0 uint32
}

type tsbases = struct {
	fdchars_per_limb    ppint32
	fdlogb2             tnmp_limb_t
	fdlog2b             tnmp_limb_t
	fdbig_base          tnmp_limb_t
	fdbig_base_inverted tnmp_limb_t
}

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tuieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type tnmp_double_limb_t = struct {
	fdd0 tnmp_limb_t
	fdd1 tnmp_limb_t
}

type tshgcd_matrix1 = struct {
	fdu [2][2]tnmp_limb_t
}

type tshgcd_matrix = struct {
	fdalloc tnmp_size_t
	fdn     tnmp_size_t
	fdp     [2][2]tnmp_ptr
}

type tsgcdext_ctx = struct {
	fdgp    tnmp_ptr
	fdgn    tnmp_size_t
	fdup    tnmp_ptr
	fdusize ppuintptr
	fdun    tnmp_size_t
	fdu0    tnmp_ptr
	fdu1    tnmp_ptr
	fdtp    tnmp_ptr
}

type tspowers = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tnpowers_t = struct {
	fdp              tnmp_ptr
	fdn              tnmp_size_t
	fdshift          tnmp_size_t
	fddigits_in_base tnsize_t
	fdbase           ppint32
}

type tsdoprnt_params_t = struct {
	fdbase         ppint32
	fdconv         ppint32
	fdexpfmt       ppuintptr
	fdexptimes4    ppint32
	fdfill         ppuint8
	fdjustify      ppint32
	fdprec         ppint32
	fdshowbase     ppint32
	fdshowpoint    ppint32
	fdshowtrailing ppint32
	fdsign         ppuint8
	fdwidth        ppint32
}

type tngmp_doscan_scan_t = ppuintptr

type tngmp_doscan_step_t = ppuintptr

type tngmp_doscan_get_t = ppuintptr

type tngmp_doscan_unget_t = ppuintptr

type tsgmp_doscan_funs_t = struct {
	fdscan  tngmp_doscan_scan_t
	fdstep  tngmp_doscan_step_t
	fdget   tngmp_doscan_get_t
	fdunget tngmp_doscan_unget_t
}

type tn__jmp_buf = [26]ppuint64

type tnjmp_buf = [1]ts__jmp_buf_tag

type ts__jmp_buf_tag = struct {
	fd__jb tn__jmp_buf
	fd__fl ppuint64
	fd__ss [16]ppuint64
}

type tnsigjmp_buf = [1]ts__jmp_buf_tag

type tndss_func = ppuintptr

type tndsi_func = ppuintptr

type tndsi_div_func = ppuintptr

type tnddsi_div_func = ppuintptr

type tnddss_div_func = ppuintptr

type tnds_func = ppuintptr

func Xmpz_xinvert(cgtls *iqlibc.ppTLS, aar tnmpz_ptr, aaa tnmpz_srcptr, aab tnmpz_srcptr) {

	var aares ppint32
	pp_ = aares
	aares = X__gmpz_invert(cgtls, aar, aaa, aab)
	if aares == 0 {
		X__gmpz_set_ui(cgtls, aar, ppuint64(0))
	}
}

var sidss = [16]struct {
	fdfptr       tndss_func
	fdfname      ppuintptr
	fdisdivision ppint32
	fdisslow     ppint32
}{
	0: {
		fdfname: "mpz_add\x00",
	},
	1: {
		fdfname: "mpz_sub\x00",
	},
	2: {
		fdfname: "mpz_mul\x00",
	},
	3: {
		fdfname:      "mpz_cdiv_q\x00",
		fdisdivision: ppint32(1),
	},
	4: {
		fdfname:      "mpz_cdiv_r\x00",
		fdisdivision: ppint32(1),
	},
	5: {
		fdfname:      "mpz_fdiv_q\x00",
		fdisdivision: ppint32(1),
	},
	6: {
		fdfname:      "mpz_fdiv_r\x00",
		fdisdivision: ppint32(1),
	},
	7: {
		fdfname:      "mpz_tdiv_q\x00",
		fdisdivision: ppint32(1),
	},
	8: {
		fdfname:      "mpz_tdiv_r\x00",
		fdisdivision: ppint32(1),
	},
	9: {
		fdfname:      "mpz_mod\x00",
		fdisdivision: ppint32(1),
	},
	10: {
		fdfname:      "mpz_xinvert\x00",
		fdisdivision: ppint32(1),
		fdisslow:     ppint32(1),
	},
	11: {
		fdfname:  "mpz_gcd\x00",
		fdisslow: ppint32(1),
	},
	12: {
		fdfname:  "mpz_lcm\x00",
		fdisslow: ppint32(1),
	},
	13: {
		fdfname: "mpz_and\x00",
	},
	14: {
		fdfname: "mpz_ior\x00",
	},
	15: {
		fdfname: "mpz_xor\x00",
	},
}

func init() {
	p := iqunsafe.ppPointer(&sidss)
	*(*uintptr)(iqunsafe.ppAdd(p, 0)) = pp__ccgo_fp(X__gmpz_add)
	*(*uintptr)(iqunsafe.ppAdd(p, 24)) = pp__ccgo_fp(X__gmpz_sub)
	*(*uintptr)(iqunsafe.ppAdd(p, 48)) = pp__ccgo_fp(X__gmpz_mul)
	*(*uintptr)(iqunsafe.ppAdd(p, 72)) = pp__ccgo_fp(X__gmpz_cdiv_q)
	*(*uintptr)(iqunsafe.ppAdd(p, 96)) = pp__ccgo_fp(X__gmpz_cdiv_r)
	*(*uintptr)(iqunsafe.ppAdd(p, 120)) = pp__ccgo_fp(X__gmpz_fdiv_q)
	*(*uintptr)(iqunsafe.ppAdd(p, 144)) = pp__ccgo_fp(X__gmpz_fdiv_r)
	*(*uintptr)(iqunsafe.ppAdd(p, 168)) = pp__ccgo_fp(X__gmpz_tdiv_q)
	*(*uintptr)(iqunsafe.ppAdd(p, 192)) = pp__ccgo_fp(X__gmpz_tdiv_r)
	*(*uintptr)(iqunsafe.ppAdd(p, 216)) = pp__ccgo_fp(X__gmpz_mod)
	*(*uintptr)(iqunsafe.ppAdd(p, 240)) = pp__ccgo_fp(Xmpz_xinvert)
	*(*uintptr)(iqunsafe.ppAdd(p, 264)) = pp__ccgo_fp(X__gmpz_gcd)
	*(*uintptr)(iqunsafe.ppAdd(p, 288)) = pp__ccgo_fp(X__gmpz_lcm)
	*(*uintptr)(iqunsafe.ppAdd(p, 312)) = pp__ccgo_fp(X__gmpz_and)
	*(*uintptr)(iqunsafe.ppAdd(p, 336)) = pp__ccgo_fp(X__gmpz_ior)
	*(*uintptr)(iqunsafe.ppAdd(p, 360)) = pp__ccgo_fp(X__gmpz_xor)
}

var sidsi = [11]struct {
	fdfptr  tndsi_func
	fdfname ppuintptr
	fdmod   ppint32
}{
	0: {
		fdfname: "mpz_add_ui\x00",
	},
	1: {
		fdfname: "mpz_mul_ui\x00",
	},
	2: {
		fdfname: "mpz_sub_ui\x00",
	},
	3: {
		fdfname: "mpz_fdiv_q_2exp\x00",
		fdmod:   ppint32(0x1000),
	},
	4: {
		fdfname: "mpz_fdiv_r_2exp\x00",
		fdmod:   ppint32(0x1000),
	},
	5: {
		fdfname: "mpz_cdiv_q_2exp\x00",
		fdmod:   ppint32(0x1000),
	},
	6: {
		fdfname: "mpz_cdiv_r_2exp\x00",
		fdmod:   ppint32(0x1000),
	},
	7: {
		fdfname: "mpz_tdiv_q_2exp\x00",
		fdmod:   ppint32(0x1000),
	},
	8: {
		fdfname: "mpz_tdiv_r_2exp\x00",
		fdmod:   ppint32(0x1000),
	},
	9: {
		fdfname: "mpz_mul_2exp\x00",
		fdmod:   ppint32(0x100),
	},
	10: {
		fdfname: "mpz_pow_ui\x00",
		fdmod:   ppint32(0x10),
	},
}

func init() {
	p := iqunsafe.ppPointer(&sidsi)
	*(*uintptr)(iqunsafe.ppAdd(p, 0)) = pp__ccgo_fp(X__gmpz_add_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 24)) = pp__ccgo_fp(X__gmpz_mul_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 48)) = pp__ccgo_fp(X__gmpz_sub_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 72)) = pp__ccgo_fp(X__gmpz_fdiv_q_2exp)
	*(*uintptr)(iqunsafe.ppAdd(p, 96)) = pp__ccgo_fp(X__gmpz_fdiv_r_2exp)
	*(*uintptr)(iqunsafe.ppAdd(p, 120)) = pp__ccgo_fp(X__gmpz_cdiv_q_2exp)
	*(*uintptr)(iqunsafe.ppAdd(p, 144)) = pp__ccgo_fp(X__gmpz_cdiv_r_2exp)
	*(*uintptr)(iqunsafe.ppAdd(p, 168)) = pp__ccgo_fp(X__gmpz_tdiv_q_2exp)
	*(*uintptr)(iqunsafe.ppAdd(p, 192)) = pp__ccgo_fp(X__gmpz_tdiv_r_2exp)
	*(*uintptr)(iqunsafe.ppAdd(p, 216)) = pp__ccgo_fp(X__gmpz_mul_2exp)
	*(*uintptr)(iqunsafe.ppAdd(p, 240)) = pp__ccgo_fp(X__gmpz_pow_ui)
}

var sidsi_div = [6]struct {
	fdfptr  tndsi_div_func
	fdfname ppuintptr
}{
	0: {
		fdfname: "mpz_cdiv_q_ui\x00",
	},
	1: {
		fdfname: "mpz_cdiv_r_ui\x00",
	},
	2: {
		fdfname: "mpz_fdiv_q_ui\x00",
	},
	3: {
		fdfname: "mpz_fdiv_r_ui\x00",
	},
	4: {
		fdfname: "mpz_tdiv_q_ui\x00",
	},
	5: {
		fdfname: "mpz_tdiv_r_ui\x00",
	},
}

func init() {
	p := iqunsafe.ppPointer(&sidsi_div)
	*(*uintptr)(iqunsafe.ppAdd(p, 0)) = pp__ccgo_fp(X__gmpz_cdiv_q_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 16)) = pp__ccgo_fp(X__gmpz_cdiv_r_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 32)) = pp__ccgo_fp(X__gmpz_fdiv_q_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 48)) = pp__ccgo_fp(X__gmpz_fdiv_r_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 64)) = pp__ccgo_fp(X__gmpz_tdiv_q_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 80)) = pp__ccgo_fp(X__gmpz_tdiv_r_ui)
}

var siddsi_div = [3]struct {
	fdfptr   tnddsi_div_func
	fdfname  ppuintptr
	fdisslow ppint32
}{
	0: {
		fdfname: "mpz_cdiv_qr_ui\x00",
	},
	1: {
		fdfname: "mpz_fdiv_qr_ui\x00",
	},
	2: {
		fdfname: "mpz_tdiv_qr_ui\x00",
	},
}

func init() {
	p := iqunsafe.ppPointer(&siddsi_div)
	*(*uintptr)(iqunsafe.ppAdd(p, 0)) = pp__ccgo_fp(X__gmpz_cdiv_qr_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 24)) = pp__ccgo_fp(X__gmpz_fdiv_qr_ui)
	*(*uintptr)(iqunsafe.ppAdd(p, 48)) = pp__ccgo_fp(X__gmpz_tdiv_qr_ui)
}

var siddss_div = [3]struct {
	fdfptr   tnddss_div_func
	fdfname  ppuintptr
	fdisslow ppint32
}{
	0: {
		fdfname: "mpz_cdiv_qr\x00",
	},
	1: {
		fdfname: "mpz_fdiv_qr\x00",
	},
	2: {
		fdfname: "mpz_tdiv_qr\x00",
	},
}

func init() {
	p := iqunsafe.ppPointer(&siddss_div)
	*(*uintptr)(iqunsafe.ppAdd(p, 0)) = pp__ccgo_fp(X__gmpz_cdiv_qr)
	*(*uintptr)(iqunsafe.ppAdd(p, 24)) = pp__ccgo_fp(X__gmpz_fdiv_qr)
	*(*uintptr)(iqunsafe.ppAdd(p, 48)) = pp__ccgo_fp(X__gmpz_tdiv_qr)
}

var sids = [4]struct {
	fdfptr   tnds_func
	fdfname  ppuintptr
	fdnonneg ppint32
}{
	0: {
		fdfname: "mpz_abs\x00",
	},
	1: {
		fdfname: "mpz_com\x00",
	},
	2: {
		fdfname: "mpz_neg\x00",
	},
	3: {
		fdfname:  "mpz_sqrt\x00",
		fdnonneg: ppint32(1),
	},
}

func init() {
	p := iqunsafe.ppPointer(&sids)
	*(*uintptr)(iqunsafe.ppAdd(p, 0)) = pp__ccgo_fp(X__gmpz_abs)
	*(*uintptr)(iqunsafe.ppAdd(p, 24)) = pp__ccgo_fp(X__gmpz_com)
	*(*uintptr)(iqunsafe.ppAdd(p, 48)) = pp__ccgo_fp(X__gmpz_neg)
	*(*uintptr)(iqunsafe.ppAdd(p, 72)) = pp__ccgo_fp(X__gmpz_sqrt)
}

func Xrealloc_if_reducing(cgtls *iqlibc.ppTLS, aar tnmpz_ptr) {

	var ccv1, ccv2 ppint32
	pp_, pp_ = ccv1, ccv2
	if (*tn__mpz_struct)(iqunsafe.ppPointer(aar)).fd_mp_size >= 0 {
		ccv1 = (*tn__mpz_struct)(iqunsafe.ppPointer(aar)).fd_mp_size
	} else {
		ccv1 = -(*tn__mpz_struct)(iqunsafe.ppPointer(aar)).fd_mp_size
	}
	if ccv1 < (*tn__mpz_struct)(iqunsafe.ppPointer(aar)).fd_mp_alloc {
		if (*tn__mpz_struct)(iqunsafe.ppPointer(aar)).fd_mp_size >= 0 {
			ccv2 = (*tn__mpz_struct)(iqunsafe.ppPointer(aar)).fd_mp_size
		} else {
			ccv2 = -(*tn__mpz_struct)(iqunsafe.ppPointer(aar)).fd_mp_size
		}
		X__gmpz_realloc(cgtls, aar, ppint64(ccv2))
	}
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(208)
	defer cgtls.ppFree(208)

	var aa__gmp_l, aa__nail, aa__nail1, aa__nail10, aa__nail100, aa__nail101, aa__nail102, aa__nail103, aa__nail104, aa__nail105, aa__nail106, aa__nail107, aa__nail108, aa__nail109, aa__nail11, aa__nail110, aa__nail111, aa__nail112, aa__nail113, aa__nail114, aa__nail12, aa__nail13, aa__nail14, aa__nail15, aa__nail16, aa__nail17, aa__nail18, aa__nail19, aa__nail2, aa__nail20, aa__nail21, aa__nail22, aa__nail23, aa__nail24, aa__nail25, aa__nail26, aa__nail27, aa__nail28, aa__nail29, aa__nail3, aa__nail30, aa__nail31, aa__nail32, aa__nail33, aa__nail34, aa__nail35, aa__nail36, aa__nail37, aa__nail38, aa__nail39, aa__nail4, aa__nail40, aa__nail41, aa__nail42, aa__nail43, aa__nail44, aa__nail45, aa__nail46, aa__nail47, aa__nail48, aa__nail49, aa__nail5, aa__nail50, aa__nail51, aa__nail52, aa__nail53, aa__nail54, aa__nail55, aa__nail56, aa__nail57, aa__nail58, aa__nail59, aa__nail6, aa__nail60, aa__nail61, aa__nail62, aa__nail63, aa__nail64, aa__nail65, aa__nail66, aa__nail67, aa__nail68, aa__nail69, aa__nail7, aa__nail70, aa__nail71, aa__nail72, aa__nail73, aa__nail74, aa__nail75, aa__nail76, aa__nail77, aa__nail78, aa__nail79, aa__nail8, aa__nail80, aa__nail81, aa__nail82, aa__nail83, aa__nail84, aa__nail85, aa__nail86, aa__nail87, aa__nail88, aa__nail89, aa__nail9, aa__nail90, aa__nail91, aa__nail92, aa__nail93, aa__nail94, aa__nail95, aa__nail96, aa__nail97, aa__nail98, aa__nail99 tnmp_limb_t
	var aa__gmp_n, aa__i, aa__i1, aa__i10, aa__i100, aa__i101, aa__i102, aa__i103, aa__i104, aa__i105, aa__i106, aa__i107, aa__i108, aa__i109, aa__i11, aa__i110, aa__i111, aa__i112, aa__i113, aa__i114, aa__i12, aa__i13, aa__i14, aa__i15, aa__i16, aa__i17, aa__i18, aa__i19, aa__i2, aa__i20, aa__i21, aa__i22, aa__i23, aa__i24, aa__i25, aa__i26, aa__i27, aa__i28, aa__i29, aa__i3, aa__i30, aa__i31, aa__i32, aa__i33, aa__i34, aa__i35, aa__i36, aa__i37, aa__i38, aa__i39, aa__i4, aa__i40, aa__i41, aa__i42, aa__i43, aa__i44, aa__i45, aa__i46, aa__i47, aa__i48, aa__i49, aa__i5, aa__i50, aa__i51, aa__i52, aa__i53, aa__i54, aa__i55, aa__i56, aa__i57, aa__i58, aa__i59, aa__i6, aa__i60, aa__i61, aa__i62, aa__i63, aa__i64, aa__i65, aa__i66, aa__i67, aa__i68, aa__i69, aa__i7, aa__i70, aa__i71, aa__i72, aa__i73, aa__i74, aa__i75, aa__i76, aa__i77, aa__i78, aa__i79, aa__i8, aa__i80, aa__i81, aa__i82, aa__i83, aa__i84, aa__i85, aa__i86, aa__i87, aa__i88, aa__i89, aa__i9, aa__i90, aa__i91, aa__i92, aa__i93, aa__i94, aa__i95, aa__i96, aa__i97, aa__i98, aa__i99 tnmp_size_t
	var aa__gmp_p tnmp_ptr
	var aabsi, aain2i, aar1, aar2, aasize_range, ccv11, ccv126, ccv128, ccv13, ccv131, ccv133, ccv15, ccv17, ccv19, ccv21, ccv23, ccv25, ccv27, ccv29, ccv31, ccv33, ccv35, ccv5, ccv7, ccv9 ppuint64
	var aaenvval ppuintptr
	var aai, aareps_nondefault, ccv101, ccv102, ccv104, ccv106, ccv107, ccv109, ccv111, ccv113, ccv115, ccv117, ccv119, ccv120, ccv122, ccv124, ccv134, ccv136, ccv138, ccv139, ccv141, ccv143, ccv145, ccv147, ccv149, ccv150, ccv152, ccv154, ccv156, ccv158, ccv160, ccv161, ccv163, ccv165, ccv166, ccv168, ccv170, ccv171, ccv172, ccv174, ccv176, ccv177, ccv179, ccv181, ccv182, ccv184, ccv186, ccv187, ccv189, ccv191, ccv192, ccv194, ccv196, ccv197, ccv199, ccv201, ccv202, ccv204, ccv206, ccv207, ccv208, ccv210, ccv212, ccv213, ccv215, ccv217, ccv218, ccv219, ccv221, ccv223, ccv224, ccv226, ccv228, ccv229, ccv231, ccv233, ccv234, ccv236, ccv238, ccv239, ccv241, ccv243, ccv244, ccv246, ccv248, ccv249, ccv251, ccv253, ccv254, ccv256, ccv258, ccv259, ccv261, ccv263, ccv264, ccv266, ccv268, ccv269, ccv271, ccv273, ccv274, ccv276, ccv278, ccv279, ccv281, ccv283, ccv284, ccv286, ccv288, ccv289, ccv291, ccv293, ccv294, ccv296, ccv298, ccv299, ccv301, ccv303, ccv304, ccv306, ccv308, ccv309, ccv311, ccv313, ccv314, ccv316, ccv318, ccv319, ccv321, ccv323, ccv324, ccv326, ccv328, ccv329, ccv331, ccv333, ccv334, ccv336, ccv338, ccv339, ccv341, ccv343, ccv344, ccv346, ccv348, ccv349, ccv351, ccv353, ccv354, ccv356, ccv358, ccv359, ccv361, ccv363, ccv364, ccv366, ccv368, ccv369, ccv371, ccv373, ccv374, ccv376, ccv378, ccv379, ccv381, ccv383, ccv384, ccv386, ccv388, ccv389, ccv391, ccv393, ccv394, ccv396, ccv398, ccv399, ccv401, ccv403, ccv404, ccv406, ccv408, ccv409, ccv411, ccv413, ccv414, ccv416, ccv418, ccv419, ccv421, ccv423, ccv424, ccv426, ccv428, ccv429, ccv43, ccv431, ccv433, ccv434, ccv436, ccv438, ccv439, ccv441, ccv443, ccv444, ccv446, ccv448, ccv449, ccv45, ccv451, ccv453, ccv454, ccv456, ccv458, ccv459, ccv461, ccv463, ccv464, ccv466, ccv468, ccv469, ccv47, ccv471, ccv473, ccv474, ccv476, ccv478, ccv479, ccv481, ccv483, ccv486, ccv487, ccv488, ccv49, ccv490, ccv492, ccv493, ccv495, ccv497, ccv498, ccv50, ccv500, ccv502, ccv503, ccv505, ccv507, ccv508, ccv510, ccv512, ccv513, ccv515, ccv517, ccv518, ccv52, ccv520, ccv522, ccv523, ccv525, ccv527, ccv528, ccv530, ccv532, ccv533, ccv535, ccv537, ccv538, ccv54, ccv540, ccv542, ccv543, ccv545, ccv547, ccv548, ccv55, ccv550, ccv552, ccv553, ccv555, ccv557, ccv559, ccv561, ccv562, ccv564, ccv566, ccv567, ccv569, ccv57, ccv571, ccv572, ccv574, ccv576, ccv577, ccv579, ccv581, ccv583, ccv584, ccv586, ccv588, ccv589, ccv59, ccv591, ccv593, ccv594, ccv596, ccv598, ccv599, ccv601, ccv603, ccv604, ccv605, ccv607, ccv609, ccv61, ccv610, ccv612, ccv614, ccv615, ccv617, ccv619, ccv62, ccv620, ccv621, ccv623, ccv625, ccv626, ccv628, ccv630, ccv631, ccv633, ccv635, ccv636, ccv637, ccv639, ccv64, ccv641, ccv642, ccv644, ccv646, ccv647, ccv649, ccv651, ccv66, ccv67, ccv69, ccv71, ccv72, ccv74, ccv76, ccv77, ccv79, ccv81, ccv82, ccv84, ccv86, ccv87, ccv89, ccv91, ccv92, ccv94, ccv96, ccv97, ccv99 ppint32
	var aapass, aareps, ccv1 ppuint32
	var aarands tngmp_randstate_ptr
	var aarefretval, aaretval tnmp_bitcnt_t
	var aarepfactor ppfloat64
	var ccv103, ccv108, ccv114, ccv116, ccv121, ccv135, ccv140, ccv146, ccv151, ccv157, ccv162, ccv167, ccv173, ccv178, ccv183, ccv188, ccv193, ccv198, ccv203, ccv209, ccv214, ccv220, ccv225, ccv230, ccv235, ccv240, ccv245, ccv250, ccv255, ccv260, ccv265, ccv270, ccv275, ccv280, ccv285, ccv290, ccv295, ccv300, ccv305, ccv310, ccv315, ccv320, ccv325, ccv330, ccv335, ccv340, ccv345, ccv350, ccv355, ccv360, ccv365, ccv370, ccv375, ccv380, ccv385, ccv390, ccv395, ccv400, ccv405, ccv410, ccv415, ccv420, ccv425, ccv430, ccv435, ccv44, ccv440, ccv445, ccv450, ccv455, ccv46, ccv460, ccv465, ccv470, ccv475, ccv480, ccv489, ccv494, ccv499, ccv504, ccv509, ccv51, ccv514, ccv519, ccv524, ccv529, ccv534, ccv539, ccv544, ccv549, ccv554, ccv556, ccv558, ccv56, ccv563, ccv568, ccv573, ccv578, ccv580, ccv585, ccv590, ccv595, ccv600, ccv606, ccv611, ccv616, ccv622, ccv627, ccv63, ccv632, ccv638, ccv643, ccv648, ccv68, ccv73, ccv78, ccv83, ccv88, ccv93, ccv98 ppbool
	var ccv12, ccv125, ccv130, ccv16, ccv20, ccv24, ccv28, ccv32, ccv37, ccv39, ccv4, ccv41, ccv485, ccv8 tnmpz_srcptr
	var ccv36, ccv38, ccv40, ccv484 tnmpz_ptr
	var pp_ /* bs at bp+160 */ tnmpz_t
	var pp_ /* end at bp+176 */ ppuintptr
	var pp_ /* in1 at bp+0 */ tnmpz_t
	var pp_ /* in2 at bp+16 */ tnmpz_t
	var pp_ /* in3 at bp+32 */ tnmpz_t
	var pp_ /* ref1 at bp+96 */ tnmpz_t
	var pp_ /* ref2 at bp+112 */ tnmpz_t
	var pp_ /* ref3 at bp+128 */ tnmpz_t
	var pp_ /* res1 at bp+48 */ tnmpz_t
	var pp_ /* res2 at bp+64 */ tnmpz_t
	var pp_ /* res3 at bp+80 */ tnmpz_t
	var pp_ /* t at bp+144 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa__gmp_l, aa__gmp_n, aa__gmp_p, aa__i, aa__i1, aa__i10, aa__i100, aa__i101, aa__i102, aa__i103, aa__i104, aa__i105, aa__i106, aa__i107, aa__i108, aa__i109, aa__i11, aa__i110, aa__i111, aa__i112, aa__i113, aa__i114, aa__i12, aa__i13, aa__i14, aa__i15, aa__i16, aa__i17, aa__i18, aa__i19, aa__i2, aa__i20, aa__i21, aa__i22, aa__i23, aa__i24, aa__i25, aa__i26, aa__i27, aa__i28, aa__i29, aa__i3, aa__i30, aa__i31, aa__i32, aa__i33, aa__i34, aa__i35, aa__i36, aa__i37, aa__i38, aa__i39, aa__i4, aa__i40, aa__i41, aa__i42, aa__i43, aa__i44, aa__i45, aa__i46, aa__i47, aa__i48, aa__i49, aa__i5, aa__i50, aa__i51, aa__i52, aa__i53, aa__i54, aa__i55, aa__i56, aa__i57, aa__i58, aa__i59, aa__i6, aa__i60, aa__i61, aa__i62, aa__i63, aa__i64, aa__i65, aa__i66, aa__i67, aa__i68, aa__i69, aa__i7, aa__i70, aa__i71, aa__i72, aa__i73, aa__i74, aa__i75, aa__i76, aa__i77, aa__i78, aa__i79, aa__i8, aa__i80, aa__i81, aa__i82, aa__i83, aa__i84, aa__i85, aa__i86, aa__i87, aa__i88, aa__i89, aa__i9, aa__i90, aa__i91, aa__i92, aa__i93, aa__i94, aa__i95, aa__i96, aa__i97, aa__i98, aa__i99, aa__nail, aa__nail1, aa__nail10, aa__nail100, aa__nail101, aa__nail102, aa__nail103, aa__nail104, aa__nail105, aa__nail106, aa__nail107, aa__nail108, aa__nail109, aa__nail11, aa__nail110, aa__nail111, aa__nail112, aa__nail113, aa__nail114, aa__nail12, aa__nail13, aa__nail14, aa__nail15, aa__nail16, aa__nail17, aa__nail18, aa__nail19, aa__nail2, aa__nail20, aa__nail21, aa__nail22, aa__nail23, aa__nail24, aa__nail25, aa__nail26, aa__nail27, aa__nail28, aa__nail29, aa__nail3, aa__nail30, aa__nail31, aa__nail32, aa__nail33, aa__nail34, aa__nail35, aa__nail36, aa__nail37, aa__nail38, aa__nail39, aa__nail4, aa__nail40, aa__nail41, aa__nail42, aa__nail43, aa__nail44, aa__nail45, aa__nail46, aa__nail47, aa__nail48, aa__nail49, aa__nail5, aa__nail50, aa__nail51, aa__nail52, aa__nail53, aa__nail54, aa__nail55, aa__nail56, aa__nail57, aa__nail58, aa__nail59, aa__nail6, aa__nail60, aa__nail61, aa__nail62, aa__nail63, aa__nail64, aa__nail65, aa__nail66, aa__nail67, aa__nail68, aa__nail69, aa__nail7, aa__nail70, aa__nail71, aa__nail72, aa__nail73, aa__nail74, aa__nail75, aa__nail76, aa__nail77, aa__nail78, aa__nail79, aa__nail8, aa__nail80, aa__nail81, aa__nail82, aa__nail83, aa__nail84, aa__nail85, aa__nail86, aa__nail87, aa__nail88, aa__nail89, aa__nail9, aa__nail90, aa__nail91, aa__nail92, aa__nail93, aa__nail94, aa__nail95, aa__nail96, aa__nail97, aa__nail98, aa__nail99, aabsi, aaenvval, aai, aain2i, aapass, aar1, aar2, aarands, aarefretval, aarepfactor, aareps, aareps_nondefault, aaretval, aasize_range, ccv1, ccv101, ccv102, ccv103, ccv104, ccv106, ccv107, ccv108, ccv109, ccv11, ccv111, ccv113, ccv114, ccv115, ccv116, ccv117, ccv119, ccv12, ccv120, ccv121, ccv122, ccv124, ccv125, ccv126, ccv128, ccv13, ccv130, ccv131, ccv133, ccv134, ccv135, ccv136, ccv138, ccv139, ccv140, ccv141, ccv143, ccv145, ccv146, ccv147, ccv149, ccv15, ccv150, ccv151, ccv152, ccv154, ccv156, ccv157, ccv158, ccv16, ccv160, ccv161, ccv162, ccv163, ccv165, ccv166, ccv167, ccv168, ccv17, ccv170, ccv171, ccv172, ccv173, ccv174, ccv176, ccv177, ccv178, ccv179, ccv181, ccv182, ccv183, ccv184, ccv186, ccv187, ccv188, ccv189, ccv19, ccv191, ccv192, ccv193, ccv194, ccv196, ccv197, ccv198, ccv199, ccv20, ccv201, ccv202, ccv203, ccv204, ccv206, ccv207, ccv208, ccv209, ccv21, ccv210, ccv212, ccv213, ccv214, ccv215, ccv217, ccv218, ccv219, ccv220, ccv221, ccv223, ccv224, ccv225, ccv226, ccv228, ccv229, ccv23, ccv230, ccv231, ccv233, ccv234, ccv235, ccv236, ccv238, ccv239, ccv24, ccv240, ccv241, ccv243, ccv244, ccv245, ccv246, ccv248, ccv249, ccv25, ccv250, ccv251, ccv253, ccv254, ccv255, ccv256, ccv258, ccv259, ccv260, ccv261, ccv263, ccv264, ccv265, ccv266, ccv268, ccv269, ccv27, ccv270, ccv271, ccv273, ccv274, ccv275, ccv276, ccv278, ccv279, ccv28, ccv280, ccv281, ccv283, ccv284, ccv285, ccv286, ccv288, ccv289, ccv29, ccv290, ccv291, ccv293, ccv294, ccv295, ccv296, ccv298, ccv299, ccv300, ccv301, ccv303, ccv304, ccv305, ccv306, ccv308, ccv309, ccv31, ccv310, ccv311, ccv313, ccv314, ccv315, ccv316, ccv318, ccv319, ccv32, ccv320, ccv321, ccv323, ccv324, ccv325, ccv326, ccv328, ccv329, ccv33, ccv330, ccv331, ccv333, ccv334, ccv335, ccv336, ccv338, ccv339, ccv340, ccv341, ccv343, ccv344, ccv345, ccv346, ccv348, ccv349, ccv35, ccv350, ccv351, ccv353, ccv354, ccv355, ccv356, ccv358, ccv359, ccv36, ccv360, ccv361, ccv363, ccv364, ccv365, ccv366, ccv368, ccv369, ccv37, ccv370, ccv371, ccv373, ccv374, ccv375, ccv376, ccv378, ccv379, ccv38, ccv380, ccv381, ccv383, ccv384, ccv385, ccv386, ccv388, ccv389, ccv39, ccv390, ccv391, ccv393, ccv394, ccv395, ccv396, ccv398, ccv399, ccv4, ccv40, ccv400, ccv401, ccv403, ccv404, ccv405, ccv406, ccv408, ccv409, ccv41, ccv410, ccv411, ccv413, ccv414, ccv415, ccv416, ccv418, ccv419, ccv420, ccv421, ccv423, ccv424, ccv425, ccv426, ccv428, ccv429, ccv43, ccv430, ccv431, ccv433, ccv434, ccv435, ccv436, ccv438, ccv439, ccv44, ccv440, ccv441, ccv443, ccv444, ccv445, ccv446, ccv448, ccv449, ccv45, ccv450, ccv451, ccv453, ccv454, ccv455, ccv456, ccv458, ccv459, ccv46, ccv460, ccv461, ccv463, ccv464, ccv465, ccv466, ccv468, ccv469, ccv47, ccv470, ccv471, ccv473, ccv474, ccv475, ccv476, ccv478, ccv479, ccv480, ccv481, ccv483, ccv484, ccv485, ccv486, ccv487, ccv488, ccv489, ccv49, ccv490, ccv492, ccv493, ccv494, ccv495, ccv497, ccv498, ccv499, ccv5, ccv50, ccv500, ccv502, ccv503, ccv504, ccv505, ccv507, ccv508, ccv509, ccv51, ccv510, ccv512, ccv513, ccv514, ccv515, ccv517, ccv518, ccv519, ccv52, ccv520, ccv522, ccv523, ccv524, ccv525, ccv527, ccv528, ccv529, ccv530, ccv532, ccv533, ccv534, ccv535, ccv537, ccv538, ccv539, ccv54, ccv540, ccv542, ccv543, ccv544, ccv545, ccv547, ccv548, ccv549, ccv55, ccv550, ccv552, ccv553, ccv554, ccv555, ccv556, ccv557, ccv558, ccv559, ccv56, ccv561, ccv562, ccv563, ccv564, ccv566, ccv567, ccv568, ccv569, ccv57, ccv571, ccv572, ccv573, ccv574, ccv576, ccv577, ccv578, ccv579, ccv580, ccv581, ccv583, ccv584, ccv585, ccv586, ccv588, ccv589, ccv59, ccv590, ccv591, ccv593, ccv594, ccv595, ccv596, ccv598, ccv599, ccv600, ccv601, ccv603, ccv604, ccv605, ccv606, ccv607, ccv609, ccv61, ccv610, ccv611, ccv612, ccv614, ccv615, ccv616, ccv617, ccv619, ccv62, ccv620, ccv621, ccv622, ccv623, ccv625, ccv626, ccv627, ccv628, ccv63, ccv630, ccv631, ccv632, ccv633, ccv635, ccv636, ccv637, ccv638, ccv639, ccv64, ccv641, ccv642, ccv643, ccv644, ccv646, ccv647, ccv648, ccv649, ccv651, ccv66, ccv67, ccv68, ccv69, ccv7, ccv71, ccv72, ccv73, ccv74, ccv76, ccv77, ccv78, ccv79, ccv8, ccv81, ccv82, ccv83, ccv84, ccv86, ccv87, ccv88, ccv89, ccv9, ccv91, ccv92, ccv93, ccv94, ccv96, ccv97, ccv98, ccv99
	aareps = ppuint32(400)

	Xtests_start(cgtls)
	aareps_nondefault = 0
	if aaargc > ppint32(1) {
		aareps = iqlibc.ppUint32FromInt64(Xstrtol(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8)), cgbp+176, 0))
		if *(*ppuint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 176)))) != 0 || aareps <= ppuint32(0) {
			Xfprintf(cgtls, Xstderr, "Invalid test count: %s.\n\x00", iqlibc.ppVaList(cgbp+192, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8))))
			Xexit(cgtls, ppint32(1))
		}
		aaargv += 8
		aaargc--
		aareps_nondefault = ppint32(1)
	}
	aaenvval = Xgetenv(cgtls, "GMP_CHECK_REPFACTOR\x00")
	if aaenvval != iqlibc.ppUintptrFromInt32(0) {
		aarepfactor = Xstrtod(cgtls, aaenvval, cgbp+176)
		if *(*ppuint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 176)))) != 0 || aarepfactor <= iqlibc.ppFloat64FromInt32(0) {
			Xfprintf(cgtls, Xstderr, "Invalid repfactor: %f.\n\x00", iqlibc.ppVaList(cgbp+192, aarepfactor))
			Xexit(cgtls, ppint32(1))
		}
		aareps = ppuint32(ppfloat64(aareps) * aarepfactor)
		if aareps > iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)) {
			ccv1 = aareps
		} else {
			ccv1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1))
		}
		aareps = ccv1
		aareps_nondefault = ppint32(1)
	}
	if aareps_nondefault != 0 {
		Xprintf(cgtls, "Running test with %ld repetitions (include this in bug reports)\n\x00", iqlibc.ppVaList(cgbp+192, iqlibc.ppInt64FromUint32(aareps)))
	}

	if !(X__gmp_rands_initialized != 0) {
		X__gmp_rands_initialized = ppuint8(1)
		X__gmp_randinit_mt_noseed(cgtls, ppuintptr(iqunsafe.ppPointer(&X__gmp_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	aarands = ppuintptr(iqunsafe.ppPointer(&X__gmp_rands))

	X__gmpz_init(cgtls, cgbp+160)

	X__gmpz_init(cgtls, cgbp)
	X__gmpz_init(cgtls, cgbp+16)
	X__gmpz_init(cgtls, cgbp+32)
	X__gmpz_init(cgtls, cgbp+96)
	X__gmpz_init(cgtls, cgbp+112)
	X__gmpz_init(cgtls, cgbp+128)
	X__gmpz_init(cgtls, cgbp+48)
	X__gmpz_init(cgtls, cgbp+64)
	X__gmpz_init(cgtls, cgbp+80)
	X__gmpz_init(cgtls, cgbp+144)

	X__gmpz_set_ui(cgtls, cgbp+48, ppuint64(1)) /* force allocation */
	X__gmpz_set_ui(cgtls, cgbp+64, ppuint64(1)) /* force allocation */
	X__gmpz_set_ui(cgtls, cgbp+80, ppuint64(1)) /* force allocation */

	aapass = ppuint32(1)
	for {
		if !(aapass <= aareps) {
			break
		}

		if Xisatty(cgtls, ppint32(mvSTDOUT_FILENO)) != 0 {

			Xprintf(cgtls, "\r%d/%d passes\x00", iqlibc.ppVaList(cgbp+192, aapass, aareps))
			Xfflush(cgtls, Xstdout)
		}

		X__gmpz_urandomb(cgtls, cgbp+160, aarands, ppuint64(32))
		/* Make size_range gradually bigger with each pass. */
		ccv4 = cgbp + 160
		aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv4)).fd_mp_d
		aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv4)).fd_mp_size)
		aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
		if aa__gmp_n != 0 {
			ccv7 = aa__gmp_l
		} else {
			ccv7 = ppuint64(0)
		}
		ccv5 = ccv7
		goto cg_6
	cg_6:
		aasize_range = ccv5%ppuint64(aapass*iqlibc.ppUint32FromInt32(15)/aareps+iqlibc.ppUint32FromInt32(1)) + ppuint64(8)

		X__gmpz_urandomb(cgtls, cgbp+160, aarands, aasize_range)
		if aapass>>0&ppuint32(3) == ppuint32(3) {
			ccv8 = cgbp + 160
			aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv8)).fd_mp_d
			aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv8)).fd_mp_size)
			aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
			if aa__gmp_n != 0 {
				ccv11 = aa__gmp_l
			} else {
				ccv11 = ppuint64(0)
			}
			ccv9 = ccv11
			goto cg_10
		cg_10:
			X__gmpz_urandomb(cgtls, cgbp+160, aarands, ccv9%(aasize_range-ppuint64(7))+ppuint64(7))
		}
		ccv12 = cgbp + 160
		aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv12)).fd_mp_d
		aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv12)).fd_mp_size)
		aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
		if aa__gmp_n != 0 {
			ccv15 = aa__gmp_l
		} else {
			ccv15 = ppuint64(0)
		}
		ccv13 = ccv15
		goto cg_14
	cg_14:
		X__gmpz_rrandomb(cgtls, cgbp, aarands, ccv13)
		X__gmpz_urandomb(cgtls, cgbp+160, aarands, aasize_range)
		if aapass>>ppint32(2)&ppuint32(3) == ppuint32(3) {
			ccv16 = cgbp + 160
			aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv16)).fd_mp_d
			aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv16)).fd_mp_size)
			aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
			if aa__gmp_n != 0 {
				ccv19 = aa__gmp_l
			} else {
				ccv19 = ppuint64(0)
			}
			ccv17 = ccv19
			goto cg_18
		cg_18:
			X__gmpz_urandomb(cgtls, cgbp+160, aarands, ccv17%(aasize_range-ppuint64(7))+ppuint64(7))
		}
		ccv20 = cgbp + 160
		aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv20)).fd_mp_d
		aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv20)).fd_mp_size)
		aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
		if aa__gmp_n != 0 {
			ccv23 = aa__gmp_l
		} else {
			ccv23 = ppuint64(0)
		}
		ccv21 = ccv23
		goto cg_22
	cg_22:
		X__gmpz_rrandomb(cgtls, cgbp+16, aarands, ccv21)
		X__gmpz_urandomb(cgtls, cgbp+160, aarands, aasize_range)
		if aapass>>ppint32(4)&ppuint32(3) == ppuint32(3) {
			ccv24 = cgbp + 160
			aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv24)).fd_mp_d
			aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv24)).fd_mp_size)
			aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
			if aa__gmp_n != 0 {
				ccv27 = aa__gmp_l
			} else {
				ccv27 = ppuint64(0)
			}
			ccv25 = ccv27
			goto cg_26
		cg_26:
			X__gmpz_urandomb(cgtls, cgbp+160, aarands, ccv25%(aasize_range-ppuint64(7))+ppuint64(7))
		}
		ccv28 = cgbp + 160
		aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv28)).fd_mp_d
		aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv28)).fd_mp_size)
		aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
		if aa__gmp_n != 0 {
			ccv31 = aa__gmp_l
		} else {
			ccv31 = ppuint64(0)
		}
		ccv29 = ccv31
		goto cg_30
	cg_30:
		X__gmpz_rrandomb(cgtls, cgbp+32, aarands, ccv29)

		X__gmpz_urandomb(cgtls, cgbp+160, aarands, ppuint64(3))
		ccv32 = cgbp + 160
		aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv32)).fd_mp_d
		aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv32)).fd_mp_size)
		aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
		if aa__gmp_n != 0 {
			ccv35 = aa__gmp_l
		} else {
			ccv35 = ppuint64(0)
		}
		ccv33 = ccv35
		goto cg_34
	cg_34:
		aabsi = ccv33
		if aabsi&ppuint64(1) != ppuint64(0) {
			ccv36 = cgbp
			ccv37 = cgbp
			if ccv36 != ccv37 {
				X__gmpz_set(cgtls, ccv36, ccv37)
			}
			(*tn__mpz_struct)(iqunsafe.ppPointer(ccv36)).fd_mp_size = -(*tn__mpz_struct)(iqunsafe.ppPointer(ccv36)).fd_mp_size
		}
		if aabsi&ppuint64(2) != ppuint64(0) {
			ccv38 = cgbp + 16
			ccv39 = cgbp + 16
			if ccv38 != ccv39 {
				X__gmpz_set(cgtls, ccv38, ccv39)
			}
			(*tn__mpz_struct)(iqunsafe.ppPointer(ccv38)).fd_mp_size = -(*tn__mpz_struct)(iqunsafe.ppPointer(ccv38)).fd_mp_size
		}
		if aabsi&ppuint64(4) != ppuint64(0) {
			ccv40 = cgbp + 32
			ccv41 = cgbp + 32
			if ccv40 != ccv41 {
				X__gmpz_set(cgtls, ccv40, ccv41)
			}
			(*tn__mpz_struct)(iqunsafe.ppPointer(ccv40)).fd_mp_size = -(*tn__mpz_struct)(iqunsafe.ppPointer(ccv40)).fd_mp_size
		}

		aai = 0
		for {
			if !(iqlibc.ppUint64FromInt32(aai) < iqlibc.ppUint64FromInt64(384)/iqlibc.ppUint64FromInt64(24)) {
				break
			}

			if ccv44 = sidss[aai].fdisdivision != 0; ccv44 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size < 0 {
					ccv43 = -ppint32(1)
				} else {
					ccv43 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size > 0)
				}
			}
			if ccv44 && ccv43 == 0 {
				goto cg_42
			}
			if sidss[aai].fdisslow != 0 && aasize_range > ppuint64(19) {
				goto cg_42
			}

			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{sidss[aai].fdfptr})))(cgtls, cgbp+96, cgbp, cgbp+16)

			if ccv46 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv46 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv45 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv45 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv46 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv45-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(295), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv47 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv47 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv47)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(295), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv49 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv49 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i < ppint64(ccv49)) {
						break
					}
					aa__nail = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(295), "__nail == 0\x00")
					}
					goto cg_48
				cg_48:
					;
					aa__i++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			if aapass&ppuint32(1) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+48)
			}
			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{sidss[aai].fdfptr})))(cgtls, cgbp+48, cgbp+48, cgbp+16)

			if ccv51 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv51 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv50 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv50 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv51 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv50-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(299), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv52 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv52 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv52)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(299), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i1 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv54 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv54 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i1 < ppint64(ccv54)) {
						break
					}
					aa__nail1 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i1)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail1 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(299), "__nail == 0\x00")
					}
					goto cg_53
				cg_53:
					;
					aa__i1++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, sidss[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			if aapass&ppuint32(1) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+48)
			}
			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{sidss[aai].fdfptr})))(cgtls, cgbp+48, cgbp, cgbp+48)

			if ccv56 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv56 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv55 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv55 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv56 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv55-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(305), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv57 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv57 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv57)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(305), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i2 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv59 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv59 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i2 < ppint64(ccv59)) {
						break
					}
					aa__nail2 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i2)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail2 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(305), "__nail == 0\x00")
					}
					goto cg_58
				cg_58:
					;
					aa__i2++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, sidss[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			goto cg_42
		cg_42:
			;
			aai++
		}

		aai = 0
		for {
			if !(iqlibc.ppUint64FromInt32(aai) < iqlibc.ppUint64FromInt64(72)/iqlibc.ppUint64FromInt64(24)) {
				break
			}

			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size < 0 {
				ccv61 = -ppint32(1)
			} else {
				ccv61 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size > 0)
			}
			if ccv61 == 0 {
				goto cg_60
			}

			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{siddss_div[aai].fdfptr})))(cgtls, cgbp+96, cgbp+112, cgbp, cgbp+16)

			if ccv63 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv63 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv62 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv62 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv63 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv62-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(316), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv64 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv64 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv64)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(316), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i3 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv66 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv66 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i3 < ppint64(ccv66)) {
						break
					}
					aa__nail3 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i3)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail3 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(316), "__nail == 0\x00")
					}
					goto cg_65
				cg_65:
					;
					aa__i3++
				}
			}

			if ccv68 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size == 0; !ccv68 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
					ccv67 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
				} else {
					ccv67 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv68 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_d + ppuintptr(ccv67-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(317), "((ref2)->_mp_size) == 0 || ((ref2)->_mp_d)[((((ref2)->_mp_size)) >= 0 ? (((ref2)->_mp_size)) : -(((ref2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
				ccv69 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
			} else {
				ccv69 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_alloc >= ccv69)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(317), "((ref2)->_mp_alloc) >= ((((ref2)->_mp_size)) >= 0 ? (((ref2)->_mp_size)) : -(((ref2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i4 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
						ccv71 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
					} else {
						ccv71 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
					}
					if !(aa__i4 < ppint64(ccv71)) {
						break
					}
					aa__nail4 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_d + ppuintptr(aa__i4)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail4 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(317), "__nail == 0\x00")
					}
					goto cg_70
				cg_70:
					;
					aa__i4++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			Xmpz_clobber(cgtls, cgbp+64)
			if aapass&ppuint32(1) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+48)
			}
			if aapass&ppuint32(2) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+64)
			}
			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{siddss_div[aai].fdfptr})))(cgtls, cgbp+48, cgbp+64, cgbp+48, cgbp+16)

			if ccv73 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv73 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv72 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv72 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv73 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv72-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(322), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv74 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv74 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv74)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(322), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i5 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv76 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv76 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i5 < ppint64(ccv76)) {
						break
					}
					aa__nail5 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i5)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail5 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(322), "__nail == 0\x00")
					}
					goto cg_75
				cg_75:
					;
					aa__i5++
				}
			}

			if ccv78 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv78 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv77 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv77 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv78 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv77-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(323), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv79 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv79 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv79)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(323), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i6 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv81 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv81 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i6 < ppint64(ccv81)) {
						break
					}
					aa__nail6 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i6)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail6 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(323), "__nail == 0\x00")
					}
					goto cg_80
				cg_80:
					;
					aa__i6++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, siddss_div[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp)
			if aapass&ppuint32(1) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+48)
			}
			if aapass&ppuint32(2) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+64)
			}
			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{siddss_div[aai].fdfptr})))(cgtls, cgbp+48, cgbp+64, cgbp+64, cgbp+16)

			if ccv83 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv83 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv82 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv82 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv83 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv82-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(330), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv84 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv84 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv84)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(330), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i7 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv86 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv86 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i7 < ppint64(ccv86)) {
						break
					}
					aa__nail7 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i7)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail7 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(330), "__nail == 0\x00")
					}
					goto cg_85
				cg_85:
					;
					aa__i7++
				}
			}

			if ccv88 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv88 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv87 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv87 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv88 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv87-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(331), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv89 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv89 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv89)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(331), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i8 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv91 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv91 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i8 < ppint64(ccv91)) {
						break
					}
					aa__nail8 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i8)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail8 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(331), "__nail == 0\x00")
					}
					goto cg_90
				cg_90:
					;
					aa__i8++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, siddss_div[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			Xmpz_clobber(cgtls, cgbp+64)
			if aapass&ppuint32(1) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+48)
			}
			if aapass&ppuint32(2) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+64)
			}
			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{siddss_div[aai].fdfptr})))(cgtls, cgbp+48, cgbp+64, cgbp, cgbp+48)

			if ccv93 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv93 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv92 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv92 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv93 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv92-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(338), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv94 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv94 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv94)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(338), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i9 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv96 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv96 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i9 < ppint64(ccv96)) {
						break
					}
					aa__nail9 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i9)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail9 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(338), "__nail == 0\x00")
					}
					goto cg_95
				cg_95:
					;
					aa__i9++
				}
			}

			if ccv98 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv98 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv97 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv97 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv98 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv97-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(339), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv99 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv99 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv99)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(339), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i10 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv101 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv101 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i10 < ppint64(ccv101)) {
						break
					}
					aa__nail10 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i10)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail10 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(339), "__nail == 0\x00")
					}
					goto cg_100
				cg_100:
					;
					aa__i10++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, siddss_div[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp+16)
			if aapass&ppuint32(1) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+48)
			}
			if aapass&ppuint32(2) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+64)
			}
			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{siddss_div[aai].fdfptr})))(cgtls, cgbp+48, cgbp+64, cgbp, cgbp+64)

			if ccv103 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv103 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv102 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv102 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv103 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv102-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(346), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv104 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv104 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv104)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(346), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i11 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv106 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv106 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i11 < ppint64(ccv106)) {
						break
					}
					aa__nail11 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i11)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail11 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(346), "__nail == 0\x00")
					}
					goto cg_105
				cg_105:
					;
					aa__i11++
				}
			}

			if ccv108 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv108 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv107 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv107 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv108 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv107-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(347), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv109 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv109 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv109)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(347), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i12 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv111 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv111 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i12 < ppint64(ccv111)) {
						break
					}
					aa__nail12 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i12)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail12 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(347), "__nail == 0\x00")
					}
					goto cg_110
				cg_110:
					;
					aa__i12++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, siddss_div[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			goto cg_60
		cg_60:
			;
			aai++
		}

		aai = 0
		for {
			if !(iqlibc.ppUint64FromInt32(aai) < iqlibc.ppUint64FromInt64(96)/iqlibc.ppUint64FromInt64(24)) {
				break
			}

			if ccv114 = sids[aai].fdnonneg != 0; ccv114 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size < 0 {
					ccv113 = -ppint32(1)
				} else {
					ccv113 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size > 0)
				}
			}
			if ccv114 && ccv113 < 0 {
				goto cg_112
			}

			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{sids[aai].fdfptr})))(cgtls, cgbp+96, cgbp)

			if ccv116 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv116 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv115 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv115 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv116 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv115-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(358), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv117 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv117 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv117)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(358), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i13 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv119 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv119 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i13 < ppint64(ccv119)) {
						break
					}
					aa__nail13 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i13)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail13 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(358), "__nail == 0\x00")
					}
					goto cg_118
				cg_118:
					;
					aa__i13++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			if aapass&ppuint32(1) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+48)
			}
			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{sids[aai].fdfptr})))(cgtls, cgbp+48, cgbp+48)

			if ccv121 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv121 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv120 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv120 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv121 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv120-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(362), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv122 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv122 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv122)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(362), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i14 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv124 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv124 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i14 < ppint64(ccv124)) {
						break
					}
					aa__nail14 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i14)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail14 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(362), "__nail == 0\x00")
					}
					goto cg_123
				cg_123:
					;
					aa__i14++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, sids[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			goto cg_112
		cg_112:
			;
			aai++
		}

		ccv125 = cgbp + 16
		aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv125)).fd_mp_d
		aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv125)).fd_mp_size)
		aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
		if aa__gmp_n != 0 {
			ccv128 = aa__gmp_l
		} else {
			ccv128 = ppuint64(0)
		}
		ccv126 = ccv128
		goto cg_127
	cg_127:
		aain2i = ccv126

		aai = 0
		for {
			if !(iqlibc.ppUint64FromInt32(aai) < iqlibc.ppUint64FromInt64(264)/iqlibc.ppUint64FromInt64(24)) {
				break
			}

			if sidsi[aai].fdmod != 0 {
				ccv130 = cgbp + 16
				aa__gmp_p = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv130)).fd_mp_d
				aa__gmp_n = ppint64((*tn__mpz_struct)(iqunsafe.ppPointer(ccv130)).fd_mp_size)
				aa__gmp_l = *(*tnmp_limb_t)(iqunsafe.ppPointer(aa__gmp_p))
				if aa__gmp_n != 0 {
					ccv133 = aa__gmp_l
				} else {
					ccv133 = ppuint64(0)
				}
				ccv131 = ccv133
				goto cg_132
			cg_132:
				aain2i = ccv131 % iqlibc.ppUint64FromInt32(sidsi[aai].fdmod)
			}

			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, ppuint64))(iqunsafe.ppPointer(&struct{ ppuintptr }{sidsi[aai].fdfptr})))(cgtls, cgbp+96, cgbp, aain2i)

			if ccv135 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv135 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv134 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv134 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv135 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv134-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(375), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv136 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv136 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv136)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(375), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i15 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv138 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv138 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i15 < ppint64(ccv138)) {
						break
					}
					aa__nail15 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i15)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail15 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(375), "__nail == 0\x00")
					}
					goto cg_137
				cg_137:
					;
					aa__i15++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			if aapass&ppuint32(1) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+48)
			}
			if aapass&ppuint32(2) != 0 {
				Xrealloc_if_reducing(cgtls, cgbp+48)
			}
			(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, ppuint64))(iqunsafe.ppPointer(&struct{ ppuintptr }{sidsi[aai].fdfptr})))(cgtls, cgbp+48, cgbp+48, aain2i)

			if ccv140 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv140 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv139 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv139 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv140 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv139-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(379), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv141 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv141 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv141)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(379), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i16 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv143 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv143 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i16 < ppint64(ccv143)) {
						break
					}
					aa__nail16 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i16)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail16 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(379), "__nail == 0\x00")
					}
					goto cg_142
				cg_142:
					;
					aa__i16++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, sidsi[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			goto cg_129
		cg_129:
			;
			aai++
		}

		if aain2i != ppuint64(0) { /* Don't divide by 0.  */

			aai = 0
			for {
				if !(iqlibc.ppUint64FromInt32(aai) < iqlibc.ppUint64FromInt64(96)/iqlibc.ppUint64FromInt64(16)) {
					break
				}

				aar1 = (*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, ppuint64) ppuint64)(iqunsafe.ppPointer(&struct{ ppuintptr }{sidsi_div[aai].fdfptr})))(cgtls, cgbp+96, cgbp, aain2i)

				if ccv146 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv146 {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv145 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv145 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv146 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv145-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(389), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
				}
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv147 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv147 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv147)) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(389), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
				} /* let whole loop go dead when no nails */
				if mvGMP_NAIL_BITS != 0 {
					aa__i17 = 0
					for {
						if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
							ccv149 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
						} else {
							ccv149 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
						}
						if !(aa__i17 < ppint64(ccv149)) {
							break
						}
						aa__nail17 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i17)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
						if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail17 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
							X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(389), "__nail == 0\x00")
						}
						goto cg_148
					cg_148:
						;
						aa__i17++
					}
				}

				X__gmpz_set(cgtls, cgbp+48, cgbp)
				aar2 = (*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, ppuint64) ppuint64)(iqunsafe.ppPointer(&struct{ ppuintptr }{sidsi_div[aai].fdfptr})))(cgtls, cgbp+48, cgbp+48, aain2i)

				if ccv151 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv151 {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv150 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv150 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv151 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv150-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(393), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
				}
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv152 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv152 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv152)) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(393), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
				} /* let whole loop go dead when no nails */
				if mvGMP_NAIL_BITS != 0 {
					aa__i18 = 0
					for {
						if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
							ccv154 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
						} else {
							ccv154 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
						}
						if !(aa__i18 < ppint64(ccv154)) {
							break
						}
						aa__nail18 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i18)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
						if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail18 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
							X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(393), "__nail == 0\x00")
						}
						goto cg_153
					cg_153:
						;
						aa__i18++
					}
				}
				if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || aar1 != aar2 {
					Xdump(cgtls, sidsi_div[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
					Xexit(cgtls, ppint32(1))
				}

				goto cg_144
			cg_144:
				;
				aai++
			}

			aai = 0
			for {
				if !(iqlibc.ppUint64FromInt32(aai) < iqlibc.ppUint64FromInt64(72)/iqlibc.ppUint64FromInt64(24)) {
					break
				}

				aar1 = (*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_ptr, tnmpz_srcptr, ppuint64) ppuint64)(iqunsafe.ppPointer(&struct{ ppuintptr }{siddsi_div[aai].fdfptr})))(cgtls, cgbp+96, cgbp+112, cgbp, aain2i)

				if ccv157 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv157 {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv156 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv156 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv157 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv156-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(401), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
				}
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv158 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv158 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv158)) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(401), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
				} /* let whole loop go dead when no nails */
				if mvGMP_NAIL_BITS != 0 {
					aa__i19 = 0
					for {
						if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
							ccv160 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
						} else {
							ccv160 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
						}
						if !(aa__i19 < ppint64(ccv160)) {
							break
						}
						aa__nail19 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i19)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
						if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail19 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
							X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(401), "__nail == 0\x00")
						}
						goto cg_159
					cg_159:
						;
						aa__i19++
					}
				}

				X__gmpz_set(cgtls, cgbp+48, cgbp)
				Xmpz_clobber(cgtls, cgbp+64)
				aar2 = (*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_ptr, tnmpz_srcptr, ppuint64) ppuint64)(iqunsafe.ppPointer(&struct{ ppuintptr }{siddsi_div[aai].fdfptr})))(cgtls, cgbp+48, cgbp+64, cgbp+48, aain2i)

				if ccv162 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv162 {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv161 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv161 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv162 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv161-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(406), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
				}
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv163 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv163 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv163)) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(406), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
				} /* let whole loop go dead when no nails */
				if mvGMP_NAIL_BITS != 0 {
					aa__i20 = 0
					for {
						if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
							ccv165 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
						} else {
							ccv165 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
						}
						if !(aa__i20 < ppint64(ccv165)) {
							break
						}
						aa__nail20 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i20)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
						if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail20 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
							X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(406), "__nail == 0\x00")
						}
						goto cg_164
					cg_164:
						;
						aa__i20++
					}
				}
				if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || aar1 != aar2 {
					Xdump(cgtls, siddsi_div[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
					Xexit(cgtls, ppint32(1))
				}

				Xmpz_clobber(cgtls, cgbp+48)
				X__gmpz_set(cgtls, cgbp+64, cgbp)
				(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_ptr, tnmpz_srcptr, ppuint64) ppuint64)(iqunsafe.ppPointer(&struct{ ppuintptr }{siddsi_div[aai].fdfptr})))(cgtls, cgbp+48, cgbp+64, cgbp+64, aain2i)

				if ccv167 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv167 {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv166 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv166 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv167 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv166-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(413), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
				}
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv168 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv168 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv168)) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(413), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
				} /* let whole loop go dead when no nails */
				if mvGMP_NAIL_BITS != 0 {
					aa__i21 = 0
					for {
						if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
							ccv170 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
						} else {
							ccv170 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
						}
						if !(aa__i21 < ppint64(ccv170)) {
							break
						}
						aa__nail21 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i21)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
						if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail21 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
							X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(413), "__nail == 0\x00")
						}
						goto cg_169
					cg_169:
						;
						aa__i21++
					}
				}
				if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || aar1 != aar2 {
					Xdump(cgtls, siddsi_div[aai].fdfname, cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
					Xexit(cgtls, ppint32(1))
				}

				goto cg_155
			cg_155:
				;
				aai++
			}
		}

		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size < 0 {
			ccv171 = -ppint32(1)
		} else {
			ccv171 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size > 0)
		}
		if ccv171 >= 0 {

			X__gmpz_sqrtrem(cgtls, cgbp+96, cgbp+112, cgbp)

			if ccv173 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv173 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv172 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv172 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv173 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv172-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(422), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv174 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv174 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv174)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(422), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i22 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv176 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv176 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i22 < ppint64(ccv176)) {
						break
					}
					aa__nail22 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i22)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail22 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(422), "__nail == 0\x00")
					}
					goto cg_175
				cg_175:
					;
					aa__i22++
				}
			}

			if ccv178 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size == 0; !ccv178 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
					ccv177 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
				} else {
					ccv177 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv178 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_d + ppuintptr(ccv177-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(423), "((ref2)->_mp_size) == 0 || ((ref2)->_mp_d)[((((ref2)->_mp_size)) >= 0 ? (((ref2)->_mp_size)) : -(((ref2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
				ccv179 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
			} else {
				ccv179 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_alloc >= ccv179)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(423), "((ref2)->_mp_alloc) >= ((((ref2)->_mp_size)) >= 0 ? (((ref2)->_mp_size)) : -(((ref2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i23 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
						ccv181 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
					} else {
						ccv181 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
					}
					if !(aa__i23 < ppint64(ccv181)) {
						break
					}
					aa__nail23 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_d + ppuintptr(aa__i23)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail23 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(423), "__nail == 0\x00")
					}
					goto cg_180
				cg_180:
					;
					aa__i23++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			X__gmpz_sqrtrem(cgtls, cgbp+48, cgbp+64, cgbp+48)

			if ccv183 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv183 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv182 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv182 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv183 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv182-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(427), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv184 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv184 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv184)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(427), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i24 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv186 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv186 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i24 < ppint64(ccv186)) {
						break
					}
					aa__nail24 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i24)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail24 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(427), "__nail == 0\x00")
					}
					goto cg_185
				cg_185:
					;
					aa__i24++
				}
			}

			if ccv188 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv188 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv187 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv187 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv188 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv187-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(428), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv189 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv189 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv189)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(428), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i25 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv191 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv191 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i25 < ppint64(ccv191)) {
						break
					}
					aa__nail25 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i25)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail25 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(428), "__nail == 0\x00")
					}
					goto cg_190
				cg_190:
					;
					aa__i25++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_sqrtrem\x00", cgbp, iqlibc.ppUintptrFromInt32(0), iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+64, cgbp)
			X__gmpz_sqrtrem(cgtls, cgbp+48, cgbp+64, cgbp+64)

			if ccv193 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv193 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv192 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv192 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv193 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv192-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(434), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv194 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv194 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv194)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(434), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i26 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv196 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv196 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i26 < ppint64(ccv196)) {
						break
					}
					aa__nail26 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i26)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail26 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(434), "__nail == 0\x00")
					}
					goto cg_195
				cg_195:
					;
					aa__i26++
				}
			}

			if ccv198 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv198 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv197 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv197 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv198 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv197-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(435), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv199 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv199 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv199)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(435), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i27 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv201 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv201 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i27 < ppint64(ccv201)) {
						break
					}
					aa__nail27 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i27)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail27 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(435), "__nail == 0\x00")
					}
					goto cg_200
				cg_200:
					;
					aa__i27++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_sqrtrem\x00", cgbp, iqlibc.ppUintptrFromInt32(0), iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			X__gmpz_sqrtrem(cgtls, cgbp+48, cgbp+48, cgbp+48)

			if ccv203 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv203 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv202 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv202 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv203 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv202-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(441), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv204 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv204 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv204)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(441), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i28 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv206 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv206 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i28 < ppint64(ccv206)) {
						break
					}
					aa__nail28 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i28)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail28 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(441), "__nail == 0\x00")
					}
					goto cg_205
				cg_205:
					;
					aa__i28++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+112, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_sqrtrem\x00", cgbp, iqlibc.ppUintptrFromInt32(0), iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}
		}

		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size < 0 {
			ccv207 = -ppint32(1)
		} else {
			ccv207 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size > 0)
		}
		if ccv207 >= 0 {

			X__gmpz_root(cgtls, cgbp+96, cgbp, aain2i%ppuint64(0x100)+ppuint64(1))

			if ccv209 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv209 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv208 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv208 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv209 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv208-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(449), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv210 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv210 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv210)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(449), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i29 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv212 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv212 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i29 < ppint64(ccv212)) {
						break
					}
					aa__nail29 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i29)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail29 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(449), "__nail == 0\x00")
					}
					goto cg_211
				cg_211:
					;
					aa__i29++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			X__gmpz_root(cgtls, cgbp+48, cgbp+48, aain2i%ppuint64(0x100)+ppuint64(1))

			if ccv214 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv214 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv213 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv213 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv214 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv213-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(453), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv215 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv215 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv215)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(453), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i30 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv217 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv217 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i30 < ppint64(ccv217)) {
						break
					}
					aa__nail30 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i30)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail30 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(453), "__nail == 0\x00")
					}
					goto cg_216
				cg_216:
					;
					aa__i30++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_root\x00", cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}
		}

		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size < 0 {
			ccv218 = -ppint32(1)
		} else {
			ccv218 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size > 0)
		}
		if ccv218 >= 0 {

			X__gmpz_rootrem(cgtls, cgbp+96, cgbp+112, cgbp, aain2i%ppuint64(0x100)+ppuint64(1))

			if ccv220 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv220 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv219 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv219 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv220 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv219-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(461), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv221 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv221 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv221)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(461), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i31 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv223 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv223 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i31 < ppint64(ccv223)) {
						break
					}
					aa__nail31 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i31)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail31 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(461), "__nail == 0\x00")
					}
					goto cg_222
				cg_222:
					;
					aa__i31++
				}
			}

			if ccv225 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size == 0; !ccv225 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
					ccv224 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
				} else {
					ccv224 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv225 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_d + ppuintptr(ccv224-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(462), "((ref2)->_mp_size) == 0 || ((ref2)->_mp_d)[((((ref2)->_mp_size)) >= 0 ? (((ref2)->_mp_size)) : -(((ref2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
				ccv226 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
			} else {
				ccv226 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_alloc >= ccv226)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(462), "((ref2)->_mp_alloc) >= ((((ref2)->_mp_size)) >= 0 ? (((ref2)->_mp_size)) : -(((ref2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i32 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
						ccv228 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
					} else {
						ccv228 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
					}
					if !(aa__i32 < ppint64(ccv228)) {
						break
					}
					aa__nail32 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_d + ppuintptr(aa__i32)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail32 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(462), "__nail == 0\x00")
					}
					goto cg_227
				cg_227:
					;
					aa__i32++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			X__gmpz_rootrem(cgtls, cgbp+48, cgbp+64, cgbp+48, aain2i%ppuint64(0x100)+ppuint64(1))

			if ccv230 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv230 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv229 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv229 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv230 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv229-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(466), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv231 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv231 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv231)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(466), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i33 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv233 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv233 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i33 < ppint64(ccv233)) {
						break
					}
					aa__nail33 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i33)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail33 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(466), "__nail == 0\x00")
					}
					goto cg_232
				cg_232:
					;
					aa__i33++
				}
			}

			if ccv235 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv235 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv234 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv234 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv235 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv234-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(467), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv236 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv236 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv236)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(467), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i34 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv238 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv238 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i34 < ppint64(ccv238)) {
						break
					}
					aa__nail34 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i34)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail34 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(467), "__nail == 0\x00")
					}
					goto cg_237
				cg_237:
					;
					aa__i34++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_rootrem\x00", cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+64, cgbp)
			X__gmpz_rootrem(cgtls, cgbp+48, cgbp+64, cgbp+64, aain2i%ppuint64(0x100)+ppuint64(1))

			if ccv240 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv240 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv239 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv239 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv240 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv239-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(473), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv241 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv241 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv241)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(473), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i35 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv243 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv243 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i35 < ppint64(ccv243)) {
						break
					}
					aa__nail35 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i35)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail35 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(473), "__nail == 0\x00")
					}
					goto cg_242
				cg_242:
					;
					aa__i35++
				}
			}

			if ccv245 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv245 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv244 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv244 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv245 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv244-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(474), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv246 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv246 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv246)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(474), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i36 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv248 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv248 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i36 < ppint64(ccv248)) {
						break
					}
					aa__nail36 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i36)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail36 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(474), "__nail == 0\x00")
					}
					goto cg_247
				cg_247:
					;
					aa__i36++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_rootrem\x00", cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}
		}

		if aasize_range < ppuint64(18) { /* run fewer tests since gcdext is slow */

			X__gmpz_gcdext(cgtls, cgbp+96, cgbp+112, cgbp+128, cgbp, cgbp+16)

			if ccv250 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv250 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv249 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv249 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv250 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv249-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(482), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv251 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv251 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv251)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(482), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i37 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv253 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv253 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i37 < ppint64(ccv253)) {
						break
					}
					aa__nail37 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i37)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail37 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(482), "__nail == 0\x00")
					}
					goto cg_252
				cg_252:
					;
					aa__i37++
				}
			}

			if ccv255 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size == 0; !ccv255 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
					ccv254 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
				} else {
					ccv254 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv255 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_d + ppuintptr(ccv254-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(483), "((ref2)->_mp_size) == 0 || ((ref2)->_mp_d)[((((ref2)->_mp_size)) >= 0 ? (((ref2)->_mp_size)) : -(((ref2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
				ccv256 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
			} else {
				ccv256 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_alloc >= ccv256)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(483), "((ref2)->_mp_alloc) >= ((((ref2)->_mp_size)) >= 0 ? (((ref2)->_mp_size)) : -(((ref2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i38 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_size >= 0 {
						ccv258 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
					} else {
						ccv258 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mp_size
					}
					if !(aa__i38 < ppint64(ccv258)) {
						break
					}
					aa__nail38 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mp_d + ppuintptr(aa__i38)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail38 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(483), "__nail == 0\x00")
					}
					goto cg_257
				cg_257:
					;
					aa__i38++
				}
			}

			if ccv260 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mp_size == 0; !ccv260 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mp_size >= 0 {
					ccv259 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 128)).fd_mp_size
				} else {
					ccv259 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 128)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv260 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mp_d + ppuintptr(ccv259-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(484), "((ref3)->_mp_size) == 0 || ((ref3)->_mp_d)[((((ref3)->_mp_size)) >= 0 ? (((ref3)->_mp_size)) : -(((ref3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mp_size >= 0 {
				ccv261 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 128)).fd_mp_size
			} else {
				ccv261 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 128)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mp_alloc >= ccv261)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(484), "((ref3)->_mp_alloc) >= ((((ref3)->_mp_size)) >= 0 ? (((ref3)->_mp_size)) : -(((ref3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i39 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mp_size >= 0 {
						ccv263 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 128)).fd_mp_size
					} else {
						ccv263 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 128)).fd_mp_size
					}
					if !(aa__i39 < ppint64(ccv263)) {
						break
					}
					aa__nail39 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mp_d + ppuintptr(aa__i39)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail39 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(484), "__nail == 0\x00")
					}
					goto cg_262
				cg_262:
					;
					aa__i39++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			Xmpz_clobber(cgtls, cgbp+64)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+48, cgbp+16)
			if ccv265 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv265 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv264 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv264 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv265 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv264-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(506), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv266 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv266 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv266)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(506), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i40 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv268 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv268 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i40 < ppint64(ccv268)) {
						break
					}
					aa__nail40 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i40)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail40 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(506), "__nail == 0\x00")
					}
					goto cg_267
				cg_267:
					;
					aa__i40++
				}
			}
			if ccv270 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv270 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv269 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv269 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv270 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv269-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(506), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv271 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv271 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv271)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(506), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i41 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv273 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv273 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i41 < ppint64(ccv273)) {
						break
					}
					aa__nail41 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i41)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail41 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(506), "__nail == 0\x00")
					}
					goto cg_272
				cg_272:
					;
					aa__i41++
				}
			}
			if ccv275 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv275 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv274 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv274 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv275 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv274-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(506), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv276 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv276 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv276)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(506), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i42 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv278 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv278 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i42 < ppint64(ccv278)) {
						break
					}
					aa__nail42 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i42)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail42 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(506), "__nail == 0\x00")
					}
					goto cg_277
				cg_277:
					;
					aa__i42++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+48, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+64, cgbp+16)
			if ccv280 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv280 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv279 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv279 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv280 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv279-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(511), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv281 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv281 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv281)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(511), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i43 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv283 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv283 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i43 < ppint64(ccv283)) {
						break
					}
					aa__nail43 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i43)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail43 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(511), "__nail == 0\x00")
					}
					goto cg_282
				cg_282:
					;
					aa__i43++
				}
			}
			if ccv285 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv285 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv284 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv284 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv285 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv284-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(511), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv286 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv286 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv286)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(511), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i44 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv288 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv288 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i44 < ppint64(ccv288)) {
						break
					}
					aa__nail44 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i44)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail44 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(511), "__nail == 0\x00")
					}
					goto cg_287
				cg_287:
					;
					aa__i44++
				}
			}
			if ccv290 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv290 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv289 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv289 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv290 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv289-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(511), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv291 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv291 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv291)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(511), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i45 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv293 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv293 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i45 < ppint64(ccv293)) {
						break
					}
					aa__nail45 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i45)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail45 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(511), "__nail == 0\x00")
					}
					goto cg_292
				cg_292:
					;
					aa__i45++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+64, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			Xmpz_clobber(cgtls, cgbp+64)
			X__gmpz_set(cgtls, cgbp+80, cgbp)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+80, cgbp+16)
			if ccv295 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv295 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv294 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv294 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv295 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv294-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(516), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv296 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv296 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv296)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(516), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i46 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv298 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv298 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i46 < ppint64(ccv298)) {
						break
					}
					aa__nail46 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i46)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail46 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(516), "__nail == 0\x00")
					}
					goto cg_297
				cg_297:
					;
					aa__i46++
				}
			}
			if ccv300 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv300 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv299 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv299 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv300 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv299-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(516), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv301 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv301 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv301)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(516), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i47 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv303 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv303 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i47 < ppint64(ccv303)) {
						break
					}
					aa__nail47 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i47)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail47 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(516), "__nail == 0\x00")
					}
					goto cg_302
				cg_302:
					;
					aa__i47++
				}
			}
			if ccv305 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv305 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv304 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv304 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv305 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv304-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(516), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv306 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv306 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv306)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(516), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i48 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv308 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv308 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i48 < ppint64(ccv308)) {
						break
					}
					aa__nail48 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i48)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail48 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(516), "__nail == 0\x00")
					}
					goto cg_307
				cg_307:
					;
					aa__i48++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+80, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			Xmpz_clobber(cgtls, cgbp+64)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp, cgbp+48)
			if ccv310 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv310 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv309 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv309 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv310 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv309-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(521), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv311 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv311 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv311)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(521), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i49 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv313 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv313 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i49 < ppint64(ccv313)) {
						break
					}
					aa__nail49 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i49)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail49 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(521), "__nail == 0\x00")
					}
					goto cg_312
				cg_312:
					;
					aa__i49++
				}
			}
			if ccv315 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv315 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv314 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv314 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv315 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv314-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(521), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv316 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv316 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv316)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(521), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i50 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv318 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv318 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i50 < ppint64(ccv318)) {
						break
					}
					aa__nail50 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i50)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail50 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(521), "__nail == 0\x00")
					}
					goto cg_317
				cg_317:
					;
					aa__i50++
				}
			}
			if ccv320 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv320 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv319 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv319 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv320 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv319-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(521), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv321 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv321 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv321)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(521), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i51 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv323 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv323 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i51 < ppint64(ccv323)) {
						break
					}
					aa__nail51 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i51)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail51 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(521), "__nail == 0\x00")
					}
					goto cg_322
				cg_322:
					;
					aa__i51++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp, cgbp+48, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp+16)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp, cgbp+64)
			if ccv325 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv325 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv324 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv324 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv325 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv324-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(526), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv326 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv326 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv326)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(526), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i52 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv328 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv328 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i52 < ppint64(ccv328)) {
						break
					}
					aa__nail52 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i52)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail52 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(526), "__nail == 0\x00")
					}
					goto cg_327
				cg_327:
					;
					aa__i52++
				}
			}
			if ccv330 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv330 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv329 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv329 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv330 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv329-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(526), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv331 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv331 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv331)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(526), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i53 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv333 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv333 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i53 < ppint64(ccv333)) {
						break
					}
					aa__nail53 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i53)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail53 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(526), "__nail == 0\x00")
					}
					goto cg_332
				cg_332:
					;
					aa__i53++
				}
			}
			if ccv335 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv335 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv334 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv334 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv335 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv334-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(526), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv336 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv336 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv336)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(526), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i54 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv338 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv338 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i54 < ppint64(ccv338)) {
						break
					}
					aa__nail54 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i54)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail54 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(526), "__nail == 0\x00")
					}
					goto cg_337
				cg_337:
					;
					aa__i54++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp, cgbp+64, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			Xmpz_clobber(cgtls, cgbp+64)
			X__gmpz_set(cgtls, cgbp+80, cgbp+16)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp, cgbp+80)
			if ccv340 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv340 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv339 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv339 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv340 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv339-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(531), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv341 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv341 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv341)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(531), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i55 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv343 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv343 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i55 < ppint64(ccv343)) {
						break
					}
					aa__nail55 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i55)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail55 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(531), "__nail == 0\x00")
					}
					goto cg_342
				cg_342:
					;
					aa__i55++
				}
			}
			if ccv345 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv345 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv344 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv344 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv345 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv344-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(531), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv346 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv346 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv346)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(531), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i56 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv348 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv348 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i56 < ppint64(ccv348)) {
						break
					}
					aa__nail56 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i56)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail56 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(531), "__nail == 0\x00")
					}
					goto cg_347
				cg_347:
					;
					aa__i56++
				}
			}
			if ccv350 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv350 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv349 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv349 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv350 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv349-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(531), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv351 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv351 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv351)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(531), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i57 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv353 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv353 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i57 < ppint64(ccv353)) {
						break
					}
					aa__nail57 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i57)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail57 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(531), "__nail == 0\x00")
					}
					goto cg_352
				cg_352:
					;
					aa__i57++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp, cgbp+80, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			X__gmpz_set(cgtls, cgbp+64, cgbp+16)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+48, cgbp+64)
			if ccv355 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv355 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv354 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv354 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv355 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv354-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(536), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv356 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv356 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv356)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(536), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i58 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv358 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv358 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i58 < ppint64(ccv358)) {
						break
					}
					aa__nail58 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i58)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail58 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(536), "__nail == 0\x00")
					}
					goto cg_357
				cg_357:
					;
					aa__i58++
				}
			}
			if ccv360 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv360 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv359 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv359 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv360 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv359-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(536), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv361 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv361 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv361)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(536), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i59 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv363 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv363 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i59 < ppint64(ccv363)) {
						break
					}
					aa__nail59 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i59)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail59 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(536), "__nail == 0\x00")
					}
					goto cg_362
				cg_362:
					;
					aa__i59++
				}
			}
			if ccv365 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv365 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv364 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv364 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv365 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv364-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(536), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv366 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv366 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv366)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(536), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i60 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv368 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv368 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i60 < ppint64(ccv368)) {
						break
					}
					aa__nail60 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i60)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail60 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(536), "__nail == 0\x00")
					}
					goto cg_367
				cg_367:
					;
					aa__i60++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+48, cgbp+64, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			Xmpz_clobber(cgtls, cgbp+64)
			X__gmpz_set(cgtls, cgbp+80, cgbp+16)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+48, cgbp+80)
			if ccv370 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv370 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv369 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv369 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv370 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv369-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(541), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv371 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv371 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv371)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(541), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i61 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv373 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv373 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i61 < ppint64(ccv373)) {
						break
					}
					aa__nail61 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i61)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail61 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(541), "__nail == 0\x00")
					}
					goto cg_372
				cg_372:
					;
					aa__i61++
				}
			}
			if ccv375 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv375 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv374 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv374 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv375 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv374-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(541), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv376 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv376 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv376)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(541), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i62 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv378 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv378 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i62 < ppint64(ccv378)) {
						break
					}
					aa__nail62 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i62)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail62 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(541), "__nail == 0\x00")
					}
					goto cg_377
				cg_377:
					;
					aa__i62++
				}
			}
			if ccv380 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv380 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv379 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv379 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv380 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv379-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(541), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv381 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv381 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv381)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(541), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i63 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv383 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv383 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i63 < ppint64(ccv383)) {
						break
					}
					aa__nail63 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i63)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail63 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(541), "__nail == 0\x00")
					}
					goto cg_382
				cg_382:
					;
					aa__i63++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+48, cgbp+80, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp)
			X__gmpz_set(cgtls, cgbp+80, cgbp+16)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+64, cgbp+80)
			if ccv385 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv385 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv384 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv384 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv385 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv384-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(546), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv386 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv386 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv386)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(546), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i64 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv388 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv388 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i64 < ppint64(ccv388)) {
						break
					}
					aa__nail64 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i64)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail64 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(546), "__nail == 0\x00")
					}
					goto cg_387
				cg_387:
					;
					aa__i64++
				}
			}
			if ccv390 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv390 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv389 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv389 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv390 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv389-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(546), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv391 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv391 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv391)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(546), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i65 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv393 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv393 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i65 < ppint64(ccv393)) {
						break
					}
					aa__nail65 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i65)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail65 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(546), "__nail == 0\x00")
					}
					goto cg_392
				cg_392:
					;
					aa__i65++
				}
			}
			if ccv395 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv395 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv394 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv394 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv395 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv394-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(546), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv396 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv396 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv396)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(546), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i66 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv398 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv398 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i66 < ppint64(ccv398)) {
						break
					}
					aa__nail66 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i66)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail66 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(546), "__nail == 0\x00")
					}
					goto cg_397
				cg_397:
					;
					aa__i66++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+64, cgbp+80, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			X__gmpz_set(cgtls, cgbp+64, cgbp)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+64, cgbp+48)
			if ccv400 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv400 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv399 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv399 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv400 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv399-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(551), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv401 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv401 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv401)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(551), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i67 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv403 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv403 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i67 < ppint64(ccv403)) {
						break
					}
					aa__nail67 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i67)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail67 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(551), "__nail == 0\x00")
					}
					goto cg_402
				cg_402:
					;
					aa__i67++
				}
			}
			if ccv405 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv405 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv404 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv404 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv405 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv404-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(551), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv406 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv406 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv406)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(551), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i68 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv408 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv408 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i68 < ppint64(ccv408)) {
						break
					}
					aa__nail68 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i68)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail68 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(551), "__nail == 0\x00")
					}
					goto cg_407
				cg_407:
					;
					aa__i68++
				}
			}
			if ccv410 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv410 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv409 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv409 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv410 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv409-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(551), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv411 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv411 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv411)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(551), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i69 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv413 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv413 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i69 < ppint64(ccv413)) {
						break
					}
					aa__nail69 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i69)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail69 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(551), "__nail == 0\x00")
					}
					goto cg_412
				cg_412:
					;
					aa__i69++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+64, cgbp+48, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			Xmpz_clobber(cgtls, cgbp+64)
			X__gmpz_set(cgtls, cgbp+80, cgbp)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+80, cgbp+48)
			if ccv415 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv415 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv414 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv414 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv415 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv414-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(556), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv416 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv416 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv416)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(556), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i70 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv418 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv418 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i70 < ppint64(ccv418)) {
						break
					}
					aa__nail70 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i70)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail70 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(556), "__nail == 0\x00")
					}
					goto cg_417
				cg_417:
					;
					aa__i70++
				}
			}
			if ccv420 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv420 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv419 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv419 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv420 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv419-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(556), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv421 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv421 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv421)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(556), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i71 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv423 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv423 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i71 < ppint64(ccv423)) {
						break
					}
					aa__nail71 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i71)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail71 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(556), "__nail == 0\x00")
					}
					goto cg_422
				cg_422:
					;
					aa__i71++
				}
			}
			if ccv425 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv425 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv424 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv424 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv425 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv424-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(556), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv426 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv426 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv426)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(556), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i72 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv428 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv428 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i72 < ppint64(ccv428)) {
						break
					}
					aa__nail72 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i72)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail72 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(556), "__nail == 0\x00")
					}
					goto cg_427
				cg_427:
					;
					aa__i72++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+80, cgbp+48, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp+16)
			X__gmpz_set(cgtls, cgbp+80, cgbp)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+80, cgbp+64)
			if ccv430 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv430 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv429 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv429 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv430 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv429-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(561), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv431 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv431 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv431)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(561), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i73 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv433 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv433 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i73 < ppint64(ccv433)) {
						break
					}
					aa__nail73 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i73)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail73 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(561), "__nail == 0\x00")
					}
					goto cg_432
				cg_432:
					;
					aa__i73++
				}
			}
			if ccv435 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv435 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv434 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv434 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv435 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv434-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(561), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv436 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv436 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv436)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(561), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i74 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv438 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv438 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i74 < ppint64(ccv438)) {
						break
					}
					aa__nail74 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i74)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail74 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(561), "__nail == 0\x00")
					}
					goto cg_437
				cg_437:
					;
					aa__i74++
				}
			}
			if ccv440 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv440 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv439 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv439 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv440 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv439-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(561), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv441 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv441 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv441)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(561), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i75 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv443 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv443 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i75 < ppint64(ccv443)) {
						break
					}
					aa__nail75 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i75)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail75 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(561), "__nail == 0\x00")
					}
					goto cg_442
				cg_442:
					;
					aa__i75++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+80, cgbp+64, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			Xmpz_clobber(cgtls, cgbp+64)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, iqlibc.ppUintptrFromInt32(0), cgbp+48, cgbp+16)
			if ccv445 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv445 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv444 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv444 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv445 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv444-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(565), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv446 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv446 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv446)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(565), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i76 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv448 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv448 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i76 < ppint64(ccv448)) {
						break
					}
					aa__nail76 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i76)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail76 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(565), "__nail == 0\x00")
					}
					goto cg_447
				cg_447:
					;
					aa__i76++
				}
			}
			if ccv450 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv450 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv449 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv449 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv450 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv449-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(565), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv451 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv451 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv451)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(565), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i77 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv453 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv453 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i77 < ppint64(ccv453)) {
						break
					}
					aa__nail77 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i77)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail77 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(565), "__nail == 0\x00")
					}
					goto cg_452
				cg_452:
					;
					aa__i77++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+48, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, iqlibc.ppUintptrFromInt32(0), cgbp+64, cgbp+16)
			if ccv455 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv455 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv454 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv454 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv455 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv454-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(569), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv456 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv456 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv456)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(569), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i78 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv458 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv458 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i78 < ppint64(ccv458)) {
						break
					}
					aa__nail78 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i78)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail78 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(569), "__nail == 0\x00")
					}
					goto cg_457
				cg_457:
					;
					aa__i78++
				}
			}
			if ccv460 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv460 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv459 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv459 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv460 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv459-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(569), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv461 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv461 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv461)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(569), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i79 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv463 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv463 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i79 < ppint64(ccv463)) {
						break
					}
					aa__nail79 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i79)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail79 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(569), "__nail == 0\x00")
					}
					goto cg_462
				cg_462:
					;
					aa__i79++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+64, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			Xmpz_clobber(cgtls, cgbp+64)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, iqlibc.ppUintptrFromInt32(0), cgbp, cgbp+48)
			if ccv465 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv465 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv464 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv464 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv465 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv464-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(573), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv466 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv466 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv466)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(573), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i80 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv468 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv468 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i80 < ppint64(ccv468)) {
						break
					}
					aa__nail80 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i80)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail80 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(573), "__nail == 0\x00")
					}
					goto cg_467
				cg_467:
					;
					aa__i80++
				}
			}
			if ccv470 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv470 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv469 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv469 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv470 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv469-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(573), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv471 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv471 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv471)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(573), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i81 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv473 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv473 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i81 < ppint64(ccv473)) {
						break
					}
					aa__nail81 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i81)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail81 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(573), "__nail == 0\x00")
					}
					goto cg_472
				cg_472:
					;
					aa__i81++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp, cgbp+48, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp+16)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, iqlibc.ppUintptrFromInt32(0), cgbp, cgbp+64)
			if ccv475 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv475 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv474 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv474 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv475 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv474-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(577), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv476 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv476 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv476)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(577), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i82 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv478 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv478 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i82 < ppint64(ccv478)) {
						break
					}
					aa__nail82 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i82)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail82 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(577), "__nail == 0\x00")
					}
					goto cg_477
				cg_477:
					;
					aa__i82++
				}
			}
			if ccv480 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv480 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv479 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv479 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv480 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv479-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(577), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv481 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv481 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv481)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(577), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i83 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv483 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv483 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i83 < ppint64(ccv483)) {
						break
					}
					aa__nail83 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i83)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail83 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(577), "__nail == 0\x00")
					}
					goto cg_482
				cg_482:
					;
					aa__i83++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp, cgbp+64, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}
			/* Identical inputs, gcd(in1, in1). Then the result should be
			   gcd = abs(in1), s = 0, t = sgn(in1). */
			ccv484 = cgbp + 96
			ccv485 = cgbp
			if ccv484 != ccv485 {
				X__gmpz_set(cgtls, ccv484, ccv485)
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(ccv484)).fd_mp_size >= 0 {
				ccv486 = (*tn__mpz_struct)(iqunsafe.ppPointer(ccv484)).fd_mp_size
			} else {
				ccv486 = -(*tn__mpz_struct)(iqunsafe.ppPointer(ccv484)).fd_mp_size
			}
			(*tn__mpz_struct)(iqunsafe.ppPointer(ccv484)).fd_mp_size = ccv486
			X__gmpz_set_ui(cgtls, cgbp+112, ppuint64(0))
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size < 0 {
				ccv487 = -ppint32(1)
			} else {
				ccv487 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp)).fd_mp_size > 0)
			}
			X__gmpz_set_si(cgtls, cgbp+128, ppint64(ccv487))

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			Xmpz_clobber(cgtls, cgbp+64)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+48, cgbp+48)
			if ccv489 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv489 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv488 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv488 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv489 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv488-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(605), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv490 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv490 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv490)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(605), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i84 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv492 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv492 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i84 < ppint64(ccv492)) {
						break
					}
					aa__nail84 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i84)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail84 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(605), "__nail == 0\x00")
					}
					goto cg_491
				cg_491:
					;
					aa__i84++
				}
			}
			if ccv494 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv494 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv493 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv493 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv494 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv493-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(605), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv495 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv495 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv495)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(605), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i85 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv497 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv497 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i85 < ppint64(ccv497)) {
						break
					}
					aa__nail85 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i85)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail85 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(605), "__nail == 0\x00")
					}
					goto cg_496
				cg_496:
					;
					aa__i85++
				}
			}
			if ccv499 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv499 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv498 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv498 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv499 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv498-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(605), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv500 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv500 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv500)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(605), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i86 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv502 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv502 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i86 < ppint64(ccv502)) {
						break
					}
					aa__nail86 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i86)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail86 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(605), "__nail == 0\x00")
					}
					goto cg_501
				cg_501:
					;
					aa__i86++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+48, cgbp+48, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+64, cgbp+64)
			if ccv504 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv504 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv503 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv503 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv504 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv503-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(610), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv505 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv505 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv505)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(610), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i87 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv507 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv507 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i87 < ppint64(ccv507)) {
						break
					}
					aa__nail87 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i87)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail87 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(610), "__nail == 0\x00")
					}
					goto cg_506
				cg_506:
					;
					aa__i87++
				}
			}
			if ccv509 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv509 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv508 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv508 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv509 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv508-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(610), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv510 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv510 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv510)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(610), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i88 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv512 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv512 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i88 < ppint64(ccv512)) {
						break
					}
					aa__nail88 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i88)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail88 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(610), "__nail == 0\x00")
					}
					goto cg_511
				cg_511:
					;
					aa__i88++
				}
			}
			if ccv514 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv514 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv513 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv513 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv514 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv513-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(610), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv515 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv515 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv515)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(610), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i89 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv517 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv517 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i89 < ppint64(ccv517)) {
						break
					}
					aa__nail89 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i89)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail89 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(610), "__nail == 0\x00")
					}
					goto cg_516
				cg_516:
					;
					aa__i89++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+64, cgbp+64, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			Xmpz_clobber(cgtls, cgbp+64)
			X__gmpz_set(cgtls, cgbp+80, cgbp)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, cgbp+80, cgbp+80, cgbp+80)
			if ccv519 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv519 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv518 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv518 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv519 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv518-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(615), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv520 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv520 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv520)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(615), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i90 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv522 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv522 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i90 < ppint64(ccv522)) {
						break
					}
					aa__nail90 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i90)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail90 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(615), "__nail == 0\x00")
					}
					goto cg_521
				cg_521:
					;
					aa__i90++
				}
			}
			if ccv524 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv524 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv523 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv523 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv524 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv523-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(615), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv525 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv525 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv525)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(615), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i91 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv527 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv527 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i91 < ppint64(ccv527)) {
						break
					}
					aa__nail91 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i91)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail91 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(615), "__nail == 0\x00")
					}
					goto cg_526
				cg_526:
					;
					aa__i91++
				}
			}
			if ccv529 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size == 0; !ccv529 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
					ccv528 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				} else {
					ccv528 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv529 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(ccv528-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(615), "((res3)->_mp_size) == 0 || ((res3)->_mp_d)[((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
				ccv530 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			} else {
				ccv530 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_alloc >= ccv530)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(615), "((res3)->_mp_alloc) >= ((((res3)->_mp_size)) >= 0 ? (((res3)->_mp_size)) : -(((res3)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i92 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_size >= 0 {
						ccv532 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					} else {
						ccv532 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 80)).fd_mp_size
					}
					if !(aa__i92 < ppint64(ccv532)) {
						break
					}
					aa__nail92 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mp_d + ppuintptr(aa__i92)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail92 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(615), "__nail == 0\x00")
					}
					goto cg_531
				cg_531:
					;
					aa__i92++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 || X__gmpz_cmp(cgtls, cgbp+128, cgbp+80) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+80, cgbp+80, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			Xmpz_clobber(cgtls, cgbp+64)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, iqlibc.ppUintptrFromInt32(0), cgbp+48, cgbp+48)
			if ccv534 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv534 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv533 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv533 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv534 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv533-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(620), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv535 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv535 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv535)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(620), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i93 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv537 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv537 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i93 < ppint64(ccv537)) {
						break
					}
					aa__nail93 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i93)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail93 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(620), "__nail == 0\x00")
					}
					goto cg_536
				cg_536:
					;
					aa__i93++
				}
			}
			if ccv539 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv539 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv538 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv538 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv539 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv538-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(620), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv540 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv540 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv540)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(620), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i94 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv542 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv542 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i94 < ppint64(ccv542)) {
						break
					}
					aa__nail94 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i94)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail94 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(620), "__nail == 0\x00")
					}
					goto cg_541
				cg_541:
					;
					aa__i94++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+48, cgbp+48, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			Xmpz_clobber(cgtls, cgbp+48)
			X__gmpz_set(cgtls, cgbp+64, cgbp)
			Xmpz_clobber(cgtls, cgbp+80)
			X__gmpz_gcdext(cgtls, cgbp+48, cgbp+64, iqlibc.ppUintptrFromInt32(0), cgbp+64, cgbp+64)
			if ccv544 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv544 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv543 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv543 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv544 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv543-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(625), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv545 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv545 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv545)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(625), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i95 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv547 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv547 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i95 < ppint64(ccv547)) {
						break
					}
					aa__nail95 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i95)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail95 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(625), "__nail == 0\x00")
					}
					goto cg_546
				cg_546:
					;
					aa__i95++
				}
			}
			if ccv549 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size == 0; !ccv549 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
					ccv548 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				} else {
					ccv548 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv549 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(ccv548-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(625), "((res2)->_mp_size) == 0 || ((res2)->_mp_d)[((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
				ccv550 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			} else {
				ccv550 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_alloc >= ccv550)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(625), "((res2)->_mp_alloc) >= ((((res2)->_mp_size)) >= 0 ? (((res2)->_mp_size)) : -(((res2)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i96 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_size >= 0 {
						ccv552 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					} else {
						ccv552 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mp_size
					}
					if !(aa__i96 < ppint64(ccv552)) {
						break
					}
					aa__nail96 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mp_d + ppuintptr(aa__i96)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail96 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(625), "__nail == 0\x00")
					}
					goto cg_551
				cg_551:
					;
					aa__i96++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || X__gmpz_cmp(cgtls, cgbp+112, cgbp+64) != 0 {
				Xdump(cgtls, "mpz_gcdext\x00", cgbp+64, cgbp+64, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}
		}

		/* Don't run mpz_powm for huge exponents or when undefined.  */

		if ccv554 = aasize_range < ppuint64(17) && X__gmpz_sizeinbase(cgtls, cgbp+16, ppint32(2)) < ppuint64(250); ccv554 {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mp_size < 0 {
				ccv553 = -ppint32(1)
			} else {
				ccv553 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mp_size > 0)
			}
		}
		if ccv556 = ccv554 && ccv553 != 0; ccv556 {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size < 0 {
				ccv555 = -ppint32(1)
			} else {
				ccv555 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size > 0)
			}
		}
		if ccv556 && (ccv555 >= 0 || X__gmpz_invert(cgtls, cgbp+144, cgbp, cgbp+32) != 0) {

			X__gmpz_powm(cgtls, cgbp+96, cgbp, cgbp+16, cgbp+32)

			if ccv558 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv558 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv557 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv557 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv558 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv557-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(634), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv559 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv559 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv559)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(634), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i97 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv561 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv561 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i97 < ppint64(ccv561)) {
						break
					}
					aa__nail97 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i97)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail97 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(634), "__nail == 0\x00")
					}
					goto cg_560
				cg_560:
					;
					aa__i97++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			X__gmpz_powm(cgtls, cgbp+48, cgbp+48, cgbp+16, cgbp+32)

			if ccv563 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv563 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv562 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv562 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv563 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv562-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(638), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv564 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv564 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv564)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(638), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i98 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv566 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv566 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i98 < ppint64(ccv566)) {
						break
					}
					aa__nail98 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i98)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail98 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(638), "__nail == 0\x00")
					}
					goto cg_565
				cg_565:
					;
					aa__i98++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_powm\x00", cgbp, cgbp+16, cgbp+32)
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			X__gmpz_powm(cgtls, cgbp+48, cgbp, cgbp+48, cgbp+32)

			if ccv568 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv568 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv567 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv567 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv568 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv567-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(644), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv569 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv569 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv569)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(644), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i99 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv571 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv571 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i99 < ppint64(ccv571)) {
						break
					}
					aa__nail99 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i99)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail99 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(644), "__nail == 0\x00")
					}
					goto cg_570
				cg_570:
					;
					aa__i99++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_powm\x00", cgbp, cgbp+16, cgbp+32)
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+32)
			X__gmpz_powm(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+48)

			if ccv573 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv573 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv572 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv572 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv573 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv572-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(650), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv574 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv574 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv574)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(650), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i100 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv576 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv576 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i100 < ppint64(ccv576)) {
						break
					}
					aa__nail100 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i100)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail100 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(650), "__nail == 0\x00")
					}
					goto cg_575
				cg_575:
					;
					aa__i100++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_powm\x00", cgbp, cgbp+16, cgbp+32)
				Xexit(cgtls, ppint32(1))
			}
		}

		/* Don't run mpz_powm_ui when undefined.  */

		if ccv578 = aasize_range < ppuint64(17); ccv578 {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mp_size < 0 {
				ccv577 = -ppint32(1)
			} else {
				ccv577 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mp_size > 0)
			}
		}
		if ccv578 && ccv577 != 0 {

			X__gmpz_powm_ui(cgtls, cgbp+96, cgbp, aain2i, cgbp+32)

			if ccv580 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv580 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv579 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv579 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv580 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv579-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(659), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv581 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv581 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv581)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(659), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i101 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv583 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv583 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i101 < ppint64(ccv583)) {
						break
					}
					aa__nail101 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i101)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail101 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(659), "__nail == 0\x00")
					}
					goto cg_582
				cg_582:
					;
					aa__i101++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			X__gmpz_powm_ui(cgtls, cgbp+48, cgbp+48, aain2i, cgbp+32)

			if ccv585 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv585 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv584 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv584 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv585 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv584-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(663), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv586 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv586 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv586)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(663), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i102 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv588 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv588 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i102 < ppint64(ccv588)) {
						break
					}
					aa__nail102 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i102)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail102 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(663), "__nail == 0\x00")
					}
					goto cg_587
				cg_587:
					;
					aa__i102++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_powm_ui\x00", cgbp, cgbp+16, cgbp+32)
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+32)
			X__gmpz_powm_ui(cgtls, cgbp+48, cgbp, aain2i, cgbp+48)

			if ccv590 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv590 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv589 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv589 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv590 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv589-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(669), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv591 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv591 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv591)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(669), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i103 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv593 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv593 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i103 < ppint64(ccv593)) {
						break
					}
					aa__nail103 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i103)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail103 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(669), "__nail == 0\x00")
					}
					goto cg_592
				cg_592:
					;
					aa__i103++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_powm_ui\x00", cgbp, cgbp+16, cgbp+32)
				Xexit(cgtls, ppint32(1))
			}
		}

		aar1 = X__gmpz_gcd_ui(cgtls, cgbp+96, cgbp, aain2i)

		if ccv595 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv595 {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv594 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv594 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv595 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv594-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(676), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
		}
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
			ccv596 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
		} else {
			ccv596 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv596)) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(676), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
		} /* let whole loop go dead when no nails */
		if mvGMP_NAIL_BITS != 0 {
			aa__i104 = 0
			for {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv598 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv598 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
				if !(aa__i104 < ppint64(ccv598)) {
					break
				}
				aa__nail104 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i104)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail104 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(676), "__nail == 0\x00")
				}
				goto cg_597
			cg_597:
				;
				aa__i104++
			}
		}

		X__gmpz_set(cgtls, cgbp+48, cgbp)
		aar2 = X__gmpz_gcd_ui(cgtls, cgbp+48, cgbp+48, aain2i)

		if ccv600 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv600 {
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv599 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv599 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv600 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv599-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(680), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
		}
		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
			ccv601 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
		} else {
			ccv601 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
		}
		if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv601)) != 0), 0) != 0 {
			X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(680), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
		} /* let whole loop go dead when no nails */
		if mvGMP_NAIL_BITS != 0 {
			aa__i105 = 0
			for {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv603 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv603 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
				if !(aa__i105 < ppint64(ccv603)) {
					break
				}
				aa__nail105 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i105)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
				if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail105 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
					X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(680), "__nail == 0\x00")
				}
				goto cg_602
			cg_602:
				;
				aa__i105++
			}
		}
		if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
			Xdump(cgtls, "mpz_gcd_ui\x00", cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
			Xexit(cgtls, ppint32(1))
		}

		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size < 0 {
			ccv604 = -ppint32(1)
		} else {
			ccv604 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size > 0)
		}
		if ccv604 != 0 {

			aarefretval = X__gmpz_remove(cgtls, cgbp+96, cgbp, cgbp+16)

			if ccv606 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv606 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv605 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv605 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv606 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv605-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(690), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv607 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv607 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv607)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(690), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i106 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv609 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv609 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i106 < ppint64(ccv609)) {
						break
					}
					aa__nail106 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i106)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail106 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(690), "__nail == 0\x00")
					}
					goto cg_608
				cg_608:
					;
					aa__i106++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp)
			aaretval = X__gmpz_remove(cgtls, cgbp+48, cgbp+48, cgbp+16)

			if ccv611 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv611 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv610 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv610 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv611 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv610-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(694), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv612 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv612 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv612)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(694), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i107 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv614 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv614 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i107 < ppint64(ccv614)) {
						break
					}
					aa__nail107 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i107)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail107 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(694), "__nail == 0\x00")
					}
					goto cg_613
				cg_613:
					;
					aa__i107++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || aarefretval != aaretval {
				Xdump(cgtls, "mpz_remove\x00", cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			aaretval = X__gmpz_remove(cgtls, cgbp+48, cgbp, cgbp+48)

			if ccv616 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv616 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv615 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv615 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv616 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv615-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(700), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv617 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv617 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv617)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(700), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i108 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv619 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv619 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i108 < ppint64(ccv619)) {
						break
					}
					aa__nail108 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i108)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail108 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(700), "__nail == 0\x00")
					}
					goto cg_618
				cg_618:
					;
					aa__i108++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 || aarefretval != aaretval {
				Xdump(cgtls, "mpz_remove\x00", cgbp, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}
		}

		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size < 0 {
			ccv620 = -ppint32(1)
		} else {
			ccv620 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size > 0)
		}
		if ccv620 != 0 {

			/* Test mpz_divexact */
			X__gmpz_mul(cgtls, cgbp+144, cgbp, cgbp+16)
			X__gmpz_divexact(cgtls, cgbp+96, cgbp+144, cgbp+16)

			if ccv622 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv622 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv621 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv621 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv622 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv621-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(710), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv623 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv623 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv623)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(710), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i109 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv625 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv625 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i109 < ppint64(ccv625)) {
						break
					}
					aa__nail109 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i109)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail109 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(710), "__nail == 0\x00")
					}
					goto cg_624
				cg_624:
					;
					aa__i109++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+144)
			X__gmpz_divexact(cgtls, cgbp+48, cgbp+48, cgbp+16)

			if ccv627 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv627 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv626 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv626 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv627 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv626-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(714), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv628 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv628 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv628)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(714), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i110 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv630 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv630 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i110 < ppint64(ccv630)) {
						break
					}
					aa__nail110 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i110)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail110 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(714), "__nail == 0\x00")
					}
					goto cg_629
				cg_629:
					;
					aa__i110++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_divexact\x00", cgbp+144, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			X__gmpz_divexact(cgtls, cgbp+48, cgbp+144, cgbp+48)

			if ccv632 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv632 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv631 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv631 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv632 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv631-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(720), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv633 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv633 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv633)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(720), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i111 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv635 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv635 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i111 < ppint64(ccv635)) {
						break
					}
					aa__nail111 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i111)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail111 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(720), "__nail == 0\x00")
					}
					goto cg_634
				cg_634:
					;
					aa__i111++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_divexact\x00", cgbp+144, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}
		}

		if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size < 0 {
			ccv636 = -ppint32(1)
		} else {
			ccv636 = iqlibc.ppBoolInt32((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mp_size > 0)
		}
		if ccv636 > 0 {

			/* Test mpz_divexact_gcd, same as mpz_divexact */
			X__gmpz_mul(cgtls, cgbp+144, cgbp, cgbp+16)
			X__gmpz_divexact_gcd(cgtls, cgbp+96, cgbp+144, cgbp+16)

			if ccv638 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size == 0; !ccv638 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
					ccv637 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				} else {
					ccv637 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv638 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(ccv637-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(730), "((ref1)->_mp_size) == 0 || ((ref1)->_mp_d)[((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
				ccv639 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			} else {
				ccv639 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_alloc >= ccv639)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(730), "((ref1)->_mp_alloc) >= ((((ref1)->_mp_size)) >= 0 ? (((ref1)->_mp_size)) : -(((ref1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i112 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_size >= 0 {
						ccv641 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					} else {
						ccv641 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mp_size
					}
					if !(aa__i112 < ppint64(ccv641)) {
						break
					}
					aa__nail112 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mp_d + ppuintptr(aa__i112)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail112 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(730), "__nail == 0\x00")
					}
					goto cg_640
				cg_640:
					;
					aa__i112++
				}
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+144)
			X__gmpz_divexact_gcd(cgtls, cgbp+48, cgbp+48, cgbp+16)

			if ccv643 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv643 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv642 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv642 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv643 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv642-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(734), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv644 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv644 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv644)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(734), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i113 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv646 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv646 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i113 < ppint64(ccv646)) {
						break
					}
					aa__nail113 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i113)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail113 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(734), "__nail == 0\x00")
					}
					goto cg_645
				cg_645:
					;
					aa__i113++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_divexact_gcd\x00", cgbp+144, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}

			X__gmpz_set(cgtls, cgbp+48, cgbp+16)
			X__gmpz_divexact_gcd(cgtls, cgbp+48, cgbp+144, cgbp+48)

			if ccv648 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size == 0; !ccv648 {
				if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
					ccv647 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				} else {
					ccv647 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
				}
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(ccv648 || *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(ccv647-ppint32(1))*8)) != ppuint64(0))) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(740), "((res1)->_mp_size) == 0 || ((res1)->_mp_d)[((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size))) - 1] != 0\x00")
			}
			if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
				ccv649 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			} else {
				ccv649 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
			}
			if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_alloc >= ccv649)) != 0), 0) != 0 {
				X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(740), "((res1)->_mp_alloc) >= ((((res1)->_mp_size)) >= 0 ? (((res1)->_mp_size)) : -(((res1)->_mp_size)))\x00")
			} /* let whole loop go dead when no nails */
			if mvGMP_NAIL_BITS != 0 {
				aa__i114 = 0
				for {
					if (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_size >= 0 {
						ccv651 = (*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					} else {
						ccv651 = -(*tn__mpz_struct)(iqunsafe.ppPointer(cgbp + 48)).fd_mp_size
					}
					if !(aa__i114 < ppint64(ccv651)) {
						break
					}
					aa__nail114 = *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpz_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mp_d + ppuintptr(aa__i114)*8)) & ^(^iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0)) >> iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(iqlibc.ppBoolInt32(!(aa__nail114 == iqlibc.ppUint64FromInt32(0))) != 0), 0) != 0 {
						X__gmp_assert_fail(cgtls, "reuse.c\x00", ppint32(740), "__nail == 0\x00")
					}
					goto cg_650
				cg_650:
					;
					aa__i114++
				}
			}
			if X__gmpz_cmp(cgtls, cgbp+96, cgbp+48) != 0 {
				Xdump(cgtls, "mpz_divexact_gcd\x00", cgbp+144, cgbp+16, iqlibc.ppUintptrFromInt32(0))
				Xexit(cgtls, ppint32(1))
			}
		}

		goto cg_3
	cg_3:
		;
		aapass++
	}

	if Xisatty(cgtls, ppint32(mvSTDOUT_FILENO)) != 0 {
		Xprintf(cgtls, "\r%20s\x00", iqlibc.ppVaList(cgbp+192, "\x00"))
	}

	X__gmpz_clear(cgtls, cgbp+160)
	X__gmpz_clear(cgtls, cgbp)
	X__gmpz_clear(cgtls, cgbp+16)
	X__gmpz_clear(cgtls, cgbp+32)
	X__gmpz_clear(cgtls, cgbp+96)
	X__gmpz_clear(cgtls, cgbp+112)
	X__gmpz_clear(cgtls, cgbp+128)
	X__gmpz_clear(cgtls, cgbp+48)
	X__gmpz_clear(cgtls, cgbp+64)
	X__gmpz_clear(cgtls, cgbp+80)
	X__gmpz_clear(cgtls, cgbp+144)

	if Xisatty(cgtls, ppint32(mvSTDOUT_FILENO)) != 0 {
		Xprintf(cgtls, "\r\x00", 0)
	}

	Xtests_end(cgtls)
	Xexit(cgtls, 0)
	return cgr
}

func Xdump(cgtls *iqlibc.ppTLS, aaname ppuintptr, aain1 ppuintptr, aain2 ppuintptr, aain3 ppuintptr) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	Xprintf(cgtls, "failure in %s (\x00", iqlibc.ppVaList(cgbp+8, aaname))
	X__gmpz_out_str(cgtls, Xstdout, -ppint32(16), aain1)
	if aain2 != iqlibc.ppUintptrFromInt32(0) {

		Xprintf(cgtls, " \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, -ppint32(16), aain2)
	}
	if aain3 != iqlibc.ppUintptrFromInt32(0) {

		Xprintf(cgtls, " \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, -ppint32(16), aain3)
	}
	Xprintf(cgtls, ")\n\x00", 0)
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___gmp_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func ___gmp_randinit_mt_noseed(*iqlibc.ppTLS, ppuintptr)

var ___gmp_rands [1]tn__gmp_randstate_struct

var ___gmp_rands_initialized ppuint8

func ___gmpz_abs(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpz_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_and(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_cdiv_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_cdiv_q_2exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_cdiv_q_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_cdiv_qr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_cdiv_qr_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_cdiv_r(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_cdiv_r_2exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_cdiv_r_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_cmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmpz_com(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpz_divexact(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_divexact_gcd(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_fdiv_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_fdiv_q_2exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_fdiv_q_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_fdiv_qr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_fdiv_qr_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_fdiv_r(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_fdiv_r_2exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_fdiv_r_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_gcd(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_gcd_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_gcdext(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpz_invert(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func ___gmpz_ior(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_lcm(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_mod(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_mul_2exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_mul_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpz_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr) ppuint64

func ___gmpz_pow_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_powm(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_powm_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppuintptr)

func ___gmpz_realloc(*iqlibc.ppTLS, ppuintptr, ppint64) ppuintptr

func ___gmpz_remove(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppuint64

func ___gmpz_root(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppint32

func ___gmpz_rootrem(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_rrandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_set(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpz_set_si(*iqlibc.ppTLS, ppuintptr, ppint64)

func ___gmpz_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

func ___gmpz_sizeinbase(*iqlibc.ppTLS, ppuintptr, ppint32) ppuint64

func ___gmpz_sqrt(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpz_sqrtrem(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_sub_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_tdiv_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_tdiv_q_2exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_tdiv_q_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_tdiv_qr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_tdiv_qr_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_tdiv_r(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_tdiv_r_2exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_tdiv_r_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppuint64

func ___gmpz_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_xor(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func _exit(*iqlibc.ppTLS, ppint32)

func _fflush(*iqlibc.ppTLS, ppuintptr) ppint32

func _fprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _isatty(*iqlibc.ppTLS, ppint32) ppint32

func _mpz_clobber(*iqlibc.ppTLS, ppuintptr)

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

var _stderr ppuintptr

var _stdout ppuintptr

func _strtod(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppfloat64

func _strtol(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint64

func _tests_end(*iqlibc.ppTLS)

func _tests_start(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
